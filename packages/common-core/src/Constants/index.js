/*
 * AppConstants
 * Each action has a corresponding type, which the reducer knows and picks up on.
 * To avoid weird typos between the reducer and the actions, we save them as
 * constants here. We prefix them with 'yourproject/YourComponent' so we avoid
 * reducers accidentally picking up actions they shouldn't.
 *
 * Follow this format:
 * export const YOUR_ACTION_CONSTANT = 'yourproject/YourContainer/YOUR_ACTION_CONSTANT';
 */

export const SIGN_IN = 'app/PatientPortal/SIGN_IN';
export const SIGN_IN_SUCCESS = 'app/PatientPortal/SIGN_IN_SUCCESS';
export const SIGN_IN_FAILURE = 'app/PatientPortal/SIGN_IN_FAILURE';
export const RESET_SIGNIN = 'app/PatientPortal/RESET_SIGNIN';

export const FORGOT_PASSWORD = 'app/PatientPortal/FORGOT_PASSWORD';
export const FORGOT_PASSWORD_SEND_CODE =
  'app/PatientPortal/FORGOT_PASSWORD_SEND_CODE';
export const FORGOT_PASSWORD_VERIFY_CODE =
  'app/PatientPortal/FORGOT_PASSWORD_VERIFY_CODE';
export const FORGOT_PASSWORD_NEW_PASSWORD =
  'app/PatientPortal/FORGOT_PASSWORD_NEW_PASSWORD';

export const FORGOT_PASSWORD_SUCCESS =
  'app/PatientPortal/FORGOT_PASSWORD_SUCCESS';
export const FORGOT_PASSWORD_FAILURE =
  'app/PatientPortal/FORGOT_PASSWORD_FAILURE';
export const CLEAR_FORGOT_PASSWORD = 'app/PatientPortal/CLEAR_FORGOT_PASSWORD';
export const FORGOT_PASSWORD_RESEND_CODE =
  'app/PatientPortal/RESEND_CODE_FORGOT_PASSWORD';

export const NURSE_SIGN_IN = 'app/PatientPortal/NURSE_SIGN_IN';
export const NURSE_SIGN_IN_SUCCESS = 'app/PatientPortal/NURSE_SIGN_IN_SUCCESS';
export const NURSE_SIGN_IN_FAILURE = 'app/PatientPortal/NURSE_SIGN_IN_FAILURE';

export const LOGOUT = 'app/PatientPortal/LOGOUT';
export const LOGOUT_SUCCESS = 'app/PatientPortal/LOGOUT_SUCCESS';

export const REGISTER = 'app/PatientPortal/REGISTER';
export const REGISTER_SUCCESS = 'app/PatientPortal/REGISTER_SUCCESS';
export const REGISTER_FAILURE = 'app/PatientPortal/REGISTER_FAILURE';

export const ACCOUNT_VERIFICATION = 'app/PatientPortal/ACCOUNT_VERIFICATION';
export const ACCOUNT_VERIFICATION_SUCCESS =
  'app/PatientPortal/ACCOUNT_VERIFICATION_SUCCESS';
export const ACCOUNT_VERIFICATION_FAILURE =
  'app/PatientPortal/ACCOUNT_VERIFICATION_FAILURE';

export const GET_PATIENT_DETAIL = 'app/PatientPortal/GET_PATIENT_DETAIL';
export const GET_PATIENT_DETAIL_SUCCESS =
  'app/PatientPortal/GET_PATIENT_DETAIL_SUCCESS';
export const GET_PATIENT_DETAIL_FAILURE =
  'app/PatientPortal/GET_PATIENT_DETAIL_FAILURE';

export const UPDATE_PATIENT_DETAIL = 'app/PatientPortal/UPDATE_PATIENT_DETAIL';
export const UPDATE_PATIENT_DETAIL_SUCCESS =
  'app/PatientPortal/UPDATE_PATIENT_DETAIL_SUCCESS';
export const UPDATE_PATIENT_DETAIL_FAILURE =
  'app/PatientPortal/UPDATE_PATIENT_DETAIL_FAILURE';

export const VERIFY_AUTH = 'app/PatientPortal/VERIFY_AUTH';
export const VERIFY_AUTH_SUCCESS = 'app/PatientPortal/VERIFY_AUTH_SUCCESS';
export const VERIFY_AUTH_FAILURE = 'app/PatientPortal/VERIFY_AUTH_FAILURE';

export const SEND_VERIFICATION_CODE =
  'app/PatientPortal/SEND_VERIFICATION_CODE';
export const SEND_VERIFICATION_CODE_SUCCESS =
  'app/PatientPortal/SEND_VERIFICATION_CODE_SUCCESS';
export const SEND_VERIFICATION_CODE_FAILURE =
  'app/PatientPortal/SEND_VERIFICATION_CODE_FAILURE';

export const APP_ERROR = 'app/PatientPortal/APP_ERROR';

export const INXITE_URL = 'app/PatientPortal/INXITE_URL';
export const RESET_REGISTER = 'app/PatientPortal/RESET_REGISTER';

export const NIH_SEARCH = 'app/PatientPortal/NIH_SEARCH';
export const NIH_SEARCH_SUCCESS = 'app/PatientPortal/NIH_SEARCH_SUCCESS';
export const NIH_SEARCH_FAILURE = 'app/PatientPortal/NIH_SEARCH_FAILURE';

export const CHECK_BENEFITS = 'app/PatientPortal/CHECK_BENEFITS';
export const CHECK_BENEFITS_SUCCESS =
  'app/PatientPortal/CHECK_BENEFITS_SUCCESS';
export const CHECK_BENEFITS_FAILURE =
  'app/PatientPortal/CHECK_BENEFITS_FAILURE';

export const SEND_APPOINTMENT = 'app/MemberPortal/SEND_APPOINTMENT';
export const SEND_APPOINTMENT_SUCCESS =
  'app/MemberPortal/SEND_APPOINTMENT_SUCCESS';
export const SEND_APPOINTMENT_FAILURE =
  'app/MemberPortal/SEND_APPOINTMENT_FAILURE';
export const RESET_APPOINTMENT_DATA = 'app/MemberPortal/RESET_APPOINTMENT_DATA';
export const RESEND_VERIFICATION_CODE =
  'app/PatientPortal/RESEND_VERIFICATION_CODE';
export const RESEND_VERIFICATION_CODE_SUCCESS =
  'app/PatientPortal/RESEND_VERIFICATION_CODE_SUCCESS';
export const RESEND_VERIFICATION_CODE_FAILURE =
  'app/PatientPortal/RESEND_VERIFICATION_CODE_FAILURE';

export const RESET_RESEND_VERIFICATION_CODE =
  'app/PatientPortal/RESET_RESEND_VERIFICATION_CODE';

export const RESET_AUTH_FIELDS = 'app/PatientPortal/RESET_AUTH_FIELDS';
export const RESET_UPDATED_PATIENT = 'app/PatientPortal/RESET_UPDATED_PATIENT';

export const CHANGE_PASSWORD = 'app/PatientPortal/CHANGE_PASSWORD';
export const CHANGE_PASSWORD_SUCCESS =
  'app/PatientPortal/CHANGE_PASSWORD_SUCCESS';
export const CHANGE_PASSWORD_FAILURE =
  'app/PatientPortal/CHANGE_PASSWORD_FAILURE';
export const RESET_CHANGE_PASSWORD = 'app/PatientPortal/RESET_CHANGE_PASSWORD';
export const INIT_INXITE_SSO = 'app/PatientPortal/INIT_INXITE_SSO';
export const INIT_INXITE_SSO_SUCCESS =
  'app/PatientPortal/INIT_INXITE_SSO_SUCCESS';
export const INIT_INXITE_SSO_FAILURE =
  'app/PatientPortal/INIT_INXITE_SSO_FAILURE';

export const VERIFICATION_TO_EMAIL = 'app/PatientPortal/VERIFICATION_TO_EMAIL';

export const GET_PATIENT_DEPENDENTS =
  'app/PatientPortal/GET_PATIENT_DEPENDENTS';
export const GET_PATIENT_DEPENDENTS_SUCCESS =
  'app/PatientPortal/GET_PATIENT_DEPENDENTS_SUCCESS';
export const GET_PATIENT_DEPENDENTS_FAILURE =
  'app/PatientPortal/GET_PATIENT_DEPENDENTS_FAILURE';

export const SHOW_DEPENDENTS_MODAL = 'app/PatientPortal/SHOW_DEPENDENTS_MODAL';

export const POST_PATIENT_IMPERSONATION =
  'app/PatientPortal/POST_PATIENT_IMPERSONATION';
export const POST_PATIENT_IMPERSONATION_SUCCESS =
  'app/PatientPortal/POST_PATIENT_IMPERSONATION_SUCCESS';
export const POST_PATIENT_IMPERSONATION_FAILURE =
  'app/PatientPortal/POST_PATIENT_IMPERSONATION_FAILURE';
export const RESET_PATIENT_IMPERSONATION =
  'app/PatientPortal/RESET_PATIENT_IMPERSONATION_FAILURE';

export const GET_APP_VERSION = 'app/PatientPortal/GET_APP_VERSION';
export const GET_APP_VERSION_SUCCESS =
  'app/PatientPortal/GET_APP_VERSION_SUCCESS';
export const GET_APP_VERSION_FAILURE =
  'app/PatientPortal/GET_APP_VERSION_FAILURE';

export const POST_FAMILY_INVITE = 'app/PatientPortal/POST_FAMILY_INVITE';
export const POST_FAMILY_INVITE_SUCCESS =
  'app/PatientPortal/POST_FAMILY_INVITE_SUCCESS';
export const POST_FAMILY_INVITE_FAILURE =
  'app/PatientPortal/POST_FAMILY_INVITE_FAILURE';
export const RESET_FAMILY_INVITE_DATA =
  'app/PatientPortal/RESET_FAMILY_INVITE_DATA';

export const POST_ADD_DEPENDENT = 'app/PatientPortal/POST_ADD_DEPENDENT';
export const POST_ADD_DEPENDENT_SUCCESS =
  'app/PatientPortal/POST_ADD_DEPENDENT_SUCCESS';
export const POST_ADD_DEPENDENT_FAILURE =
  'app/PatientPortal/POST_ADD_DEPENDENT_FAILURE';
export const RESET_ADD_DEPENDENT = 'app/PatientPortal/RESET_ADD_DEPENDENT';

export const CHATBOT_AUTHENTICATION =
  'app/PatientPortal/CHATBOT_AUTHENTICATION';
export const CHATBOT_AUTHENTICATION_SUCCESS =
  'app/PatientPortal/CHATBOT_AUTHENTICATION_SUCCESS';
export const CHATBOT_AUTHENTICATION_FAILURE =
  'app/PatientPortal/CHATBOT_AUTHENTICATION_FAILURE';

export const CHATROOM_WAITING_LIST = 'app/PatientPortal/CHATROOM_WAITING_LIST';
export const CHATROOM_WAITING_LIST_SUCCESS =
  'app/PatientPortal/CHATROOM_WAITING_LIST_SUCCESS';
export const CHATROOM_WAITING_LIST_FAILURE =
  'app/PatientPortal/CHATROOM_WAITING_LIST_FAILURE';

export const SEND_CHAT_MESSAGE_TO_MEMBER =
  'app/PatientPortal/SEND_CHAT_MESSAGE_TO_MEMBER';
export const SEND_CHAT_MESSAGE_TO_MEMBER_SUCCESS =
  'app/PatientPortal/SEND_CHAT_MESSAGE_TO_MEMBER_SUCCESS';
export const SEND_CHAT_MESSAGE_TO_MEMBER_FAILURE =
  'app/PatientPortal/SEND_CHAT_MESSAGE_TO_MEMBER_FAILURE';

export const LEAVE_CHAT_ROOM = 'app/PatientPortal/LEAVE_CHAT_ROOM';
export const LEAVE_CHAT_ROOM_SUCCESS =
  'app/PatientPortal/LEAVE_CHAT_ROOM_SUCCESS';
export const LEAVE_CHAT_ROOM_FAILURE =
  'app/PatientPortal/LEAVE_CHAT_ROOM_FAILURE';

export const SEND_CHAT_MESSAGE_TO_NURSE =
  'app/PatientPortal/SEND_CHAT_MESSAGE_TO_NURSE';
export const SEND_CHAT_MESSAGE_TO_NURSE_SUCCESS =
  'app/PatientPortal/SEND_CHAT_MESSAGE_TO_NURSE_SUCCESS';
export const SEND_CHAT_MESSAGE_TO_NURSE_FAILURE =
  'app/PatientPortal/SEND_CHAT_MESSAGE_TO_NURSE_FAILURE';

export const MEMBER_JOIN_ROOM = 'app/PatientPortal/MEMBER_JOIN_ROOM';
export const MEMBER_JOIN_ROOM_SUCCESS =
  'app/PatientPortal/MEMBER_JOIN_ROOM_SUCCESS';
export const MEMBER_JOIN_ROOM_FAILURE =
  'app/PatientPortal/MEMBER_JOIN_ROOM_FAILURE';

export const MEMBER_HAS_JOINED_ROOM =
  'app/PatientPortal/MEMBER_HAS_JOINED_ROOM';

export const SELECTED_USER_TO_CHAT = 'app/PatientPortal/SELECTED_USER_TO_CHAT';
export const NURSE_CONVERSATIONS = 'app/PatientPortal/NURSE_CONVERSATIONS';
export const GET_FAMILY_MEMBERS = 'app/PatientPortal/GET_FAMILY_MEMBERS';
export const GET_FAMILY_MEMBERS_SUCCESS =
  'app/PatientPortal/GET_FAMILY_MEMBERS_SUCCESS';
export const GET_FAMILY_MEMBERS_FAILURE =
  'app/PatientPortal/GET_FAMILY_MEMBERS_FAILURE';

export const PUT_FAMILY_MEMBER_APPROVAL =
  'app/PatientPortal/PUT_FAMILY_MEMBER_APPROVAL';
export const PUT_FAMILY_MEMBER_APPROVAL_SUCCESS =
  'app/PatientPortal/PUT_FAMILY_MEMBER_APPROVAL_SUCCESS';
export const PUT_FAMILY_MEMBER_APPROVAL_FAILURE =
  'app/PatientPortal/PUT_FAMILY_MEMBER_APPROVAL_FAILURE';
export const RESET_FAMILY_MEMBER_APPROVAL =
  'app/PatientPortal/RESET_FAMILY_MEMBER_APPROVAL';

export const GET_CHAT_MESSAGES = 'app/PatientPortal/GET_CHAT_MESSAGES';
export const GET_CHAT_MESSAGES_SUCCESS =
  'app/PatientPortal/GET_CHAT_MESSAGES_SUCCESS';
export const GET_CHAT_MESSAGES_FAILURE =
  'app/PatientPortal/GET_CHAT_MESSAGES_FAILURE';
export const RESET_CHAT_MESSAGES = 'app/PatientPortal/RESET_CHAT_MESSAGES';

export const POST_END_CONVERSATION = 'app/PatientPortal/POST_END_CONVERSATION';
export const POST_END_CONVERSATION_SUCCESS =
  'app/PatientPortal/POST_END_CONVERSATION_SUCCESS';
export const POST_END_CONVERSATION_FAILURE =
  'app/PatientPortal/POST_END_CONVERSATION_FAILURE';

export const NOTIFY_NURSE = 'app/PatientPortal/NOTIFY_NURSE';
export const NOTIFY_NURSE_SUCCESS = 'app/PatientPortal/NOTIFY_NURSE_SUCCESS';
export const NOTIFY_NURSE_FAILURE = 'app/PatientPortal/NOTIFY_NURSE_FAILURE';

export const GET_MEMBER_INSURANCE_INFO_LIST =
  'app/PatientPortal/GET_MEMBER_INSURANCE_INFO_LIST';
export const GET_MEMBER_INSURANCE_INFO_LIST_SUCCESS =
  'app/PatientPortal/GET_MEMBER_INSURANCE_INFO_LIST_SUCCESS';
export const GET_MEMBER_INSURANCE_INFO_LIST_FAILURE =
  'app/PatientPortal/GET_MEMBER_INSURANCE_INFO_LIST_FAILURE';

export const GET_MEMBER_INSURANCE_INFO =
  'app/PatientPortal/GET_MEMBER_INSURANCE_INFO';
export const GET_MEMBER_INSURANCE_INFO_SUCCESS =
  'app/PatientPortal/GET_MEMBER_INSURANCE_INFO_SUCCESS';
export const GET_MEMBER_INSURANCE_INFO_FAILURE =
  'app/PatientPortal/GET_MEMBER_INSURANCE_INFO_FAILURE';

export const POST_MEMBER_INSURANCE_INFO =
  'app/PatientPortal/POST_MEMBER_INSURANCE_INFO';
export const POST_MEMBER_INSURANCE_INFO_SUCCESS =
  'app/PatientPortal/POST_MEMBER_INSURANCE_INFO_SUCCESS';
export const POST_MEMBER_INSURANCE_INFO_FAILURE =
  'app/PatientPortal/POST_MEMBER_INSURANCE_INFO_FAILURE';
export const RESET_POST_MEMBER_INSURANCE_INFO =
  'app/PatientPortal/RESET_POST_MEMBER_INSURANCE_INFO';

export const PUT_MEMBER_INSURANCE_INFO =
  'app/PatientPortal/PUT_MEMBER_INSURANCE_INFO';
export const PUT_MEMBER_INSURANCE_INFO_SUCCESS =
  'app/PatientPortal/PUT_MEMBER_INSURANCE_INFO_SUCCESS';
export const PUT_MEMBER_INSURANCE_INFO_FAILURE =
  'app/PatientPortal/PUT_MEMBER_INSURANCE_INFO_FAILURE';
export const RESET_PUT_MEMBER_INSURANCE_INFO =
  'app/PatientPortal/RESET_PUT_MEMBER_INSURANCE_INFO';
export const VERIFY_SSN = 'app/PatientPortal/VERIFY_SSN';
export const VERIFY_SSN_SUCCESS = 'app/PatientPortal/VERIFY_SSN_SUCCESS';
export const VERIFY_SSN_FAILURE = 'app/PatientPortal/VERIFY_SSN_FAILURE';

export const UPDATE_PATIENT_COMMS = 'app/PatientPortal/UPDATE_PATIENT_COMMS';
export const UPDATE_PATIENT_COMMS_SUCCESS =
  'app/PatientPortal/UPDATE_PATIENT_COMMS_SUCCESS';
export const UPDATE_PATIENT_COMMS_FAILURE =
  'app/PatientPortal/UPDATE_PATIENT_COMMS_FAILURE';

export const SEND_GI_MESSAGE = 'app/PatientPortal/SEND_GI_MESSAGE';
export const SEND_GI_MESSAGE_SUCCESS =
  'app/PatientPortal/SEND_GI_MESSAGE_SUCCESS';
export const SEND_GI_MESSAGE_FAILURE =
  'app/PatientPortal/SEND_GI_MESSAGE_FAILURE';
export const RESET_GI_MESSAGE = 'app/PatientPortal/RESET_GI_MESSAGE';

export const FIND_PATIENT = 'app/PatientPortal/FIND_PATIENT';
export const FIND_PATIENT_SUCCESS = 'app/PatientPortal/FIND_PATIENT_SUCCESS';
export const FIND_PATIENT_FAILURE = 'app/PatientPortal/FIND_PATIENT_FAILURE';

export const GET_GOOGLE_AUTH = 'app/PatientPortal/GET_GOOGLE_AUTH';
export const GET_GOOGLE_AUTH_SUCCESS =
  'app/PatientPortal/GET_GOOGLE_AUTH_SUCCESS';
export const GET_GOOGLE_AUTH_FAILURE =
  'app/PatientPortal/GET_GOOGLE_AUTH_FAILURE';

export const GET_NURSE_PATIENT_INFORMATION =
  'app/ProviderPortal/GET_NURSE_PATIENT_INFORMATION';
export const GET_NURSE_PATIENT_INFORMATION_SUCCESS =
  'app/ProviderPortal/GET_NURSE_PATIENT_INFORMATION_SUCCESS';
export const GET_NURSE_PATIENT_INFORMATION_FAILURE =
  'app/ProviderPortal/GET_NURSE_PATIENT_INFORMATION_FAILURE';

export const GET_NURSE_SELECTED_PATIENT_INFORMATION =
  'app/ProviderPortal/GET_NURSE_SELECTED_PATIENT_INFORMATION';
export const GET_NURSE_SELECTED_PATIENT_INFORMATION_SUCCESS =
  'app/ProviderPortal/GET_NURSE_SELECTED_PATIENT_INFORMATION_SUCCESS';
export const GET_NURSE_SELECTED_PATIENT_INFORMATION_FAILURE =
  'app/ProviderPortal/GET_NURSE_SELECTED_PATIENT_INFORMATION_FAILURE';

export const ADD_COMPANY_NOTES = 'app/ProviderPortal/ADD_COMPANY_NOTES';
export const ADD_COMPANY_NOTES_SUCCESS =
  'app/ProviderPortal/ADD_COMPANY_NOTES_SUCCESS';
export const ADD_COMPANY_NOTES_FAILURE =
  'app/ProviderPortal/ADD_COMPANY_NOTES_FAILURE';

export const GET_COMPANY_NOTES = 'app/ProviderPortal/GET_COMPANY_NOTES';
export const GET_COMPANY_NOTES_SUCCESS =
  'app/ProviderPortal/GET_COMPANY_NOTES_SUCCESS';
export const GET_COMPANY_NOTES_FAILURE =
  'app/ProviderPortal/GET_COMPANY_NOTES_FAILURE';
