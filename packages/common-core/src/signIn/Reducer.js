import { SIGN_IN, SIGN_IN_SUCCESS, SIGN_IN_FAILURE } from './Constant';

const defaultProps = {
  loading: false,
  data: false,
  error: false,
  saved: false,
};

export const initialState = {
  signin: {
    ...defaultProps,
  },
};

const signInReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case SIGN_IN: {
        draft.signin = {
          loading: true,
          data: false,
          error: false,
        };
        draft.verification = {
          ...defaultProps,
          params: action.payload,
        };

        break;
      }
      case SIGN_IN_SUCCESS:
        draft.signin = {
          loading: false,
          data: action.payload || false,
          error: false,
        };

        break;
      case SIGN_IN_FAILURE:
        draft.signin = {
          loading: false,
          data: false,
          error: action.payload,
        };
        break;
    }
  });

signInReducer.key = 'patient';
export default signInReducer;
