import { takeLatest, call, put, select } from 'redux-saga/effects';
import { integrationApi } from 'core/Utils/api';

import { apiResponseEvaluator } from 'core/Utils/sagaHelper';

import { SIGN_IN } from './Constant';

import { signInSuccess, signInFailure } from './Action';

const doPatientSignin = payload =>
  integrationApi.post(`/patient/login`, payload, {
    headers: {
      'Content-Type': 'application/json',
    },
  });

export function* signInPatientSaga({ payload }) {
  try {
    const response = yield call(doPatientSignin, payload);
    yield apiResponseEvaluator(response, signInSuccess, signInFailure);
  } catch (ex) {
    yield put(logAppAppError(ex));
  }
}

export default function* signInSaga() {
  yield takeLatest(SIGN_IN, signInPatientSaga);
}
