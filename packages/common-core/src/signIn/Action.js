import { SIGN_IN, SIGN_IN_SUCCESS, SIGN_IN_FAILURE } from './Constant';

export function signIn(payload) {
  return {
    type: SIGN_IN,
    payload,
  };
}

export function signInSuccess(payload) {
  return {
    type: SIGN_IN_SUCCESS,
    payload,
  };
}

export function signInFailure(payload) {
  return {
    type: SIGN_IN_FAILURE,
    payload,
  };
}
