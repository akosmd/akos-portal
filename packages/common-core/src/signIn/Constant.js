export const SIGN_IN = 'app/PatientPortal/SIGN_IN';
export const SIGN_IN_SUCCESS = 'app/PatientPortal/SIGN_IN_SUCCESS';
export const SIGN_IN_FAILURE = 'app/PatientPortal/SIGN_IN_FAILURE';
