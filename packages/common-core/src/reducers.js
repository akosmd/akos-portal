/**
 * Combine all reducers in this file and export the combined reducers.
 */

import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';

import history from 'core/Utils/history';
import languageProviderReducer from 'core/LanguageProvider/reducer';
import patientReducer, { patientMigrations } from 'core/Reducer';
// import patientReducer, { patientMigrations } from "common-core/reducer";
import legacyReducer, { legacyMigrations } from 'core/Reducer/legacy';

import { createMigrate } from 'redux-persist';

import { persist } from 'core/Utils/reduxPersist';

/**
 * Merges the main reducer with the router state and dynamically injected reducers
 */
const MIGRATION_DEBUG = false;

const patientPersistConfig = {
  key: 'persistedMember',
  version: 1.2,
  migrate: createMigrate(patientMigrations, { debug: MIGRATION_DEBUG }),
};
const legacyPersistConfig = {
  key: 'persistedLegacy',
  version: 1.2,
  migrate: createMigrate(legacyMigrations, { debug: MIGRATION_DEBUG }),
};

export default function createReducer(injectedReducers = {}) {
  const rootReducer = combineReducers({
    patient: persist(patientPersistConfig, patientReducer),
    legacy: persist(legacyPersistConfig, legacyReducer),
    language: languageProviderReducer,
    router: connectRouter(history),
    ...injectedReducers,
  });

  return rootReducer;
}
