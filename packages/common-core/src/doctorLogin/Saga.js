import { takeLatest, call, put, select } from 'redux-saga/effects';
import { connectApi } from 'core/Utils/api';

import { apiResponseEvaluator } from 'core/Utils/sagaHelper';

import {
  doctorSignIn,
  doctorSignInSuccess,
  doctorSignInFailure,
} from './Action';

const doDoctorSignIn = payload =>
  connectApi.post(`/api/pcp/doctorLogin`, payload, {
    headers: {
      'Content-Type': 'application/json',
    },
  });

export function* signInDoctorSaga({ payload }) {
  try {
    const response = yield call(doDoctorSignIn, payload);
    yield apiResponseEvaluator(
      response,
      doctorSignInSuccess,
      doctorSignInFailure,
    );
  } catch (ex) {
    // to follow
    // yield put(logAppAppError(ex));
  }
}

export default function* doctorSignInSaga() {
  yield takeLatest(doctorSignIn, signInDoctorSaga);
}
