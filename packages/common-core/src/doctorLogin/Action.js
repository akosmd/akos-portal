import { createAction } from 'redux-actions';

import {
  DOCTOR_SIGN_IN,
  DOCTOR_SIGN_IN_SUCCESS,
  DOCTOR_SIGN_IN_FAILURE,
} from './Constant';

export const doctorSignIn = createAction(DOCTOR_SIGN_IN);
export const doctorSignInSuccess = createAction(DOCTOR_SIGN_IN_SUCCESS);
export const doctorSignInFailure = createAction(DOCTOR_SIGN_IN_FAILURE);
