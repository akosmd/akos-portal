import { createSelector } from 'reselect';

import { initialState } from 'core/Reducer';
const portalSelector = state => state.patient2 || initialState;

export const makeSelectSignin = () =>
  createSelector(
    portalSelector,
    subState => subState.user.signin,
  );
