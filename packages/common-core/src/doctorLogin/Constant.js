export const DOCTOR_SIGN_IN = `${process.env.LOG_KEY}/DOCTOR_SIGN_IN`;
export const DOCTOR_SIGN_IN_SUCCESS = `${
  process.env.LOG_KEY
}/DOCTOR_SIGN_IN_SUCCESS`;
export const DOCTOR_SIGN_IN_FAILURE = `${
  process.env.LOG_KEY
}/DOCTOR_SIGN_IN_FAILURE`;
