import { handleActions } from 'redux-actions';

import {
  doctorSignIn,
  doctorSignInSuccess,
  doctorSignInFailure,
} from './Action';

export const patientMigrations = {
  1.2: previousVersionState => ({
    patient: {
      change: previousVersionState.patient,
      lastUpdate: new Date(),
    },
  }),
};

const defaultProps = {
  loading: false,
  data: false,
  error: false,
  saved: false,
};

export const initialState = {
  signin: {
    ...defaultProps,
  },
};

/* eslint-disable default-case, no-param-reassign */
const signInReducer = handleActions(
  {
    [doctorSignIn]: (state, { payload }) => ({
      ...state,
      signin: {
        loading: true,
        data: false,
        error: false,
      },
    }),
    [doctorSignInSuccess]: (state, { payload }) => ({
      ...state,
      signin: {
        loading: false,
        data: payload,
        error: false,
      },
    }),
    [doctorSignInFailure]: (state, { payload }) => ({
      ...state,
      signin: {
        loading: false,
        data: false,
        error: payload,
      },
    }),
  },
  initialState,
);
signInReducer.key = 'user';

export default signInReducer;
