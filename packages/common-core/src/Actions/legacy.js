import {
  GET_INSURANCE_ELIGIBILITY,
  GET_INSURANCE_ELIGIBILITY_SUCCESS,
  GET_INSURANCE_ELIGIBILITY_FAILURE,
  GET_PATIENT_INSURANCE,
  GET_PATIENT_INSURANCE_SUCCESS,
  GET_PATIENT_INSURANCE_FAILURE,
  FIND_INSURANCE,
  FIND_INSURANCE_SUCCESS,
  FIND_INSURANCE_FAILURE,
  SAVE_PATIENT_INSURANCE,
  SAVE_PATIENT_INSURANCE_SUCCESS,
  SAVE_PATIENT_INSURANCE_FAILURE,
  COUPON_CODE,
  COUPON_CODE_SUCCESS,
  COUPON_CODE_FAILURE,
  RESET_ELIGIBILITY_CODES,
  BRAINTREE_TOKEN,
  BRAINTREE_TOKEN_SUCCESS,
  BRAINTREE_TOKEN_FAILURE,
  REGULAR_PAYMENT_AMOUNT,
  PAYMENT_CHECKOUT,
  PAYMENT_CHECKOUT_SUCCESS,
  PAYMENT_CHECKOUT_FAILURE,
  GENERATE_PATIENT_CALL_ID,
  GENERATE_PATIENT_CALL_ID_SUCCESS,
  GENERATE_PATIENT_CALL_ID_FAILURE,
  RESET_PAYMENT_CHECKOUT,
  RESET_COUPON_CODE,
  ADD_USER_TO_WAITING_ROOM,
  ADD_USER_TO_WAITING_ROOM_SUCCESS,
  ADD_USER_TO_WAITING_ROOM_FAILURE,
  NOTIFY_PROVIDERS,
  NOTIFY_PROVIDERS_SUCCESS,
  NOTIFY_PROVIDERS_FAILURE,
  CHECK_WAITING_ROOM_STATUS,
  CHECK_WAITING_ROOM_STATUS_SUCCESS,
  CHECK_WAITING_ROOM_STATUS_FAILURE,
  GET_OPENTOK_ROOM_KEYS,
  GET_OPENTOK_ROOM_KEYS_SUCCESS,
  GET_OPENTOK_ROOM_KEYS_FAILURE,
  GET_DOC_ALIAS,
  GET_DOC_ALIAS_SUCCESS,
  GET_DOC_ALIAS_FAILURE,
  UPDATE_PATIENT_CALL_ID,
  UPDATE_PATIENT_CALL_ID_SUCCESS,
  UPDATE_PATIENT_CALL_ID_FAILURE,
  CALL_END_BY_PATIENT,
  CALL_END_BY_PATIENT_SUCCESS,
  CALL_END_BY_PATIENT_FAILURE,
  INVALIDATE_TOKEN,
  INVALIDATE_TOKEN_SUCCESS,
  INVALIDATE_TOKEN_FAILURE,
  PROVIDER_SETTINGS,
  PROVIDER_SETTINGS_SUCCESS,
  PROVIDER_SETTINGS_FAILURE,
  DISCONNECT_REASON,
  DISCONNECT_REASON_SUCCESS,
  DISCONNECT_REASON_FAILURE,
  RESET_WAITING_ROOM_FIELDS,
} from 'core/Constants/legacy';

export function getInsuranceEligibility(payload) {
  return {
    type: GET_INSURANCE_ELIGIBILITY,
    payload,
  };
}

export function getInsuranceEligibilitySuccess(payload) {
  return {
    type: GET_INSURANCE_ELIGIBILITY_SUCCESS,
    payload,
  };
}

export function getInsuranceEligibilityFailure(payload) {
  return {
    type: GET_INSURANCE_ELIGIBILITY_FAILURE,
    payload,
  };
}

export function getPatientInsurance(payload) {
  return {
    type: GET_PATIENT_INSURANCE,
    payload,
  };
}

export function getPatientInsuranceSuccess(payload) {
  return {
    type: GET_PATIENT_INSURANCE_SUCCESS,
    payload,
  };
}

export function getPatientInsuranceFailure(payload) {
  return {
    type: GET_PATIENT_INSURANCE_FAILURE,
    payload,
  };
}

export function findInsurance(payload) {
  return {
    type: FIND_INSURANCE,
    payload,
  };
}

export function findInsuranceSuccess(payload) {
  return {
    type: FIND_INSURANCE_SUCCESS,
    payload,
  };
}

export function findInsuranceFailure(payload) {
  return {
    type: FIND_INSURANCE_FAILURE,
    payload,
  };
}

export function savePatientInsurance(payload) {
  return {
    type: SAVE_PATIENT_INSURANCE,
    payload,
  };
}

export function savePatientInsuranceSuccess(payload) {
  return {
    type: SAVE_PATIENT_INSURANCE_SUCCESS,
    payload,
  };
}

export function savePatientInsuranceFailure(payload) {
  return {
    type: SAVE_PATIENT_INSURANCE_FAILURE,
    payload,
  };
}

export function checkCouponCode(payload) {
  return {
    type: COUPON_CODE,
    payload,
  };
}

export function checkCouponCodeSuccess(payload) {
  return {
    type: COUPON_CODE_SUCCESS,
    payload,
  };
}

export function checkCouponCodeFailure(payload) {
  return {
    type: COUPON_CODE_FAILURE,
    payload,
  };
}

export function getBrainTreeToken() {
  return {
    type: BRAINTREE_TOKEN,
  };
}

export function getBrainTreeTokenSuccess(payload) {
  return {
    type: BRAINTREE_TOKEN_SUCCESS,
    payload,
  };
}

export function getBrainTreeTokenFailure(payload) {
  return {
    type: BRAINTREE_TOKEN_FAILURE,
    payload,
  };
}
export function checkoutBraintree(payload) {
  return {
    type: PAYMENT_CHECKOUT,
    payload,
  };
}

export function checkoutBraintreeSuccess(payload) {
  return {
    type: PAYMENT_CHECKOUT_SUCCESS,
    payload,
  };
}

export function checkoutBraintreeFailure(payload) {
  return {
    type: PAYMENT_CHECKOUT_FAILURE,
    payload,
  };
}
export function generatePatientCallId(payload) {
  return {
    type: GENERATE_PATIENT_CALL_ID,
    payload,
  };
}

export function generatePatientCallIdSuccess(payload) {
  return {
    type: GENERATE_PATIENT_CALL_ID_SUCCESS,
    payload,
  };
}

export function generatePatientCallIdFailure(payload) {
  return {
    type: GENERATE_PATIENT_CALL_ID_FAILURE,
    payload,
  };
}

export function updatePatientCallId(payload) {
  return {
    type: UPDATE_PATIENT_CALL_ID,
    payload,
  };
}

export function updatePatientCallIdSuccess(payload) {
  return {
    type: UPDATE_PATIENT_CALL_ID_SUCCESS,
    payload,
  };
}

export function updatePatientCallIdFailure(payload) {
  return {
    type: UPDATE_PATIENT_CALL_ID_FAILURE,
    payload,
  };
}

export function setPaymentAmount(payload) {
  return {
    type: REGULAR_PAYMENT_AMOUNT,
    payload,
  };
}

export function resetEligibilityCodes() {
  return {
    type: RESET_ELIGIBILITY_CODES,
  };
}
export function resetPaymentCheckout() {
  return {
    type: RESET_PAYMENT_CHECKOUT,
  };
}
export function resetCouponCode() {
  return {
    type: RESET_COUPON_CODE,
  };
}

export function addUserToWaitingRoom(payload) {
  return {
    type: ADD_USER_TO_WAITING_ROOM,
    payload,
  };
}

export function addUserToWaitingRoomSuccess(payload) {
  return {
    type: ADD_USER_TO_WAITING_ROOM_SUCCESS,
    payload,
  };
}

export function addUserToWaitingRoomFailure(payload) {
  return {
    type: ADD_USER_TO_WAITING_ROOM_FAILURE,
    payload,
  };
}

export function notifyProviders(payload) {
  return {
    type: NOTIFY_PROVIDERS,
    payload,
  };
}

export function notifyProvidersSuccess(payload) {
  return {
    type: NOTIFY_PROVIDERS_SUCCESS,
    payload,
  };
}

export function notifyProvidersFailure(payload) {
  return {
    type: NOTIFY_PROVIDERS_FAILURE,
    payload,
  };
}

export function checkWaitingRoomStatus(payload) {
  return {
    type: CHECK_WAITING_ROOM_STATUS,
    payload,
  };
}

export function checkWaitingRoomStatusSuccess(payload) {
  return {
    type: CHECK_WAITING_ROOM_STATUS_SUCCESS,
    payload,
  };
}

export function checkWaitingRoomStatusFailure(payload) {
  return {
    type: CHECK_WAITING_ROOM_STATUS_FAILURE,
    payload,
  };
}

export function getOpenTokRoomKeys(payload) {
  return {
    type: GET_OPENTOK_ROOM_KEYS,
    payload,
  };
}

export function getOpenTokRoomKeysSuccess(payload) {
  return {
    type: GET_OPENTOK_ROOM_KEYS_SUCCESS,
    payload,
  };
}

export function getOpenTokRoomKeysFailure(payload) {
  return {
    type: GET_OPENTOK_ROOM_KEYS_FAILURE,
    payload,
  };
}

export function getDocAlias(payload) {
  return {
    type: GET_DOC_ALIAS,
    payload,
  };
}

export function getDocAliasSuccess(payload) {
  return {
    type: GET_DOC_ALIAS_SUCCESS,
    payload,
  };
}

export function getDocAliasFailure(payload) {
  return {
    type: GET_DOC_ALIAS_FAILURE,
    payload,
  };
}

export function callEndByPatient(payload) {
  return {
    type: CALL_END_BY_PATIENT,
    payload,
  };
}

export function callEndByPatientSuccess(payload) {
  return {
    type: CALL_END_BY_PATIENT_SUCCESS,
    payload,
  };
}

export function callEndByPatientFailure(payload) {
  return {
    type: CALL_END_BY_PATIENT_FAILURE,
    payload,
  };
}

export function invalidateToken(payload) {
  return {
    type: INVALIDATE_TOKEN,
    payload,
  };
}

export function invalidateTokenSuccess(payload) {
  return {
    type: INVALIDATE_TOKEN_SUCCESS,
    payload,
  };
}

export function invalidateTokenFailure(payload) {
  return {
    type: INVALIDATE_TOKEN_FAILURE,
    payload,
  };
}

export function providerSettings(payload) {
  return {
    type: PROVIDER_SETTINGS,
    payload,
  };
}

export function providerSettingsSuccess(payload) {
  return {
    type: PROVIDER_SETTINGS_SUCCESS,
    payload,
  };
}

export function providerSettingsFailure(payload) {
  return {
    type: PROVIDER_SETTINGS_FAILURE,
    payload,
  };
}

export function disconnectReason(payload) {
  return {
    type: DISCONNECT_REASON,
    payload,
  };
}

export function disconnectReasonSuccess(payload) {
  return {
    type: DISCONNECT_REASON_SUCCESS,
    payload,
  };
}

export function disconnectReasonFailure(payload) {
  return {
    type: DISCONNECT_REASON_FAILURE,
    payload,
  };
}

export function resetWaitingRoom(payload) {
  return {
    type: RESET_WAITING_ROOM_FIELDS,
    payload,
  };
}
