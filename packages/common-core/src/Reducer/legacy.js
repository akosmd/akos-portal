import produce from 'immer';

import {
  GET_INSURANCE_ELIGIBILITY,
  GET_INSURANCE_ELIGIBILITY_SUCCESS,
  GET_INSURANCE_ELIGIBILITY_FAILURE,
  GET_PATIENT_INSURANCE,
  GET_PATIENT_INSURANCE_SUCCESS,
  GET_PATIENT_INSURANCE_FAILURE,
  FIND_INSURANCE,
  FIND_INSURANCE_SUCCESS,
  FIND_INSURANCE_FAILURE,
  SAVE_PATIENT_INSURANCE,
  SAVE_PATIENT_INSURANCE_SUCCESS,
  SAVE_PATIENT_INSURANCE_FAILURE,
  COUPON_CODE,
  COUPON_CODE_SUCCESS,
  COUPON_CODE_FAILURE,
  RESET_ELIGIBILITY_CODES,
  BRAINTREE_TOKEN,
  BRAINTREE_TOKEN_SUCCESS,
  BRAINTREE_TOKEN_FAILURE,
  REGULAR_PAYMENT_AMOUNT,
  PAYMENT_CHECKOUT,
  PAYMENT_CHECKOUT_SUCCESS,
  PAYMENT_CHECKOUT_FAILURE,
  GENERATE_PATIENT_CALL_ID,
  GENERATE_PATIENT_CALL_ID_SUCCESS,
  GENERATE_PATIENT_CALL_ID_FAILURE,
  RESET_PAYMENT_CHECKOUT,
  RESET_COUPON_CODE,
  ADD_USER_TO_WAITING_ROOM,
  ADD_USER_TO_WAITING_ROOM_SUCCESS,
  ADD_USER_TO_WAITING_ROOM_FAILURE,
  NOTIFY_PROVIDERS,
  NOTIFY_PROVIDERS_SUCCESS,
  NOTIFY_PROVIDERS_FAILURE,
  CHECK_WAITING_ROOM_STATUS,
  CHECK_WAITING_ROOM_STATUS_SUCCESS,
  CHECK_WAITING_ROOM_STATUS_FAILURE,
  GET_OPENTOK_ROOM_KEYS,
  GET_OPENTOK_ROOM_KEYS_SUCCESS,
  GET_OPENTOK_ROOM_KEYS_FAILURE,
  GET_DOC_ALIAS,
  GET_DOC_ALIAS_SUCCESS,
  GET_DOC_ALIAS_FAILURE,
  UPDATE_PATIENT_CALL_ID,
  UPDATE_PATIENT_CALL_ID_SUCCESS,
  UPDATE_PATIENT_CALL_ID_FAILURE,
  CALL_END_BY_PATIENT,
  CALL_END_BY_PATIENT_SUCCESS,
  CALL_END_BY_PATIENT_FAILURE,
  INVALIDATE_TOKEN,
  INVALIDATE_TOKEN_SUCCESS,
  INVALIDATE_TOKEN_FAILURE,
  PROVIDER_SETTINGS,
  PROVIDER_SETTINGS_SUCCESS,
  PROVIDER_SETTINGS_FAILURE,
  DISCONNECT_REASON,
  DISCONNECT_REASON_SUCCESS,
  DISCONNECT_REASON_FAILURE,
  RESET_WAITING_ROOM_FIELDS,
} from 'core/Constants/legacy';

import { LOGOUT } from 'core/Constants';

const defaultProps = {
  loading: false,
  data: false,
  error: false,
  saved: false,
};

export const legacyMigrations = {
  1.2: previousVersionState => ({
    legacy: {
      change: previousVersionState.patient,
      lastUpdate: new Date(),
    },
  }),
};

export const initialState = {
  insuranceEligibility: {
    ...defaultProps,
  },
  patientInsurance: {
    ...defaultProps,
  },
  insuranceList: {
    ...defaultProps,
  },
  savedPatientInsurance: {
    ...defaultProps,
  },
  couponCode: {
    ...defaultProps,
  },
  brainTreeToken: {
    ...defaultProps,
  },
  paymentCheckout: {
    ...defaultProps,
  },
  patientCallId: {
    ...defaultProps,
  },
  updatedPatientCallId: {
    ...defaultProps,
  },
  paymentAmount: 75,
  addUserToWaitingRoom: {
    ...defaultProps,
  },
  notifyProviders: {
    ...defaultProps,
  },
  waitingRoomStatus: {
    ...defaultProps,
  },
  openTokRoomKeys: {
    ...defaultProps,
  },
  docAlias: {
    ...defaultProps,
  },
  callEndByPatient: {
    ...defaultProps,
  },
  invalidateToken: {
    ...defaultProps,
  },
  providerSettings: {
    ...defaultProps,
  },
  disconnectReason: {
    ...defaultProps,
  },
};

/* eslint-disable default-case, no-param-reassign */
const legacyReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case GET_INSURANCE_ELIGIBILITY: {
        draft.insuranceEligibility = {
          loading: true,
          data: false,
          error: false,
        };

        break;
      }
      case GET_INSURANCE_ELIGIBILITY_SUCCESS:
        draft.insuranceEligibility = {
          loading: false,
          data: action.payload || false,
          error: false,
        };

        break;
      case GET_INSURANCE_ELIGIBILITY_FAILURE:
        draft.insuranceEligibility = {
          loading: false,
          data: false,
          error: action.payload,
        };
        break;
      case GET_PATIENT_INSURANCE: {
        draft.patientInsurance = {
          loading: true,
          data: false,
          error: false,
        };

        break;
      }
      case GET_PATIENT_INSURANCE_SUCCESS:
        draft.patientInsurance = {
          loading: false,
          data: action.payload || false,
          error: false,
        };

        break;
      case GET_PATIENT_INSURANCE_FAILURE:
        draft.patientInsurance = {
          loading: false,
          data: false,
          error: action.payload,
        };
        break;
      case FIND_INSURANCE: {
        draft.insuranceList = {
          loading: true,
          data: false,
          error: false,
        };

        break;
      }
      case FIND_INSURANCE_SUCCESS:
        draft.insuranceList = {
          loading: false,
          data: action.payload || false,
          error: false,
        };

        break;
      case FIND_INSURANCE_FAILURE:
        draft.insuranceList = {
          loading: false,
          data: false,
          error: action.payload,
        };
        break;

      case SAVE_PATIENT_INSURANCE: {
        draft.savedPatientInsurance = {
          loading: true,
          data: false,
          error: false,
        };

        break;
      }
      case SAVE_PATIENT_INSURANCE_SUCCESS:
        draft.savedPatientInsurance = {
          loading: false,
          data: action.payload || false,
          error: false,
        };

        break;
      case SAVE_PATIENT_INSURANCE_FAILURE:
        draft.savedPatientInsurance = {
          loading: false,
          data: false,
          error: action.payload,
        };
        break;
      case COUPON_CODE:
        draft.couponCode = {
          loading: true,
          data: false,
          error: false,
        };

        break;

      case COUPON_CODE_SUCCESS:
        draft.couponCode = {
          loading: false,
          data: action.payload || false,
          error: false,
        };

        break;
      case COUPON_CODE_FAILURE:
        draft.couponCode = {
          loading: false,
          data: false,
          error: action.payload,
        };
        break;
      case GENERATE_PATIENT_CALL_ID:
        draft.patientCallId = {
          loading: true,
          data: false,
          error: false,
        };
        break;
      case GENERATE_PATIENT_CALL_ID_SUCCESS:
        draft.patientCallId = {
          loading: false,
          data: action.payload || false,
          error: false,
        };

        break;
      case GENERATE_PATIENT_CALL_ID_FAILURE:
        draft.patientCallId = {
          loading: false,
          data: false,
          error: action.payload,
        };
        break;
      case UPDATE_PATIENT_CALL_ID:
        draft.updatedPatientCallId = {
          loading: true,
          data: false,
          error: false,
        };
        break;
      case UPDATE_PATIENT_CALL_ID_SUCCESS:
        draft.updatedPatientCallId = {
          loading: false,
          data: action.payload || false,
          error: false,
        };

        break;
      case UPDATE_PATIENT_CALL_ID_FAILURE:
        draft.updatedPatientCallId = {
          loading: false,
          data: false,
          error: action.payload,
        };
        break;
      case PAYMENT_CHECKOUT:
        draft.paymentCheckout = {
          loading: true,
          data: false,
          error: false,
        };

        break;

      case PAYMENT_CHECKOUT_SUCCESS:
        draft.paymentCheckout = {
          loading: false,
          data: action.payload || false,
          error: false,
        };

        break;
      case PAYMENT_CHECKOUT_FAILURE:
        draft.paymentCheckout = {
          loading: false,
          data: false,
          error: action.payload,
        };
        break;

      case BRAINTREE_TOKEN: {
        draft.brainTreeToken = {
          loading: true,
          data: false,
          error: false,
        };

        break;
      }
      case BRAINTREE_TOKEN_SUCCESS:
        draft.brainTreeToken = {
          loading: false,
          data: action.payload || false,
          error: false,
        };

        break;
      case BRAINTREE_TOKEN_FAILURE:
        draft.brainTreeToken = {
          loading: false,
          data: false,
          error: action.payload,
        };
        break;
      case RESET_ELIGIBILITY_CODES:
        draft.couponCode = {
          loading: false,
          data: false,
          error: false,
        };
        draft.savedPatientInsurance = {
          loading: false,
          data: false,
          error: false,
        };
        draft.insuranceEligibility = {
          loading: false,
          data: false,
          error: false,
        };
        break;
      case LOGOUT: {
        const keys = Object.keys(initialState);
        keys.forEach(key => {
          draft[key] = initialState[key];
        });

        break;
      }
      case REGULAR_PAYMENT_AMOUNT:
        draft.paymentAmount = action.payload;
        break;
      case RESET_PAYMENT_CHECKOUT:
        draft.paymentCheckout = { ...defaultProps };
        break;
      case RESET_COUPON_CODE:
        draft.couponCode = { ...defaultProps };
        break;

      case ADD_USER_TO_WAITING_ROOM:
        draft.addUserToWaitingRoom = {
          loading: true,
          data: false,
          error: false,
        };
        break;

      case ADD_USER_TO_WAITING_ROOM_SUCCESS:
        draft.addUserToWaitingRoom = {
          loading: false,
          data: action.payload || false,
          error: false,
        };

        break;
      case ADD_USER_TO_WAITING_ROOM_FAILURE:
        draft.addUserToWaitingRoom = {
          loading: false,
          data: false,
          error: action.payload,
        };
        break;

      case NOTIFY_PROVIDERS:
        draft.notifyProviders = {
          loading: true,
          data: false,
          error: false,
        };
        break;

      case NOTIFY_PROVIDERS_SUCCESS:
        draft.notifyProviders = {
          loading: false,
          data: action.payload || false,
          error: false,
        };

        break;
      case NOTIFY_PROVIDERS_FAILURE:
        draft.notifyProviders = {
          loading: false,
          data: false,
          error: action.payload,
        };
        break;

      case CHECK_WAITING_ROOM_STATUS:
        draft.waitingRoomStatus = {
          loading: true,
          data: false,
          error: false,
        };
        break;

      case CHECK_WAITING_ROOM_STATUS_SUCCESS:
        draft.waitingRoomStatus = {
          loading: false,
          data: action.payload || false,
          error: false,
        };

        break;
      case CHECK_WAITING_ROOM_STATUS_FAILURE:
        draft.waitingRoomStatus = {
          loading: false,
          data: false,
          error: action.payload,
        };
        break;

      case GET_OPENTOK_ROOM_KEYS:
        draft.openTokRoomKeys = {
          loading: true,
          data: false,
          error: false,
        };
        break;

      case GET_OPENTOK_ROOM_KEYS_SUCCESS:
        draft.openTokRoomKeys = {
          loading: false,
          data: action.payload || false,
          error: false,
        };

        break;
      case GET_OPENTOK_ROOM_KEYS_FAILURE:
        draft.openTokRoomKeys = {
          loading: false,
          data: false,
          error: action.payload,
        };
        break;

      case GET_DOC_ALIAS:
        draft.docAlias = {
          loading: true,
          data: false,
          error: false,
        };
        break;

      case GET_DOC_ALIAS_SUCCESS:
        draft.docAlias = {
          loading: false,
          data: action.payload || false,
          error: false,
        };

        break;
      case GET_DOC_ALIAS_FAILURE:
        draft.docAlias = {
          loading: false,
          data: false,
          error: action.payload,
        };
        break;

      case CALL_END_BY_PATIENT:
        draft.callEndByPatient = {
          loading: true,
          data: false,
          error: false,
        };
        break;

      case CALL_END_BY_PATIENT_SUCCESS:
        draft.callEndByPatient = {
          loading: false,
          data: action.payload || false,
          error: false,
        };

        break;
      case CALL_END_BY_PATIENT_FAILURE:
        draft.callEndByPatient = {
          loading: false,
          data: false,
          error: action.payload,
        };
        break;

      case INVALIDATE_TOKEN:
        draft.invalidateToken = {
          loading: true,
          data: false,
          error: false,
        };
        break;

      case INVALIDATE_TOKEN_SUCCESS:
        draft.invalidateToken = {
          loading: false,
          data: action.payload || false,
          error: false,
        };

        break;
      case INVALIDATE_TOKEN_FAILURE:
        draft.invalidateToken = {
          loading: false,
          data: false,
          error: action.payload,
        };
        break;

      case PROVIDER_SETTINGS:
        draft.providerSettings = {
          loading: true,
          data: false,
          error: false,
        };
        break;

      case PROVIDER_SETTINGS_SUCCESS:
        draft.providerSettings = {
          loading: false,
          data: action.payload || false,
          error: false,
        };

        break;
      case PROVIDER_SETTINGS_FAILURE:
        draft.providerSettings = {
          loading: false,
          data: false,
          error: action.payload,
        };
        break;

      case DISCONNECT_REASON:
        draft.disconnectReason = {
          loading: true,
          data: false,
          error: false,
        };
        break;

      case DISCONNECT_REASON_SUCCESS:
        draft.disconnectReason = {
          loading: false,
          data: action.payload || false,
          error: false,
        };

        break;
      case DISCONNECT_REASON_FAILURE:
        draft.disconnectReason = {
          loading: false,
          data: false,
          error: action.payload,
        };
        break;

      case RESET_WAITING_ROOM_FIELDS:
        draft.patientCallId = defaultProps;
        draft.waitingRoomStatus = defaultProps;
        draft.addUserToWaitingRoom = defaultProps;
        draft.updatedPatientCallId = defaultProps;
        draft.brainTreeToken = defaultProps;
        break;
    }
  });

export default legacyReducer;
