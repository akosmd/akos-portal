/* eslint-disable indent */
import axios, { CancelToken } from 'axios';
import qs from 'qs';

let cancel;

export const api = axios.create({
  baseURL: `${process.env.API_URL}`,
});

export const newApi = axios.create({
  baseURL: `${process.env.NEW_API_URL}`,
});

export const integrationApi = axios.create({
  baseURL: `${process.env.INTEGRATION_API_URL}`,
});

export const restApi = axios.create({
  baseURL: `${process.env.REST_API_URL}`,
});

export const connectApi = axios.create({
  baseURL: `${process.env.CONNECT_API_URL}`,
});

export const cancellableIntegrationApi = (url, params, token) => {
  if (cancel !== undefined) {
    cancel();
  }
  return integrationApi.get(url, {
    params,
    paramsSerializer: p => qs.stringify(p),
    cancelToken: new CancelToken(function executor(c) {
      cancel = c;
    }),
    headers: {
      'Content-Type': 'text/plain',
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
    },
  });
};

[api, newApi, restApi, integrationApi, connectApi].forEach(item => {
  if (cancel !== undefined) {
    cancel();
  }

  const updateConfig = config => {
    const newConfig = Object.assign({}, config);
    newConfig.cancelToken = new CancelToken(function executor(c) {
      cancel = c;
    });
    return newConfig;
  };

  item.interceptors.request.use(
    config => updateConfig(config),
    error => Promise.reject(error),
  );
  item.interceptors.response.use(
    response => response,
    error => {
      // Do something with response error
      // if (!error.response) return Promise.reject(error);
      switch (error.response.status) {
        case 401: {
          const payload = error.response
            ? { status: 401, error: error.response.messsage || error.response }
            : {
                status: 401,
                error: 'User not Authorized',
              };
          return payload;
        }
        case 403: {
          const payload = error.response
            ? {
                status: 403,
                error: error.response.data.messsage || error.response,
              }
            : {
                status: 403,
                error: 'User not Authorized',
              };
          return payload;
        }
        case 422:
        case 404:
        case 400:
          return error.response;
        default:
          return Promise.reject(error);
      }
    },
  );
});
