import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
  palette: {
    primary: {
      light: '#41c9d0',
      main: '#12bcc5',
      dark: '#25406B',
      contrastText: '#fff',
    },
    secondary: {
      light: '#506688',
      main: '#25406B',
      dark: '#192c4a',
    },
    text: {
      primary: '#666',
    },
    background: {
      default: '#f2f2f2',
    },
  },

  typography: {
    useNextVariants: true,
    fontFamily: 'Lato, sans serif',
  },
});

export default theme;
