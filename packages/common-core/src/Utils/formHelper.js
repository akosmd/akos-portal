import produce from 'immer';
import moment from 'moment';

export const required = true;

export const initialState = {
  status: undefined,
  completed: false,
  inProgress: false,
};

export const initialField = {
  id: undefined,
  caption: '',
  errorMessage: undefined,
  placeholder: undefined,
  pristine: true,
  error: true,
  required: true,
  fullWidth: true,
  autoFocus: false,
  readOnly: false,
  value: '',
};

export const fileField = {
  base64: undefined,
  filename: undefined,
  filetype: undefined,
};

export const status = {
  inprogress: 'inprogress',
  completed: 'completed',
  error: 'error',
};

export const scrollToTop = () => {
  window.scroll({
    top: 0,
    left: 0,
    behavior: 'smooth',
  });
};

export const invalidRequiredFieldForStatus = value =>
  value === '' || value === undefined || value === null;

export const invalidEmail = email => {
  if (!email) return false;

  // eslint-disable-next-line no-useless-escape
  const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  return !regex.test(email);
};

export const isNotEqual = (value1, value2) => value1 !== value2;

export const handleChange = ({ field, state, saveStepFunc, event }) => {
  const { value } = event.target;

  const selectedField = state[field];
  let error = !!selectedField.required;
  if (selectedField.required) {
    if (selectedField.validator) {
      error = selectedField.validator();
    } else {
      error = invalidRequiredFieldForStatus(value);
    }
  }

  const updatedField = {
    ...selectedField,
    pristine: false,
    value,
    error,
  };

  const formCompleted =
    !error && !checkStatus({ source: state, currentField: field });

  const formStatus = !formCompleted ? 'error' : 'completed';
  saveStepFunc({
    ...state,
    [field]: updatedField,
    completed: formCompleted,
    status: formStatus,
  });
  return error;
};

export const handleEqualityChange = ({
  field1,
  field2,
  state,
  saveStepFunc,
  event,
}) => {
  const { value, id } = event.target;
  const selectedField = state[id];
  let hasError = false;
  let otherField = {};
  if (field1.id !== id) otherField = field1;
  if (field2.id !== id) otherField = field2;
  const requiredMessage = `${selectedField.caption} is required`;
  const errorMessage = `Mismatch with ${otherField.caption}`;
  const isEqual = !isNotEqual(otherField.value, value);
  selectedField.errorMessage = errorMessage;
  if (!isEqual) {
    hasError = true;
  } else if (selectedField.required && invalidRequiredFieldForStatus(value)) {
    hasError = true;
    selectedField.errorMessage = requiredMessage;
  } else {
    hasError = false;
    otherField.error = false;
  }

  const updatedField = {
    ...selectedField,
    pristine: false,
    value,
    error: hasError,
  };

  const formCompleted =
    isEqual && !checkStatus({ source: state, currentField: id });

  const formStatus = !formCompleted ? 'error' : 'completed';
  saveStepFunc({
    ...state,
    [id]: updatedField,
    [otherField.id]: otherField,
    completed: formCompleted,
    status: formStatus,
  });
  return !isEqual;
};

export const handleEmailChange = ({ field, state, saveStepFunc, event }) => {
  const { value } = event.target;
  const selectedField = state[field];
  let error = field.validator ? field.validator() : invalidEmail(value);
  if (!error && selectedField.required)
    error = invalidRequiredFieldForStatus(value);
  const updatedField = {
    ...selectedField,
    value,
    pristine: false,
    error,
  };

  const formCompleted =
    !error && !checkStatus({ source: state, currentField: field });

  const formStatus = !formCompleted ? 'error' : 'completed';

  saveStepFunc({
    ...state,
    [field]: updatedField,
    completed: formCompleted,
    status: formStatus,
  });
  return error;
};

export const handleDateChange = ({ field, state, saveStepFunc, value }) => {
  const selectedField = state[field];
  const error = invalidRequiredFieldForStatus(value);
  const updatedField = {
    ...selectedField,
    value,
    pristine: false,
    error,
  };

  const formCompleted =
    !error && !checkStatus({ source: state, currentField: field });

  const formStatus = !formCompleted ? 'error' : 'completed';

  saveStepFunc({
    ...state,
    [field]: updatedField,
    completed: formCompleted,
    status: formStatus,
  });
  return error;
};

export const handleUpload = ({ field, state, saveStepFunc, event }) => {
  const selectedField = state[field];

  if (event.target.files.length > 0) {
    const reader = new FileReader();
    const file = event.target.files[0];
    reader.onloadend = () => {
      const { result } = reader;
      const updatedField = {
        ...selectedField,
        base64: result.toString(),
        filename: file.name,
        filetype: file.type,
        value: file.name,
        pristine: false,
        error: false,
      };

      const formCompleted = !checkStatus({
        source: state,
        currentField: field,
      });

      const formStatus = !formCompleted ? 'error' : 'completed';

      saveStepFunc({
        ...state,
        [field]: updatedField,
        completed: formCompleted,
        status: formStatus,
      });
    };
    reader.readAsDataURL(file);
  }
};

export const checkStatus = ({ source, currentField }) => {
  const fields = Object.keys(source);
  let hasError = false;
  fields.map(fieldId => {
    if (
      source[fieldId] &&
      source[fieldId].error &&
      ((currentField && fieldId !== currentField) || !currentField)
    ) {
      hasError = true;
    }
    return undefined;
  });
  return hasError;
};

export const extractFormValues = source => {
  const keys = Object.keys(source);
  const fields = {};
  keys.forEach(key => {
    if (source[key] && source[key].id) {
      fields[key] = source[key].value;
      if (source[key].numeric) fields[key] = Number(source[key].value);
      if (source[key].isFile)
        fields[key] = {
          filetype: source[key].filetype,
          filename: source[key].filename,
          base64: source[key].base64,
          dateStamp: source[key].dateStamp,
        };
    }
  });
  return fields;
};

/* eslint-disable default-case, no-param-reassign */
export const highlightFormErrors = (source, saveStepFunc) => {
  const keys = Object.keys(source);
  let hasError = false;
  const fields = produce(source, draft => {
    keys.forEach(key => {
      if (source[key] && source[key].id && source[key].error) {
        draft[key] = {
          ...source[key],
          pristine: false,
          error: true,
        };
        hasError = true;
      } else {
        draft[key] = source[key];
      }
    });
  });
  saveStepFunc({ ...fields, status: hasError ? 'error' : 'completed' });
};

/* eslint-disable default-case, no-param-reassign */
export const setValuesFrom = (source, target, readOnly) => {
  if (!source || !target) return undefined;
  const keys = Object.keys(source);
  const fields = produce(target, draft => {
    keys.forEach(key => {
      if (draft[key]) {
        if (source[key] !== null) {
          draft[key].pristine = false;
          draft[key].error = false;
          draft[key].readOnly = readOnly;
          draft[key].value = draft[key].numeric
            ? String(source[key])
            : source[key];

          if (typeof source[key] === 'object') {
            draft[key] = {
              ...draft[key],
              ...source[key],
              readOnly,
              error: !source[key].value && source[key].filename === 'null',
              pristine: !source[key].value && source[key].filename === 'null',
            };
          }
        } else if (
          source[key] === null &&
          target[key].value !== '' &&
          target[key].value !== null
        ) {
          draft[key].error = false;
          draft[key].pristine = false;
          draft[key].readOnly = readOnly;
        } else {
          if (draft[key].required) {
            draft[key].error = true;
          }
          draft[key].value = '';
          draft[key].readOnly = readOnly;
        }
      }
    });
  });
  return fields;
};

export const formatPhoneNumber = str => {
  const cleaned = `${str}`.replace(/\D/g, '');

  const match = cleaned.match(/^(\d{5})(\d{5})$/);

  if (match) {
    return { value: `${match[1]}-${match[2]}`, error: false };
  }

  return { value: str, error: true };
};

export const handlePhoneChange = ({ field, state, saveStepFunc, event }) => {
  const { value } = event.target;

  const selectedField = state[field];
  let error = !!selectedField.required;
  if (selectedField.required) {
    if (selectedField.validator) {
      error = selectedField.validator();
    } else {
      error = invalidRequiredFieldForStatus(value);
    }
  }
  const phone = formatPhoneNumber(value);
  error = phone === null;
  const updatedField = {
    ...selectedField,
    pristine: false,
    value: phone.value,
    error: phone.error,
  };

  const formCompleted =
    !error && !checkStatus({ source: state, currentField: field });

  const formStatus = !formCompleted ? 'error' : 'completed';
  saveStepFunc({
    ...state,
    [field]: updatedField,
    completed: formCompleted,
    status: formStatus,
  });
  return error;
};

export const getKeyByValue = (object, value) => {
  const res = Object.keys(object).find(key => object[key] === value);
  return typeof res === 'string' ? res.replace('_', ' ') : value;
};

export const formatDate = value => value && moment(value).format('MMMM D YYYY');
