import { createAction } from 'redux-actions';

import {
  GET_NURSE_PATIENT_INFORMATION,
  GET_NURSE_PATIENT_INFORMATION_SUCCESS,
  GET_NURSE_PATIENT_INFORMATION_FAILURE,
  GET_NURSE_SELECTED_PATIENT_INFORMATION,
  GET_NURSE_SELECTED_PATIENT_INFORMATION_SUCCESS,
  GET_NURSE_SELECTED_PATIENT_INFORMATION_FAILURE,
  ADD_COMPANY_NOTES_SUCCESS,
  ADD_COMPANY_NOTES_FAILURE,
  ADD_COMPANY_NOTES,
  GET_COMPANY_NOTES,
  GET_COMPANY_NOTES_SUCCESS,
  GET_COMPANY_NOTES_FAILURE,
} from './Constant';

export const getNursePatientInformation = createAction(
  GET_NURSE_PATIENT_INFORMATION,
);
export const getNursePatientInformationSuccess = createAction(
  GET_NURSE_PATIENT_INFORMATION_SUCCESS,
);
export const getNursePatientInformationFailure = createAction(
  GET_NURSE_PATIENT_INFORMATION_FAILURE,
);
export const getNurseSelectedPatientInformation = createAction(
  GET_NURSE_SELECTED_PATIENT_INFORMATION,
);
// export function getNursePatientInformation(payload) {
//   return {
//     type: GET_NURSE_PATIENT_INFORMATION,
//     payload,
//   };
// }

// export function getNursePatientInformationSuccess(payload) {
//   return {
//     type: GET_NURSE_PATIENT_INFORMATION_SUCCESS,
//     payload,
//   };
// }

// export function getNursePatientInformationFailure(payload) {
//   return {
//     type: GET_NURSE_PATIENT_INFORMATION_FAILURE,
//     payload,
//   };
// }

// export function getNurseSelectedPatientInformation(payload) {
//   return {
//     type: GET_NURSE_SELECTED_PATIENT_INFORMATION,
//     payload,
//   };
// }

export function getNurseSelectedPatientInformationSuccess(payload) {
  return {
    type: GET_NURSE_SELECTED_PATIENT_INFORMATION_SUCCESS,
    payload,
  };
}

export function getNurseSelectedPatientInformationFailure(payload) {
  return {
    type: GET_NURSE_SELECTED_PATIENT_INFORMATION_FAILURE,
    payload,
  };
}

export function addCompanyNotes(payload) {
  return {
    type: ADD_COMPANY_NOTES,
    payload,
  };
}

export function addCompanyNotesSuccess(payload) {
  return {
    type: ADD_COMPANY_NOTES_SUCCESS,
    payload,
  };
}

export function addCompanyNotesFailure(payload) {
  return {
    type: ADD_COMPANY_NOTES_FAILURE,
    payload,
  };
}

export function getCompanyNotes(payload) {
  return {
    type: GET_COMPANY_NOTES,
    payload,
  };
}

export function getCompanyNotesSuccess(payload) {
  return {
    type: GET_COMPANY_NOTES_SUCCESS,
    payload,
  };
}

export function getCompanyNotesFailure(payload) {
  return {
    type: GET_COMPANY_NOTES_FAILURE,
    payload,
  };
}
