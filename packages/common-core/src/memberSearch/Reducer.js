/*
 *
 * Register reducer
 *
 */
import { handleActions } from 'redux-actions';
import produce from 'immer';
import {
  GET_NURSE_PATIENT_INFORMATION,
  GET_NURSE_PATIENT_INFORMATION_SUCCESS,
  GET_NURSE_PATIENT_INFORMATION_FAILURE,
  GET_NURSE_SELECTED_PATIENT_INFORMATION,
  GET_NURSE_SELECTED_PATIENT_INFORMATION_SUCCESS,
  GET_NURSE_SELECTED_PATIENT_INFORMATION_FAILURE,
  GET_COMPANY_NOTES,
  GET_COMPANY_NOTES_SUCCESS,
  GET_COMPANY_NOTES_FAILURE,
  ADD_COMPANY_NOTES,
  ADD_COMPANY_NOTES_FAILURE,
  ADD_COMPANY_NOTES_SUCCESS,
} from './Constant';

import {
  getNursePatientInformation,
  getNursePatientInformationSuccess,
  getNursePatientInformationFailure,
  getNurseSelectedPatientInformation,
  getNurseSelectedPatientInformationSuccess,
  getNurseSelectedPatientInformationFailure,
  addCompanyNotes,
  addCompanyNotesSuccess,
  addCompanyNotesFailure,
  getCompanyNotes,
  getCompanyNotesSuccess,
  getCompanyNotesFailure,
} from './Action';

export const patientMigrations = {
  1.2: previousVersionState => ({
    patient: {
      change: previousVersionState.patient,
      lastUpdate: new Date(),
    },
  }),
};

const defaultProps = {
  loading: false,
  data: false,
  error: false,
  saved: false,
};

export const initialState = {
  nursePatientList: {
    ...defaultProps,
    search: '',
  },
  nurseSelectedPatientList: {
    ...defaultProps,
  },
  companyNotes: {
    ...defaultProps,
  },
};

const memberSearchReducer = handleActions(
  {
    [getNursePatientInformation]: (state, { payload }) => ({
      ...state,
      nursePatientList: {
        loading: true,
        data: null,
        search: action.payload,
        error: false,
      },
      nurseSelectedPatientList: {
        ...initialState.nurseSelectedPatientList,
      },
    }),
    [getNursePatientInformationSuccess]: (state, { payload }) => ({
      ...state,
      nursePatientList: {
        ...state.nursePatientList,
        loading: false,
        data: payload,
        error: false,
      },
    }),
    [getNursePatientInformationFailure]: (state, { payload }) => ({
      ...state,
      nursePatientList: {
        ...state.nursePatientList,
        loading: false,
        data: false,
        error: payload,
      },
    }),
  },
  initialState,
);
// const memberSearchReducer = (state = initialState, action) =>
//   produce(state, draft => {
//     switch (action.type) {
//       case GET_NURSE_PATIENT_INFORMATION:
//         draft.nursePatientList = {
//           loading: true,
//           data: null,
//           search: action.payload,
//           error: false,
//         };
//         draft.nurseSelectedPatientList = {
//           ...initialState.nurseSelectedPatientList,
//         };
//         break;
//       case GET_NURSE_PATIENT_INFORMATION_SUCCESS:
//         draft.nursePatientList = {
//           ...draft.nursePatientList,
//           loading: false,
//           data: action.payload,
//           error: false,
//         };
//         break;
//       case GET_NURSE_PATIENT_INFORMATION_FAILURE:
//         draft.nursePatientList = {
//           ...draft.nursePatientList,
//           loading: false,
//           data: false,
//           error: action.payload,
//         };
//         break;
//       case GET_NURSE_SELECTED_PATIENT_INFORMATION:
//         draft.nurseSelectedPatientList = {
//           loading: true,
//           data: false,
//           error: false,
//         };
//         break;
//       case GET_NURSE_SELECTED_PATIENT_INFORMATION_SUCCESS:
//         draft.nurseSelectedPatientList = {
//           ...draft.nurseSelectedPatientList,
//           loading: false,
//           data: action.payload,
//           error: false,
//         };
//         break;
//       case GET_NURSE_SELECTED_PATIENT_INFORMATION_FAILURE:
//         draft.nurseSelectedPatientList = {
//           ...draft.nurseSelectedPatientList,
//           loading: false,
//           data: false,
//           error: action.payload,
//         };
//         break;
//       case GET_COMPANY_NOTES:
//         draft.companyNotes = {
//           loading: true,
//           data: false,
//           error: false,
//         };
//         break;
//       case GET_COMPANY_NOTES_SUCCESS:
//         draft.companyNotes = {
//           loading: false,
//           data: action.payload,
//           error: false,
//         };
//         break;
//       case GET_COMPANY_NOTES_FAILURE:
//         draft.companyNotes = {
//           loading: false,
//           data: false,
//           error: action.payload,
//         };
//         break;
//       case ADD_COMPANY_NOTES:
//         draft.companyNotes = {
//           ...draft.companyNotes,
//           loading: true,
//           error: false,
//         };
//         break;
//       case ADD_COMPANY_NOTES_SUCCESS:
//         draft.companyNotes = {
//           ...draft.companyNotes,
//           loading: false,
//           error: false,
//         };
//         break;
//       case ADD_COMPANY_NOTES_FAILURE:
//         draft.companyNotes = {
//           ...draft.companyNotes,
//           loading: false,
//           error: action.payload,
//         };
//         break;
//     }
//   });

memberSearchReducer.key = 'memberSearch';
export default memberSearchReducer;
