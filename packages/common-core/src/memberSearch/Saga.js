import { takeLatest, call, put } from 'redux-saga/effects';
import { integrationApi, cancellableIntegrationApi } from 'core/Utils/api';

import { apiResponseEvaluator } from 'core/Utils/sagaHelper';

import { makeSelectVerifyAuth, makeSelectGoogleAuth } from 'core/Selectors';

import {
  GET_NURSE_PATIENT_INFORMATION,
  GET_NURSE_SELECTED_PATIENT_INFORMATION,
  ADD_COMPANY_NOTES,
  GET_COMPANY_NOTES,
} from './Constant';
import {
  getNursePatientInformationSuccess,
  getNursePatientInformationFailure,
  getNurseSelectedPatientInformationSuccess,
  getNurseSelectedPatientInformationFailure,
  addCompanyNotesSuccess,
  addCompanyNotesFailure,
  getCompanyNotesSuccess,
  getCompanyNotesFailure,
} from './Action';

function* getVerifyToken() {
  const auth = yield select(makeSelectVerifyAuth());

  return auth.data.token;
}

function* getNurseToken() {
  const auth = yield select(makeSelectGoogleAuth());

  return auth.data.token;
}

const doGetNursePatientInformation = ({ token, payload }) =>
  integrationApi.get(`nurse/patient/find`, {
    params: { ...payload },

    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

export function* getNursePatientInformationSaga({ payload }) {
  let token = yield getVerifyToken();

  if (!token) {
    token = yield getNurseToken();
  }
  token =
    'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjViMDNiOWE2NjViNDk4NGViYTgxNTZhOWY5ZTMwYmNmODY4ZDQ4ZTM1NTE0ODI5N2VmM2VhMWY5YzRiMzJiZmMzNzA2OTlhOTE3ODdjYjJhIn0.eyJhdWQiOiIxIiwianRpIjoiNWIwM2I5YTY2NWI0OTg0ZWJhODE1NmE5ZjllMzBiY2Y4NjhkNDhlMzU1MTQ4Mjk3ZWYzZWExZjljNGIzMmJmYzM3MDY5OWE5MTc4N2NiMmEiLCJpYXQiOjE1ODUyMTQyNDMsIm5iZiI6MTU4NTIxNDI0MywiZXhwIjoxNjE2NzUwMjQzLCJzdWIiOiI0NyIsInNjb3BlcyI6W119.ObelNDxLrUoRUVb-olAQmveVAZv5tysGVzLNmH2G0UpByr7vkmAETYa1jTA-qoSmgcojj3n1_tYRreihcX-7odEOyWyGcrOZLwcd3ZhPg_sARzr45G5EFLXWQIeng5n4yE41CGFvCP18QQZovFb1XSuUD7rEGcgCERl8dGqNnrkmLzXHXYQU2NOLWtL3F7tTVw03tst6a6pfvAtq2UHHbAbyd3esi0xnKbkVWG3MZs23wPempj45xp_a0rbpVTQ6PmmQkqKjfRZmth9oJY0tV3lfhg23iFHIeaDp_C-zamoVn-P9Bv5HudNISdvLSc3vDnG9JcbAE3hRGCnfuYMAdMYicI5TgkdAu8aNrxtWcPyedMJWbNlwrlop1UTpkC6PM0STwRpgob0vhfbdNoB0DI2tcSvFd1IEfyujrEa7AWhIyjwPuCPz4zwXXmo5fmHtI4jBMjg_Bo4pozMQIHLiW85FDs7PuNjcW3x8XUixkZG0JJtjHn6BiV4VCoTMqaUaaoVGdf9PUMKOj2mkWzT_zvYsJ63AnPa-q4lDP4PDhglQCH6RGNHCY0fj_x61JKelJMeautAv19jMsFyAa39uI8AFtoxzgBZgWUOeobs0T-uaIqS7aRsd9xswR4yhv6RewmIL1Fx7XwIZ--RmXzW61flCHKqLyGmbNjsOILqzTKY';
  try {
    const response = yield call(doGetNursePatientInformation, {
      token,
      payload,
    });

    yield apiResponseEvaluator(
      response,
      getNursePatientInformationSuccess,
      getNursePatientInformationFailure,
    );
  } catch (ex) {
    yield put(getNursePatientInformationFailure(ex));
  }
}

const doGetNurseSelectedPatientInformation = ({ token, payload }) => {
  const { patientId, otherInfo, ...rest } = payload;

  return cancellableIntegrationApi(
    '/nurse/patient/information',
    {
      patient_id: patientId,
      other_info: otherInfo || [
        'company',
        'company_plan_details',
        'member_plan',
        'company_plan',
        'company_external_plans',
        'benefit_summary_file_path',
      ],
      ...rest,
    },
    token,
  );
};

export function* getNurseSelectedPatientInformationSaga({ payload }) {
  let token = yield getVerifyToken();

  if (!token) {
    token = yield getNurseToken();
  }
  token =
    'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjViMDNiOWE2NjViNDk4NGViYTgxNTZhOWY5ZTMwYmNmODY4ZDQ4ZTM1NTE0ODI5N2VmM2VhMWY5YzRiMzJiZmMzNzA2OTlhOTE3ODdjYjJhIn0.eyJhdWQiOiIxIiwianRpIjoiNWIwM2I5YTY2NWI0OTg0ZWJhODE1NmE5ZjllMzBiY2Y4NjhkNDhlMzU1MTQ4Mjk3ZWYzZWExZjljNGIzMmJmYzM3MDY5OWE5MTc4N2NiMmEiLCJpYXQiOjE1ODUyMTQyNDMsIm5iZiI6MTU4NTIxNDI0MywiZXhwIjoxNjE2NzUwMjQzLCJzdWIiOiI0NyIsInNjb3BlcyI6W119.ObelNDxLrUoRUVb-olAQmveVAZv5tysGVzLNmH2G0UpByr7vkmAETYa1jTA-qoSmgcojj3n1_tYRreihcX-7odEOyWyGcrOZLwcd3ZhPg_sARzr45G5EFLXWQIeng5n4yE41CGFvCP18QQZovFb1XSuUD7rEGcgCERl8dGqNnrkmLzXHXYQU2NOLWtL3F7tTVw03tst6a6pfvAtq2UHHbAbyd3esi0xnKbkVWG3MZs23wPempj45xp_a0rbpVTQ6PmmQkqKjfRZmth9oJY0tV3lfhg23iFHIeaDp_C-zamoVn-P9Bv5HudNISdvLSc3vDnG9JcbAE3hRGCnfuYMAdMYicI5TgkdAu8aNrxtWcPyedMJWbNlwrlop1UTpkC6PM0STwRpgob0vhfbdNoB0DI2tcSvFd1IEfyujrEa7AWhIyjwPuCPz4zwXXmo5fmHtI4jBMjg_Bo4pozMQIHLiW85FDs7PuNjcW3x8XUixkZG0JJtjHn6BiV4VCoTMqaUaaoVGdf9PUMKOj2mkWzT_zvYsJ63AnPa-q4lDP4PDhglQCH6RGNHCY0fj_x61JKelJMeautAv19jMsFyAa39uI8AFtoxzgBZgWUOeobs0T-uaIqS7aRsd9xswR4yhv6RewmIL1Fx7XwIZ--RmXzW61flCHKqLyGmbNjsOILqzTKY';

  try {
    const response = yield call(doGetNurseSelectedPatientInformation, {
      token,
      payload,
    });
    yield apiResponseEvaluator(
      response,
      getNurseSelectedPatientInformationSuccess,
      getNurseSelectedPatientInformationFailure,
    );
  } catch (ex) {
    yield put(getNurseSelectedPatientInformationFailure(ex));
  }
}

const doAddCompanyNotes = ({ token, payload }) => {
  const { brokerId, notes } = payload;
  return integrationApi.patch(
    `/nurse/company/${brokerId}/notes`,
    { notes },
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    },
  );
};

export function* addCompanyNoteSaga({ payload }) {
  let token = yield getVerifyToken();
  if (!token) {
    token = yield getNurseToken();
  }
  token =
    'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjViMDNiOWE2NjViNDk4NGViYTgxNTZhOWY5ZTMwYmNmODY4ZDQ4ZTM1NTE0ODI5N2VmM2VhMWY5YzRiMzJiZmMzNzA2OTlhOTE3ODdjYjJhIn0.eyJhdWQiOiIxIiwianRpIjoiNWIwM2I5YTY2NWI0OTg0ZWJhODE1NmE5ZjllMzBiY2Y4NjhkNDhlMzU1MTQ4Mjk3ZWYzZWExZjljNGIzMmJmYzM3MDY5OWE5MTc4N2NiMmEiLCJpYXQiOjE1ODUyMTQyNDMsIm5iZiI6MTU4NTIxNDI0MywiZXhwIjoxNjE2NzUwMjQzLCJzdWIiOiI0NyIsInNjb3BlcyI6W119.ObelNDxLrUoRUVb-olAQmveVAZv5tysGVzLNmH2G0UpByr7vkmAETYa1jTA-qoSmgcojj3n1_tYRreihcX-7odEOyWyGcrOZLwcd3ZhPg_sARzr45G5EFLXWQIeng5n4yE41CGFvCP18QQZovFb1XSuUD7rEGcgCERl8dGqNnrkmLzXHXYQU2NOLWtL3F7tTVw03tst6a6pfvAtq2UHHbAbyd3esi0xnKbkVWG3MZs23wPempj45xp_a0rbpVTQ6PmmQkqKjfRZmth9oJY0tV3lfhg23iFHIeaDp_C-zamoVn-P9Bv5HudNISdvLSc3vDnG9JcbAE3hRGCnfuYMAdMYicI5TgkdAu8aNrxtWcPyedMJWbNlwrlop1UTpkC6PM0STwRpgob0vhfbdNoB0DI2tcSvFd1IEfyujrEa7AWhIyjwPuCPz4zwXXmo5fmHtI4jBMjg_Bo4pozMQIHLiW85FDs7PuNjcW3x8XUixkZG0JJtjHn6BiV4VCoTMqaUaaoVGdf9PUMKOj2mkWzT_zvYsJ63AnPa-q4lDP4PDhglQCH6RGNHCY0fj_x61JKelJMeautAv19jMsFyAa39uI8AFtoxzgBZgWUOeobs0T-uaIqS7aRsd9xswR4yhv6RewmIL1Fx7XwIZ--RmXzW61flCHKqLyGmbNjsOILqzTKY';
  try {
    const response = yield call(doAddCompanyNotes, {
      token,
      payload,
    });
    yield apiResponseEvaluator(
      response,
      addCompanyNotesSuccess,
      addCompanyNotesFailure,
    );
  } catch (ex) {
    yield put(addCompanyNotesFailure(ex.response || ex));
  }
}

const doGetCompanyNotes = ({ token, payload }) => {
  const { brokerId } = payload;
  return integrationApi.get(`/nurse/company/${brokerId}/notes`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

export function* getCompanyNoteSaga({ payload }) {
  let token = yield getVerifyToken();
  if (!token) {
    token = yield getNurseToken();
  }
  token =
    'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjViMDNiOWE2NjViNDk4NGViYTgxNTZhOWY5ZTMwYmNmODY4ZDQ4ZTM1NTE0ODI5N2VmM2VhMWY5YzRiMzJiZmMzNzA2OTlhOTE3ODdjYjJhIn0.eyJhdWQiOiIxIiwianRpIjoiNWIwM2I5YTY2NWI0OTg0ZWJhODE1NmE5ZjllMzBiY2Y4NjhkNDhlMzU1MTQ4Mjk3ZWYzZWExZjljNGIzMmJmYzM3MDY5OWE5MTc4N2NiMmEiLCJpYXQiOjE1ODUyMTQyNDMsIm5iZiI6MTU4NTIxNDI0MywiZXhwIjoxNjE2NzUwMjQzLCJzdWIiOiI0NyIsInNjb3BlcyI6W119.ObelNDxLrUoRUVb-olAQmveVAZv5tysGVzLNmH2G0UpByr7vkmAETYa1jTA-qoSmgcojj3n1_tYRreihcX-7odEOyWyGcrOZLwcd3ZhPg_sARzr45G5EFLXWQIeng5n4yE41CGFvCP18QQZovFb1XSuUD7rEGcgCERl8dGqNnrkmLzXHXYQU2NOLWtL3F7tTVw03tst6a6pfvAtq2UHHbAbyd3esi0xnKbkVWG3MZs23wPempj45xp_a0rbpVTQ6PmmQkqKjfRZmth9oJY0tV3lfhg23iFHIeaDp_C-zamoVn-P9Bv5HudNISdvLSc3vDnG9JcbAE3hRGCnfuYMAdMYicI5TgkdAu8aNrxtWcPyedMJWbNlwrlop1UTpkC6PM0STwRpgob0vhfbdNoB0DI2tcSvFd1IEfyujrEa7AWhIyjwPuCPz4zwXXmo5fmHtI4jBMjg_Bo4pozMQIHLiW85FDs7PuNjcW3x8XUixkZG0JJtjHn6BiV4VCoTMqaUaaoVGdf9PUMKOj2mkWzT_zvYsJ63AnPa-q4lDP4PDhglQCH6RGNHCY0fj_x61JKelJMeautAv19jMsFyAa39uI8AFtoxzgBZgWUOeobs0T-uaIqS7aRsd9xswR4yhv6RewmIL1Fx7XwIZ--RmXzW61flCHKqLyGmbNjsOILqzTKY';
  try {
    const response = yield call(doGetCompanyNotes, {
      token,
      payload,
    });
    yield apiResponseEvaluator(
      response,
      getCompanyNotesSuccess,
      getCompanyNotesFailure,
    );
  } catch (ex) {
    yield put(getCompanyNotesFailure(ex));
  }
}

export default function* patientSaga() {
  yield takeLatest(
    GET_NURSE_PATIENT_INFORMATION,
    getNursePatientInformationSaga,
  );
  yield takeLatest(
    GET_NURSE_SELECTED_PATIENT_INFORMATION,
    getNurseSelectedPatientInformationSaga,
  );
  yield takeLatest(ADD_COMPANY_NOTES, addCompanyNoteSaga);
  yield takeLatest(GET_COMPANY_NOTES, getCompanyNoteSaga);
}
