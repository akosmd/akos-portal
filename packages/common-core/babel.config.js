module.exports = {
  presets: ['@babel/preset-env', '@babel/preset-react'],
  plugins: [
    ['@babel/plugin-proposal-class-properties', { loose: true }],
    'transform-es2015-modules-commonjs',
    [
      'module-resolver',
      {
        root: ['.'],
        alias: {
          core: './src',
        },
      },
    ],
  ],
};
