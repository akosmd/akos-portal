import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
  palette: {
    primary: {
      light: '#a3c3ce',
      main: '#216e8a',
      dark: '#005979',
      contrastText: '#fff',
    },
    secondary: {
      light: '#68d26f',
      main: '#00a950',
      dark: '#00a950',
    },
    text: {
      primary: '#666',
    },
    background: {
      default: '#f2f2f2',
      light: '#ffffff',
      main: '#fafafa',
      dark: '#9fa1a3',
      footer: '#165979',
    },
    input: {
      active: '#dbdbdb',
    },
    disabled: {
      button: '#9fa1a3',
    },
    border: {
      textField: '#0000003b',
    },
    boxShadow: {
      main:
        '0px 1px 3px 0px rgba(0,0,0,0.2), 0px 0px 0px 0px rgba(0,0,0,0.14), 0px 1px 1px -1px rgba(0,0,0,0.12)',
      dark:
        '0 8px 28px -12px rgba(0, 0, 0, 0.56), 0 4px 15px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(0, 0, 0, 0.2)',
    },
    chat: {
      button: '#2fbcc5',
      receiverBubble: '#99dab1',
      senderBubble: '##def3fc',
    },
  },

  typography: {
    useNextVariants: true,
    fontFamily: 'lato,sans-serif',
    color: {
      light: '#ffffff',
      gray: '#9fa1a3',
      main: '#0000008a',
      dark: '#000000',
      error: '#f44336',
    },
  },
});

export default theme;
