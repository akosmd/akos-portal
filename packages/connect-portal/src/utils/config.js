export const API_URL =
  process.env.environment === 'production'
    ? 'https://api.akosmd.com/'
    : 'https://staging-api.akosmd.com/';
export const GI_API_URL =
  process.env.environment === 'production'
    ? 'https://52.247.203.105:5000/ai/'
    : 'https://gi.akosmd.com:5000/ai/';
export const NEW_API_URL =
  process.env.environment === 'production'
    ? 'https://newapi.akosmd.com/v1'
    : 'https://staging-newapi.akosmd.com/v1/';
export const REST_API_URL =
  process.env.environment === 'production'
    ? 'https://restapi.akosmd.com/'
    : 'https://staging-restapi.akosmd.com/';

export const CONNECT_API_URL =
  process.env.environment === 'production'
    ? 'https://connect-api.akosmd.com/'
    : 'https://staging-connect-api.akosmd.com/';

export const INTEGRATION_API =
  process.env.environment === 'production'
    ? 'https://integration.akosmd.com/api/'
    : 'https://staging-integration.akosmd.com/api/';

export const INXITE_URL =
  process.env.environment === 'production'
    ? 'https://integration.akosmd.com/api/inxite/patient/'
    : 'https://staging-integration.akosmd.com/api/inxite/patient/';

export const INXITE_SSO_URL =
  process.env.environment === 'production'
    ? 'https://smartcare.inxitehealth.com/'
    : 'https://demo2.inxitehealth.com/';

export const REACT_APP_WEBCHAT_TOKEN =
  process.env.environment === 'production'
    ? 'Y14qhuHTd0Y.WC17ewX_AaP1E3jse0JoKyludlI2erHTSUwvdYC6xKk'
    : 'ef1lU60CYv8.8BGmdHKoIqiVrzYmXfXoAcbG_BjzwtiIzK_kvL3vcqc';

export const GI_WEBCHAT_TOKEN =
  process.env.environment === 'production'
    ? 'q456WjOHQQw.AeexyWZ2_vbwJQVHzSo254azU-nIYjGRgI6IHhAbIKM'
    : 'q456WjOHQQw.AeexyWZ2_vbwJQVHzSo254azU-nIYjGRgI6IHhAbIKM';

export const VDPC_CONFERENCE_URL =
  process.env.environment === 'production'
    ? 'https://medical.akosmd.com/#!/room/medical?uuid='
    : 'https://staging-medical.akosmd.com/#!/room/medical?uuid=';
// : 'https://staging-medical.akosmd.com/#!/room/VPDC?uuid=';

export const REGULAR_CONFERENCE_URL =
  process.env.environment === 'production'
    ? 'https://medical.akosmd.com/#!/room/medical?uuid='
    : 'https://staging-medical.akosmd.com/#!/room/medical?uuid=';

export const VDPC_GOOGLE_CHAT_URL =
  process.env.environment === 'production'
    ? 'https://chat.googleapis.com/v1/spaces/AAAAdwm8Cxo/messages?key=AIzaSyDdI0hCZtE6vySjMm-WEfRq3CPzqKqqsHI&token=UZXUe7-CdCGtNcSzKgZHav2Z2-TL8sqU-P0cfKAPltk%3D'
    : 'https://chat.googleapis.com/v1/spaces/AAAAwgtXzH0/messages?key=AIzaSyDdI0hCZtE6vySjMm-WEfRq3CPzqKqqsHI&token=BdiO02-s5YjeWb39PeSeJ3CrULrnxHdEDhDkkhkvZ-k%3D';

export const INDIVIDUAL_GOOGLE_CHAT_URL =
  process.env.environment === 'production'
    ? 'https://chat.googleapis.com/v1/spaces/AAAAZ_wQnFk/messages?key=AIzaSyDdI0hCZtE6vySjMm-WEfRq3CPzqKqqsHI&token=-JNBKTYwsdfOqbPzc9aBKYSzkTo6ODdp5s1w_CGhCk0%3D'
    : 'https://chat.googleapis.com/v1/spaces/AAAAZ_wQnFk/messages?key=AIzaSyDdI0hCZtE6vySjMm-WEfRq3CPzqKqqsHI&token=-JNBKTYwsdfOqbPzc9aBKYSzkTo6ODdp5s1w_CGhCk0%3D';

export const restapiBasicAuth = {
  username: '12345',
  password: '12345',
};
export const newApiBasicAuth = {
  username: '12345',
  password: '12345',
};

export const WS_HOST =
  process.env.environment === 'production'
    ? 'integration.akosmd.com'
    : 'staging-integration.akosmd.com';

export const PUSHER_KEY =
  process.env.environment === 'production'
    ? 'Ahbeiv1yOhmeiy9wEiTh2ake'
    : 'Ahbeiv1yOhmeiy9wEiTh2ake';

export const GOODBYE_MESSAGE =
  "Goodbye! If you have any further questions, please don't hesitate to contact us again.";

export const OPEN_TOK_API =
  process.env.environment === 'production' ? '45732912' : '46437422';

export const MEMBER_TYPES = {
  individual: 70001,
  VDPC: 70002,
  VDPC_Lite: 70003,
};

export const VDPC_LITE_PLAN_TYPES = {
  employee: 'EMP',
  employeeWithSpouse: 'ESP',
  employeeWithChildren: 'ECH',
  family: 'FAM',
};
