import React from 'react';
import RootProvider from './core/Provider';

const App = () => <RootProvider />;

export default App;
