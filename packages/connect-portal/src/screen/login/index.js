import loadable from 'utils/loadable';

export default loadable(() => import('common-components/molecule/doctorLogin'));
