import { combineReducers } from 'redux';
import { persistReducer } from 'redux-persist';

import storage from 'redux-persist/lib/storage';
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2';
import memberSearch from 'common-core/memberSearch/Reducer';
import doctorSignIn from 'common-core/doctorLogin/Reducer';
import patient from 'common-core/Reducer';

const reducers = combineReducers({
  patient2: combineReducers({
    [memberSearch.key]: memberSearch,
    [doctorSignIn.key]: doctorSignIn,
  }),
  ['patient']: patient,
});

const persistConfig = {
  key: 'root',
  storage: storage,
  whitelist: [],
  stateReconciler: autoMergeLevel2,
};

export default persistReducer(persistConfig, reducers);
