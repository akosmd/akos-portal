import { createStore } from 'redux';
import { persistStore } from 'redux-persist';
import { createBrowserHistory as createHistory } from 'history';

import reducer from './Reducer';
import middleware, { sagaMiddleware } from './Middleware';
import saga from './Saga';

const store = createStore(reducer, middleware);
export const history = createHistory();
export const persistor = persistStore(store);

sagaMiddleware.run(saga);

export default store;
