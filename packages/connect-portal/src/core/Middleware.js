import { applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { composeWithDevTools } from 'redux-devtools-extension';
import { createLogger } from 'redux-logger';
import { routerMiddleware } from 'react-router-redux';
import { history } from './Store';

export const sagaMiddleware = createSagaMiddleware();

let middleware = '';
if (process.env.NODE_ENV === 'production') {
  middleware = applyMiddleware(sagaMiddleware, routerMiddleware(history));
} else {
  middleware = composeWithDevTools(
    applyMiddleware(sagaMiddleware, createLogger(), routerMiddleware(history)),
  );
}
export default middleware;
