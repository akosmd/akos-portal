import { all, spawn } from 'redux-saga/effects';
import memberSearchSaga from 'common-core/memberSearch/Saga';
import doctorSignInSaga from 'common-core/doctorLogin/Saga';
//import saga from core package

export default function* saga() {
  //spawn imported saga here i.e yield spawn(importedSaga);
  // yield spawn(memberSearchSaga);
  yield spawn(doctorSignInSaga);
}
