import React from 'react';
import { Provider as StoreProvider } from 'react-redux';
import theme from './theme';
import { MuiThemeProvider } from '@material-ui/core/styles';
import { history } from './Store';

import store, { persistor } from './Store';
import { PersistGate } from 'redux-persist/integration/react';
import RootRoutes from '../component/routes';

const RootProvider = ({ children }) => (
  <StoreProvider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <MuiThemeProvider theme={theme}>
        <RootRoutes history={history} />
      </MuiThemeProvider>
    </PersistGate>
  </StoreProvider>
);

export default RootProvider;
