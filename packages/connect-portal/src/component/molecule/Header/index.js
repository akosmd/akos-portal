/**
 *
 * Header
 *
 */

import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';

import { connect } from 'react-redux';
import { compose } from 'redux';
import { push } from 'connected-react-router';
import { createStructuredSelector } from 'reselect';

import classNames from 'classnames';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Tooltip from '@material-ui/core/Tooltip';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';

import AccountIcon from '@material-ui/icons/AccountCircle';
import IconButton from '@material-ui/core/IconButton';
import DashboardIcon from '@material-ui/icons/Dashboard';
import ChatIcon from '@material-ui/icons/Chat';
import SearchIcon from '@material-ui/icons/Search';

import PropTypes from 'prop-types';

import {
  logoutUser,
  showDependentsModal,
  postEndConversation,
  getAppVersion,
} from 'common-core/Actions';
import { makeSelectGoogleAuth } from 'common-core/Selectors';

const useStyles = makeStyles(theme => ({
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    background: 'linear-gradient(to right, #55bcbe 0%,#85c3a6 100%) !important',

    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    // marginLeft: drawerWidth,

    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
    [theme.breakpoints.down('md')]: {
      marginRight: 0,
    },
  },
  hide: {
    display: 'none',
  },

  title: {
    display: 'inline !important',
    marginLeft: '1rem',
    [theme.breakpoints.up('sm')]: {
      display: 'block',
    },
  },
}));
function Header({
  toggle,
  doLogoutUser,
  doGetAppVersion,
  dispatch,
  account,
  appVersion,
  logo,
}) {
  const [values, setValues] = useState({
    anchorEl: null,
    password: null,
  });

  const classes = useStyles();

  const [version, setVersion] = useState(localStorage.getItem('memberVersion'));

  useEffect(() => {
    dispatch(doGetAppVersion());
  }, []);

  useEffect(() => {
    if (appVersion && appVersion.data && !appVersion.error) {
      const active = appVersion.data.find(app => app.is_active);
      if (active && version !== active.versions) {
        localStorage.setItem('memberVersion', active.versions);
        setVersion(active.versions);
        window.location.reload();
      }
    }
  }, [appVersion]);

  const isMenuOpen = Boolean(values.anchorEl);

  const handleProfileMenuOpen = event => {
    setValues({ ...values, anchorEl: event.target });
  };

  const handleMenuClose = () => {
    setValues({ ...values, anchorEl: null });
  };

  const handleLogout = () => {
    dispatch(doLogoutUser());
  };

  const renderProfileMenu = (
    <Menu
      anchorEl={values.anchorEl}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={isMenuOpen}
      onClose={handleMenuClose}
    >
      <MenuItem onClick={handleLogout}>Log out</MenuItem>
    </Menu>
  );

  return (
    <AppBar
      position="fixed"
      className={classNames(classes.appBar, {
        [classes.appBarShift]: toggle,
      })}
    >
      <Toolbar
        disableGutters={!toggle}
        className={classNames(classes.menuButton)}
        style={{ alignItems: 'center', justifyContent: 'space-between' }}
      >
        <div style={{ display: 'flex' }}>
          <figure
            style={{
              display: 'flex',
              width: 210,
              height: 35,
              margin: '0 10px',
            }}
          >
            {logo && <img src={logo} alt="Akos" style={{ width: '100%' }} />}
          </figure>
        </div>
        {account.data && (
          <div>
            <Tooltip title="Dashboard">
              <IconButton onClick={() => dispatch(push('/'))}>
                <DashboardIcon color="secondary" />
              </IconButton>
            </Tooltip>
            <Tooltip title="Chatroom">
              <IconButton onClick={() => dispatch(push('/chatroom'))}>
                <ChatIcon color="secondary" />
              </IconButton>
            </Tooltip>
            <Tooltip title="Search Member">
              <IconButton onClick={() => dispatch(push('/search'))}>
                <SearchIcon color="secondary" />
              </IconButton>
            </Tooltip>
            <IconButton
              className={classes.icon}
              aria-owns={isMenuOpen ? 'material-appbar' : undefined}
              aria-haspopup="true"
              onClick={handleProfileMenuOpen}
              color="inherit"
            >
              <AccountIcon />
            </IconButton>
            {renderProfileMenu}
          </div>
        )}
      </Toolbar>
    </AppBar>
  );
}

const { bool, object, func } = PropTypes;
Header.propTypes = {
  toggle: bool,
  dispatch: func.isRequired,
  doLogoutUser: func.isRequired,
  account: object.isRequired,
  doGetAppVersion: func.isRequired,

  appVersion: object,
};

Header.defaultProps = {
  toggle: false,
};

const mapStateToProps = createStructuredSelector({
  account: makeSelectGoogleAuth(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    doLogoutUser: logoutUser,
    doShowDependentsModal: showDependentsModal,
    doPostEndConversation: postEndConversation,
    doGetAppVersion: getAppVersion,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(Header);
