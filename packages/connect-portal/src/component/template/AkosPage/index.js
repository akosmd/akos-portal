/**
 *
 * AkosPage
 *
 */

import React, { useState } from 'react';
import PropTypes from 'prop-types';

import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { compose } from 'redux';
import {
  makeSelectVerifyAuth,
  makeSelectNurseSignin,
} from 'common-core/Selectors';

import classNames from 'classnames';

import { useSnackbar } from 'notistack';

import { useTheme } from '@material-ui/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';

import CssBaseline from '@material-ui/core/CssBaseline';
import { makeStyles } from '@material-ui/core/styles';
import Fab from '@material-ui/core/Fab';

import Header from 'common-components/molecule/Header';
import Footer from 'common-components/molecule/Footer';
import ChatIcon from 'asset/icons/chat';

import MyLink from 'common-components/molecule/MyLink';
import { Link, Typography } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    // padding: '0 8px',
    ...theme.mixins.toolbar,
    justifyContent: 'flex-end',
    [theme.breakpoints.down('md')]: {
      minHeight: `60px`,
    },
    [theme.breakpoints.up('sm')]: {
      minHeight: 50,
    },
  },
  drawerHeaderSmall: {
    display: 'flex',
    alignItems: 'center',
    padding: '0 8px',
    ...theme.mixins.toolbar,
    justifyContent: 'flex-end',
    [theme.breakpoints.down('md')]: {
      minHeight: `59px`,
    },
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    [theme.breakpoints.down('md')]: {
      padding: `${theme.spacing(3)}px 1rem`,
      paddingBottom: theme.spacing(8),
    },
    paddingBottom: theme.spacing(8),
  },
  container: {
    background: theme.palette.background.default,
    borderRadius: 15,
    display: 'flex',
    flex: 1,
    marginTop: 40,
    '& > div': {
      width: '100%',
    },
  },
  contentWide: {
    flexGrow: 1,
    padding: '1px',
    paddingBottom: theme.spacing(8),
  },
  contentShift: {
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginLeft: 0,
  },
  fab: {
    position: 'fixed',
    bottom: theme.spacing(8),
    right: theme.spacing(2),
    backgroundColor: theme.palette.chat.button,
  },
  fabText: {
    color: theme.typography.color.light,
  },
}));

function useWidth() {
  const theme = useTheme();
  const keys = [...theme.breakpoints.keys].reverse();
  return (
    keys.reduce((output, key) => {
      // eslint-disable-next-line react-hooks/rules-of-hooks
      const matches = useMediaQuery(theme.breakpoints.up(key));
      return !output && matches ? key : output;
    }, null) || 'xs'
  );
}

export function AkosPage(props) {
  const [toggle, setToggle] = useState(false);
  const classes = useStyles();
  // const { enqueueSnackbar } = useSnackbar();
  const { account, fullScreen, nurseSignin, ...rest } = props;
  const width = useWidth();

  const isMobile = useMediaQuery(theme => theme.breakpoints.down('sm'));
  const onToggleMenu = () => {
    setToggle(!toggle);
  };

  const { component: Component, history } = props;

  const NO_CHAT_URLS = [
    '/info',
    '/connect',
    '/360',
    '/video-conference',
    '/chatroom',
    '/chat',
  ];

  const NO_FOOTER_URLS = '/video-conference';

  let isAuthenticated =
    account.data !== false &&
    !NO_CHAT_URLS.includes(history.location.pathname.toLowerCase());

  if (!isAuthenticated) {
    isAuthenticated =
      nurseSignin.data !== false &&
      (history.location.pathname.toLowerCase() !== '/info' ||
        history.location.pathname.includes('/chatroom/nurse'));

    if (
      isAuthenticated &&
      history.location.pathname.includes('/chatroom/nurse')
    ) {
      isAuthenticated = false;
    }
  }

  const renderFooter = () => {
    if (
      history.location.pathname.toLowerCase() !== '/' &&
      NO_FOOTER_URLS.includes(history.location.pathname.toLowerCase())
    )
      return null;

    return <Footer width={width} />;
  };

  return (
    <div className={classes.root}>
      <CssBaseline />
      <Header
        {...rest}
        toggle={toggle}
        onToggle={onToggleMenu}
        isAuthenticated={isAuthenticated}
      />

      <main
        className={classNames(
          fullScreen ? classes.contentWide : classes.content,
        )}
      >
        <div className={classes.container}>
          {/* <div
            className={
              fullScreen ? classes.drawerHeaderSmall : classes.drawerHeader
            }
          /> */}
          {Component && (
            <Component
              {...rest}
              // enqueueSnackbar={enqueueSnackbar}
              width={width}
            />
          )}
          {isAuthenticated && (
            <Link component={MyLink} to="/chat">
              <Fab variant="extended" aria-label="Chat" className={classes.fab}>
                <ChatIcon noMargin={isMobile} />
                <Typography className={classes.fabText}>
                  {!isMobile ? 'Chat with our care navigator' : null}
                </Typography>
              </Fab>
            </Link>
          )}
        </div>
      </main>

      {renderFooter()}
    </div>
  );
}

const { func, node, oneOfType, object, bool } = PropTypes;
AkosPage.propTypes = {
  component: oneOfType([func, object, node]),
  account: object.isRequired,
  nurseSignin: object.isRequired,
  history: object.isRequired,
  fullScreen: bool,
};

const mapStateToProps = createStructuredSelector({
  account: makeSelectVerifyAuth(),
  nurseSignin: makeSelectNurseSignin(),
});

const withConnect = connect(mapStateToProps);

export default compose(withConnect)(AkosPage);
