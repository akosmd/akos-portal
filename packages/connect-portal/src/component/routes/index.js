import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Login from 'screen/login';
import Home from 'screen/home';
import AkosPage from 'template/AkosPage';

export default ({ history }) => (
  <Router>
    <Switch>
      <Route
        exact
        path="/login"
        component={() => <AkosPage component={Login} history={history} />}
      />
      <Route exact path="/" component={Home} />
    </Switch>
  </Router>
);
