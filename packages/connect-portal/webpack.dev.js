const merge = require('webpack-merge');
const Dotenv = require('dotenv-webpack');

const config = require('./webpack.config.js');

module.exports = merge(config, {
  plugins: [
    new Dotenv({
      path: '.env.development',
    }),
  ],
  mode: 'development',
  devtool: 'inline-source-map',
  devServer: {
    historyApiFallback: true,
    stats: {
      children: false,
      maxModules: 0,
    },
    host: 'localhost',
    port: 3008,
    hot: true,
    inline: true,
    watchOptions: {
      poll: true,
    },
    stats: {
      colors: true,
      reasons: true,
      chunks: false,
    },
    overlay: {
      warnings: true,
      errors: true,
    },
  },
});
