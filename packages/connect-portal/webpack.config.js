const path = require('path');

const HtmlWebPackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: ['@babel/polyfill', './src/index.js'],
  devtool: '#eval-source-map',
  output: {
    filename: 'app.js',
    path: __dirname + '/build',
  },
  resolve: {
    alias: {
      '@component': path.resolve(__dirname, 'src/component/'),
      '@atom': path.resolve(__dirname, 'src/component/atom/'),
      '@screen': path.resolve(__dirname, 'src/screen/'),
      '@molecule': path.resolve(__dirname, 'src/component/molecule/'),
      '@core': path.resolve(__dirname, 'src/core/'),
    },
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
        },
      },
      {
        test: /\.css$/,
        use: [
          {
            loader: 'style-loader',
          },
          {
            loader: 'css-loader',
            options: {
              modules: true,
              importLoaders: 1,
              localIdentName: '[name]_[local]_[hash:base64]',
              sourceMap: true,
              minimize: true,
            },
          },
        ],
      },
      {
        test: /\.svg$/,
        use: [
          {
            loader: 'babel-loader',
          },
          {
            loader: 'react-svg-loader',
            options: {
              jsx: true,
            },
          },
        ],
      },
    ],
  },
  devServer: {
    historyApiFallback: true,
    stats: {
      children: false,
      maxModules: 0,
    },
    host: 'localhost',
    port: 3008,
    hot: true,
    inline: true,
    watchOptions: {
      poll: true,
    },
  },
  plugins: [
    new HtmlWebPackPlugin({
      title: 'Connect Portal',
      template: './src/index.html',
    }),
  ],
};
