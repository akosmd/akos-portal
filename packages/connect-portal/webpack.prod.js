const merge = require('webpack-merge');
const Dotenv = require('dotenv-webpack');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const config = require('./webpack.config.js');

module.exports = merge(config, {
  mode: 'production',

  plugins: [
    new CleanWebpackPlugin(),
    new Dotenv({
      path: '.env.production',
    }),
  ],
});
