import React from 'react';
import { string, bool, any } from 'prop-types';
import { styled } from '@material-ui/core/styles';
import BaseButton from '@material-ui/core/Button';
import { CircularProgress } from '@material-ui/core';

const StyledButton = styled(BaseButton)(({ theme, color, background }) => ({
  color: color || theme.typography.color.light,
  background: background ? background.normal : theme.palette.button.main,
  '&:hover': {
    background: background ? background.hover : theme.palette.button.hover,
  },
}));

const Loader = () => <CircularProgress size="2rem" color="secondary" />;
const Button = ({ loading, children, ...rest }) => (
  <StyledButton {...rest}>{loading ? <Loader /> : children}</StyledButton>
);

Button.propTypes = {
  variant: string,
  loading: bool,
  children: any,
};

export default Button;
