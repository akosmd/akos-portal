import React from 'react';
import PropTypes from 'prop-types';
import { styled } from '@material-ui/styles';

const StyledDiv = styled('div')(({ theme, background, color }) => ({
  zIndex: theme.zIndex.drawer + 1,
  position: 'fixed',
  padding: '1rem',
  display: 'flex',
  left: 0,
  bottom: 0,
  width: '100%',
  backgroundColor: background || theme.palette.background.footer,
  color: color || theme.typography.color.light,
  textAlign: 'center',
  alignItems: 'center',
  justifyContent: 'space-between',
}));

function Footer({ width, reserved, email, phone, background, color }) {
  return (
    <StyledDiv color={color} background={background}>
      {reserved && (
        <div>
          Copyright &copy; {new Date(Date.now()).getFullYear()} {reserved}
        </div>
      )}
      {width !== 'xs' && email && <div>Email: {email}</div>}
      {width !== 'xs' && phone && <div>Phone: {phone}</div>}
    </StyledDiv>
  );
}

Footer.propTypes = {
  width: PropTypes.string,
  reserved: PropTypes.string,
  email: PropTypes.string,
  phone: PropTypes.string,
};

export default Footer;
