/**
 *
 * Asynchronously loads the component for Inxite
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
