import { styled } from "@material-ui/styles";

export default styled("div")(({ direction, wrap, flex, justify, padding }) => ({
	display: "flex",
	flexDirection: direction || "row",
	flexWrap: wrap || "wrap",
	flex: flex || 1,
	padding: padding || "20px 0",
	justifyContent: justify || "left"
}));
