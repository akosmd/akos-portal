import React from 'react';
import { any } from 'prop-types';
import ReactQuill from 'react-quill';
import { Typography } from '@material-ui/core';

import 'react-quill/dist/quill.snow.css';
import './custom.css';

const formats = [
  'header',
  'font',
  'size',
  'bold',
  'italic',
  'underline',
  'strike',
  'blockquote',
  'list',
  'bullet',
  'indent',
  'link',
];

const modules = {
  toolbar: [
    [{ header: '1' }, { header: '2' }],
    [{ size: [] }],
    ['bold', 'italic', 'underline', 'strike', 'blockquote'],
    [
      { list: 'ordered' },
      { list: 'bullet' },
      { indent: '-1' },
      { indent: '+1' },
    ],
    ['clean'],
  ],
};

const TextArea = ({ error, ...rest }) => (
  <>
    <ReactQuill formats={formats} modules={modules} theme="snow" {...rest} />
    {error && <Typography color="error">{error}</Typography>}
  </>
);

TextArea.propTypes = {
  error: any,
};

export default TextArea;
