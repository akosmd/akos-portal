/**
 *
 * Select
 *
 */

import React from 'react';

import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';

import PropTypes from 'prop-types';

function Selection({ onChange, field, className, options, disabled }) {
  const inputLabel = React.useRef(null);
  const [labelWidth, setLabelWidth] = React.useState(0);
  React.useEffect(() => {
    setLabelWidth(inputLabel.current.offsetWidth);
  }, []);

  const getOptions = opts =>
    opts.map(({ value, name }) => (
      <option value={value} key={value}>
        {name}
      </option>
    ));
  if (!field) return null;

  const { error, pristine, variant, readOnly } = field;

  return (
    <FormControl
      margin="normal"
      required={field.required}
      fullWidth
      variant={variant}
      className={className}
      disabled={disabled || readOnly}
      error={error && !pristine}
    >
      <InputLabel
        htmlFor={field.id}
        ref={inputLabel}
        error={error && !pristine}
      >
        {field.error && !pristine
          ? field.errorMessage || field.caption
          : field.caption}
      </InputLabel>
      <Select
        native
        value={field.value !== null ? field.value : ''}
        id={field.id}
        name={field.name}
        labelWidth={labelWidth}
        onChange={onChange}
      >
        {getOptions(options)}
      </Select>
    </FormControl>
  );
}

const {
  string,
  bool,
  shape,
  func,
  arrayOf,
  any,
  oneOfType,
  number,
} = PropTypes;
Selection.propTypes = {
  field: shape({
    id: string.isRequired,
    value: oneOfType([string, number]).isRequired,
    caption: string.isRequired,
    errorMessage: string,
    placeholder: string,
    error: bool,
    required: bool,
    fullWidth: bool,
    autoFocus: bool,
  }),
  onChange: func,
  className: string,

  options: arrayOf(
    shape({
      value: any.isRequired,
      name: string.isRequired,
    }),
  ),
  disabled: bool,
};

Selection.defaultProps = {
  onChange: undefined,
  className: undefined,
  disabled: false,
};

export default Selection;
