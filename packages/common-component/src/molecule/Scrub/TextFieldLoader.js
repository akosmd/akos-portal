import React from 'react';
import { withStyles } from '@material-ui/core/styles';

const styles = {
  '@keyframes loaderEffect': {
    '0%': {
      backgroundPosition: '-468px 0',
    },
    '100%': {
      backgroundPosition: '468px 0',
    },
  },
  animatedBackground: {
    animationDuration: '4s',
    animationFillMode: 'forwards',
    animationIterationCount: 'infinite',
    animationTimingFunction: 'linear',
    height: 80,
    animationName: '$loaderEffect',
    background: `linear-gradient(to right, #fff 8%, #f2f2f2 18%, #fff 33%)`,
    width: '100%',
  },
  border: {
    display: 'flex',
    flexDirection: 'row',
    background: '#fff',
    height: 12,
  },
  horizontalBorder: {
    display: 'flex',
    flexDirection: 'row',
    background: '#fff',
    height: 12,
  },
  verticalBorder: {
    flexDirection: 'row',
    background: '#fff',
    width: 12,
    height: 80,
  },
  item: {
    height: 56,
    background: 'transparent',
    width: 339,
  },
  row: {
    display: 'flex',
    flexDirection: 'row',
  },
  column: {
    display: 'flex',
    flexDirection: 'column',
  },
};

export default withStyles(styles)(({ classes }) => (
  <div className={classes.animatedBackground}>
    <div className={classes.row}>
      <div className={classes.verticalBorder} />
      <div className={classes.column}>
        <div className={classes.horizontalBorder} />
        <div className={classes.item} />
        <div className={classes.horizontalBorder} />
      </div>
      <div className={classes.verticalBorder} />
    </div>
  </div>
));
