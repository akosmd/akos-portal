/**
 *
 * DatePickerField
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import KeyboardEventHandler from 'react-keyboard-event-handler';

import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from '@material-ui/pickers';
import { FormControl } from '@material-ui/core';

function DatePickerField(props) {
  const { field, fullWidth, onChange, keyEvents, ...rest } = props;
  const min = 0;
  const max = 500;
  const random = Math.random() * (+max - +min) + +min;

  if (!field) return null;

  const { readOnly, error, value, caption, errorMessage, pristine } = field;
  let message = caption;
  if (!errorMessage && error && !pristine) {
    message = `Invalid ${caption}`;
  }
  if (errorMessage && error && !pristine) {
    message = errorMessage || `Invalid ${caption}`;
  }
  return !keyEvents ? (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <FormControl
        margin="normal"
        required={field.required}
        fullWidth={fullWidth}
      >
        <KeyboardDatePicker
          {...rest}
          id={`${random}-${field.id}`}
          required={field.required}
          error={error && !pristine}
          disabled={readOnly}
          label={message}
          format="MM/dd/yyyy"
          value={value !== '' ? value : null}
          onChange={onChange}
          KeyboardButtonProps={{
            'aria-label': 'change date',
          }}
          inputVariant="outlined"
        />
      </FormControl>
    </MuiPickersUtilsProvider>
  ) : (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <FormControl
        margin="normal"
        required={field.required}
        fullWidth={fullWidth}
      >
        <KeyboardEventHandler
          handleKeys={keyEvents.handleKeys}
          onKeyEvent={keyEvents.onKeyEvent}
        >
          <KeyboardDatePicker
            {...rest}
            id={`${random}-${field.id}`}
            required={field.required}
            error={error && !pristine}
            disabled={readOnly}
            label={message}
            format="MM/dd/yyyy"
            value={value !== '' ? value : null}
            onChange={onChange}
            KeyboardButtonProps={{
              'aria-label': 'change date',
            }}
            inputVariant="outlined"
          />
        </KeyboardEventHandler>
      </FormControl>
    </MuiPickersUtilsProvider>
  );
}

const { string, bool, shape, func, object } = PropTypes;
DatePickerField.propTypes = {
  field: shape({
    id: string.isRequired,
    value: string,
    caption: string.isRequired,
    errorMessage: string,
    placeholder: string,
    error: bool,
    required: bool,
  }),
  fullWidth: bool,
  onChange: func,
  keyEvents: object,
};

export default DatePickerField;
