/**
 *
 * Asynchronously loads the component for AuthenticatedRoute
 *
 */

import loadable from '@core/Utils/loadable';

export default loadable(() => import('./index'));
