/**
 *
 * Asynchronously loads the component for Card
 *
 */

import loadable from '@core/Utils/loadable';

export default loadable(() => import('./index'));
