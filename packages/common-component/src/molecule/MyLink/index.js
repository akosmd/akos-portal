/**
 *
 * MyLink
 *
 */

import React, { memo } from 'react';
import { Link as RouterLink } from 'react-router-dom';

const MyLink = React.forwardRef((props, ref) => (
  <RouterLink innerRef={ref} {...props} />
));

MyLink.propTypes = {};

export default memo(MyLink);
