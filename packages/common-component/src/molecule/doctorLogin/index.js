/**
 *
 * Login
 *
 */

import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { compose } from 'redux';

import { Grid, Typography, Card, CardContent } from '@material-ui/core';
import TextField from 'molecule/TextField';
import GradientButton from 'molecule/GradientButton';

import { makeStyles } from '@material-ui/styles';

import {
  resetAuthFields,
  setNotificationType,
  resetSignin,
} from 'common-core/Actions';
import { makeSelectSignin } from 'common-core/doctorLogin/Selector';
import { doctorSignIn } from 'common-core/doctorLogin/Action';

import {
  initialField,
  initialState,
  handleChange,
  highlightFormErrors,
  extractFormValues,
} from 'common-core/utils/formHelper';

const useStyles = makeStyles(theme => ({
  root: {
    padding: 20,
    borderRadius: 10,
    boxShadow: theme.palette.boxShadow.main,
  },
  wrapper: {
    height: 'calc(100vh - 115px)',
    display: 'flex',
    flex: 1,
    justifyContent: 'center',
  },
  headerContainer: {
    paddingBottom: 10,
  },
  footerContainer: {
    padding: '10px 0',
    borderTop: `1px solid ${theme.palette.background.default}`,
  },
  container: {
    padding: '10px 0',
  },
  text: {
    padding: 10,
    textAlign: 'center',
  },
  buttonLabel: {
    fontWeight: 'bold',
    color: theme.typography.color.dark,
  },
}));

const email = {
  ...initialField,
  id: 'email',
  caption: 'User ID',
  placeholder: 'Email',
  errorMessage: 'Email is required',
};
const password = {
  ...initialField,
  id: 'password',
  caption: 'Password',
  placeholder: 'Enter Password',
  errorMessage: 'Password is required',
};
export function Login({ dispatch, doDoctorSignIn, signin }) {
  const classes = useStyles();

  const [login, setLogin] = useState({
    email,
    password,
    ...initialState,
  });

  useEffect(() => {
    console.warn('logged', signin);
  }, [signin]);
  const onInputChange = field => event => {
    handleChange({
      field: field.id,
      state: login,
      event,
      saveStepFunc: setLogin,
    });
  };

  const handleSubmit = () => {
    if (!login.completed) {
      //   enqueueSnackbar('Please input your Email and Password', {
      //     variant: 'error',
      //   });
      highlightFormErrors(login, setLogin);
    }
    if (login.completed) {
      const params = extractFormValues(login);
      dispatch(
        doDoctorSignIn({
          ...params,
          employerId: '',
        }),
      );
    }
  };

  return (
    <Grid
      container
      spacing={2}
      direction="column"
      className={classes.wrapper}
      alignContent="center"
    >
      <Grid item xs={12} md={5}>
        <Card className={classes.root}>
          <CardContent>
            <Grid item xs={12} className={classes.headerContainer}>
              <Typography variant="h5">
                SIGN IN
                <Typography variant="body1" color="textPrimary">
                  Sign in with your registered phone number or email address
                </Typography>
              </Typography>
            </Grid>

            <Grid item xs={12} sm={12} className={classes.container}>
              <TextField
                keyEvents={{
                  handleKeys: ['enter'],
                  onKeyEvent: handleSubmit,
                }}
                field={login.email}
                variant="outlined"
                caption="User ID"
                onChange={onInputChange(login.email)}
                required
              />
              <TextField
                keyEvents={{
                  handleKeys: ['enter'],
                  onKeyEvent: handleSubmit,
                }}
                caption="Password"
                field={login.password}
                type="password"
                variant="outlined"
                onChange={onInputChange(login.password)}
              />
            </Grid>
            <Grid item xs={12} className={classes.container}>
              <GradientButton
                variant="contained"
                fullWidth
                onClick={handleSubmit}
                color="primary"
                size="large"
                loading={signin.loading}
              >
                {'Sign in'}
              </GradientButton>
            </Grid>
          </CardContent>
        </Card>
      </Grid>
    </Grid>
  );
}

const { func, object } = PropTypes;

Login.propTypes = {
  dispatch: func.isRequired,
  doDoctorSignIn: func.isRequired,
  doResetAuthFields: func.isRequired,
  signin: object,
  enqueueSnackbar: func.isRequired,
  doSetNotificationType: func.isRequired,
  doResetSignin: func.isRequired,
};
const mapStateToProps = createStructuredSelector({
  signin: makeSelectSignin(),
});
function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    doDoctorSignIn: doctorSignIn,
    doResetAuthFields: resetAuthFields,
    doSetNotificationType: setNotificationType,
    doResetSignin: resetSignin,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(Login);
