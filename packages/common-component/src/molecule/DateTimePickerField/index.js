/**
 *
 * DateTimePickerField
 *
 */

import React from 'react';
import PropTypes from 'prop-types';

import DateFnsUtils from '@date-io/date-fns';
import { MuiPickersUtilsProvider, DateTimePicker } from '@material-ui/pickers';

import { FormControl } from '@material-ui/core';

function DateTimePickerField(props) {
  const { field, fullWidth, onChange, ...rest } = props;
  const min = 0;
  const max = 500;
  const random = Math.random() * (+max - +min) + +min;

  if (!field) return null;

  const { readOnly, error, value, caption, errorMessage, pristine } = field;
  let message = caption;
  if (!errorMessage && error && !pristine) {
    message = `Invalid ${caption}`;
  }

  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <FormControl
        margin="normal"
        required={field.required}
        fullWidth={fullWidth}
      >
        <DateTimePicker
          {...rest}
          id={`${random}-${field.id}`}
          required={field.required}
          error={error && !pristine}
          invalidLabel={error && !pristine}
          readOnly={readOnly}
          label={message}
          // format="MM/dd/yyyy"
          value={value !== '' ? value : null}
          onChange={onChange}
          inputVariant="outlined"
          variant="inline"
        />
      </FormControl>
    </MuiPickersUtilsProvider>
  );
}

const { string, bool, shape, func } = PropTypes;
DateTimePickerField.propTypes = {
  field: shape({
    id: string.isRequired,
    value: string,
    caption: string.isRequired,
    errorMessage: string,
    placeholder: string,
    error: bool,
    required: bool,
  }),
  fullWidth: bool,
  onChange: func,
};

export default DateTimePickerField;
