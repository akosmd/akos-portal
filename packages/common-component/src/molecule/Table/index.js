import React, { useState, useEffect } from 'react';

import PropTypes from 'prop-types';
import lo from 'lodash';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import CircularProgress from '@material-ui/core/CircularProgress';

import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';

import Paper from '@material-ui/core/Paper';

import { Typography } from '@material-ui/core';

import EnhancedTableToolbar from './tableToolBar';
import EnhancedTableHead from './tableHead';

const styles = makeStyles({
  root: {
    width: '100%',
  },
  table: {
    minWidth: '100%',
  },
  tableWrapper: {
    overflowX: 'auto',
  },
});

export function AkosTable(props) {
  const [table, setTable] = useState({
    order: 'desc',
    orderBy: 'submittedAt',
    selected: [],
    page: 0,
    rowsPerPage: 10,
    showDialog: false,
  });

  useEffect(() => {
    setTable({ ...table, page: 0 });
  }, [props.resetPage]);

  const classes = styles();
  const handleRequestSort = (event, property) => {
    const orderBy = property;
    let order = 'desc';

    if (table.orderBy === property && table.order === 'desc') {
      order = 'asc';
    }
    setTable({ ...table, order, orderBy });
  };

  const handleSelectAllClick = () => {
    // TODO: make this work
    // if (event.target.checked) {
    //   this.setState(state => ({ selected: state.data.map(n => n.id) }));
    //   return;
    // }
    // this.setState({ selected: [] });
  };

  // const handleClick = id => {
  //   const { data } = props;
  //   const selected = data.find(r => r.token === id);

  //   setTable({ ...table, selected, showDialog: true });
  // };

  // const handleToggleDialog = () => {
  //   setTable({ ...table, showDialog: !table.showDialog });
  // };

  const handleChangePage = (event, newPage) => {
    setTable({ ...table, page: newPage });
  };

  const handleChangeRowsPerPage = event => {
    setTable({ ...table, rowsPerPage: event.target.value });
  };

  // const isSelected = currentId => {
  //   const {
  //     selected: { id },
  //   } = table;
  //   return id === currentId;
  // };

  const renderLoading = () => {
    const { loading, header } = props;
    if (!loading) return null;

    return (
      <TableBody variant="variant">
        <TableRow>
          <TableCell align="center" colSpan={header.length}>
            <CircularProgress color="secondary" />
          </TableCell>
        </TableRow>
      </TableBody>
    );
  };

  const renderEmptyRow = () => {
    const { loading, data, header } = props;
    if (loading || (data && data.length > 0)) return null;

    return (
      <TableBody>
        <TableRow>
          <TableCell align="center" colSpan={header.length}>
            <Typography>Nothing to display</Typography>
          </TableCell>
        </TableRow>
      </TableBody>
    );
  };

  const {
    data = [],
    title,
    header,
    body: BodyComponent,
    loading,
    options,
    onOptionsClick,
    subTitle,
    avatar,
    elevation,
  } = props;
  const { order, orderBy, selected, rowsPerPage, page } = table;

  const filteredData = lo
    .orderBy(data, [orderBy], [order])
    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage);

  return (
    <Grid container>
      <Grid item xs={12}>
        <Paper className={classes.root} elevation={elevation}>
          <EnhancedTableToolbar
            numSelected={selected.length}
            title={title}
            options={options}
            subTitle={subTitle}
            avatar={avatar}
            onOptionsClick={onOptionsClick}
          />
          <div className={classes.tableWrapper}>
            <Table className={classes.table} aria-labelledby="tableTitle">
              <EnhancedTableHead
                numSelected={selected.length}
                order={order}
                orderBy={orderBy}
                header={header}
                title={title}
                onSelectAllClick={handleSelectAllClick}
                onRequestSort={handleRequestSort}
                rowCount={filteredData.length}
              />
              {renderLoading()}
              {!loading && (
                <BodyComponent
                  isSelected={false}
                  data={filteredData}
                  row={undefined}
                />
              )}
              {renderEmptyRow()}
            </Table>
          </div>
          {data.length > 5 && (
            <TablePagination
              rowsPerPageOptions={[5, 10, 25]}
              component="div"
              count={data.length || 0}
              rowsPerPage={rowsPerPage}
              page={page}
              backIconButtonProps={{
                'aria-label': 'Previous Page',
              }}
              nextIconButtonProps={{
                'aria-label': 'Next Page',
              }}
              onChangePage={handleChangePage}
              onChangeRowsPerPage={handleChangeRowsPerPage}
            />
          )}
        </Paper>
      </Grid>
    </Grid>
  );
}

const { object, string, array, bool, func, oneOfType, number } = PropTypes;
AkosTable.propTypes = {
  title: string.isRequired,
  subTitle: string,
  header: array.isRequired,
  body: func.isRequired,
  loading: bool,
  data: oneOfType([object, array]).isRequired,
  options: func,
  avatar: func,
  onOptionsClick: func,
  elevation: number,
  resetPage: bool,
};

AkosTable.defaultProps = {
  loading: false,
  subTitle: undefined,
  avatar: undefined,
  onOptionsClick: undefined,
  options: undefined,
  elevation: 1,
};

export default AkosTable;
