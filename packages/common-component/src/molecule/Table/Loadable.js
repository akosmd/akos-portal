/**
 *
 * Asynchronously loads the component for Table
 *
 */

import loadable from '@core/Utils/loadable';

export default loadable(() => import('./index'));
