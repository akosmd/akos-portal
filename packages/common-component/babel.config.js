module.exports = {
  presets: ['@babel/preset-env', '@babel/preset-react'],
  plugins: [
    'transform-es2015-modules-commonjs',
    'css-modules-transform',
    [
      'module-resolver',
      {
        root: ['.'],
        alias: {
          compound: './src/compound',
          molecule: './src/molecule',
          'common-core': 'akos-core/lib',
        },
      },
    ],
  ],
};
