/**
 * Combine all reducers in this file and export the combined reducers.
 */

import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import _ from 'lodash';

import history from '@core/Utils/history';
import languageProviderReducer from 'containers/LanguageProvider/reducer';
import patientReducer, { patientMigrations } from '@core/Reducer';
import legacyReducer, { legacyMigrations } from '@core/Reducer/legacy';

import { createMigrate } from 'redux-persist';

import { persist } from '@core/Utils/reduxPersist';
import memberSearchReducer from '@core/memberSearch/Reducer';

/**
 * Merges the main reducer with the router state and dynamically injected reducers
 */
const MIGRATION_DEBUG = false;

const patientPersistConfig = {
  key: 'persistedMember',
  version: 1.2,
  migrate: createMigrate(patientMigrations, { debug: MIGRATION_DEBUG }),
};
const legacyPersistConfig = {
  key: 'persistedLegacy',
  version: 1.2,
  migrate: createMigrate(legacyMigrations, { debug: MIGRATION_DEBUG }),
};

export default function createReducer(injectedReducers = {}) {
  const rootReducer = combineReducers({
    patient: persist(patientPersistConfig, [
      ...new Set([].concat([patientReducer, memberSearchReducer])),
    ]),
    legacy: persist(legacyPersistConfig, legacyReducer),
    language: languageProviderReducer,
    router: connectRouter(history),
    ...injectedReducers,
  });

  return rootReducer;
}
