import React from 'react';
import PropTypes from 'prop-types';
import { Container } from './style';

const PharmacyIcon = props => (
  <Container {...props}>
    <svg
      xmlns="http://www.w3.org/2000/svg"
      id="Layer_3"
      height="512px"
      viewBox="0 0 64 64"
      width="512px"
    >
      <g>
        <path
          d="m62 29h-11.586l11.232-11.232c.861-.861 1.354-2.051 1.354-3.268s-.493-2.407-1.354-3.268l-2.879-2.879c-.86-.86-2.05-1.353-3.267-1.353s-2.407.493-3.268 1.354l-20.646 20.646h-29.586c-.553 0-1 .447-1 1v11c0 8.901 6.501 16.294 15 17.729v3.271c0 .553.447 1 1 1h30c.553 0 1-.447 1-1v-3.271c8.499-1.435 15-8.828 15-17.729v-11c0-.553-.447-1-1-1zm-6-8.414-6.586-6.586 1.586-1.586 6.586 6.586zm1.354-10.818 2.879 2.879c.487.488.767 1.163.767 1.853s-.28 1.365-.768 1.854l-1.232 1.232-6.586-6.586 1.232-1.232c.977-.975 2.731-.975 3.708 0zm-9.354 5.646 6.586 6.586-7 7h-13.172zm13 15.586v2h-58v-2zm-15 30h-28v-2.051c.333.019.662.051 1 .051h26c.338 0 .667-.032 1-.051zm-1-4h-26c-8.822 0-16-7.178-16-16v-6h58v6c0 8.822-7.178 16-16 16z"
          data-original="#000000"
          data-old_color="#000000"
          // fill={props.color}
        />
        <path
          d="m38 43h-3v-3c0-.553-.447-1-1-1h-4c-.553 0-1 .447-1 1v3h-3c-.553 0-1 .447-1 1v4c0 .553.447 1 1 1h3v3c0 .553.447 1 1 1h4c.553 0 1-.447 1-1v-3h3c.553 0 1-.447 1-1v-4c0-.553-.447-1-1-1zm-1 4h-3c-.553 0-1 .447-1 1v3h-2v-3c0-.553-.447-1-1-1h-3v-2h3c.553 0 1-.447 1-1v-3h2v3c0 .553.447 1 1 1h3z"
          data-original="#000000"
          data-old_color="#000000"
          // fill={props.color}
        />
        <path
          d="m12.293 11.707c.834.834 1.942 1.293 3.121 1.293h.172c1.179 0 2.287-.459 3.121-1.293s1.293-1.942 1.293-3.121v-.172c0-1.179-.459-2.287-1.293-3.121l-3-3c-.834-.834-1.942-1.293-3.121-1.293h-.172c-1.179 0-2.287.459-3.121 1.293s-1.293 1.942-1.293 3.121v.172c0 1.179.459 2.287 1.293 3.121zm5.707-3.293v.172c0 .645-.251 1.251-.707 1.707s-1.063.707-1.707.707h-.172c-.645 0-1.251-.251-1.707-.707l-.793-.793 3.586-3.586.793.793c.456.456.707 1.063.707 1.707zm-8-3c0-.645.251-1.251.707-1.707s1.063-.707 1.707-.707h.172c.645 0 1.251.251 1.707.707l.793.793-3.586 3.586-.793-.793c-.456-.456-.707-1.063-.707-1.707z"
          data-original="#000000"
          data-old_color="#000000"
          // fill={props.color}
        />
        <path
          d="m18 22.414v.172c0 1.179.459 2.287 1.293 3.121s1.942 1.293 3.121 1.293h.172c1.179 0 2.287-.459 3.121-1.293l3-3c.834-.834 1.293-1.942 1.293-3.121v-.172c0-1.179-.459-2.287-1.293-3.121s-1.942-1.293-3.121-1.293h-.172c-1.179 0-2.287.459-3.121 1.293l-3 3c-.834.834-1.293 1.942-1.293 3.121zm7.414-5.414h.172c.645 0 1.251.251 1.707.707s.707 1.063.707 1.707v.172c0 .645-.251 1.251-.707 1.707l-.793.793-3.586-3.586.793-.793c.456-.456 1.063-.707 1.707-.707zm-5.414 5.414c0-.645.251-1.251.707-1.707l.793-.793 3.586 3.586-.793.793c-.456.456-1.063.707-1.707.707h-.172c-.645 0-1.251-.251-1.707-.707s-.707-1.063-.707-1.707z"
          data-original="#000000"
          data-old_color="#000000"
          // fill={props.color}
        />
        <path
          d="m11.121 27h.879c.553 0 1-.447 1-1v-.879c0-1.635-.637-3.172-1.793-4.328s-2.693-1.793-4.328-1.793h-.879c-.553 0-1 .447-1 1v.879c0 1.635.637 3.172 1.793 4.328s2.693 1.793 4.328 1.793zm-1.328-4.793c.738.738 1.173 1.749 1.205 2.791-1.042-.032-2.053-.467-2.791-1.205s-1.173-1.749-1.205-2.791c1.042.032 2.053.467 2.791 1.205z"
          data-original="#000000"
          data-old_color="#000000"
          // fill={props.color}
        />
        <path
          d="m33 13h.879c1.635 0 3.172-.637 4.328-1.793s1.793-2.693 1.793-4.328v-.879c0-.553-.447-1-1-1h-.879c-1.635 0-3.172.637-4.328 1.793s-1.793 2.693-1.793 4.328v.879c0 .553.447 1 1 1zm2.207-4.793c.738-.738 1.749-1.173 2.791-1.205-.032 1.042-.467 2.053-1.205 2.791s-1.749 1.173-2.791 1.205c.032-1.042.467-2.053 1.205-2.791z"
          data-original="#000000"
          data-old_color="#000000"
          // fill={props.color}
        />
      </g>
    </svg>
  </Container>
);
const { string } = PropTypes;
PharmacyIcon.propTypes = {
  color: string,
  width: string,
  height: string,
};
PharmacyIcon.defaultProps = {
  width: '2.5rem',
  height: '2.5rem',
  color: '#01BCC5',
};

export default PharmacyIcon;
