/**
 *
 * Asynchronously loads the component for Header
 *
 */

import loadable from '@core/Utils/loadable';

export default loadable(() => import('./index'));
