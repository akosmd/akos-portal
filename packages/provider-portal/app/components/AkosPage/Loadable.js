/**
 *
 * Asynchronously loads the component for AkosPage
 *
 */

import loadable from '@core/Utils/loadable';

export default loadable(() => import('./index'));
