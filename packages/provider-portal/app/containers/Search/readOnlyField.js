import React from 'react';
import PropTypes from 'prop-types';
import TextField from '@components/molecule/TextField';

const ReadOnlyField = ({ value, caption, id, type }) => {
  const defaultValue = type === 'number' ? '0' : '';
  return (
    <TextField
      field={{
        id,
        caption,
        value: value || defaultValue,
        fullWidth: true,
        readOnly: true,
        shrink: true,
      }}
      type={type}
      variant="outlined"
    />
  );
};

const { any, string } = PropTypes;

ReadOnlyField.propTypes = {
  value: any,
  caption: string.isRequired,
  id: string.isRequired,
  type: string,
};

export default ReadOnlyField;
