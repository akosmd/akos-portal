import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import RichTextArea from '@components/molecule/RichTextArea';
import Button from '@components/molecule/Button';
import { addCompanyNotes, getCompanyNotes } from '@core/Actions';
import {
  makeSelectNurseSelectedPatientList,
  makeSelectCompanyNotesList,
} from '@core/Selectors';

import {
  Grid,
  CardContent,
  Card,
  CardHeader,
  Typography,
} from '@material-ui/core';

const CompanyNotes = ({
  dispatch,
  doAddCompanyNotes,
  doGetCompanyNotes,
  selectedPatient,
  companyNotes,
}) => {
  const [view, setView] = useState('view');
  const [richText, setRichText] = useState('');

  useEffect(() => {
    if (selectedPatient.data) {
      dispatch(
        doGetCompanyNotes({
          brokerId: selectedPatient.data.company.id,
        }),
      );
    }
  }, [selectedPatient]);
  useEffect(() => {
    setRichText(companyNotes.data && companyNotes.data.notes);
  }, [companyNotes.data]);
  const handleButton = (action, notes) => {
    if (notes && selectedPatient.data) {
      dispatch(
        doAddCompanyNotes({
          brokerId: selectedPatient.data.company.id,
          notes,
        }),
      );
    }
    setView(action);
  };

  return selectedPatient.loading ? null : (
    <Card>
      <CardHeader
        subheader={<Typography variant="h6"> {'Company Notes'}</Typography>}
      />
      <CardContent>
        <Grid container spacing={1}>
          <Grid item xs={12}>
            <RichTextArea
              value={richText || ''}
              onChange={val => setRichText(val)}
              readOnly={view === 'view'}
              error={companyNotes.error && companyNotes.error.statusText}
            />
          </Grid>
          {view === 'view' && !companyNotes.error ? (
            <Grid item xs={12} md={2}>
              <Button
                loading={companyNotes.loading}
                onClick={() => handleButton('new', undefined)}
              >
                {'Update Note'}
              </Button>
            </Grid>
          ) : (
            <>
              <Grid item xs={12} md={2}>
                <Button
                  loading={companyNotes.loading}
                  onClick={() => handleButton('view', richText)}
                >
                  {'Save Changes'}
                </Button>
              </Grid>
              <Grid item xs={12} md={2}>
                <Button
                  onClick={() => {
                    handleButton('view', undefined);
                    setRichText(companyNotes.data && companyNotes.data.notes);
                  }}
                  type="TextButton"
                  loading={companyNotes.loading}
                >
                  {'Discard'}
                </Button>
              </Grid>
            </>
          )}
        </Grid>
      </CardContent>
    </Card>
  );
};

const mapStateToProps = createStructuredSelector({
  selectedPatient: makeSelectNurseSelectedPatientList(),
  companyNotes: makeSelectCompanyNotesList(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    doAddCompanyNotes: addCompanyNotes,
    doGetCompanyNotes: getCompanyNotes,
  };
}

const { func, object } = PropTypes;

CompanyNotes.propTypes = {
  dispatch: func.isRequired,
  doAddCompanyNotes: func.isRequired,
  doGetCompanyNotes: func.isRequired,
  selectedPatient: object.isRequired,
  companyNotes: object.isRequired,
};

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(CompanyNotes);
