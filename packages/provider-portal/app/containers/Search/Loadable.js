/**
 *
 * Asynchronously loads the component for Benefits
 *
 */

import loadable from '@core/Utils/loadable';

export default loadable(() => import('./index'));
