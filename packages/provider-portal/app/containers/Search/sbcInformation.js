import React from 'react';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import {
  Grid,
  Typography,
  ExpansionPanelSummary as BaseExpansionPanelSummary,
  ExpansionPanel as BaseExpansionPanel,
  ExpansionPanelDetails,
  Card,
  CardContent,
  CardHeader,
} from '@material-ui/core';

import { makeStyles, withStyles } from '@material-ui/core/styles';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

import { makeSelectNurseSelectedPatientList } from '@core/Selectors';
import { formatDate } from '@core/Utils/formHelper';

import TextField from './readOnlyField';

const ExpansionPanel = withStyles({
  root: {
    border: '1px solid rgba(0, 0, 0, .125)',
    '&:not(:last-child)': {
      borderBottom: 0,
    },
    '&:first-child': {
      borderTopLeftRadius: '.5rem',
      borderTopRightRadius: '.5rem',
    },
    '&:last-child': {
      borderBottomLeftRadius: '.5rem',
      borderBottomRightRadius: '.5rem',
    },
    '&:before': {
      display: 'none',
    },
    '&$expanded': {
      margin: 'auto',
    },
  },
  expanded: {},
})(BaseExpansionPanel);

const ExpansionPanelSummary = withStyles({
  root: {
    marginBottom: -1,
    minHeight: 56,
    '&$expanded': {
      minHeight: 56,
    },
  },
  content: {
    '&$expanded': {
      margin: '12px 0',
    },
  },
  expanded: {},
})(BaseExpansionPanelSummary);

const useStyles = makeStyles(theme => ({
  root: {
    border: '1px solid rgba(0, 0, 0, .125)',
    boxShadow: 'none',
    '&:not(:last-child)': {
      borderBottom: 0,
    },
    '&:before': {
      display: 'none',
    },
    '&$expanded': {
      margin: 'auto',
    },
  },
  details: { backgroundColor: '#e8e8e8', padding: '8px' },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    flexBasis: '33.33%',
    [theme.breakpoints.down('md')]: {
      flexBasis: '50%',
    },
    flexShrink: 0,
    height: '2.2rem',
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    width: '100%',
    [theme.breakpoints.down('md')]: {
      textAlign: 'right',
      fontSize: theme.typography.pxToRem(12),
    },
  },
  tertiaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    flexBasis: '90%',
    width: '100%',
    textAlign: 'right',
    [theme.breakpoints.down('md')]: {
      display: 'none',
    },
  },

  headerText: {
    height: '2.2rem',
  },
  inline: {
    display: 'inline',
  },
  content: {
    '&$expanded': {
      margin: '12px 0',
    },
  },
  expanded: {
    margin: '.5rem',
  },
  title: {
    padding: '12px 0',
  },
}));

const MedicalQuestions = ({ questions }) => {
  const classes = useStyles();
  return (
    <Grid item xs={12}>
      <Typography className={classes.title} variant="h6">
        IMPORTANT QUESTIONS
      </Typography>
      {questions &&
        questions.map((q, idx) => (
          <ExpansionPanel key={`question-${idx + 1}`} elevation={0}>
            <ExpansionPanelSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1bh-content"
              id="panel1bh-header"
            >
              <Typography color="primary">{q.question}</Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <Typography className={classes.answerText}>{q.answer}</Typography>
            </ExpansionPanelDetails>
          </ExpansionPanel>
        ))}
    </Grid>
  );
};

const MedicalEvents = ({ events }) => {
  const classes = useStyles();
  return (
    <Grid item xs={12}>
      <Typography className={classes.title} variant="h6">
        COMMON MEDICAL EVENTS
      </Typography>
      {events.map((q, idx) => (
        <ExpansionPanel key={`question-${idx + 1}`} elevation={0}>
          <ExpansionPanelSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1bh-content"
            id="panel1bh-header"
          >
            <Typography color="primary">{q.event}</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <Grid container>
              <Grid item xs={12} md={6}>
                <Typography>
                  <strong>Services You May Need</strong>
                </Typography>
                <ul>
                  {q.services.map((service, i) => (
                    <li key={`service-${i + 1}`}>{service}</li>
                  ))}
                </ul>
              </Grid>
              <Grid item xs={12} md={6}>
                <Typography>
                  <strong>What You Will Pay</strong>
                </Typography>
                {q.youPay[0].header ? (
                  <Grid container spacing={1} direction="column">
                    <Grid item>
                      <strong>{q.youPay[0].header}</strong>
                    </Grid>
                    <Grid item xs={12}>
                      <ul>
                        {q.youPay[0].pay.map((youPay, x) => (
                          <li key={`youPay-${x + 1}`}>{youPay}</li>
                        ))}
                      </ul>
                    </Grid>
                    {q.youPay[1] && (
                      <Grid item>
                        <strong>{q.youPay[1].header}</strong>
                      </Grid>
                    )}

                    {q.youPay[1] && (
                      <Grid item xs={12}>
                        <ul>
                          {q.youPay[1].pay.map((youPay, x) => (
                            <li key={`youPay-${x + 1}`}>{youPay}</li>
                          ))}
                        </ul>
                      </Grid>
                    )}
                  </Grid>
                ) : (
                  <ul>
                    {q.youPay.map((youPay, i) => (
                      <li key={`youPay-${i + 1}`}>{youPay}</li>
                    ))}
                  </ul>
                )}
              </Grid>
            </Grid>
          </ExpansionPanelDetails>
        </ExpansionPanel>
      ))}
    </Grid>
  );
};

const Information = ({ selectedPatient }) => {
  const { information } = selectedPatient.data;
  const { sbcInfo } = selectedPatient.data.information;
  const questions =
    information.sbcInfo && JSON.parse(information.sbcInfo.sbc_questions);
  const events =
    information.sbcInfo && JSON.parse(information.sbcInfo.sbc_events);
  return (
    <Card>
      <CardHeader
        subheader={<Typography variant="h6"> SBC Information</Typography>}
      />
      <CardContent>
        <Grid container spacing={3}>
          <Grid item xs={4}>
            <TextField
              caption="Carrier Name"
              value={sbcInfo && sbcInfo.carrier_name}
              id="carrierName"
            />
          </Grid>
          <Grid item xs={4}>
            <TextField
              caption="Policy Number"
              value={sbcInfo && sbcInfo.policyNumber}
              id="policyNumber"
            />
          </Grid>
          <Grid item xs={4}>
            <TextField
              caption="Group Number"
              value={sbcInfo && sbcInfo.group_number}
              id="groupNumber"
            />
          </Grid>
          <Grid item xs={4}>
            <TextField
              caption="Renewal Date"
              value={sbcInfo && formatDate(sbcInfo.renewal_date_at)}
              id="renewalDate"
            />
          </Grid>
          {questions && <MedicalQuestions questions={questions} />}
          {events && <MedicalEvents events={events} />}
        </Grid>
      </CardContent>
    </Card>
  );
};

const { array, object } = PropTypes;

Information.propTypes = {
  selectedPatient: object.isRequired,
};

MedicalQuestions.propTypes = {
  questions: array.isRequired,
};

MedicalEvents.propTypes = {
  events: array.isRequired,
};

const mapStateToProps = createStructuredSelector({
  selectedPatient: makeSelectNurseSelectedPatientList(),
});

export default compose(
  connect(
    mapStateToProps,
    null,
  ),
)(Information);
