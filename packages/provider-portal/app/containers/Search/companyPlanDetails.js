import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';

import { Grid, TableBody, TableRow, TableCell } from '@material-ui/core';

import { makeSelectNurseSelectedPatientList } from '@core/Selectors';
import AkosTable from '@components/molecule/Table';

const tableHeader = [
  {
    id: 'planTemplateType',
    numeric: false,
    disablePadding: false,
    label: 'Plan Template Type',
  },
  {
    id: 'cost',
    numeric: true,
    disablePadding: false,
    label: 'Cost',
  },

  {
    id: 'mentalHealthCost',
    numeric: true,
    disablePadding: false,
    label: 'Mental Health Cost',
  },
  {
    id: 'isWithMentalHealth',
    numeric: true,
    disablePadding: false,
    label: 'Mental Health Included',
  },
];

const MemberPlanBody = tableData => {
  const { data } = tableData;
  const computed = {
    ...data,
    cost: data.cost_with_commission,
    mentalHealthCost: data.mental_health_with_commission_cost,
  };
  return (
    <TableBody>
      <TableRow key="member-plan" hover tabIndex={-1}>
        <TableCell padding="default" variant="body">
          {computed.plan_template_type}
        </TableCell>
        <TableCell padding="default" variant="body" align="right">
          USD {computed.cost}
        </TableCell>
        <TableCell padding="default" variant="body" align="right">
          USD {computed.mentalHealthCost}
        </TableCell>
        <TableCell variant="body" align="right">
          {data.is_with_mental_health === 1 ? 'Yes' : 'No'}
        </TableCell>
      </TableRow>
    </TableBody>
  );
};

const PlanListBody = tableData => {
  const { data } = tableData;
  const computed = data.map(item => ({
    ...item,
    cost: item.cost_with_commission,
    mentalHealthCost: item.mental_health_with_commission_cost,
  }));
  return (
    <TableBody>
      {computed.map((item, i) => (
        <TableRow key={`company-plan-details-${i + 1}`} hover tabIndex={-1}>
          <TableCell padding="default" variant="body">
            {item.id === 64 ? 'Episode of Care' : item.plan_template_type}
          </TableCell>
          <TableCell padding="default" variant="body" align="right">
            USD {item.cost}
          </TableCell>

          <TableCell variant="body" align="right">
            USD {item.mentalHealthCost}
          </TableCell>
          <TableCell variant="body" align="right">
            {item.is_with_mental_health === 1 ? 'Yes' : 'No'}
          </TableCell>
        </TableRow>
      ))}
    </TableBody>
  );
};

function PlanDetails({ selectedPatient }) {
  const { company_plan_details: planList, loading, member_plan: memberPlan } =
    selectedPatient.data && selectedPatient.data;
  return (
    <Grid container alignItems="center" justify="center" spacing={2}>
      <Grid item xs={12}>
        {memberPlan.length === 0 ? (
          <AkosTable
            title="Company Plan Details"
            loading={loading}
            data={planList || []}
            header={tableHeader}
            body={PlanListBody}
          />
        ) : (
          <AkosTable
            title="Member Plan"
            loading={loading}
            data={memberPlan ? [0] : []}
            header={tableHeader}
            body={MemberPlanBody}
          />
        )}
      </Grid>
    </Grid>
  );
}

const { object } = PropTypes;

PlanDetails.propTypes = {
  selectedPatient: object,
};

const mapStateToProps = createStructuredSelector({
  selectedPatient: makeSelectNurseSelectedPatientList(),
});

export default compose(
  connect(
    mapStateToProps,
    null,
  ),
)(PlanDetails);
