/**
 *
 * Faq
 *
 */
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import _ from 'lodash';
import {
  Paper,
  Grid,
  TableBody,
  TableRow,
  TableCell,
  IconButton,
  InputBase,
  AppBar,
  Toolbar,
  Button,
  Typography,
} from '@material-ui/core';
import { createStructuredSelector } from 'reselect';
import { makeStyles } from '@material-ui/core/styles';
import SearchIcon from '@material-ui/icons/Search';
import CloseIcon from '@material-ui/icons/Close';
import moment from 'moment';
import KeyboardEventHandler from 'react-keyboard-event-handler';

import {
  getNursePatientInformation,
  getNurseSelectedPatientInformation,
} from '@core/memberSearch/Action';
import {
  makeSelectNursePatientList,
  makeSelectNurseSelectedPatientList,
} from '@core/Selectors';

import AkosTable from '@components/molecule/Table';
import BaseModal from '@material-ui/core/Dialog';

import PatientInformation from './patientInformation';
import CompanyDetails from './companyDetails';
import CompanyNotes from './companyNotes';
import SBCInformation from './sbcInformation';
import CompanyPlanDetails from './companyPlanDetails';

const useStyles = makeStyles(theme => ({
  root: {
    padding: '2px 4px',
    display: 'flex',
    alignItems: 'center',
    width: 400,
    marginTop: '20px',
  },
  input: {
    marginLeft: theme.spacing(1),
    flex: 1,
  },
  iconButton: {
    padding: 10,
  },
  divider: {
    height: 28,
    margin: 4,
  },

  appBar: {
    position: 'relative',
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1,
  },
}));

const tableHeader = [
  {
    id: 'firstName',
    numeric: false,
    disablePadding: false,
    label: 'Firstname',
  },
  {
    id: 'lastName',
    numeric: false,
    disablePadding: false,
    label: 'Lastname',
  },
  {
    id: 'email',
    numeric: false,
    disablePadding: false,
    label: 'Email Address',
  },
  {
    id: 'birthDateAt',
    numeric: false,
    disablePadding: false,
    label: 'Date of Birth',
  },
];

function MemberSearch({
  dispatch,
  doGetNursePatientInformation,
  doGetNurseSelectedPatientInformation,
  patientList,
  selectedPatientList,
}) {
  const classes = useStyles();
  const { data: patientListData, loading } = patientList;
  const [open, setOpen] = useState(false);
  const [visiblePanel, setVisiblePanel] = useState(0);
  const [find, setFind] = useState('');
  const onMemberSearch = value => {
    const params = {
      limit: 100,
      find: value,
      look_up_columns: ['email', 'last_name', 'first_name', 'phone'],
    };
    const debounced = _.debounce(
      () => dispatch(doGetNursePatientInformation(params)),
      2000,
    );
    if (value) debounced();
  };
  const isVdpc =
    selectedPatientList.data &&
    selectedPatientList.data.information.membershipTypeId !== 70001;

  const renderBody = tableData => {
    const { data } = tableData;
    return (
      <TableBody>
        {data.map((row, i) => (
          <TableRow
            className={classes.pointer}
            key={`search-result-${i + 1}`}
            hover
            tabIndex={-1}
            onClick={() => {
              setVisiblePanel(0);
              setOpen(true);
              dispatch(
                doGetNurseSelectedPatientInformation({ patientId: row.id }),
              );
            }}
          >
            <TableCell padding="default" variant="body">
              {row.firstName || ''}
            </TableCell>
            <TableCell padding="default" variant="body">
              {row.lastName || ''}
            </TableCell>
            <TableCell variant="body">{row.email || ''}</TableCell>
            <TableCell variant="body">
              {row.birthDateAt && moment(row.birthDateAt).format('MMMM D YYYY')}
            </TableCell>
          </TableRow>
        ))}
      </TableBody>
    );
  };

  const renderSearch = () => (
    <KeyboardEventHandler
      handleKeys={['enter']}
      onKeyEvent={(_key, e) => onMemberSearch(e.target.value)}
    >
      <Paper className={classes.root}>
        <InputBase
          className={classes.input}
          placeholder="Search by first name, last name, email or phone"
          inputProps={{
            'aria-label': 'Search by first name, last name, email or phone',
          }}
          onChange={e => {
            setFind(e.target.value);
            onMemberSearch(e.target.value);
          }}
          defaultValue={patientList.search.find || ''}
        />
        <IconButton
          className={classes.iconButton}
          aria-label="search"
          onClick={() => onMemberSearch(find)}
        >
          <SearchIcon />
        </IconButton>
      </Paper>
    </KeyboardEventHandler>
  );
  return (
    <Grid container alignItems="center" justify="center" spacing={2}>
      <BaseModal
        open={open}
        onClose={() => setOpen(false)}
        fullScreen
        scroll="body"
      >
        <AppBar className={classes.appBar}>
          <Toolbar>
            <IconButton
              edge="start"
              color="inherit"
              onClick={() => setOpen(false)}
              aria-label="close"
            >
              <CloseIcon />
            </IconButton>
            <Typography variant="h6" className={classes.title}>
              Member Information
            </Typography>
            {
              <Button
                onClick={() => setVisiblePanel(0)}
                disabled={selectedPatientList.loading || visiblePanel === 0}
              >
                Patient Information
              </Button>
            }
            {
              <Button
                onClick={() => setVisiblePanel(1)}
                disabled={selectedPatientList.loading || visiblePanel === 1}
              >
                SBC Information
              </Button>
            }
            {
              <Button
                onClick={() => setVisiblePanel(2)}
                disabled={selectedPatientList.loading || visiblePanel === 2}
              >
                Plan Details
              </Button>
            }
          </Toolbar>
        </AppBar>
        {visiblePanel === 0 && (
          <>
            {isVdpc && <CompanyNotes />}
            <PatientInformation />
            <CompanyDetails />
          </>
        )}
        {visiblePanel === 1 && <SBCInformation />}
        {visiblePanel === 2 && <CompanyPlanDetails />}
      </BaseModal>
      <Grid item />
      <Grid item xs={12}>
        <AkosTable
          title="Member Search"
          loading={loading}
          data={patientListData || []}
          header={tableHeader}
          body={renderBody}
          options={renderSearch}
        />
      </Grid>
    </Grid>
  );
}

const { func, object } = PropTypes;
MemberSearch.propTypes = {
  dispatch: func.isRequired,
  doGetNursePatientInformation: func.isRequired,
  doGetNurseSelectedPatientInformation: func.isRequired,
  patientList: object,
  selectedPatientList: object,
};

const mapStateToProps = createStructuredSelector({
  patientList: makeSelectNursePatientList(),
  selectedPatientList: makeSelectNurseSelectedPatientList(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    doGetNursePatientInformation: getNursePatientInformation,
    doGetNurseSelectedPatientInformation: getNurseSelectedPatientInformation,
  };
}

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(MemberSearch);
