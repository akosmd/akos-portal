import React from 'react';
import { TextFieldLoader } from '@components/molecule/Scrub';
import {
  Card,
  CardHeader,
  CardContent,
  Grid,
  Typography,
} from '@material-ui/core';

export default () => (
  <Card>
    <CardHeader
      subheader={<Typography variant="h6"> Patient Information</Typography>}
    />
    <CardContent>
      <Grid container spacing={3}>
        <Grid item xs={4}>
          <TextFieldLoader />
        </Grid>
        <Grid item xs={4}>
          <TextFieldLoader />
        </Grid>
        <Grid item xs={4}>
          <TextFieldLoader />
        </Grid>
        <Grid item xs={4}>
          <TextFieldLoader />
        </Grid>
        <Grid item xs={4}>
          <TextFieldLoader />
        </Grid>
        <Grid item xs={4}>
          <TextFieldLoader />
        </Grid>
        <Grid item xs={4}>
          <TextFieldLoader />
        </Grid>
        <Grid item xs={4}>
          <TextFieldLoader />
        </Grid>
        <Grid item xs={4}>
          <TextFieldLoader />
        </Grid>
        <Grid item xs={4}>
          <TextFieldLoader />
        </Grid>
        <Grid item xs={4}>
          <TextFieldLoader />
        </Grid>
        <Grid item xs={4}>
          <TextFieldLoader />
        </Grid>
        <Grid item xs={4}>
          <TextFieldLoader />
        </Grid>
        <Grid item xs={4}>
          <TextFieldLoader />
        </Grid>
        <Grid item xs={4}>
          <TextFieldLoader />
        </Grid>
        <Grid item xs={4}>
          <TextFieldLoader />
        </Grid>
        <Grid item xs={4}>
          <TextFieldLoader />
        </Grid>
        <Grid item xs={4}>
          <TextFieldLoader />
        </Grid>
      </Grid>
      <Grid item xs={4}>
        <TextFieldLoader />
      </Grid>
      <Grid item xs={4}>
        <TextFieldLoader />
      </Grid>
      <Grid item xs={4}>
        <TextFieldLoader />
      </Grid>
    </CardContent>
  </Card>
);
