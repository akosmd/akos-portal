import React from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { CopyToClipboard } from 'react-copy-to-clipboard';

import {
  Grid,
  Typography,
  Card,
  CardContent,
  Button,
  CardHeader,
} from '@material-ui/core';

import { makeSelectNurseSelectedPatientList } from '@core/Selectors';
import { MEMBER_TYPES } from '@core/Utils/config';
import { getKeyByValue, formatDate } from '@core/Utils/formHelper';
import CardLoader from './cardLoader';
import ReadOnlyField from './readOnlyField';
import 'react-quill/dist/quill.snow.css';

export default compose(
  connect(
    createStructuredSelector({
      selectedPatient: makeSelectNurseSelectedPatientList(),
    }),
    null,
  ),
)(({ selectedPatient }) => {
  const { information } = selectedPatient.data && selectedPatient.data;
  const VDPC_CONFERENCE_URL =
    process.env.environment === 'production'
      ? 'https://medical.akosmd.com/#!/room/VPDC/landing?uuid='
      : 'https://staging-medical.akosmd.com/#!/room/VPDC/landing?uuid=';

  const link = information
    ? `${VDPC_CONFERENCE_URL}${information.uuid}&location=MemberPortal&auto=1`
    : '';
  const renderHeader = () => (
    <Typography variant="h6">
      Patient Information
      <div>
        <Typography>
          <Typography color="secondary">
            Video Conference Link: {link}
          </Typography>
          <CopyToClipboard text={link}>
            <Button>Copy link to Clip Board</Button>
          </CopyToClipboard>
        </Typography>
      </div>
    </Typography>
  );
  return selectedPatient.loading ? (
    <CardLoader />
  ) : (
    <Card>
      <CardHeader subheader={renderHeader()} />
      <CardContent>
        <Grid container spacing={3}>
          <Grid item xs={4}>
            <ReadOnlyField
              caption="First Name"
              value={information && information.firstName}
              id="firstName"
            />
          </Grid>
          <Grid item xs={4}>
            <ReadOnlyField
              caption="Last Name"
              value={information && information.lastName}
              id="lastName"
            />
          </Grid>
          <Grid item xs={4}>
            <ReadOnlyField
              caption="Email"
              value={information && information.email}
              id="email"
            />
          </Grid>
          <Grid item xs={4}>
            <ReadOnlyField
              caption="Phone Number"
              value={information && information.phoneNumber}
              id="phoneNumber"
            />
          </Grid>
          <Grid item xs={4}>
            <ReadOnlyField
              caption="Birthdate"
              value={information && formatDate(information.birthDateAt)}
              id="birthDate"
            />
          </Grid>
          <Grid item xs={4}>
            <ReadOnlyField
              caption="Gender"
              value={
                information && information.genderId === 12000
                  ? 'Male'
                  : 'Female'
              }
              id="gender"
            />
          </Grid>
          <Grid item xs={4}>
            <ReadOnlyField
              caption="Ethnicity"
              value={information && information.ethnicity}
              id="ethnicity"
            />
          </Grid>
          <Grid item xs={4}>
            <ReadOnlyField
              caption="Race"
              value={information && information.race}
              id="race"
            />
          </Grid>
          <Grid item xs={4} />
          <Grid item xs={4}>
            <ReadOnlyField
              caption="Employer Code"
              value={information && information.employerCode}
              id="employerCode"
            />
          </Grid>
          <Grid item xs={4}>
            <ReadOnlyField
              caption="Member Group Code"
              value={information && information.memberGroupCode}
              id="memberGroupCode"
            />
          </Grid>
          <Grid item xs={4}>
            <ReadOnlyField
              caption="Membership Type"
              value={getKeyByValue(MEMBER_TYPES, information.membershipTypeId)}
              id="membershipType"
            />
          </Grid>
          <Grid item xs={6}>
            <ReadOnlyField
              caption="Address 1"
              value={information && information.address1}
              id="address1"
            />
          </Grid>
          <Grid item xs={6}>
            <ReadOnlyField
              caption="Address 2"
              value={information && information.address2}
              id="address2"
            />
          </Grid>
          <Grid item xs={4}>
            <ReadOnlyField
              caption="Street"
              value={information && information.street}
              id="street"
            />
          </Grid>
          <Grid item xs={4}>
            <ReadOnlyField
              caption="Street 1"
              value={information && information.street1}
              id="street1"
            />
          </Grid>
          <Grid item xs={4}>
            <ReadOnlyField
              caption="Street 2"
              value={information && information.street2}
              id="street2"
            />
          </Grid>
          <Grid item xs={4}>
            <ReadOnlyField
              caption="City"
              value={information && information.city}
              id="city"
            />
          </Grid>
          <Grid item xs={4}>
            <ReadOnlyField
              caption="State"
              value={information && information.state}
              id="state"
            />
          </Grid>
          <Grid item xs={4}>
            <ReadOnlyField
              caption="Zipcode"
              value={information && information.zipCode}
              id="zipcode"
            />
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
});
