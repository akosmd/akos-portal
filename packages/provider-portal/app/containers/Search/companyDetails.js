import React from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';

import {
  Grid,
  CardContent,
  Card,
  CardHeader,
  Typography,
} from '@material-ui/core';

import { makeSelectNurseSelectedPatientList } from '@core/Selectors';
import ReadonlyField from './readOnlyField';

export default compose(
  connect(
    createStructuredSelector({
      selectedPatient: makeSelectNurseSelectedPatientList(),
    }),
    null,
  ),
)(({ selectedPatient }) => {
  const { company } = selectedPatient.data && selectedPatient.data;

  return selectedPatient.loading ? (
    <></>
  ) : (
    <Card>
      <CardHeader
        subheader={<Typography variant="h6"> Company Information</Typography>}
      />
      <CardContent>
        <Grid container spacing={3}>
          <Grid item xs={4}>
            <ReadonlyField
              value={company && company.company_name}
              caption="Company Name"
              id="companyName"
            />
          </Grid>
          <Grid item xs={4}>
            <ReadonlyField
              value={company && company.company_code}
              caption="Company Code"
              id="companyCode"
            />
          </Grid>
          <Grid item xs={4}>
            <ReadonlyField
              value={company && company.email}
              caption="Email"
              id="companyEmail"
            />
          </Grid>
          <Grid item xs={4}>
            <ReadonlyField
              value={company && company.phone_number}
              caption="Phone Number"
              id="companyPhoneNumber"
            />
          </Grid>
          <Grid item xs={8}>
            <ReadonlyField
              value={company && company.nature_of_business}
              caption="Nature of Business"
              id="natureOfBusiness"
            />
          </Grid>
          <Grid item xs={12}>
            <Typography>Contact Person</Typography>
          </Grid>
          <Grid item xs={12}>
            <ReadonlyField
              value={company && company.executive_name}
              caption="Name"
              id="contactPersonName"
            />
          </Grid>
          <Grid item xs={4}>
            <ReadonlyField
              value={company && company.executive_title}
              caption="Position"
              id="contactPersonPosition"
            />
          </Grid>
          <Grid item xs={4}>
            <ReadonlyField
              value={company && company.executive_phone_number}
              caption="Phone Number"
              id="contactPersonPhoneNumber"
            />
          </Grid>
          <Grid item xs={4}>
            <ReadonlyField
              value={company && company.executive_email}
              caption="Email"
              id="contactPersonEmail"
            />
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
});
