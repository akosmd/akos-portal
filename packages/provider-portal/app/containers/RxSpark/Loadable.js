/**
 *
 * Asynchronously loads the component for GoodRx
 *
 */

import loadable from '@core/Utils/loadable';

export default loadable(() => import('./index'));
