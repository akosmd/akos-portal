/**
 *
 * Card
 *
 */

import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import SvgIcon from '@material-ui/core/SvgIcon';

import { makeStyles } from '@material-ui/core/styles';

import CloseIcon from '@material-ui/icons/Close';
import { mdiPill } from '@mdi/js';
import TextField from '@components/molecule/TextField';
import GradientButton from '@components/molecule/Button';
import KeyboardEventHandler from 'react-keyboard-event-handler';

import MuiTextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';

import {
  Card,
  CardContent,
  CardHeader,
  Avatar,
  IconButton,
  Grid,
} from '@material-ui/core';

import {
  initialField,
  initialState,
  handleChange,
} from '@core/Utils/formHelper';
import { nihSearch } from '@core/Actions';
import { makeNIHSearchResult } from '@core/Selectors';

const fields = {
  drugName: {
    ...initialField,
    id: 'drugName',
    caption: 'Drug Name',
  },

  zipCode: {
    ...initialField,
    id: 'zipCode',
    caption: 'Zip Code',
    required: false,
    error: false,
  },
  ...initialState,
};

const useStyles = makeStyles(theme => ({
  content: {
    margin: '.25rem',
    transition: 'all .5s ease',

    '&:hover': {
      color: '#fff',
      backgroundColor: 'inherit',
    },
  },
  card: {
    boxShadow:
      '0px 1px 3px 0px rgba(0,0,0,0.2), 0px 0px 0px 0px rgba(0,0,0,0.14), 0px 1px 1px -1px rgba(0,0,0,0.12)',
  },
  suggestionsContainerOpen: {
    position: 'absolute',
    zIndex: 1,
    marginTop: theme.spacing(1),
    left: 0,
    right: 0,
  },
  suggestion: {
    display: 'block',
  },
  suggestionsList: {
    margin: 0,
    padding: 0,
    listStyleType: 'none',
  },
  title: {
    marginTop: '1.5rem',
  },
  link: {
    textDecoration: 'none',
  },
  avatar: {
    // margin: 10,
    backgroundColor: '#00C49F',
  },
}));

function GoodRx({ onClick, dispatch, doSearch, searchResult, zipCode }) {
  const [rxFields, setRxFields] = useState({ ...fields });
  const [anchorEl, setAnchorEl] = useState(null);
  const [suggestions, setSuggestions] = useState([]);

  useEffect(() => {
    if (zipCode && zipCode !== '') {
      setRxFields({
        ...rxFields,
        zipCode: { ...fields.zipCode, value: zipCode },
      });
    }
  }, [zipCode]);
  useEffect(() => {
    if (searchResult && searchResult.data && searchResult.data.length > 1) {
      let tmpResult = [];
      if (searchResult && searchResult.data[1]) {
        tmpResult = searchResult.data[1].map(d => ({ title: d }));
        setSuggestions(tmpResult);
      }
    }
  }, [searchResult]);
  const styles = useStyles();

  const renderSearch = defaultValue => (
    <Autocomplete
      autoComplete={searchResult !== undefined}
      id="combo-box-demo"
      options={suggestions || []}
      getOptionLabel={option => option.title}
      onChange={(o, val) => handleSelectDrug(val.title)}
      noOptionsText={
        rxFields.drugName.value
          ? `Could not find Medicine ${rxFields.drugName.value}`
          : 'Type Medicine name to search'
      }
      loadingText={`Searching for ${rxFields.drugName.value}`}
      defaultValue={defaultValue}
      loading={searchResult.loading}
      renderInput={params => (
        <MuiTextField
          {...params}
          label={
            !rxFields.drugName.pristine && rxFields.drugName.error
              ? 'Medicine Provider is required'
              : 'Type to search'
          }
          variant="outlined"
          error={!rxFields.drugName.pristine && rxFields.drugName.error}
          onChange={onInputChange(rxFields.drugName)}
          fullWidth
        />
      )}
    />
  );

  const onInputChange = field => event => {
    handleChange({
      field: field.id,
      state: rxFields,
      event,
      saveStepFunc: setRxFields,
    });
    setAnchorEl(anchorEl ? null : event.currentTarget);
  };

  const handleSearch = () => {
    dispatch(doSearch(rxFields.drugName.value));
  };

  const handleSelectDrug = value => {
    setRxFields({ ...rxFields, drugName: { ...fields.drugName, value } });
  };
  const onSearch = () => {
    const { value } = rxFields.drugName;
    const strip = value.match(/\(([^)]+)\)/);
    const param =
      strip && strip.length > 0 ? value.replace(strip[0], '') : value;

    const URL = `  https://rx.akosmd.com/${param
      .trim()
      .toLowerCase()}?&drug-name=${param.trim().toLowerCase()}&location=${
      rxFields.zipCode.value
    }`;

    window.open(URL, '_blank');
  };
  const renderCard = () => (
    <Card elevation={1} className={styles.card}>
      <CardHeader
        avatar={
          <Avatar aria-label="recipe" className={styles.avatar} color="primary">
            <SvgIcon>
              <path d={mdiPill} />
            </SvgIcon>
          </Avatar>
        }
        action={
          <IconButton aria-label="settings" onClick={onClick}>
            <CloseIcon />
          </IconButton>
        }
        title="Pharmacy Lookup"
      />

      <CardContent className={styles.content}>
        <Grid container spacing={2} alignItems="stretch" justify="center">
          <Grid item xs={12} md={5}>
            <KeyboardEventHandler
              handleKeys={['alphanumeric', 'enter', 'backspace', 'del']}
              onKeyEvent={handleSearch}
            >
              {renderSearch()}
              {/* <TextField
                field={rxFields.drugName}
                margin="none"
                variant="outlined"
                onChange={onInputChange(rxFields.drugName)}
              />
              <Popper
                id="search-result"
                open={showResult}
                anchorEl={anchorEl}
                transition
              >
                <Paper>
                  {suggestions.map((suggestion, idx) => (
                    <MenuItem
                      key={`menu-${idx + 1}`}
                      onClick={() => handleSelectDrug(suggestion)}
                    >
                      {suggestion}
                    </MenuItem>
                  ))}
                </Paper>
              </Popper> */}
            </KeyboardEventHandler>
          </Grid>
          <Grid item xs={12} md={4}>
            <TextField
              variant="outlined"
              field={rxFields.zipCode}
              margin="none"
              onChange={onInputChange(rxFields.zipCode)}
            />
          </Grid>
          <Grid item xs={12} md={3}>
            <GradientButton
              size="medium"
              style={{ padding: '14px 8px' }}
              onClick={onSearch}
            >
              Search
            </GradientButton>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );

  return renderCard();
}
const { func, string, oneOfType, node } = PropTypes;
GoodRx.propTypes = {
  onClick: func,
  title: string,
  href: string,
  zipCode: string,
  icon: oneOfType([node, func]),
};

const mapStateToProps = createStructuredSelector({
  searchResult: makeNIHSearchResult(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    doSearch: nihSearch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(GoodRx);
