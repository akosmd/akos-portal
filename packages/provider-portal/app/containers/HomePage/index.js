/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { push } from 'connected-react-router';

import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { classes } from 'istanbul-lib-coverage';
import { Grid } from '@material-ui/core';

import { makeSelectVerifyAuth } from '@core/Selectors';
import ChatIcon from '@material-ui/icons/Chat';
import SearchIcon from '@material-ui/icons/Search';

import AkosCard from '@components/molecule/AkosCard';

function HomePage({ dispatch }) {
  return (
    <div className={classes.root}>
      <Grid container spacing={1} justify="center" alignItems="stretch">
        <Grid item xs={12} sm={6} md={6} lg={4} xl={3}>
          <AkosCard
            title="Chatroom"
            icon={() => <ChatIcon fontSize="large" />}
            onClick={() => dispatch(push('/chatroom'))}
          />
        </Grid>
        <Grid item xs={12} sm={6} md={6} lg={4} xl={3}>
          <AkosCard
            title="Member Search"
            icon={() => <SearchIcon fontSize="large" />}
            onClick={() => dispatch(push('/search'))}
          />
        </Grid>
      </Grid>
    </div>
  );
}

const { func } = PropTypes;
HomePage.propTypes = {
  dispatch: func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  account: makeSelectVerifyAuth(),
});

const withConnect = connect(
  mapStateToProps,
  null,
);

export default compose(withConnect)(HomePage);
