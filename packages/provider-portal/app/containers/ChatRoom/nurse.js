import React, { useEffect } from 'react';

import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { push } from 'connected-react-router';

import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import { Grid, CardHeader, Typography, Button } from '@material-ui/core';

import {
  getUsersWaitingInRoom,
  sendMessageToMember,
  setNurseConversation,
  leaveChatRoom,
  setSelectedUsertoChat,
} from '@core/Actions';

import {
  makeSelectGoogleAuth,
  makeSelectedUserToChat,
  makeSelectChatWaitingInRoom,
} from '@core/Selectors';

import 'pusher-js';

import Login from './login';
import WaitingRoom from './waitingroom';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },

  grid: {
    width: '99%',
    margin: '0 auto',
    [theme.breakpoints.down('md')]: {
      height: '95vh',
    },
    [theme.breakpoints.up('md')]: {
      height: '98vh',
    },
  },
  card: {
    minWidth: 375,
    height: 500,
    minHeight: 400,
    position: 'fixed',
    bottom: theme.spacing(10),
    right: theme.spacing(2),
    [theme.breakpoints.down('md')]: {
      minWidth: '96%',
      height: '90%',
      width: '96%',
      right: theme.spacing(1),
    },
    display: 'flex',
    flexDirection: 'column',
  },

  cardFullScreen: {
    display: 'flex',
    height: '90%',
    flexDirection: 'column',
    alignContent: 'stretch',
    justifyContent: 'stretch',
  },
  cardContent: {
    minHeight: 300,
    width: '100%',
    height: '100%',
  },

  cardHeaderTitle: {
    color: theme.palette.secondary.main,
  },

  cardRoot: {
    background: 'linear-gradient(to right, #55bcbe 0%,#85c3a6 100%) !important',
  },
  cardSubHeader: {
    color: theme.palette.primary.main,
  },
  fullContent: {
    display: 'flex',
    height: '90vh',
    overflowY: 'scroll',
    padding: 0,
    paddingBottom: '60px !important',
    [theme.breakpoints.up('md')]: {
      paddingBottom: '0 !important',
    },
  },

  chatAvatar: {
    background: '#12bcc5',
  },
  userList: {
    borderRight: '1px solid #f2f2f2',
  },
}));

// eslint-disable-next-line no-unused-vars
function NurseRoom({
  dispatch,
  signin,
  doGetUsersInRoom,

  doSetSelectedUsertoChat,
  enqueueSnackbar,
}) {
  useEffect(() => {
    if (signin.data && !signin.error) {
      dispatch(doGetUsersInRoom());
    }
  }, [signin]);

  const classes = useStyles();

  const addSelectedUser = user => {
    dispatch(doSetSelectedUsertoChat(user));
    dispatch(push(`/chatroom/nurse/${user.conferenceNo}`));
  };

  if (!signin.data || signin.error) {
    return <Login enqueueSnackbar={enqueueSnackbar} />;
  }

  return (
    <Grid container>
      <Grid item xs={12}>
        <Card elevation={1} className={classes.cardFullScreen}>
          <CardHeader
            className={classes.cardRoot}
            action={
              <Button onClick={() => dispatch(doGetUsersInRoom())}>
                Refresh List
              </Button>
            }
            subheader={
              <Typography variant="h6">Members in Waiting Room</Typography>
            }
            classes={{
              title: classes.cardHeaderTitle,
              subheader: classes.cardSubHeader,
            }}
          />
          <CardContent className={classes.fullContent}>
            <WaitingRoom onSelectedUser={addSelectedUser} />
          </CardContent>
        </Card>
      </Grid>
    </Grid>
  );
}

const { object, func } = PropTypes;
NurseRoom.propTypes = {
  dispatch: func.isRequired,
  signin: object.isRequired,
  doGetUsersInRoom: func.isRequired,
  doSetSelectedUsertoChat: func.isRequired,
  enqueueSnackbar: func.isRequired,
};
const mapStateToProps = createStructuredSelector({
  signin: makeSelectGoogleAuth(),
  selectedUserToChat: makeSelectedUserToChat(),
  usersInRoom: makeSelectChatWaitingInRoom(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    doGetUsersInRoom: getUsersWaitingInRoom,
    doSendMessageToMember: sendMessageToMember,
    doSetNurseConversations: setNurseConversation,
    doSetSelectedUsertoChat: setSelectedUsertoChat,
    doLeaveRoom: leaveChatRoom,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(NurseRoom);
