import React, { useState, useEffect } from 'react';
import { createStructuredSelector } from 'reselect';
import { push } from 'connected-react-router';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import { compose } from 'redux';

import { getGoogleAuth } from '@core/Actions';
import { makeSelectGoogleAuth } from '@core/Selectors';
import { Typography } from '@material-ui/core';

import { LOGIN_URL } from './config';

const GoogleAuth = ({
  history,
  dispatch,
  doGoogleAuth,
  googleAuth,
  enqueueSnackbar,
}) => {
  const [validating, setValidating] = useState(false);
  const {
    location: { search },
  } = history;

  useEffect(() => {
    if (
      !googleAuth.data &&
      !googleAuth.error &&
      search &&
      !validating &&
      !googleAuth.loading
    ) {
      const token = search.replace('?token=', '');
      setValidating(true);
      dispatch(doGoogleAuth(token));
    }
  }, []);

  useEffect(() => {
    if (googleAuth.error)
      enqueueSnackbar('Unable to Authenticate your account', {
        variant: 'error',
      });
    if (googleAuth.data) {
      dispatch(push('/'));
    }
  }, [googleAuth]);

  if (googleAuth.error)
    return (
      <div>
        <Typography variant="h6" color="error">
          Unknown user
        </Typography>
        <Typography>
          The account you tried to login is not authorized. Please click{' '}
          <a href={LOGIN_URL}>here</a> to login with an authorized account
        </Typography>
      </div>
    );

  return null;
};

const { func, object } = PropTypes;

GoogleAuth.propTypes = {
  history: object.isRequired,
  googleAuth: object.isRequired,
  dispatch: func.isRequired,
  doGoogleAuth: func.isRequired,
  enqueueSnackbar: func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  googleAuth: makeSelectGoogleAuth(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    doGoogleAuth: getGoogleAuth,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(GoogleAuth);
