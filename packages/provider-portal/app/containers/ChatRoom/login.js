/**
 *
 * Login
 *
 */

import React, { useEffect } from 'react';
import PropTypes from 'prop-types';

import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { compose } from 'redux';

import { Grid } from '@material-ui/core';

import Button from '@components/molecule/Button';
import { resetSignin } from '@core/Actions';
import { makeSelectNurseSignin } from '@core/Selectors';

import { LOGIN_URL } from './config';

export function Login({ dispatch, signin, doResetSignin }) {
  useEffect(() => {
    if (signin.data) dispatch(doResetSignin());
  }, []);

  return (
    <div>
      <Grid container spacing={2} direction="column">
        <Grid item xs={12} md={3}>
          <Button
            variant="contained"
            size="large"
            href={LOGIN_URL}
            loading={signin.loading}
          >
            {'Login with your Akos Google Account'}
          </Button>
        </Grid>
      </Grid>
    </div>
  );
}

const { func, object } = PropTypes;

Login.propTypes = {
  dispatch: func.isRequired,
  signin: object,
  doResetSignin: func.isRequired,
};
const mapStateToProps = createStructuredSelector({
  signin: makeSelectNurseSignin(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    doResetSignin: resetSignin,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(Login);
