import React, { Fragment, useEffect } from 'react';

import PropTypes from 'prop-types';

import { makeStyles } from '@material-ui/core/styles';
import Echo from 'laravel-echo';

import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import 'pusher-js';

import { setSelectedUsertoChat, getUsersWaitingInRoom } from '@core/Actions';
import {
  makeSelectChatWaitingInRoom,
  makeSelectGoogleAuth,
} from '@core/Selectors';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
// import Divider from '@material-ui/core/Divider';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';

import { CircularProgress } from '@material-ui/core';

import getOptions, { USER_TYPES } from './config';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    // maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
  inline: {
    display: 'inline',
  },
  userContainer: {
    width: '100%',
  },
}));

function WaitingRoom({
  dispatch,
  usersInRoom,
  signin,
  onSelectedUser,
  doGetUsersInRoom,
  user: selectedUser,
}) {
  const classes = useStyles();

  useEffect(() => {
    const roomEcho = new Echo(getOptions(signin.data, USER_TYPES.nurse));
    roomEcho
      .join('MemberWaitingRoom')
      .here(() => {})
      .joining(user => {
        reFetchUsersInRoom(user);
      })
      .leaving(() => {
        dispatch(doGetUsersInRoom());
      });
  }, []);

  const reFetchUsersInRoom = user => {
    if (!usersInRoom.data && !usersInRoom.loading) dispatch(doGetUsersInRoom());
    const exists = usersInRoom.data
      ? usersInRoom.data.find(u => u.id === user.id)
      : false;
    if (!exists) dispatch(doGetUsersInRoom());
  };

  const handleSelectUser = user => {
    onSelectedUser(user);
    // dispatch(doSetSelectedUsertoChat(user));
  };
  return (
    <div className={classes.userContainer}>
      {usersInRoom.data && (
        <List className={classes.root}>
          {usersInRoom.data.map((user, idx) => (
            <Fragment key={`${user.uuid}-${idx + 1}`}>
              <ListItem
                alignItems="flex-start"
                button
                selected={selectedUser && user.id === selectedUser.id}
                onClick={() => handleSelectUser(user)}
              >
                <ListItemAvatar>
                  <Avatar style={{ textTransform: 'uppercase' }}>{`${
                    user.firstName[0]
                  }${user.lastName[0]}`}</Avatar>
                </ListItemAvatar>
                <ListItemText
                  primary={`${user.firstName} ${user.lastName}`}
                  secondary={
                    <React.Fragment>
                      <Typography
                        component="span"
                        variant="body2"
                        className={classes.inline}
                        color="textPrimary"
                      >
                        Conference Id
                      </Typography>
                      {` —  ${user.conferenceNo}`}
                    </React.Fragment>
                  }
                />
              </ListItem>
              {/* <Divider variant="inset" component="li" /> */}
            </Fragment>
          ))}
        </List>
      )}
      {usersInRoom.loading && (
        <Typography component="div" align="center">
          <CircularProgress color="secondary" />
        </Typography>
      )}
    </div>
  );
}

const { object, func } = PropTypes;
WaitingRoom.propTypes = {
  dispatch: func.isRequired,
  usersInRoom: object.isRequired,
  user: object,
  signin: object.isRequired,
  doGetUsersInRoom: func.isRequired,
  onSelectedUser: func.isRequired,
};
const mapStateToProps = createStructuredSelector({
  usersInRoom: makeSelectChatWaitingInRoom(),
  signin: makeSelectGoogleAuth(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    doSetSelectedUsertoChat: setSelectedUsertoChat,
    // doSetNurseConversations: setNurseConversation,
    doGetUsersInRoom: getUsersWaitingInRoom,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(WaitingRoom);
