import { WS_HOST, PUSHER_KEY } from 'utils/config';

export const USER_TYPES = {
  nurse: 10008,
  member: 10009,
};

export const LOGIN_URL =
  process.env.environment === 'production'
    ? 'https://integration.akosmd.com/api/nurse/login'
    : 'https://staging-integration.akosmd.com/api/nurse/login';

const getOptions = (signedInUser, userType = 10008) => {
  if (!signedInUser) return;
  // eslint-disable-next-line consistent-return
  return {
    broadcaster: 'pusher',
    key: PUSHER_KEY,
    wsHost: WS_HOST,
    enabledTransports: ['ws', 'wss'],
    disableStats: true,
    authEndpoint: `${process.env.INTEGRATION_API_URL}chat/${
      userType === USER_TYPES.member ? 'member' : 'user'
    }/broadcast/auth`,
    auth: {
      params: {
        UserTypeID: userType,
      },
      headers: {
        Authorization: `Bearer ${signedInUser.token}`,
      },
    },
  };
};

export default getOptions;
