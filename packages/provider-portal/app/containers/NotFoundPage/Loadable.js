/**
 * Asynchronously loads the component for NotFoundPage
 */

import loadable from '@core/Utils/loadable';

export default loadable(() => import('./index'));
