import { spawn } from 'redux-saga/effects';
import signIn from '@core/signIn/Saga';
import memberSearch from '@core/memberSearch/Saga';

export default function* saga() {
  yield spawn(signIn);
  yield spawn(memberSearch);
}
