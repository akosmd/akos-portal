/**
 *
 * App.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 */

import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Switch, Route } from 'react-router-dom';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { compose } from 'redux';

import AkosPage from 'components/AkosPage';
import AuthenticatedRoute from '@components/molecule/AuthenticatedRoute';

import NotFoundPage from 'containers/NotFoundPage/Loadable';
import HomePage from 'containers/HomePage';
import Search from 'containers/Search';

import NurseWaitingRoom from 'containers/ChatRoom/nurse';
import NurseAuth from 'containers/ChatRoom/auth';
import NurseToMemberChat from 'containers/ChatRoom/nurse-member-chat';

import { makeSelectGoogleAuth } from '@core/Selectors';

import GlobalStyle from '../../global-styles';

function App(props) {
  const { auth, history } = props;
  const [isAuthenticated, setAuthenticated] = useState(
    auth.data && auth.data !== false,
  );

  useEffect(() => {
    const { runtime } = props;
    if (
      process.env.NODE_ENV === 'production' ||
      process.env.NODE_ENV === 'staging'
    ) {
      runtime.install({
        onUpdating: () => {
          // eslint-disable-next-line no-console
          console.log('SW Event:', 'updating resources');
        },
        onUpdateReady: () => {
          // eslint-disable-next-line no-console
          console.log('SW Event:', 'applying file updates');

          runtime.applyUpdate();
        },
        onUpdated: () => {
          // eslint-disable-next-line no-console
          console.log('SW Event:', 'resources updated successfully');
        },
        onUpdateFailed: () => {
          // eslint-disable-next-line no-console
          console.log(
            'SW Event:',
            'an error occured while trying to update resources',
          );
        },
      });
    }
  }, []);

  useEffect(() => {
    setAuthenticated(auth.data && auth.data !== false && !auth.error);
  }, [auth]);

  return (
    <div>
      <Switch>
        <AuthenticatedRoute
          isAuthenticated={isAuthenticated}
          exact
          path="/"
          component={() => <AkosPage component={HomePage} history={history} />}
        />

        <Route
          path="/login"
          component={() => (
            <AkosPage component={NurseWaitingRoom} history={history} />
          )}
        />

        <AuthenticatedRoute
          isAuthenticated={isAuthenticated}
          exact
          path="/chatroom"
          component={() => (
            <AkosPage component={NurseWaitingRoom} history={history} />
          )}
        />

        {/* <AuthenticatedRoute
          isAuthenticated={isAuthenticated}
          exact
          path="/search"
          component={() => <AkosPage component={Search} history={history} />}
        /> */}

        <Route
          path="/search"
          exact
          component={() => <AkosPage component={Search} history={history} />}
        />

        <Route
          path="/chatroom/auth"
          exact
          component={() => <AkosPage component={NurseAuth} history={history} />}
        />
        <Route path="/chatroom/nurse/:id" component={NurseToMemberChat} />

        <Route component={NotFoundPage} />
      </Switch>
      <GlobalStyle />
    </div>
  );
}

const { object } = PropTypes;

App.propTypes = {
  auth: object,

  history: object,
};

const mapStateToProps = createStructuredSelector({
  auth: makeSelectGoogleAuth(),
});

const mapDispatchToProps = dispatch => ({
  dispatch,
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(App);
