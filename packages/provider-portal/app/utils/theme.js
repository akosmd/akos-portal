import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
  palette: {
    primary: {
      light: '#41c9d0',
      main: '#12bcc5',
      dark: '#25406B',
      contrastText: '#fff',
    },
    secondary: {
      light: '#506688',
      main: '#25406B',
      dark: '#192c4a',
    },
    text: {
      primary: '#666',
    },
    background: {
      default: '#f2f2f2',
      footer: '#25406B',
    },
    button: {
      main: 'linear-gradient(to right, #55bcbe 0%,#85c3a6 100%)',
      hover: 'linear-gradient(to right, #49b7b9 0%,#85c3a6 100%) !important',
    },
  },

  typography: {
    useNextVariants: true,
    fontFamily: 'Lato, sans serif',
    color: {
      light: '#ffffff',
      gray: '#9fa1a3',
      main: '#0000008a',
      dark: '#000000',
      error: '#f44336',
    },
  },
});

export default theme;
