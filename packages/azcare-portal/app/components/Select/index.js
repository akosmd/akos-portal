/**
 *
 * Select
 *
 */

import React from 'react';

import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import KeyboardEventHandler from 'react-keyboard-event-handler';

import PropTypes, { object } from 'prop-types';

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  label: {
    minHeight: 33,
  },
  input: {
    marginBottom: 8,
  },
});

function Selection({ onChange, field, options, disabled, variant, keyEvents }) {
  const classes = useStyles();
  const getOptions = opts =>
    opts.map(({ value, name }) => (
      <option value={value} key={value}>
        {name}
      </option>
    ));
  if (!field) return null;
  const { error, pristine, readOnly, caption, errorMessage } = field;
  let message = field.required ? `${caption}*` : caption;
  if (error && !pristine && field.required)
    message = !errorMessage ? `${caption} is required` : errorMessage;

  return (
    <div>
      <InputLabel
        htmlFor={field.id}
        error={error && !pristine}
        className={classes.label}
      >
        {message}
      </InputLabel>
      <KeyboardEventHandler
        handleKeys={keyEvents && keyEvents.handleKeys}
        onKeyEvent={keyEvents && keyEvents.onKeyEvent}
      >
        <Select
          error={field.error && !pristine}
          native
          value={field.value !== null ? field.value : ''}
          id={field.id}
          name={field.name}
          className={classes.input}
          onChange={onChange}
          fullWidth
          required={field.required}
          variant={variant}
          disabled={disabled || readOnly}
        >
          {getOptions(options)}
        </Select>
      </KeyboardEventHandler>
    </div>
  );
}

const {
  string,
  bool,
  shape,
  func,
  arrayOf,
  any,
  oneOfType,
  number,
} = PropTypes;
Selection.propTypes = {
  field: shape({
    id: string.isRequired,
    value: oneOfType([string, number]).isRequired,
    caption: string.isRequired,
    errorMessage: string,
    placeholder: string,
    error: bool,
    required: bool,
    fullWidth: bool,
    autoFocus: bool,
  }),
  keyEvents: object,
  onChange: func,

  options: arrayOf(
    shape({
      value: any.isRequired,
      name: string.isRequired,
    }),
  ),
  disabled: bool,
  variant: string,
};

Selection.defaultProps = {
  onChange: undefined,
  disabled: false,
  variant: undefined,
};

export default Selection;
