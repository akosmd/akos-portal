/**
 *
 * DateTimePickerField
 *
 */

import React from 'react';
import PropTypes from 'prop-types';

import DateFnsUtils from '@date-io/date-fns';
import { MuiPickersUtilsProvider, DateTimePicker } from '@material-ui/pickers';
import InputLabel from '@material-ui/core/InputLabel';
import KeyboardEventHandler from 'react-keyboard-event-handler';
import DateRangeIcon from '@material-ui/icons/DateRange';
import { InputAdornment } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  container: {
    border: `1px solid ${theme.palette.border.textField}`,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  label: {
    minHeight: 33,
  },
}));

function DateTimePickerField(props) {
  const { field, onChange, keyEvents, ...rest } = props;
  const min = 0;
  const max = 500;
  const random = Math.random() * (+max - +min) + +min;
  const classes = useStyles();

  if (!field) return null;

  const { readOnly, error, value, pristine, caption, errorMessage } = field;
  let message = field.required ? `${caption}*` : caption;
  if (error && !pristine && field.required)
    message = !errorMessage ? `${caption} is required` : errorMessage;

  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <KeyboardEventHandler
        handleKeys={keyEvents && keyEvents.handleKeys}
        onKeyEvent={keyEvents && keyEvents.onKeyEvent}
      >
        <InputLabel
          className={classes.label}
          htmlFor={field.id}
          error={error && !pristine}
        >
          {message}
        </InputLabel>
        <DateTimePicker
          {...rest}
          id={`${random}-${field.id}`}
          required={field.required}
          error={error && !pristine}
          invalidLabel={error && !pristine}
          readOnly={readOnly}
          // format="MM/dd/yyyy"
          value={value !== '' ? value : null}
          onChange={onChange}
          inputVariant="outlined"
          variant="inline"
          InputProps={{
            endAdornment: (
              <InputAdornment position="end">
                <DateRangeIcon fontSize="large" />
              </InputAdornment>
            ),
          }}
          fullWidth
        />
      </KeyboardEventHandler>
    </MuiPickersUtilsProvider>
  );
}

const { string, bool, shape, func, object } = PropTypes;
DateTimePickerField.propTypes = {
  field: shape({
    id: string.isRequired,
    value: string,
    caption: string.isRequired,
    errorMessage: string,
    placeholder: string,
    error: bool,
    required: bool,
  }),
  fullWidth: bool,
  onChange: func,
  keyEvents: object,
};

export default DateTimePickerField;
