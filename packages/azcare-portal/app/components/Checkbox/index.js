/**
 *
 * Checkbox
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Check from '@material-ui/core/Checkbox';

function Checkbox({
  field,
  margin,
  className,
  onChange,
  labelPlacement,
  ...rest
}) {
  if (!field) return null;
  const {
    error,
    caption,
    errorMessage,
    pristine,
    value,
    readOnly,
    fullWidth,
    ...fieldRest
  } = field;
  return (
    <FormControl margin={margin} fullWidth={fullWidth} className={className}>
      <FormControlLabel
        control={
          <Check
            checked={!!value}
            onChange={onChange}
            color="secondary"
            disabled={readOnly}
            {...{ ...fieldRest, ...rest }}
          />
        }
        label={caption}
        labelPlacement={labelPlacement}
      />
    </FormControl>
  );
}

const { string, bool, shape, func, number } = PropTypes;
Checkbox.propTypes = {
  field: shape({
    id: string.isRequired,
    value: number,
    caption: string.isRequired,
    errorMessage: string,
    placeholder: string,
    error: bool,
    required: bool,
    fullWidth: bool,
    autoFocus: bool,
    readOnly: bool,
  }),
  onChange: func,
  margin: string,
  className: string,
  labelPlacement: string,
};

Checkbox.defaultProps = {
  margin: 'normal',
  onChange: undefined,
  className: undefined,
  labelPlacement: 'end',
};

export default Checkbox;
