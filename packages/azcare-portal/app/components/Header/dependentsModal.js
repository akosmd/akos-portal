import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { push } from 'connected-react-router';

import { compose } from 'redux';

import { makeStyles, withStyles } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemText from '@material-ui/core/ListItemText';
import DialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import LinearProgress from '@material-ui/core/LinearProgress';
import PersonIcon from '@material-ui/icons/Person';

import {
  makeShowDependentsModal,
  makePatientImpersonation,
  makeSelectInxiteSso,
} from 'containers/App/selectors';
import {
  showDependentsModal,
  postPatientImpersonation,
  resetPatientImpersonation,
  initializeInxiteSso,
} from 'containers/App/actions';

const useStyles = makeStyles(theme => ({
  avatar: {
    backgroundColor: theme.palette.background.default,
    color: theme.palette.primary.main,
  },
  selected: {
    backgroundColor: theme.palette.secondary.main,
  },
  root: {
    width: '100%',
  },
}));

const DialogContent = withStyles(() => ({
  root: {
    margin: 0,
    padding: 0,
  },
}))(MuiDialogContent);

function DependentsDialog({
  dispatch,
  doOpenDependentsModal,
  modal,
  accounts,
  inxiteSso,
  patientImpersonation,
  doPostPatientImpersonation,
  doResetPatientImpersonation,
  doInitInxiteSso,
}) {
  const classes = useStyles();

  const [initSso, setInitSso] = useState(false);

  useEffect(() => {
    if (!patientImpersonation.loading && patientImpersonation.data && initSso) {
      handleClose();
    }
  }, [patientImpersonation]);

  useEffect(() => {
    if (inxiteSso.data && patientImpersonation.data && !initSso) {
      setInitSso(true);
    } else {
      setInitSso(false);
    }
  }, [patientImpersonation]);

  const handleClose = () => {
    if (!modal.shownAtleastOnce) {
      dispatch(doResetPatientImpersonation());
    }
    dispatch(doOpenDependentsModal(false));
  };

  const handleListItemClick = ({ id }) => {
    if (id === accounts[0].id) {
      dispatch(doResetPatientImpersonation());
      dispatch(doInitInxiteSso(accounts[0].uuid));
      setInitSso(true);
      handleClose();
      dispatch(push('/'));
    } else if (id !== accounts[selectedPatient].id) {
      dispatch(doPostPatientImpersonation(id));
      setInitSso(true);
      handleClose();
      dispatch(push('/'));
    } else {
      handleClose();
      setInitSso(false);
    }
  };

  const selectedPatient =
    (patientImpersonation.data &&
      accounts.findIndex(
        ({ id }) => patientImpersonation.data.patient.id === id,
      )) ||
    0;

  return (
    <div>
      <Dialog
        onClose={handleClose}
        aria-labelledby="Dependents-dialog"
        open={modal.show}
        disableBackdropClick
        disableEscapeKeyDown
      >
        <DialogTitle id="Dependents-dialog">Select Patient Account</DialogTitle>
        <DialogContent dividers>
          {patientImpersonation.loading ? (
            <div className={classes.root}>
              <LinearProgress />
            </div>
          ) : (
            <List>
              {accounts.map((account, index) => (
                <ListItem
                  button
                  onClick={() => handleListItemClick(account)}
                  key={account.id}
                  className={
                    (index === selectedPatient && classes.selected) || undefined
                  }
                >
                  <ListItemAvatar>
                    <Avatar className={classes.avatar}>
                      <PersonIcon />
                    </Avatar>
                  </ListItemAvatar>
                  <ListItemText primary={account.firstName} />
                </ListItem>
              ))}
            </List>
          )}
        </DialogContent>
        {!patientImpersonation.loading && (
          <DialogActions>
            <Button onClick={handleClose}>Cancel</Button>
          </DialogActions>
        )}
      </Dialog>
    </div>
  );
}

const { object, func, array } = PropTypes;
DependentsDialog.propTypes = {
  dispatch: func.isRequired,
  doOpenDependentsModal: func.isRequired,
  modal: object.isRequired,
  accounts: array.isRequired,
  inxiteSso: object.isRequired,
  patientImpersonation: object.isRequired,
  doPostPatientImpersonation: func.isRequired,
  doInitInxiteSso: func.isRequired,
  doResetPatientImpersonation: func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  modal: makeShowDependentsModal(),
  patientImpersonation: makePatientImpersonation(),
  inxiteSso: makeSelectInxiteSso(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    doOpenDependentsModal: showDependentsModal,
    doPostPatientImpersonation: postPatientImpersonation,
    doResetPatientImpersonation: resetPatientImpersonation,
    doInitInxiteSso: initializeInxiteSso,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(DependentsDialog);
