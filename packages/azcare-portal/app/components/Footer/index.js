import React from 'react';
import PropTypes from 'prop-types';

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  footer: {
    zIndex: theme.zIndex.drawer + 1,
    position: 'fixed',
    padding: '1rem',
    display: 'flex',
    left: 0,
    bottom: 0,
    width: '100%',
    backgroundColor: theme.palette.background.light,
    color: theme.typography.color.dark,
    textAlign: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  textContainer: {
    padding: '0 25px',
  },
  middleContainer: {
    borderLeft: `1px solid ${theme.palette.background.dark}`,
    borderRight: `1px solid ${theme.palette.background.dark}`,
    padding: '0 25px',
  },
}));
function Footer({ width }) {
  const classes = useStyles();
  return (
    <div className={classes.footer}>
      <div className={classes.textContainer}>
        Copyright &copy; {new Date(Date.now()).getFullYear()} AZCarePlan | All
        Rights Reserved
      </div>
      {width !== 'xs' && (
        <div className={classes.middleContainer}>
          Email: Info@AZCarePlan.com
        </div>
      )}
      {width !== 'xs' && (
        <div className={classes.textContainer}>Phone: 818.399.8996</div>
      )}
    </div>
  );
}

Footer.propTypes = {
  width: PropTypes.string,
};

export default Footer;
