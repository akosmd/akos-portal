/* eslint-disable func-names */
/* eslint-disable no-param-reassign */
/* eslint-disable no-return-assign */
/**
 *
 * Chat
 *
 */

import React, { useEffect, useState } from 'react';
import { DirectLine } from 'botframework-directlinejs';
import ReactWebChat from 'botframework-webchat';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';
import { push } from 'connected-react-router';

import { connect } from 'react-redux';
import { compose } from 'redux';

import { makeStyles } from '@material-ui/core/styles';
import Tooltip from '@material-ui/core/Tooltip';
import HomeIcon from '@material-ui/icons/Home';
// import HospitalIcon from '@material-ui/icons/LocalHospital';

import {
  Grid,
  Typography,
  Card,
  CardContent,
  IconButton,
} from '@material-ui/core';
import { REACT_APP_WEBCHAT_TOKEN } from 'utils/config';

import HelpIcon from '@material-ui/icons/Help';
import ExitIcon from '@material-ui/icons/ExitToApp';
import VideoCallIcon from '@material-ui/icons/VideoCall';
import PhoneIcon from '@material-ui/icons/Phone';

import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';

import 'typeface-lato';

import azcareIcon from 'images/azcare.png';

import {
  makeSelectVerifyAuth,
  makePatientImpersonation,
} from 'containers/App/selectors';

import './adaptiveCard.css';

const useStyles = makeStyles(theme => ({
  headerContainer: {
    padding: '20px 0',
  },
  headerTitle: {
    alignSelf: 'center',
  },
  headerIcons: {
    textAlign: 'end',
  },
  root: {
    display: 'flex',
  },
  speedDialWrapper: {
    position: 'relative',
    marginTop: theme.spacing(3),
    height: 380,
  },
  speedDial: {
    position: 'absolute',
    '&.MuiSpeedDial-directionUp, &.MuiSpeedDial-directionLeft': {
      bottom: theme.spacing(15),
      right: theme.spacing(2),
    },
    '&.MuiSpeedDial-directionDown, &.MuiSpeedDial-directionRight': {
      top: theme.spacing(2),
      bottom: theme.spacing(15),
      left: theme.spacing(2),
    },
  },
  grid: {
    width: '99%',
    margin: '0 auto',
    height: 'calc(100vh - 170px)',
    paddingRight: 16,
  },
  card: {
    minWidth: 375,
    height: 500,
    minHeight: 400,
    position: 'fixed',
    bottom: theme.spacing(10),
    right: theme.spacing(2),
    [theme.breakpoints.down('md')]: {
      minWidth: '96%',
      height: '90%',
      width: '96%',
      right: theme.spacing(1),
    },
    display: 'flex',
    flexDirection: 'column',
  },

  cardFullScreen: {
    display: 'flex',
    // height: '97%',
    flexDirection: 'column',
    alignContent: 'stretch',
    justifyContent: 'stretch',
  },
  cardContent: {
    minHeight: 300,
    width: '100%',
    height: '100%',
  },

  cardHeaderTitle: {
    color: theme.palette.secondary.main,
  },

  cardRoot: {
    background: '#fff',
    boxShadow:
      '0px 2px 4px -1px rgba(0,0,0,0.2), 0px 4px 5px 0px rgba(0,0,0,0.14), 0px 1px 10px 0px rgba(0,0,0,0.12)',
    marginBottom: 2,
  },
  cardSubHeader: {
    color: theme.palette.primary.main,
  },
  fullContent: {
    display: 'flex',
    // height: '90vh',
    height: 'calc(100vh - 250px)',
    // overflowY: 'scroll',
    padding: 0,
    paddingBottom: '60px !important',
    [theme.breakpoints.up('md')]: {
      paddingBottom: '0 !important',
    },
  },

  chatAvatar: {
    background: '#12bcc5',
  },
  logo: {
    display: 'flex',
    width: 210,
    height: 35,
    margin: '0 10px',
    [theme.breakpoints.down('md')]: {
      marginRight: 0,
      width: 150,
      height: 35,
    },
  },
  iconButton: {
    background: theme.palette.background.light,
    borderRadius: 5,
    padding: 5,
    margin: '0 10px',
    boxShadow: theme.palette.boxShadow.main,
  },
}));

const styleSet = window.WebChat.createStyleSet({
  rootHeight: '100%',
  rootWidth: '100%',
  bubbleBackground: '#12bcc5',
  bubbleBorderWidth: 'none',
  bubbleBorderColor: '#12bcc5',
  bubbleTextColor: '#fff',
  bubbleBorderRadius: '0px 5px 5px 5px',
  primaryFont: "'Lato', 'sans-serif'",
  bubbleFromUserBorderRadius: '6px',
  bubbleFromUserBackground: '#e1ffc7',
  cardEmphasisBackgroundColor: '#fff',
  fontFamily: 'Lato, Arial, sans-serif',
  botAvatarImage: azcareIcon,
  userAvatarInitials: 'A',
  minHeight: 'calc(100vh - 295px)',
});

function VdpcChat({ dispatch, patient, impersonatedPatient, history }) {
  const [helpAnchor, setHelpAnchor] = useState(null);

  const isMenuOpen = Boolean(helpAnchor);

  const directLine = new DirectLine({
    token: REACT_APP_WEBCHAT_TOKEN,
  });
  const actualPatient = impersonatedPatient.data
    ? impersonatedPatient
    : patient;

  const classes = useStyles();

  useEffect(() => {
    directLine.activity$.subscribe(activity => {
      if (activity && activity.type === 'endOfConversation') {
        setTimeout(() => {
          dispatch(push('/'));
        }, 2000);
      }
    });
  }, []);

  useEffect(() => {
    startScenario();
  }, [patient]);

  useEffect(() => {
    setCallNowEvent();
  }, [directLine.activity$]);

  const handleHelpOpen = event => {
    setHelpAnchor(event.target);
  };

  const hanleHelpClose = () => {
    setHelpAnchor(null);
  };

  const handleHelp = message => {
    directLine
      .postActivity({
        from: { id: patient.data.patient.email },
        type: 'message',
        text: message,
      })
      // eslint-disable-next-line no-unused-vars
      .subscribe(() => {
        setCallNowEvent();
        setVideoConfNowEvent();
      });
  };

  const handleStartOver = () => {
    const first = document.querySelectorAll('ul')[0];
    if (first) first.innerHTML = '';

    startScenario();
  };

  const handleQuit = () => {
    directLine
      .postActivity({
        from: { id: patient.data.patient.email },
        type: 'message',
        text: 'quit',
      })
      .subscribe(activity => {
        if (activity) {
          setTimeout(() => {
            dispatch(push('/'));
          }, 3000);
        }
      });
  };

  const startScenario = () => {
    const trigger = 'patient_portal';
    const { location } = history;
    let talkToProvider = false;
    const { genderId, ...rest } = actualPatient.data.patient;
    const params = {
      ...rest,
      gender: genderId === 12000 ? 'M' : 'F',
      token: actualPatient.data.token,
    };
    if (location.search.includes('talkToProvider=true')) {
      talkToProvider = true;
    }
    directLine
      .postActivity({
        type: 'event',
        value: {
          trigger,
          args: {
            patientDetails: params,
            talkToProvider,
          },
        },
        from: { id: patient.data.patient.email },
        name: 'BeginDebugScenario',
      })
      .subscribe(() => {
        setCallNowEvent();
        setVideoConfNowEvent();
      });
  };

  const setCallNowEvent = () => {
    const el = document.getElementsByClassName('style-custom');
    if (el && el.length > 0) {
      // eslint-disable-next-line func-names

      Array.from(el).map(
        button =>
          (button.onclick = function(e) {
            e.preventDefault();
            handleClickCallNow();
          }),
      );
    }
  };

  const handleClickCallNow = () => {
    const tel = document.getElementById('tel');
    if (tel) {
      tel.click();
    }
  };

  const setVideoConfNowEvent = () => {
    const el = document.querySelectorAll(
      "[aria-label^='Connect Now (Video Conference)']",
    );
    if (el && el.length > 0) {
      // eslint-disable-next-line func-names
      Array.from(el).map(
        button =>
          (button.onclick = function(e) {
            e.preventDefault();
            dispatch(push('/video-conference'));
          }),
      );
    }
  };

  directLine.activity$.subscribe(() => {
    setTimeout(() => {
      setVideoConfNowEvent();
    }, 500);
  });
  const renderHelp = (
    <Menu
      anchorEl={helpAnchor}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={isMenuOpen}
      onClose={hanleHelpClose}
    >
      <MenuItem onClick={handleStartOver}>Start over</MenuItem>
      <MenuItem onClick={() => handleHelp('Talk to provider now')}>
        Talk to Provider now
      </MenuItem>
      <MenuItem onClick={handleQuit}>Quit</MenuItem>
    </Menu>
  );

  const renderFullScreen = () => (
    <Grid
      container
      spacing={1}
      alignContent="center"
      justify="center"
      className={classes.root}
    >
      <Grid item xs={12} md={12} lg={10}>
        <a href="tel:+1-818-399-8996" id="tel" style={{ display: 'none' }}>
          Call
        </a>
        <Grid
          container
          direction="row"
          justify="space-around"
          alignItems="stretch"
          className={classes.grid}
        >
          <Grid item xs={12}>
            <Grid container className={classes.headerContainer}>
              <Grid item xs={12} sm={2} className={classes.headerTitle}>
                <Typography>TALK TO PROVIDER</Typography>
              </Grid>
              <Grid item xs={12} sm={10} className={classes.headerIcons}>
                <Tooltip title="Back to Home">
                  <IconButton
                    aria-label="Home"
                    onClick={() => history.push('/')}
                    className={classes.iconButton}
                  >
                    <HomeIcon />
                  </IconButton>
                </Tooltip>
                <Tooltip title="Help">
                  <IconButton
                    aria-label="Help"
                    onClick={handleHelpOpen}
                    className={classes.iconButton}
                  >
                    <HelpIcon />
                  </IconButton>
                </Tooltip>
                <Tooltip title="Video Conference Now">
                  <IconButton
                    aria-label="Video Conference Now"
                    onClick={() => history.push('/video-conference')}
                    // onClick={handleVideoConference}
                    className={classes.iconButton}
                  >
                    <VideoCallIcon />
                  </IconButton>
                </Tooltip>
                <Tooltip title="Call now">
                  <IconButton
                    aria-label="Call now"
                    onClick={handleClickCallNow}
                    className={classes.iconButton}
                  >
                    <PhoneIcon />
                  </IconButton>
                </Tooltip>

                <Tooltip title="Quit Chat">
                  <IconButton
                    aria-label="Quit"
                    onClick={handleQuit}
                    className={classes.iconButton}
                  >
                    <ExitIcon />
                  </IconButton>
                </Tooltip>
                {renderHelp}
              </Grid>
            </Grid>
            <Card elevation={1} className={classes.cardFullScreen}>
              <CardContent className={classes.fullContent}>
                <ReactWebChat
                  directLine={directLine}
                  styleSet={styleSet}
                  userID={patient.data.patient.email}
                />
              </CardContent>
            </Card>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );

  return renderFullScreen();
}

const { object, func } = PropTypes;
VdpcChat.propTypes = {
  patient: object.isRequired,
  impersonatedPatient: object.isRequired,
  history: object.isRequired,
  dispatch: func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  patient: makeSelectVerifyAuth(),
  impersonatedPatient: makePatientImpersonation(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(VdpcChat);
