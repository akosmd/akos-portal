import React from 'react';
import PropTypes from 'prop-types';

import VdpcChat from './vdpc';
import IndividualChat from './individual';
import AssuredHealthChat from './assuredHealth';

function ChatBotHandler({ membershipTypeId, history }) {
  const chatControls = {
    70001: <IndividualChat history={history} />,
    70002: <VdpcChat history={history} />,
    70003: <VdpcChat history={history} />,
    70004: <AssuredHealthChat history={history} />,
  };

  if (!membershipTypeId) return null;
  return chatControls[membershipTypeId];
}

const { object, number } = PropTypes;
ChatBotHandler.propTypes = {
  membershipTypeId: number.isRequired,
  history: object.isRequired,
};

export default ChatBotHandler;
