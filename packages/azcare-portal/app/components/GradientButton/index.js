import React from 'react';
import { string, bool, any } from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import BaseButton from '@material-ui/core/Button';
import { CircularProgress } from '@material-ui/core';

const GradientButton = withStyles(theme => ({
  root: {
    color: theme.typography.color.light,
    background: theme.palette.primary.main,
    '&:hover': {
      background: theme.palette.secondary.light,
    },
  },
}))(BaseButton);

const Loader = () => <CircularProgress size="2rem" color="secondary" />;
const Button = ({ variant, loading, children, ...rest }) =>
  variant ? (
    <BaseButton variant={variant} {...rest} color="primary">
      {loading ? <Loader /> : children}
    </BaseButton>
  ) : (
    <GradientButton {...rest}>{loading ? <Loader /> : children}</GradientButton>
  );

Button.propTypes = {
  variant: string,
  loading: bool,
  children: any,
};

export default Button;
