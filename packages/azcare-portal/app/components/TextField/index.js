/**
 *
 * TextField
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import BaseTextInput from '@material-ui/core/TextField';
import BaseInputLabel from '@material-ui/core/InputLabel';
import KeyboardEventHandler from 'react-keyboard-event-handler';
import { withStyles, styled } from '@material-ui/core/styles';

const TextInput = withStyles({
  root: {
    '&.Mui-focused fieldset': {
      height: 56,
    },
  },
})(BaseTextInput);

const InputLabel = styled(({ error, ...rest }) => <BaseInputLabel {...rest} />)(
  ({ error, theme }) => ({
    color: error ? theme.typography.color.error : theme.typography.color.main,
  }),
);

function TextField({
  field,
  type,
  margin,
  onChange,
  variant,
  keyEvents,
  color,
}) {
  if (!field) return null;
  const {
    error,
    caption,
    errorMessage,
    id,
    pristine,
    readOnly,
    ...rest
  } = field;
  let message = field.required ? `${caption}*` : caption;
  if (error && !pristine && field.required)
    message = !errorMessage ? `${caption} is required` : errorMessage;

  const dummy = Date.now();
  return (
    <>
      {caption && (
        <InputLabel htmlFor={field.id} error={field.error && !pristine}>
          {message}
        </InputLabel>
      )}
      <KeyboardEventHandler
        handleKeys={keyEvents && keyEvents.handleKeys}
        onKeyEvent={keyEvents && keyEvents.onKeyEvent}
      >
        <TextInput
          id={id}
          name={`${dummy}-${id}`}
          error={error && !pristine}
          {...rest}
          onChange={onChange}
          margin={margin}
          type={type}
          fullWidth={field.fullWidth}
          autoComplete="off"
          inputProps={{ readOnly, autoComplete: 'off' }}
          variant={variant}
          color={color || 'primary'}
        />
      </KeyboardEventHandler>
    </>
  );
}

const { string, bool, shape, func, object } = PropTypes;
TextField.propTypes = {
  field: shape({
    id: string.isRequired,
    value: string,
    caption: string.isRequired,
    errorMessage: string,
    placeholder: string,
    error: bool,
    required: bool,
    fullWidth: bool,
    autoFocus: bool,
  }),
  onChange: func,
  type: string,
  margin: string,
  className: string,
  inputComponent: func,
  variant: string,
  keyEvents: object,
  color: string,
};

TextField.defaultProps = {
  type: 'text',
  margin: 'normal',
  onChange: undefined,
  className: undefined,
  inputComponent: undefined,
};

export default TextField;
