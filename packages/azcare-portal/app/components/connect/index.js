import React, { useState, Fragment, useEffect } from 'react';
import PropTypes from 'prop-types';
import { createStructuredSelector } from 'reselect';

import { connect } from 'react-redux';
import { compose } from 'redux';

import moment from 'moment';

import { Grid, Typography } from '@material-ui/core';

import AkosCard from 'components/AkosCard';
import CashPayment from 'containers/Payment/cash';
import InsurancePayment from 'containers/Payment/insurance';
import BenefitsIcon from 'components/icons/benefits';
import PricingiCon from 'components/icons/pricingPlan';

import {
  getBrainTreeToken,
  generatePatientCallId,
  resetPaymentCheckout,
} from 'containers/App/legacyActions';

import {
  makeSelectSignin,
  makePatientImpersonation,
} from 'containers/App/selectors';
import {
  makeSelectPaymentCheckout,
  makeSelectPaymentAmount,
} from 'containers/App/legacySelectors';

import VideoConf from 'containers/opentok-conf';
function Connect({
  patient,
  paymentCheckout,
  amount,
  impersonatedPatient,
  enqueueSnackbar,
  doGetBrainTreeToken,
  doGeneratePatientCallId,
  doResetPaymentCheckout,
  dispatch,
}) {
  const [paymentType, setPaymentType] = useState(false);
  const [actualPatient, setActualPatient] = useState(patient);
  const [paymentString, setPaymentString] = useState('');
  const [showVideoConf, setShowVideoConf] = useState(false);

  const roomName = 'medical';
  const scrollRef = React.useRef(null);
  const fillerRef = React.useRef(null);

  useEffect(() => {
    dispatch(doGetBrainTreeToken());
  }, []);

  useEffect(() => {
    if (paymentCheckout.data && paymentCheckout.data.status_code === 200) {
      handleShowVideoConf();
    }
  }, [paymentCheckout]);

  useEffect(() => {
    if (patient.data) {
      const tmpPatient = impersonatedPatient.data
        ? impersonatedPatient.data.patient
        : patient.data.patient;
      setActualPatient(tmpPatient);
      const params = {
        patientId: tmpPatient.patientLegacyId,
        patientEmail: tmpPatient.email,
        patientDob: tmpPatient.birthDateAt,
        uuid: tmpPatient.uuid,
        staffid: 500,
        call_started: moment(Date.now()).format('YYYY/MM/DD'),
        room_name: roomName,
        call_type: 'connect',
        device_type: window.navigator.userAgent,
      };
      dispatch(doGeneratePatientCallId(params));
    }
  }, [patient]);

  const handleShowVideoConf = () => {
    dispatch(doResetPaymentCheckout());
    setShowVideoConf(true);
  };

  const paymentTypes = {
    cash: (
      <Fragment>
        <CashPayment
          patient={actualPatient || undefined}
          amount={amount}
          onConferenceClick={handleShowVideoConf}
          enqueueSnackbar={enqueueSnackbar}
        />
      </Fragment>
    ),
    insurance: (
      <Fragment>
        <InsurancePayment
          patient={actualPatient || undefined}
          amount={amount}
          onConferenceClick={handleShowVideoConf}
          enqueueSnackbar={enqueueSnackbar}
        />
      </Fragment>
    ),
  };

  const handleAutoScroll = () => {
    if (!paymentType) fillerRef.current.scrollIntoView({ behavior: 'smooth' });
    if (scrollRef.current !== null) {
      scrollRef.current.scrollIntoView({ behavior: 'smooth' });
    }
  };

  const handleSetPaymentType = payment => {
    handleAutoScroll();
    setPaymentType(paymentTypes[payment]);
    setPaymentString(payment);
  };

  if (showVideoConf) return <VideoConf roomName={roomName} />;

  return (
    <>
      <Grid
        container
        spacing={2}
        alignItems="center"
        justify="center"
        id="payment-container"
      >
        <Grid item xs={12}>
          <Typography variant="h5">Payment Options</Typography>
        </Grid>
        <Grid item xs={12} sm={6} md={6} lg={4} xl={3}>
          <AkosCard
            title="Insurance"
            icon={BenefitsIcon}
            selected={paymentString === 'insurance'}
            onClick={() => handleSetPaymentType('insurance')}
          />
        </Grid>
        <Grid item xs={12} sm={6} md={6} lg={4} xl={3}>
          <AkosCard
            title="Self Pay"
            icon={PricingiCon}
            selected={paymentString === 'cash'}
            onClick={() => handleSetPaymentType('cash')}
          />
        </Grid>
      </Grid>
      <Grid
        container
        spacing={2}
        alignItems="center"
        justify="center"
        ref={scrollRef}
      >
        <Grid item xs={12} lg={8} xl={6}>
          {paymentType !== null && paymentType}
        </Grid>
      </Grid>
      <div style={{ paddingTop: 500 }} ref={fillerRef} />
    </>
  );
}

const { object, func, number } = PropTypes;
Connect.propTypes = {
  patient: object.isRequired,
  dispatch: func.isRequired,
  amount: number,
  impersonatedPatient: object.isRequired,
  paymentCheckout: object.isRequired,
  enqueueSnackbar: func.isRequired,
  doGetBrainTreeToken: func.isRequired,
  doGeneratePatientCallId: func.isRequired,
  doResetPaymentCheckout: func.isRequired,
};

Connect.defaultProps = {
  amount: 75,
};
const mapStateToProps = createStructuredSelector({
  patient: makeSelectSignin(),
  impersonatedPatient: makePatientImpersonation(),
  paymentCheckout: makeSelectPaymentCheckout(),
  grandTotal: makeSelectPaymentAmount(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    doGetBrainTreeToken: getBrainTreeToken,
    doGeneratePatientCallId: generatePatientCallId,
    doResetPaymentCheckout: resetPaymentCheckout,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(Connect);
