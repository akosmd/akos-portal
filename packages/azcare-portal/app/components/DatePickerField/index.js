/**
 *
 * DatePickerField
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import KeyboardEventHandler from 'react-keyboard-event-handler';

import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from '@material-ui/pickers';
import { makeStyles } from '@material-ui/core';
import InputLabel from '@material-ui/core/InputLabel';

const useStyles = makeStyles({
  input: {
    marginTop: 16,
    marginBottom: 8,
  },
});

function DatePickerField(props) {
  const { field, fullWidth, onChange, keyEvents, ...rest } = props;
  const min = 0;
  const max = 500;
  const random = Math.random() * (+max - +min) + +min;
  const classes = useStyles();

  if (!field) return null;

  const { readOnly, error, value, caption, errorMessage, pristine } = field;
  let message = field.required ? `${caption}*` : caption;
  if (error && !pristine && field.required)
    message = !errorMessage ? `${caption} is required` : errorMessage;

  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <>
        <KeyboardEventHandler
          handleKeys={keyEvents && keyEvents.handleKeys}
          onKeyEvent={keyEvents && keyEvents.onKeyEvent}
        >
          <InputLabel htmlFor={field.id} error={error && !pristine}>
            {message}
          </InputLabel>
          <KeyboardDatePicker
            className={classes.input}
            {...rest}
            id={`${random}-${field.id}`}
            required={field.required}
            error={error && !pristine}
            disabled={readOnly}
            format="MM/dd/yyyy"
            value={value !== '' ? value : null}
            onChange={onChange}
            KeyboardButtonProps={{
              'aria-label': 'change date',
            }}
            fullWidth={fullWidth}
            inputVariant="outlined"
          />
        </KeyboardEventHandler>
      </>
    </MuiPickersUtilsProvider>
  );
}

const { string, bool, shape, func, object } = PropTypes;
DatePickerField.propTypes = {
  field: shape({
    id: string.isRequired,
    value: string,
    caption: string.isRequired,
    errorMessage: string,
    placeholder: string,
    error: bool,
    required: bool,
  }),
  fullWidth: bool,
  onChange: func,
  keyEvents: object,
};

export default DatePickerField;
