/**
 *
 * Asynchronously loads the component for StatesSelect
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
