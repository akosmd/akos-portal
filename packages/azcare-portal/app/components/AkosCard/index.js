/**
 *
 * Card
 *
 */

import React from 'react';
import PropTypes, { bool } from 'prop-types';

import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import {
  Card,
  CardContent,
  CardActionArea,
  CircularProgress,
} from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  content: {
    margin: '1.5rem',
  },
  card: {
    // minHeight: '13rem',
    boxShadow: theme.palette.boxShadow.main,
  },
  cardSelected: {
    // margin: '1rem',
    zoom: 1.1,
    height: '100%',
    color: theme.typography.color.light,
    backgroundColor: theme.palette.primary.main,
    '& svg': {
      fill: theme.palette.background.light,
      color: theme.typography.color.light,
    },
    boxShadow: theme.palette.boxShadow.dark,
    transition: theme.transitions.create('all', {
      easing: theme.transitions.easing.easeInOut,
    }),
  },
  title: {
    marginTop: '1.5rem',
  },
  caption: {
    minHeight: 64,
    marginBottom: '1.5rem',
  },
  actionArea: {
    transition: 'all .5s ease',

    '&:hover': {
      color: theme.typography.color.light,
      backgroundColor: theme.palette.primary.dark,
    },
    '&:hover svg': {
      fill: theme.palette.background.light,
      color: theme.typography.color.light,
    },
  },
  disabledArea: {
    backgroundColor: theme.palette.disabled.button,
  },
  link: {
    textDecoration: 'none',
  },
}));
function AkosCard({
  title,
  icon: Icon,
  href,
  onClick,
  loading,
  disabled,
  selected,
  caption,
  loaderStyle,
}) {
  const classes = useStyles();

  const renderCard = () => (
    <Card
      elevation={1}
      className={selected ? classes.cardSelected : classes.card}
    >
      <CardActionArea
        className={disabled ? classes.disabledArea : classes.actionArea}
        onClick={onClick}
      >
        <CardContent className={classes.content}>
          {caption && (
            <Typography
              color="secondary"
              variant="h6"
              className={classes.caption}
            >
              {loading ? 'Please wait...' : caption || ''}
            </Typography>
          )}
          {!loading && (
            <Typography component="div" align="center">
              {Icon && <Icon />}
            </Typography>
          )}
          {loading && (
            <Typography style={loaderStyle} component="div" align="center">
              <CircularProgress size="2rem" />
            </Typography>
          )}

          {title && (
            <Typography align="center" variant="h6" className={classes.title}>
              {loading ? 'Please wait...' : title || ''}
            </Typography>
          )}
        </CardContent>
      </CardActionArea>
    </Card>
  );

  const renderLink = () => (
    <a
      href={href}
      className={classes.link}
      target="_blank"
      rel="noopener noreferrer"
    >
      {renderCard()}
    </a>
  );
  if (href && href !== '') return renderLink();

  return renderCard();
}
const { object, func, string, oneOfType, node } = PropTypes;
AkosCard.propTypes = {
  onClick: func,
  title: string,
  loading: bool,
  selected: bool,
  href: string,
  disabled: bool,
  icon: oneOfType([node, func]),
  caption: string,
  loaderStyle: object,
};

AkosCard.defaultProps = {
  loading: false,
};
export default AkosCard;
