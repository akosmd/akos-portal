/**
 *
 * TextArea
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import Input from '@material-ui/core/Input';
import TextInput from '@material-ui/core/TextField';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';

function TextArea({
  field,
  type,
  margin,
  rowSize,
  className,
  onChange,
  inputComponent,
  variant,
}) {
  if (!field) return null;
  const { error, caption, errorMessage, pristine, readOnly, ...rest } = field;
  let message = caption;
  if (error && !pristine && field.required)
    message = !errorMessage ? `${caption} is required.` : errorMessage;

  if (variant === 'outlined')
    return (
      <>
        <InputLabel htmlFor={field.id} error={error && !pristine}>
          {field.error && !pristine ? message : field.caption}
        </InputLabel>
        <TextInput
          id="outlined-name"
          error={error && !pristine}
          {...rest}
          onChange={onChange}
          margin={margin}
          type={type}
          multiline
          fullWidth={field.fullWidth}
          inputProps={{ readOnly }}
          variant="outlined"
          rows={rowSize}
        />
      </>
    );
  return (
    <FormControl
      margin={margin}
      required={field.required}
      fullWidth={field.fullWidth}
      className={className}
    >
      <InputLabel htmlFor={field.id} error={error && !pristine}>
        {field.error && !pristine ? message : field.caption}
      </InputLabel>

      <Input
        {...rest}
        error={error && !pristine}
        type={type}
        placeholder={field.placeholder || ''}
        autoFocus={field.autoFocus}
        fullWidth={field.fullWidth}
        variant={variant}
        multiline
        rows={rowSize}
        onChange={onChange}
        inputComponent={inputComponent}
      />
    </FormControl>
  );
}

const { string, bool, shape, func, number } = PropTypes;
TextArea.propTypes = {
  field: shape({
    id: string.isRequired,
    value: string,
    caption: string.isRequired,
    errorMessage: string,
    placeholder: string,
    error: bool,
    required: bool,
    fullWidth: bool,
    autoFocus: bool,
  }),
  onChange: func,
  type: string,
  margin: string,
  className: string,
  inputComponent: func,
  variant: string,
  rowSize: number,
};

TextArea.defaultProps = {
  type: 'text',
  margin: 'normal',
  onChange: undefined,
  className: undefined,
  inputComponent: undefined,
};

export default TextArea;
