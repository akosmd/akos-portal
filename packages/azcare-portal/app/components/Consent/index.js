import React from 'react';
import PropTypes from 'prop-types';

import BottomScrollListener from 'react-bottom-scroll-listener';

import { Grid, Typography } from '@material-ui/core';
import Checkbox from 'components/Checkbox';

import { makeStyles } from '@material-ui/core/styles';
const useStyles = makeStyles(theme => ({
  scroll: {
    overflow: 'auto',
    maxHeight: '300px',
    border: `1px solid ${theme.palette.input.active}`,
    borderRadius: '5px',
    margin: '1rem',
  },
  list: {
    ...theme.typography.body1,
  },
}));

function ConsentForm(props) {
  const {
    profile,
    readConsent,
    handleChange,
    setReadConsent,
    setProfile,
  } = props;

  const classes = useStyles();

  const onCheckboxChange = field => () => {
    const event = {
      target: {
        value: field.value !== 1 ? 1 : 0,
      },
    };
    handleChange({
      field: field.id,
      state: profile,
      event,
      saveStepFunc: setProfile,
    });
  };

  const handleReadConsent = () => setReadConsent(true);

  return (
    <Grid container spacing={4}>
      <Grid item xs={12}>
        <Typography variant="h6">
          Consent and Notice of Privacy Practices
        </Typography>
      </Grid>
      <BottomScrollListener onBottom={handleReadConsent} offset={25}>
        {scrollRef => (
          <Grid ref={scrollRef} item xs={12} md={12} className={classes.scroll}>
            <Typography variant="h4" align="center">
              <u>AZ Care Policies and Procedures Manual Virtual</u>
            </Typography>
            <Typography variant="h5">
              <u>Virtual Visit Consent Policy</u>
            </Typography>
            <Typography variant="h6">A. PURPOSE</Typography>
            <Typography variant="body1" gutterBottom>
              Defined as the practice of health care delivery, diagnosis,
              consultation, treatment, transfer of medical data, and education
              using interactive audio, video, or data communications. Before
              delivering any health care by telemedicine, a health care
              practitioner who has the ultimate authority over the care or
              primary diagnosis of a patient must obtain the patient’s verbal
              and written informed consent. The informed consent procedure must
              ensure that at least all of the following information is given to
              the patient verbally and in writing.
            </Typography>
            <Typography variant="h6">B. POLICY</Typography>
            <Typography variant="body1" gutterBottom>
              The patient must sign a written statement before the delivery of
              health care by telemedicine, indicating that the patient
              understands the written information provided above and that this
              information has been discussed with the health care practitioner,
              or someone designated by him or her. This law does not apply when
              the patient is not directly involved in the telemedicine
              interaction (i.e., when one health care practitioner consults with
              another health care practitioner). However, all existing
              confidentiality protections for patient’s medical information will
              continue to apply. The law does not apply in an emergency
              situation in which the patient is unable to give informed consent
              and the patient’s representative is not available. The law also
              does not apply to a patient who is under the jurisdiction of the
              Department of Corrections.
            </Typography>
            <ol className={classes.list}>
              <li>
                The patient has the option to withhold or withdraw consent at
                any time without affectinghis or her right to future health care
                or treatment, and without risking a loss or withdrawalof any
                program benefits to which the patient would otherwise be
                entitled.
              </li>
              <li>
                A description of the potential risks, consequences, and benefits
                of telemedicine will bedisclosed at the patients request.
              </li>
              <li>All existing confidentiality protections apply.</li>
              <li>
                The patient is guaranteed access to all medical information
                transmitted during atelemedicine consultation, and copies of
                this information are available for a reasonablefee.
              </li>
              <li>
                Dissemination of any patient-identifiable images or information
                from the telemedicineinteraction to researchers or others will
                not occur without the patient’s consent.
              </li>
            </ol>
            <Typography variant="h6">C. PROCEDURE</Typography>
            <ol className={classes.list}>
              <li>
                A copy of the consent form must be given to the patient and a
                copy must be retained inthe patient’s medical record. Copies of
                court orders, etc. relating to the consent will alsobecome part
                of the patient’s medical record.
              </li>
            </ol>
            <Typography variant="h5" align="center" gutterBottom>
              <strong>AZ Care Patient Virtual Visit Consent Form</strong>
            </Typography>
            <Typography variant="body1" gutterBottom>
              I understand that my health care provider AZ Care wishes me to
              have a telemedicine consultation with. This means that I and/or my
              healthcare provider or designee will, through interactive video
              and /or audio connection, be able to consult with the above named
              consultant about my condition.
            </Typography>
            <Typography variant="body1" gutterBottom>
              My healthcare provider has explained to me how the telemedicine
              technology will be used to do such a consultation
            </Typography>
            <Typography variant="body1" gutterBottom>
              <strong>
                I understand there are potential risks with this technology:
              </strong>
            </Typography>
            <ol className={classes.list}>
              <li>
                The video connection may not work or that it may stop working
                during the consultation which I will have the right to choose to
                carry on the visit through audio (phone consultation) or decline
                moving forward with the consultation.
              </li>
              <li>
                I may be required to go to the location of the consulting
                physician if it is felt that the information obtained via
                telemedicine was not sufficient to make a diagnosis.
              </li>
            </ol>
            <Typography variant="body1" gutterBottom>
              <strong>The benefits of a telemedicine consultation are:</strong>
            </Typography>
            <ol className={classes.list}>
              <li>You may not need to travel to the consult location.</li>
              <li>
                You have access to a specialist through this consultation.
              </li>
              <li>
                You have the ability to a more convenient and affordable
                healthcare solution.
              </li>
            </ol>
            <Typography variant="body1" gutterBottom>
              I give my consent to be interviewed by the consulting health care
              provider. I also understand other individuals may be present to
              operate the video equipment and that they will take reasonable
              steps to maintain confidentiality of the information obtained.
            </Typography>
            <Typography variant="body1" gutterBottom>
              I authorize the release of any relevant medical information about
              me to the consulting health care provider, any staff the
              consulting health care provider supervises, third party payers and
              other healthcare providers who may need this information for
              continuing care purposes.
            </Typography>
            <Typography variant="body1" gutterBottom>
              I hereby release AZ Care TeleClinic and its personnel and any
              other person participating in my care from any and all liability
              which may arise from the taking and authorized use of such
              videotapes, digital recording films and photographs.
            </Typography>
            <Typography variant="body1" gutterBottom>
              I have read this document and understand the risk and benefits of
              the telemedicine consultation and have had my questions regarding
              the procedure explained and I hereby consent to participate in a
              telemedicine visit under the conditions described in this
              document.
            </Typography>
            <Typography variant="h5" align="center" gutterBottom>
              <u>
                Notice of Privacy Practices for Protected Health Information
              </u>
            </Typography>
            <Typography variant="h5" align="center" gutterBottom>
              <u>
                This notice describes how medical information about you may be
                used and disclosed and how you can get access to this
                Information. Please review it carefully!
              </u>
            </Typography>
            <Typography variant="body1" gutterBottom>
              With your consent, the practice is permitted by federal privacy
              laws to make uses an disclosures of your health information for
              purposes of treatment, payment, and health care operations.
              Protected health information we create and obtain providing our
              services to you. Such information may include documenting your
              symptoms, examination and test results, diagnoses, treatment and
              applying for future care or treatment. It also includes billing
              documents for those services.
            </Typography>
            <Typography variant="body1" gutterBottom>
              <strong>
                Example of uses of your health information for treatment
                purposes:
              </strong>
            </Typography>
            <Typography variant="body1" gutterBottom>
              A nurse obtains treatment information about you and records it in
              a health record. During the course of your treatment, the doctor
              determines a need to consult with another specialist in the area.
              The doctor will share the information with such specialist and
              obtain input.
            </Typography>
            <Typography variant="body1" gutterBottom>
              <strong>
                Example of Use of your health information for payment purposes:
              </strong>
            </Typography>
            <Typography variant="body1" gutterBottom>
              We submit a request for payment to your health insurance company.
              The health insurance company requests information from us
              regarding medical care given. We will provide information to them
              about you and the care given.
            </Typography>
            <Typography variant="body1" gutterBottom>
              <strong>
                Example of use of your information for health care operations:
              </strong>
            </Typography>
            <Typography variant="body1" gutterBottom>
              We obtain services from our insurers or other business associates
              such as quality assessment, quality improvement, outcome,
              evaluation, protocol and clinical guidelines development, training
              programs, credentialing, medical review, legal services, and
              insurance. We will share information about you with such insurers
              or other business associates as necessary to obtain these services
            </Typography>
            <Typography variant="body1" gutterBottom>
              <strong>Your Health Information Rights:</strong>
            </Typography>
            <Typography variant="body1" gutterBottom>
              The health record we maintain and billing records are the physical
              property of the practice. The information in it, however, belongs
              to you. You have a right to:
            </Typography>
            <ul className={classes.list}>
              <li>
                Request a restriction on certain uses and disclosures of your
                health information by delivering request in writing to our
                office. We are not required to grant the request but we will
                comply with any request granted;
              </li>
              <li>
                Request that you be allowed to inspect and copy your health
                record and billing record.
              </li>
              <li>
                You may exercise this right by delivering the request in writing
                to our office;
              </li>
              <li>
                Appeal a denial of access to your protected health information
                except in certain circumstances;
              </li>
              <li>
                Request that your health care record be amended to correct
                incomplete or incorrect information by delivering a written
                request to our office;
              </li>
              <li>
                File a statement of disagreement if your amendment is denied,
                and require that the request for amendment and any denial be
                attached in all future disclosures of your protected health
                information;
              </li>
              <li>
                Obtain an accounting of disclosures of your health information
                as required to be maintained by law by delivering a written
                request to our office. An accounting will not include internal
                uses of information for treatment. payment. or operations.
                disclosures made you or made at your request, or disclosures
                made to family members or friends in the course of providing
                care;
              </li>
              <li>
                Request that communication of your health information be made by
                alternative means or at an alternative location by delivering
                the request in writing to our office; and,
              </li>
              <li>
                Revoke authorizations that you made previously to use or
                disclose information except to the extent information or action
                has already been taken by delivering a written revocation to our
                office.
              </li>
            </ul>
            <Typography variant="body1" gutterBottom>
              If you want to exercise any of the above rights, please contact
              our administrator, in person or in writing, during normal hours,
              S(he) will provide you with assistance on the steps to take to
              exercise your rights.
            </Typography>
            <Typography variant="body1" gutterBottom>
              <strong>Our responsibilities:</strong>
            </Typography>
            <Typography variant="body1" gutterBottom>
              The practice is required to:
            </Typography>
            <ul className={classes.list}>
              <li>
                Maintain the privacy of your health information as required by
                law;
              </li>
              <li>
                Provide you with a notice of our duties and privacy practices as
                to the information we collect and maintain about you;
              </li>
              <li>Abide by the terms of this Notice:</li>
              <li>
                Notify you if we cannot accommodate a reasonable requests
                regarding methods to communicate health information with you.
              </li>
            </ul>
            <Typography variant="body1" gutterBottom>
              {
                'We reserve the right to amend, change, or eliminate provisions in our privacy practices and access practices and to enact new provisions regarding the protected health information we maintain. ff our information practices change, we will amend our Notice. You are entitled to receive a revised copy of the Notice by calling and requesting a copy of our "Notice" or by visiting our office and picking up a copy.'
              }
            </Typography>
            <Typography variant="body1" gutterBottom>
              <strong>To Request Information or File a Complaint</strong>
            </Typography>
            <Typography variant="body1" gutterBottom>
              If you have questions ,would like additional information. or want
              to report a problem regarding the handling of your information,
              you may contact our office administrator. Additionally, if you
              believe your privacy rights have been violated, you may file a
              written complaint at our office by delivering the written
              complaint to our office administrator. You may also file a
              complaint by mailing ir or e-mailing it to the Secretary of Health
              and Human Services whose street address and e-mail address is 200
              lndependency Ave S.W. Washington, D.C., 20201, phone#
              I-877-696-6775{' '}
              <a href="http://HHS.gov" target="_blank" rel="noreferrer">
                http://HHS.gov
              </a>
            </Typography>
            <ul className={classes.list}>
              <li>
                We cannot, and will not, require you to waive the right to file
                a complaint with the Secretary of Health and Human Services
                (HHS) as a condition of receiving treatment from the practice.
              </li>
              <li>
                We cannot, and will not, retaliate against you for filing a
                complaint with the Secretary.
              </li>
            </ul>
            <Typography variant="body1" gutterBottom>
              <strong>Other Disclosures and Uses</strong>
            </Typography>
            <Typography variant="body1" gutterBottom>
              <strong>Notification</strong>
            </Typography>
            <Typography variant="body1" gutterBottom>
              Unless you object, we may use or disclose your protected health
              information to notify, or assist in notifying, a family member,
              personal representative, or other person responsible for your
              care, about your location, and about your general condition, or
              your death.
            </Typography>
            <Typography variant="body1" gutterBottom>
              <strong>Communication with Family</strong>
            </Typography>
            <Typography variant="body1" gutterBottom>
              {
                "Using our best judgement, we may disclose to a family member, other relative, close personal friend, or any other person you identify, health information relevant to that person's involvement in your care or in payment for such care if you do not object or in an emergency."
              }
            </Typography>
            <Typography variant="body1" gutterBottom>
              <strong>Food and Drug Administration (FDA)</strong>
            </Typography>
            <Typography variant="body1" gutterBottom>
              We may disclose to the FDA your protected health information
              relating to adverse events with respect to products and product
              defects, or post-marketing surveillance information to enable
              product recalls, repairs, or replacements.
            </Typography>
            <Typography variant="body1" gutterBottom>
              <strong>Workman's Compensation</strong>
            </Typography>
            <Typography variant="body1" gutterBottom>
              {
                "If you are seeking compensation through Workman's Compensation, we may disclose your protected health information to the extent necessary to comply with laws relating to Workman's Compensation."
              }
            </Typography>
            <Typography variant="body1" gutterBottom>
              <strong>Public Health</strong>
            </Typography>
            <Typography variant="body1" gutterBottom>
              As required by law, we may disclose your protected health
              information to public health or legal authorities charged with
              preventing or controlling disease, injury, or disability.
            </Typography>
            <Typography variant="body1" gutterBottom>
              <strong>Abuse and Neglect</strong>
            </Typography>
            <Typography variant="body1" gutterBottom>
              We may disclose your protected health information to public
              authorities as allowed by law to report abuse or neglect.
            </Typography>
            <Typography variant="body1" gutterBottom>
              <strong>Correctional Institutions</strong>
            </Typography>
            <Typography variant="body1" gutterBottom>
              If you are an inmate ofa correctional institution, we may disclose
              to the institution, or its agents, your protected health
              information necessary for your health and the health and safety of
              other individuals.
            </Typography>
            <Typography variant="body1" gutterBottom>
              <strong>Law Enforcement</strong>
            </Typography>
            <Typography variant="body1" gutterBottom>
              We may disclose your protected health information for law
              enforcement purposes as required by law, such as when required by
              a court order, or in cases involving felony prosecutions, or to
              the extent an individual is in the custody of law enforcement.
            </Typography>
            <Typography variant="body1" gutterBottom>
              <strong>Health Oversight</strong>
            </Typography>
            <Typography variant="body1" gutterBottom>
              Federal law allows us to release your protected health information
              to appropriate oversight agencies or for the health oversight
              activities.
            </Typography>
            <Typography variant="body1" gutterBottom>
              <strong>Judicial/ Administrative Proceedings</strong>
            </Typography>
            <Typography variant="body1" gutterBottom>
              We may disclose your protected health information in the course of
              any judicial or administrative proceeding as allowed or required
              by law, with your consent, or as directed by a proper court order.
            </Typography>
            <Typography variant="body1" gutterBottom>
              <strong>Other Uses</strong>
            </Typography>
            <Typography variant="body1" gutterBottom>
              Other uses and disclosures besides those identified in this Notice
              will be made only as otherwise authorized by law or with your
              written authorization and you may revoke the authorization as
              previously provided.
            </Typography>
            <Typography variant="body1" gutterBottom>
              <strong>Website</strong>
            </Typography>
            <Typography variant="body1" gutterBottom>
              If we maintain a website that provides information about our
              entity, this Notice will be on the website. Effective Date:
              04/01/2003
            </Typography>
            <Typography variant="body1" gutterBottom>
              {
                "I hereby acknowledge that I have recieved a copy of this practice's Notice of Privacy Practices. I have been given the opportunity to ask any questions I may have regarding this Notice."
              }
            </Typography>
          </Grid>
        )}
      </BottomScrollListener>
      <Grid item xs={12}>
        <Checkbox
          field={profile.consent}
          onChange={onCheckboxChange(profile.consent)}
          disabled={!readConsent}
        />
      </Grid>
    </Grid>
  );
}

const { func, object, bool } = PropTypes;
ConsentForm.propTypes = {
  profile: object.isRequired,
  readConsent: bool,
  handleChange: func.isRequired,
  setReadConsent: func.isRequired,
  setProfile: func.isRequired,
};

export default ConsentForm;
