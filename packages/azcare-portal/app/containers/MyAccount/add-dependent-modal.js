import React, { useState } from 'react';
import PropTypes from 'prop-types';

import { Grid, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';

import TextField from 'components/TextField';
import DatePickerField from 'components/DatePickerField';
import SelectField from 'components/Select';
import StatesSelect from 'components/StatesSelect';
import DateFnsUtils from '@date-io/date-fns';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';

import { handleChange, handleDateChange } from 'utils/formHelper';

import { dependentModel } from 'containers/Register/model';
import moment from 'moment';

import countries from 'utils/countries.json';

const useStyles = makeStyles({
  formContainer: {
    width: '100%',
  },
});

export function AddDependentModal({ open, onToggle, onSubmit }) {
  const [register, setRegister] = useState({ ...dependentModel });

  const classes = useStyles();

  const onInputChange = field => event => {
    handleChange({
      field: field.id,
      state: register,
      event,
      saveStepFunc: setRegister,
    });
  };

  const onDateChange = field => value => {
    handleDateChange({
      field: field.id,
      state: register,
      value,
      saveStepFunc: setRegister,
    });
  };

  const onCountryChange = event => {
    register.state = {
      ...register.state,
      value: '',
      error: true,
      pristine: true,
    };

    handleChange({
      field: register.country.id,
      state: register,
      event,
      saveStepFunc: setRegister,
    });
  };

  return (
    <Dialog
      onClose={onToggle}
      aria-labelledby="Dependents-dialog"
      open={open}
      disableBackdropClick
      disableEscapeKeyDown
    >
      <DialogTitle id="Dependents-dialog">
        Provide Dependent Details
      </DialogTitle>
      <DialogContent>
        <Grid container spacing={2} direction="column">
          <Grid item xs={12} className={classes.formContainer}>
            <Grid container spacing={3} justify="center" alignItems="center">
              <Grid item xs={12} md={6}>
                <TextField
                  field={register.firstName}
                  variant="outlined"
                  onChange={onInputChange(register.firstName)}
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <TextField
                  field={register.middleName}
                  variant="outlined"
                  onChange={onInputChange(register.middleName)}
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <TextField
                  field={register.lastName}
                  variant="outlined"
                  onChange={onInputChange(register.lastName)}
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <DatePickerField
                    format="MM/dd/yyyy"
                    openTo="year"
                    minDate={moment().add(-18, 'year')}
                    maxDate={moment()}
                    clearable
                    fullWidth
                    field={register.dateOfBirth}
                    onChange={onDateChange(register.dateOfBirth)}
                    disableFuture
                  />
                </MuiPickersUtilsProvider>
              </Grid>
              <Grid item xs={12} md={6}>
                <SelectField
                  field={register.gender}
                  options={[
                    { name: '', value: '' },
                    { name: 'Male', value: '12000' },
                    { name: 'Female', value: '12001' },
                  ]}
                  variant="outlined"
                  onChange={onInputChange(register.gender)}
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <TextField
                  field={register.address1}
                  variant="outlined"
                  onChange={onInputChange(register.address1)}
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <TextField
                  field={register.address2}
                  variant="outlined"
                  onChange={onInputChange(register.address2)}
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <TextField
                  field={register.city}
                  variant="outlined"
                  onChange={onInputChange(register.city)}
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <StatesSelect
                  field={register.state}
                  variant="outlined"
                  onChange={onInputChange(register.state)}
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <SelectField
                  field={register.country}
                  options={countries}
                  variant="outlined"
                  onChange={onCountryChange}
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <TextField
                  field={register.zipCode}
                  variant="outlined"
                  onChange={onInputChange(register.zipCode)}
                />
              </Grid>
              <Grid item xs={12} md={6}>
                &nbsp;
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </DialogContent>
      <DialogActions>
        <Button onClick={onToggle} color="primary">
          Cancel
        </Button>
        <Button onClick={() => onSubmit(register, setRegister)} color="primary">
          Add Dependent
        </Button>
      </DialogActions>
    </Dialog>
  );
}

const { func, bool } = PropTypes;
AddDependentModal.propTypes = {
  onToggle: func.isRequired,
  onSubmit: func.isRequired,
  open: bool.isRequired,
};

export default AddDependentModal;
