/**
 *
 * Appointment
 *
 */

import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { push } from 'connected-react-router';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';

import KeyboardEventHandler from 'react-keyboard-event-handler';

import moment from 'moment';
import TextArea from 'components/TextArea';
import GradientButton from 'components/GradientButton';
import SelectField from 'components/Select';
import DateTimePickerField from 'components/DateTimePickerField';

import { Grid, Typography, Card } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';

import {
  handleChange,
  highlightFormErrors,
  handleDateChange,
  extractFormValues,
} from 'utils/formHelper';

import {
  makeSelectAppointment,
  makeSelectSignin,
  makePatientImpersonation,
} from 'containers/App/selectors';
import { sendAppointment, resetAppointmentData } from 'containers/App/actions';
import model from './model';

const useStyles = makeStyles(theme => ({
  root: {
    padding: '20px 20px 50px 20px',
    borderRadius: 10,
    boxShadow: theme.palette.boxShadow.main,
  },
  wrapper: {
    height: 'calc(100vh - 115px)',
    display: 'flex',
    flex: 1,
    justifyContent: 'center',
  },
  headerContainer: {
    paddingBottom: 20,
  },
  footerContainer: {
    padding: '20px 0',
  },
  container: {
    padding: '10px 0',
  },
  text: {
    padding: 10,
    textAlign: 'center',
  },
  buttonLabel: {
    fontWeight: 'bold',
    color: theme.typography.color.dark,
  },
}));

function Appointment({
  dispatch,
  appointmentData,
  doSendAppointment,
  doResetAppointment,
  selectCurrentUser,
  impersonatedPatient,
  enqueueSnackbar,
}) {
  const [appointment, setAppointment] = useState({ ...model });
  const classes = useStyles();

  useEffect(() => {
    if (appointmentData.data && !appointmentData.error) {
      enqueueSnackbar('Your appointment was sent!', {
        variant: 'success',
      });
      setAppointment({ ...model });
      dispatch(doResetAppointment());
      setTimeout(() => dispatch(push('/')), 5000);
    }
  }, [appointmentData]);

  const onInputChange = field => event => {
    handleChange({
      field: field.id,
      state: appointment,
      event,
      saveStepFunc: setAppointment,
    });
  };

  const onDateChange = field => value => {
    handleDateChange({
      field: field.id,
      state: appointment,
      value,
      saveStepFunc: setAppointment,
    });
  };

  const handleSubmit = () => {
    if (!appointment.completed) {
      highlightFormErrors(appointment, setAppointment);
      enqueueSnackbar('Please fill out all fields marked with asterisk *', {
        variant: 'error',
      });
    } else {
      const { patient } = selectCurrentUser.data;
      const formValues = extractFormValues(appointment);
      const firstOption = moment(formValues.firstOption).format(
        'YYYY-MM-DD HH:mm:ss',
      );
      const secondOption = moment(formValues.secondOption).format(
        'YYYY-MM-DD HH:mm:ss',
      );

      /*eslint-disable */
      const patientNotes = `\nAppointment with: ${
        formValues.appointmentWith
      }\n \nDetails:\n${formValues.patientNotes}.${
        impersonatedPatient.data
          ? ` This appointment is for my dependent: ${
              impersonatedPatient.data.patient.firstName
            } ${impersonatedPatient.data.patient.lastName}`
          : ''
      }`;
      /* eslint-enable */

      const params = {
        patientID: patient.id,
        firstChoice: firstOption,
        secondChoice: secondOption,
        patientNotes,
      };

      if (patient) dispatch(doSendAppointment(params));
    }
  };

  return (
    <Grid
      container
      spacing={3}
      className={classes.wrapper}
      alignContent="center"
    >
      <Grid item xs={12} md={7}>
        <Card className={classes.root}>
          <div className={classes.headerContainer}>
            <Typography variant="h5">
              MAKE AN APPOINTMENT
              <Typography variant="body1" color="textPrimary">
                Please provide the required details of your appointment.
              </Typography>
            </Typography>
          </div>
          <Grid container spacing={2} className={classes.container}>
            <Grid item xs={12} sm={6}>
              <DateTimePickerField
                keyEvents={{
                  handleKeys: ['enter'],
                  onKeyEvent: handleSubmit,
                }}
                openTo="date"
                clearable
                fullWidth
                field={appointment.firstChoice}
                onChange={onDateChange(appointment.firstChoice)}
                disablePast
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <DateTimePickerField
                keyEvents={{
                  handleKeys: ['enter'],
                  onKeyEvent: handleSubmit,
                }}
                openTo="date"
                clearable
                fullWidth
                field={appointment.secondChoice}
                onChange={onDateChange(appointment.secondChoice)}
                disablePast
              />
            </Grid>
          </Grid>
          <SelectField
            keyEvents={{
              handleKeys: ['enter'],
              onKeyEvent: handleSubmit,
            }}
            field={appointment.appointmentWith}
            options={[
              { name: '', value: '' },
              { name: 'Our Care Team', value: 'Our Care Team' },
              { name: 'Healthcare Navigator', value: 'Healthcare Navigator' },
            ]}
            variant="outlined"
            onChange={onInputChange(appointment.appointmentWith)}
          />
          <KeyboardEventHandler
            handleKeys={['enter']}
            onKeyEvent={handleSubmit}
          >
            <TextArea
              field={appointment.patientNotes}
              variant="outlined"
              rowSize={10}
              onChange={onInputChange(appointment.patientNotes)}
            />
          </KeyboardEventHandler>
          <Grid container spacing={1} className={classes.footerContainer}>
            <Grid item xs={12} sm={4}>
              <GradientButton
                size="large"
                onClick={handleSubmit}
                loading={appointmentData.loading}
                fullWidth
              >
                SEND
              </GradientButton>
            </Grid>
            <Grid item xs={12} sm={4}>
              <GradientButton
                size="large"
                onClick={() => dispatch(push('/'))}
                loading={appointmentData.loading}
                fullWidth
                variant="outlined"
              >
                <Typography className={classes.buttonLabel}>CANCEL</Typography>
              </GradientButton>
            </Grid>
          </Grid>
        </Card>
      </Grid>
    </Grid>
  );
}

const { func, object } = PropTypes;
Appointment.propTypes = {
  enqueueSnackbar: func.isRequired,
  dispatch: func.isRequired,
  doSendAppointment: func.isRequired,
  doResetAppointment: func.isRequired,
  appointmentData: object.isRequired,
  selectCurrentUser: object.isRequired,
  impersonatedPatient: object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  appointmentData: makeSelectAppointment(),
  selectCurrentUser: makeSelectSignin(),
  impersonatedPatient: makePatientImpersonation(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    doSendAppointment: sendAppointment,
    doResetAppointment: resetAppointmentData,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(Appointment);
