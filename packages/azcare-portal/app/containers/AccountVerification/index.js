/**
 *
 * Account Verification
 *
 */

import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { push } from 'connected-react-router';

import KeyboardEventHandler from 'react-keyboard-event-handler';

import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { compose } from 'redux';

import { Grid, Typography, Card } from '@material-ui/core';

import TextField from 'components/TextField';
import GradientButton from 'components/GradientButton';

import {
  accountVerification,
  resetAuthFields,
  resetVerificationCode,
  setNotificationType,
} from 'containers/App/actions';
import {
  makeAccountVerification,
  makeSelectVerificationCode,
} from 'containers/App/selectors';

import Link from '@material-ui/core/Link';
import MyLink from 'components/MyLink';

import { makeStyles } from '@material-ui/styles';

import {
  initialField,
  initialState,
  handleChange,
  highlightFormErrors,
  extractFormValues,
  formatPhoneNumber,
} from 'utils/formHelper';

const employeeValidator = {
  ...initialField,
  id: 'employeeValidator',
  caption: 'Enter Phone or Email',
  errorMessage: 'Email or Phone is required',
};
const employerCode = {
  ...initialField,
  id: 'employerCode',
  caption: 'Employer Code',
};

const useStyles = makeStyles(theme => ({
  root: {
    padding: 20,
    borderRadius: 10,
    boxShadow: theme.palette.boxShadow.main,
  },
  wrapper: {
    height: 'calc(100vh - 115px)',
    display: 'flex',
    flex: 1,
    justifyContent: 'center',
  },
  footerContainer: {
    padding: '10px 0',
    borderTop: `1px solid ${theme.palette.background.default}`,
    textAlign: 'center',
    margin: `15px 0`,
    display: 'flex',
    flexDirection: 'column',
  },
  text: {
    padding: 10,
    textAlign: 'center',
  },
  buttonLabel: {
    fontWeight: 'bold',
    color: theme.typography.color.dark,
  },
  paddingBottom: {
    paddingBottom: 10,
  },
}));
export function AccountVerification({
  dispatch,
  account,
  verificationCode,
  doAccountVerification,
  history,
  enqueueSnackbar,
  doResetAuthFields,
  doResetVerificationCode,
  doSetNotificationType,
}) {
  const [verification, setVerification] = useState({
    employeeValidator,
    employerCode,
    ...initialState,
  });

  const classes = useStyles();

  useEffect(() => {
    if (account.data && account.data.member) {
      dispatch(doResetAuthFields());
      dispatch(push('/verify-code'));
    } else if (account.error && account.error.status === 401) {
      enqueueSnackbar('Invalid Email/Phone or Employer Code', {
        variant: 'error',
      });
    } else if (account.error && account.error.status === 403) {
      enqueueSnackbar('Your account has already been verified.', {
        variant: 'error',
      });
    }
  }, [account]);

  useEffect(() => {
    if (verificationCode.data) {
      enqueueSnackbar(
        `New Authentication code sent to your email ${
          account.data.member.email
        }`,
        {
          variant: 'success',
        },
      );
      dispatch(doResetVerificationCode());
    }
  }, [verificationCode]);

  useEffect(() => {
    const {
      location: { search },
    } = history;
    if (search !== '') evaluateQs(search);
  }, [history]);

  const evaluateQs = search => {
    const params = search.split('&');
    const employer = params[0].replace('?employer=', '');
    setVerification({
      ...verification,
      employerCode: {
        ...employerCode,
        value: employer,
        error: false,
        pristine: false,
      },
    });
  };
  const onInputChange = field => event => {
    handleChange({
      field: field.id,
      state: verification,
      event,
      saveStepFunc: setVerification,
    });
  };

  const handleSubmit = () => {
    if (!verification.completed) {
      enqueueSnackbar('Please input your Email or Phone and Employer Code', {
        variant: 'error',
      });
      highlightFormErrors(verification, setVerification);
    } else {
      const params = extractFormValues(verification);
      const isEmail = params.employeeValidator.includes('@');
      dispatch(doSetNotificationType(isEmail));
      dispatch(
        doAccountVerification({
          ...params,
          employeeValidator: parseInt(params.employeeValidator, 10)
            ? formatPhoneNumber(params.employeeValidator)
            : params.employeeValidator,
        }),
      );
    }
  };

  return (
    <Grid
      container
      spacing={2}
      direction="column"
      alignContent="center"
      className={classes.wrapper}
    >
      <Grid item xs={12} md={4}>
        <Card className={classes.root}>
          <Typography
            variant="h4"
            color="primary"
            className={classes.paddingBottom}
          >
            Account Verification
            <Typography variant="body1" color="textPrimary">
              Verify your account by providing your Phone Number or Email
              address and Employer code
            </Typography>
          </Typography>
          <Grid container spacing={1}>
            <Grid item xs={12}>
              <KeyboardEventHandler
                handleKeys={['enter']}
                onKeyEvent={handleSubmit}
              >
                <TextField
                  field={verification.employeeValidator}
                  variant="outlined"
                  onChange={onInputChange(verification.employeeValidator)}
                />
              </KeyboardEventHandler>
            </Grid>
            <Grid item xs={12}>
              <KeyboardEventHandler
                handleKeys={['enter']}
                onKeyEvent={handleSubmit}
              >
                <TextField
                  field={verification.employerCode}
                  variant="outlined"
                  onChange={onInputChange(verification.employerCode)}
                />
              </KeyboardEventHandler>
            </Grid>
          </Grid>
          <Grid item xs={12} className={classes.text}>
            <Typography
              variant="body1"
              component="span"
              className={classes.text}
            >
              Email or Phone not specified? You can verify using your{' '}
              <Link
                to="/verify2"
                component={MyLink}
                variant="body1"
                color="primary"
              >
                <strong>Member Id or Social Security Number</strong>
              </Link>
            </Typography>
          </Grid>
          <GradientButton
            variant="contained"
            size="large"
            onClick={handleSubmit}
            loading={account.loading}
            fullWidth
            className={classes.paddingBottom}
          >
            {'Proceed'}
          </GradientButton>
          <Grid xs={12} className={classes.footerContainer}>
            <Typography
              variant="body1"
              component="span"
              className={classes.text}
            >
              Account already verified?
            </Typography>
            <GradientButton
              onClick={() => dispatch(push('/login'))}
              variant="outlined"
              size="large"
              fullWidth
              className={classes.text}
            >
              <Typography className={classes.buttonLabel}>
                {'SIGN IN'}
              </Typography>
            </GradientButton>
          </Grid>
        </Card>
      </Grid>
    </Grid>
  );
}

const { func, object } = PropTypes;

AccountVerification.propTypes = {
  dispatch: func.isRequired,
  account: object.isRequired,
  verificationCode: object.isRequired,
  doResetAuthFields: func.isRequired,
  doAccountVerification: func.isRequired,
  doResetVerificationCode: func.isRequired,
  history: object,
  enqueueSnackbar: func.isRequired,
  doSetNotificationType: func.isRequired,
};
const mapStateToProps = createStructuredSelector({
  account: makeAccountVerification(),
  verificationCode: makeSelectVerificationCode(),
});
function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    doAccountVerification: accountVerification,
    doResetAuthFields: resetAuthFields,
    doResetVerificationCode: resetVerificationCode,
    doSetNotificationType: setNotificationType,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(AccountVerification);
