/**
 *
 * Account Verification
 *
 */

import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { push } from 'connected-react-router';
import { useParams } from 'react-router-dom';

import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { compose } from 'redux';
import moment from 'moment';

import { Grid, Typography, CircularProgress, Button } from '@material-ui/core';

import TextField from 'components/TextField';
import GradientButton from 'components/GradientButton';

import {
  verifySsn,
  resetAuthFields,
  resetVerificationCode,
  setNotificationType,
} from 'containers/App/actions';
import { makeSelectVerifySsn } from 'containers/App/selectors';

import Link from '@material-ui/core/Link';
import DatePickerField from 'components/DatePickerField';
import MyLink from 'components/MyLink';

import {
  initialField,
  initialState,
  handleChange,
  highlightFormErrors,
  handleDateChange,
  extractFormValues,
} from 'utils/formHelper';

const employerCode = {
  ...initialField,
  id: 'employerCode',
  caption: 'Employer Code',
};
const ssn = {
  ...initialField,
  id: 'ssn',
  caption: 'Member Id / Social Security Number',
};
const dateOfBirth = {
  ...initialField,
  id: 'dateOfBirth',
  caption: 'Date of Birth',
};
export function AccountVerificationWithSsn({
  dispatch,
  verifiedAccount,
  doVerifySsn,
  enqueueSnackbar,
  doResetAuthFields,
}) {
  const [verification, setVerification] = useState({
    employerCode,
    ssn,
    dateOfBirth,
    ...initialState,
  });

  const action = (
    <Button onClick={() => dispatch(push('/login'))}>Login</Button>
  );
  useEffect(() => {
    if (verifiedAccount && verifiedAccount.data) {
      dispatch(doResetAuthFields());
      dispatch(push('/info-ssn'));
    } else if (
      verifiedAccount &&
      verifiedAccount.error &&
      verifiedAccount.error.status === 401
    ) {
      enqueueSnackbar('Invalid Information has been provided, please verify', {
        variant: 'error',
      });
    } else if (
      verifiedAccount &&
      verifiedAccount.error &&
      verifiedAccount.error.status === 409
    ) {
      enqueueSnackbar('Member already verified', {
        variant: 'warning',
        autoHideDuration: 3000,
        action,
      });
    }
  }, [verifiedAccount]);

  const { id } = useParams();
  useEffect(() => {
    setVerification({
      ...verification,
      employerCode: {
        ...employerCode,
        value: id !== undefined ? id : undefined,
        error: id === undefined,
        pristine: id === undefined,
      },
    });
  }, []);

  const onInputChange = field => event => {
    handleChange({
      field: field.id,
      state: verification,
      event,
      saveStepFunc: setVerification,
    });
  };

  const onDateChange = field => value => {
    const years = moment().diff(moment(value), 'years');
    if (years > 18) {
      handleDateChange({
        field: field.id,
        state: verification,
        value: moment(value),
        saveStepFunc: setVerification,
      });
    } else {
      handleDateChange({
        field: field.id,
        state: verification,
        value: null,
        error: true,

        saveStepFunc: setVerification,
      });
    }
  };
  const handleSubmit = () => {
    if (!verification.completed) {
      enqueueSnackbar('Please fill out all fields', {
        variant: 'error',
      });
      highlightFormErrors(verification, setVerification);
    } else {
      const params = extractFormValues(verification);
      const { dateOfBirth: dob, ...rest } = params;

      dispatch(
        doVerifySsn({ ...rest, dateOfBirth: moment(dob).format('YYYY-MM-DD') }),
      );
    }
  };

  return (
    <Grid container spacing={2} direction="column">
      <Grid item xs={12}>
        <Typography variant="h4" color="primary">
          Account Verification
          <Typography variant="body1" color="textPrimary">
            Verify your account by filling out this form
          </Typography>
        </Typography>
      </Grid>
      <Grid item xs={12} md={3}>
        <Grid container spacing={1}>
          <Grid item xs={12}>
            <TextField
              field={verification.employerCode}
              variant="outlined"
              onChange={onInputChange(verification.employerCode)}
              keyEvents={{ handleKeys: ['enter'], onKeyEvent: handleSubmit }}
            />
          </Grid>
        </Grid>
        <Grid item xs={12}>
          <TextField
            field={verification.ssn}
            variant="outlined"
            onChange={onInputChange(verification.ssn)}
            keyEvents={{ handleKeys: ['enter'], onKeyEvent: handleSubmit }}
          />
        </Grid>
        <Grid item xs={12}>
          <DatePickerField
            format="MM/dd/yyyy"
            openTo="year"
            initialFocusedDate="1980-01-01"
            maxDate={moment().add(-19, 'year')}
            clearable
            fullWidth
            field={verification.dateOfBirth}
            onChange={onDateChange(verification.dateOfBirth)}
            keyEvents={{ handleKeys: ['enter'], onKeyEvent: handleSubmit }}
            disableFuture
          />
        </Grid>
      </Grid>

      <Grid item xs={12} md={3}>
        <Typography variant="body1" component="span">
          Account already verified?{' '}
          <Link
            to="/login"
            component={MyLink}
            variant="body1"
            color="secondary"
          >
            <strong>Sign In</strong>
          </Link>
        </Typography>
      </Grid>
      <Grid item xs={12} md={3}>
        <GradientButton variant="contained" size="large" onClick={handleSubmit}>
          {verifiedAccount && verifiedAccount.loading && (
            <CircularProgress color="secondary" />
          )}
          {verifiedAccount && !verifiedAccount.loading && 'Proceed'}
        </GradientButton>
      </Grid>
    </Grid>
  );
}

const { func, object } = PropTypes;

AccountVerificationWithSsn.propTypes = {
  dispatch: func.isRequired,
  verifiedAccount: object.isRequired,
  doResetAuthFields: func.isRequired,
  doVerifySsn: func.isRequired,
  enqueueSnackbar: func.isRequired,
};
const mapStateToProps = createStructuredSelector({
  verifiedAccount: makeSelectVerifySsn(),
});
function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    doVerifySsn: verifySsn,
    doResetAuthFields: resetAuthFields,
    doResetVerificationCode: resetVerificationCode,
    doSetNotificationType: setNotificationType,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(AccountVerificationWithSsn);
