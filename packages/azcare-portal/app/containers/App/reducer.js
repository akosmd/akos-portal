/*
 *
 * Register reducer
 *
 */
import produce from 'immer';
import {
  APP_ERROR,
  SIGN_IN,
  SIGN_IN_SUCCESS,
  SIGN_IN_FAILURE,
  FORGOT_PASSWORD,
  FORGOT_PASSWORD_SUCCESS,
  FORGOT_PASSWORD_FAILURE,
  FORGOT_PASSWORD_SEND_CODE,
  FORGOT_PASSWORD_VERIFY_CODE,
  FORGOT_PASSWORD_NEW_PASSWORD,
  REGISTER,
  REGISTER_SUCCESS,
  REGISTER_FAILURE,
  SEND_VERIFICATION_CODE,
  SEND_VERIFICATION_CODE_SUCCESS,
  SEND_VERIFICATION_CODE_FAILURE,
  RESEND_VERIFICATION_CODE,
  RESEND_VERIFICATION_CODE_SUCCESS,
  RESEND_VERIFICATION_CODE_FAILURE,
  INXITE_URL,
  NIH_SEARCH,
  NIH_SEARCH_SUCCESS,
  NIH_SEARCH_FAILURE,
  CHECK_BENEFITS,
  CHECK_BENEFITS_SUCCESS,
  CHECK_BENEFITS_FAILURE,
  ACCOUNT_VERIFICATION,
  ACCOUNT_VERIFICATION_SUCCESS,
  ACCOUNT_VERIFICATION_FAILURE,
  VERIFY_AUTH,
  VERIFY_AUTH_SUCCESS,
  VERIFY_AUTH_FAILURE,
  GET_PATIENT_DETAIL,
  GET_PATIENT_DETAIL_SUCCESS,
  GET_PATIENT_DETAIL_FAILURE,
  UPDATE_PATIENT_DETAIL,
  UPDATE_PATIENT_DETAIL_SUCCESS,
  UPDATE_PATIENT_DETAIL_FAILURE,
  LOGOUT,
  RESET_REGISTER,
  SEND_APPOINTMENT,
  SEND_APPOINTMENT_SUCCESS,
  SEND_APPOINTMENT_FAILURE,
  RESET_APPOINTMENT_DATA,
  RESET_AUTH_FIELDS,
  RESET_RESEND_VERIFICATION_CODE,
  RESET_UPDATED_PATIENT,
  CHANGE_PASSWORD,
  CHANGE_PASSWORD_SUCCESS,
  CHANGE_PASSWORD_FAILURE,
  INIT_INXITE_SSO,
  INIT_INXITE_SSO_SUCCESS,
  INIT_INXITE_SSO_FAILURE,
  RESET_CHANGE_PASSWORD,
  VERIFICATION_TO_EMAIL,
  GET_PATIENT_DEPENDENTS,
  GET_PATIENT_DEPENDENTS_FAILURE,
  GET_PATIENT_DEPENDENTS_SUCCESS,
  SHOW_DEPENDENTS_MODAL,
  POST_PATIENT_IMPERSONATION,
  POST_PATIENT_IMPERSONATION_SUCCESS,
  POST_PATIENT_IMPERSONATION_FAILURE,
  RESET_PATIENT_IMPERSONATION,
  GET_APP_VERSION,
  GET_APP_VERSION_SUCCESS,
  GET_APP_VERSION_FAILURE,
  POST_FAMILY_INVITE,
  POST_FAMILY_INVITE_SUCCESS,
  POST_FAMILY_INVITE_FAILURE,
  RESET_FAMILY_INVITE_DATA,
  POST_ADD_DEPENDENT,
  POST_ADD_DEPENDENT_SUCCESS,
  POST_ADD_DEPENDENT_FAILURE,
  RESET_ADD_DEPENDENT,
  CHATBOT_AUTHENTICATION,
  CHATBOT_AUTHENTICATION_SUCCESS,
  CHATBOT_AUTHENTICATION_FAILURE,
  CHATROOM_WAITING_LIST,
  CHATROOM_WAITING_LIST_SUCCESS,
  CHATROOM_WAITING_LIST_FAILURE,
  NURSE_SIGN_IN,
  NURSE_SIGN_IN_SUCCESS,
  NURSE_SIGN_IN_FAILURE,
  SEND_CHAT_MESSAGE_TO_MEMBER,
  SEND_CHAT_MESSAGE_TO_MEMBER_SUCCESS,
  SEND_CHAT_MESSAGE_TO_MEMBER_FAILURE,
  MEMBER_JOIN_ROOM,
  MEMBER_JOIN_ROOM_SUCCESS,
  MEMBER_JOIN_ROOM_FAILURE,
  MEMBER_HAS_JOINED_ROOM,
  SEND_CHAT_MESSAGE_TO_NURSE,
  SEND_CHAT_MESSAGE_TO_NURSE_SUCCESS,
  SEND_CHAT_MESSAGE_TO_NURSE_FAILURE,
  SELECTED_USER_TO_CHAT,
  NURSE_CONVERSATIONS,
  RESET_SIGNIN,
  GET_FAMILY_MEMBERS,
  GET_FAMILY_MEMBERS_FAILURE,
  GET_FAMILY_MEMBERS_SUCCESS,
  PUT_FAMILY_MEMBER_APPROVAL,
  PUT_FAMILY_MEMBER_APPROVAL_FAILURE,
  PUT_FAMILY_MEMBER_APPROVAL_SUCCESS,
  RESET_FAMILY_MEMBER_APPROVAL,
  LEAVE_CHAT_ROOM,
  LEAVE_CHAT_ROOM_SUCCESS,
  LEAVE_CHAT_ROOM_FAILURE,
  GET_CHAT_MESSAGES,
  GET_CHAT_MESSAGES_SUCCESS,
  GET_CHAT_MESSAGES_FAILURE,
  RESET_CHAT_MESSAGES,
  POST_END_CONVERSATION,
  POST_END_CONVERSATION_SUCCESS,
  POST_END_CONVERSATION_FAILURE,
  NOTIFY_NURSE,
  NOTIFY_NURSE_SUCCESS,
  NOTIFY_NURSE_FAILURE,
  GET_MEMBER_INSURANCE_INFO_LIST,
  GET_MEMBER_INSURANCE_INFO_LIST_SUCCESS,
  GET_MEMBER_INSURANCE_INFO_LIST_FAILURE,
  GET_MEMBER_INSURANCE_INFO,
  GET_MEMBER_INSURANCE_INFO_SUCCESS,
  GET_MEMBER_INSURANCE_INFO_FAILURE,
  POST_MEMBER_INSURANCE_INFO,
  POST_MEMBER_INSURANCE_INFO_SUCCESS,
  POST_MEMBER_INSURANCE_INFO_FAILURE,
  RESET_POST_MEMBER_INSURANCE_INFO,
  PUT_MEMBER_INSURANCE_INFO,
  PUT_MEMBER_INSURANCE_INFO_SUCCESS,
  PUT_MEMBER_INSURANCE_INFO_FAILURE,
  RESET_PUT_MEMBER_INSURANCE_INFO,
  VERIFY_SSN,
  VERIFY_SSN_SUCCESS,
  VERIFY_SSN_FAILURE,
  UPDATE_PATIENT_COMMS,
  UPDATE_PATIENT_COMMS_SUCCESS,
  UPDATE_PATIENT_COMMS_FAILURE,
  SEND_GI_MESSAGE,
  SEND_GI_MESSAGE_SUCCESS,
  SEND_GI_MESSAGE_FAILURE,
  RESET_GI_MESSAGE,
  FIND_PATIENT,
  FIND_PATIENT_SUCCESS,
  FIND_PATIENT_FAILURE,
  CLEAR_FORGOT_PASSWORD,
  FORGOT_PASSWORD_RESEND_CODE,
} from './constants';

export const patientMigrations = {
  1.2: previousVersionState => ({
    patient: {
      change: previousVersionState.patient,
      lastUpdate: new Date(),
    },
  }),
};

const defaultProps = {
  loading: false,
  data: false,
  error: false,
  saved: false,
};

export const initialState = {
  signin: {
    ...defaultProps,
  },
  forgotPassword: {
    ...defaultProps,
    data: {
      type: 0,
    },
  },
  register: {
    ...defaultProps,
  },
  verification: {
    ...defaultProps,
  },

  inxiteUrl: '',
  appError: false,
  nihSearchResult: {
    ...defaultProps,
  },
  checkBenefits: {
    ...defaultProps,
  },
  accountVerification: {
    ...defaultProps,
  },
  verifyAuth: {
    ...defaultProps,
  },
  patientDetail: {
    ...defaultProps,
  },
  appointment: {
    ...defaultProps,
  },
  updatedPatient: {
    ...defaultProps,
  },
  changePassword: {
    ...defaultProps,
  },
  inxiteSso: {
    ...defaultProps,
  },
  sentToEmail: {
    ...defaultProps,
  },
  patientDependents: {
    ...defaultProps,
  },
  showDependentsModal: {
    show: false,
    shownAtleastOnce: false,
  },
  patientImpersonation: {
    ...defaultProps,
  },
  showInviteMemberModal: {
    show: false,
  },
  familyInvite: {
    ...defaultProps,
  },
  addDependent: {
    ...defaultProps,
  },
  chatbotAuth: {
    ...defaultProps,
  },
  chatWaitingRoom: {
    ...defaultProps,
  },
  nurseSignin: {
    ...defaultProps,
  },
  messageToMember: {
    ...defaultProps,
  },
  messageToNurse: {
    ...defaultProps,
  },
  memberJoinRoom: {
    ...defaultProps,
  },
  memberJoinRoomStatus: false,
  selectedUserToChat: false,
  nurseConversations: [],
  familyMembers: {
    ...defaultProps,
  },
  approveFamilyMember: {
    ...defaultProps,
  },
  leaveRoom: {
    ...defaultProps,
  },
  chatMessages: {
    ...defaultProps,
  },
  endConversation: {
    ...defaultProps,
  },
  notifyNurse: {
    ...defaultProps,
  },
  memberInsuranceInfoList: {
    ...defaultProps,
  },
  memberInsuranceInfo: {
    ...defaultProps,
  },
  addMemberInsuranceInfo: {
    ...defaultProps,
  },
  updateMemberInsuranceInfo: {
    ...defaultProps,
  },
  verifySsn: {
    ...defaultProps,
  },
  updatePatientComms: {
    ...defaultProps,
  },
  giMessage: {
    ...defaultProps,
  },
  findPatient: {
    ...defaultProps,
  },
};

/* eslint-disable default-case, no-param-reassign */
const registerReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case SIGN_IN: {
        draft.signin = {
          loading: true,
          data: false,
          error: false,
        };
        draft.verification = {
          ...defaultProps,
          params: action.payload,
        };

        break;
      }
      case SIGN_IN_SUCCESS:
        draft.signin = {
          loading: false,
          data: action.payload || false,
          error: false,
        };

        break;
      case SIGN_IN_FAILURE:
        draft.signin = {
          loading: false,
          data: false,
          error: action.payload,
        };
        break;
      case FORGOT_PASSWORD:
        draft.forgotPassword = {
          ...initialState.forgotPassword,
          loading: true,
          error: false,
        };
        break;
      case FORGOT_PASSWORD_SUCCESS:
        draft.forgotPassword = {
          loading: false,
          data: action.payload,
          error: false,
        };

        break;

      case FORGOT_PASSWORD_FAILURE:
        draft.forgotPassword = {
          loading: false,
          data: action.payload.data,
          error: action.payload.error,
        };
        break;
      case FORGOT_PASSWORD_NEW_PASSWORD:
      case FORGOT_PASSWORD_VERIFY_CODE:
      case FORGOT_PASSWORD_SEND_CODE:
        draft.forgotPassword = {
          ...draft.forgotPassword,
          loading: true,
        };
        break;
      case CLEAR_FORGOT_PASSWORD:
        draft.forgotPassword = {
          ...initialState.forgotPassword,
        };
        break;
      case FORGOT_PASSWORD_RESEND_CODE:
        draft.forgotPassword = {
          data: {
            ...action.payload,
            type: 1,
          },
        };
        break;
      case LOGOUT: {
        const keys = Object.keys(initialState);
        keys.forEach(key => {
          draft[key] = initialState[key];
        });

        break;
      }
      case RESET_REGISTER:
        draft.register = {
          loading: true,
          data: false,
          error: false,
        };
        break;
      case RESET_AUTH_FIELDS:
        draft.verification = {
          loading: false,
          data: false,
          error: false,
        };
        draft.verifyAuth = {
          loading: false,
          data: false,
          error: false,
        };
        draft.verifyAuth = {
          loading: false,
          data: false,
          error: false,
        };
        draft.register = {
          loading: false,
          data: false,
          error: false,
        };
        draft.updatedPatient = {
          loading: false,
          data: false,
          error: false,
        };
        break;

      case REGISTER:
        draft.register = {
          loading: true,
          data: false,
          error: false,
        };
        break;
      case REGISTER_SUCCESS:
        draft.register = {
          loading: false,
          data: action.payload || false,
          error: false,
        };
        break;
      case REGISTER_FAILURE:
        draft.register = {
          loading: false,
          data: false,
          error: action.payload,
        };
        break;
      case RESEND_VERIFICATION_CODE:
      case SEND_VERIFICATION_CODE:
        draft.verification = {
          loading: true,
          data: false,
          error: false,
        };
        break;
      case RESEND_VERIFICATION_CODE_SUCCESS:
      case SEND_VERIFICATION_CODE_SUCCESS:
        draft.verification = {
          loading: false,
          data: action.payload || false,
          error: false,
        };
        break;
      case RESEND_VERIFICATION_CODE_FAILURE:
      case SEND_VERIFICATION_CODE_FAILURE:
        draft.verification = {
          loading: false,
          data: false,
          error: action.payload,
        };
        break;
      case NIH_SEARCH:
        draft.nihSearchResult = {
          loading: true,
          data: false,
          error: false,
        };
        break;
      case NIH_SEARCH_SUCCESS:
        draft.nihSearchResult = {
          loading: false,
          data: action.payload,
          error: false,
        };
        break;
      case NIH_SEARCH_FAILURE:
        draft.nihSearchResult = {
          loading: false,
          data: false,
          error: action.payload,
        };
        break;
      case ACCOUNT_VERIFICATION:
        draft.accountVerification = {
          loading: true,
          data: false,
          error: false,
        };
        break;
      case ACCOUNT_VERIFICATION_SUCCESS:
        draft.accountVerification = {
          loading: false,
          data: action.payload,
          error: false,
        };
        break;
      case ACCOUNT_VERIFICATION_FAILURE:
        draft.accountVerification = {
          loading: false,
          data: false,
          error: action.payload,
        };
        break;
      case UPDATE_PATIENT_DETAIL:
        draft.updatedPatient = {
          saving: true,
          error: false,
        };
        break;
      case UPDATE_PATIENT_DETAIL_SUCCESS:
        draft.updatedPatient = {
          loading: false,
          data: action.payload,
          error: false,
        };
        break;
      case UPDATE_PATIENT_DETAIL_FAILURE:
        draft.updatedPatient = {
          loading: false,
          data: false,
          error: action.payload,
        };
        break;
      case VERIFY_AUTH:
        draft.verifyAuth = {
          loading: true,
          data: false,
          error: false,
        };
        break;
      case VERIFY_AUTH_SUCCESS:
        draft.verifyAuth = {
          loading: false,
          data: action.payload,
          error: false,
        };
        break;
      case VERIFY_AUTH_FAILURE: {
        draft.verifyAuth = {
          loading: false,
          data: false,
          error: action.payload,
        };
        break;
      }
      case GET_PATIENT_DETAIL:
        draft.patientDetail = {
          loading: true,
          data: false,
          error: false,
        };
        break;
      case GET_PATIENT_DETAIL_SUCCESS:
        draft.patientDetail = {
          loading: false,
          data: action.payload,
          error: false,
        };
        break;
      case GET_PATIENT_DETAIL_FAILURE: {
        draft.patientDetail = {
          loading: false,
          data: false,
          error: action.payload,
        };
        break;
      }
      case CHECK_BENEFITS:
        draft.checkBenefits = {
          loading: true,
          data: false,
          error: false,
        };
        break;
      case CHECK_BENEFITS_SUCCESS:
        draft.checkBenefits = {
          loading: false,
          data: action.payload,
          error: false,
        };
        break;
      case CHECK_BENEFITS_FAILURE:
        draft.checkBenefits = {
          loading: false,
          data: false,
          error: action.payload,
        };
        break;
      case INIT_INXITE_SSO:
        draft.inxiteSso = {
          loading: true,
          data: false,
          error: false,
        };
        break;
      case INIT_INXITE_SSO_SUCCESS:
        draft.inxiteSso = {
          loading: false,
          data: action.payload,
          error: false,
        };
        break;
      case INIT_INXITE_SSO_FAILURE:
        draft.inxiteSso = {
          loading: false,
          data: false,
          error: action.payload,
        };
        break;
      case APP_ERROR:
        draft.appError = {
          error: action.payload,
        };
        break;
      case INXITE_URL:
        draft.inxiteUrl = action.payload;
        break;
      case SEND_APPOINTMENT:
        draft.appointment = {
          loading: true,
          data: false,
          error: false,
        };
        break;
      case SEND_APPOINTMENT_SUCCESS:
        draft.appointment = {
          loading: false,
          data: action.payload || false,
          error: false,
        };
        break;
      case SEND_APPOINTMENT_FAILURE:
        draft.appointment = {
          loading: false,
          data: false,
          error: action.payload,
        };
        break;
      case CHATROOM_WAITING_LIST:
        draft.chatWaitingRoom = {
          loading: true,
          data: false,
          error: false,
        };
        break;
      case CHATROOM_WAITING_LIST_SUCCESS:
        draft.chatWaitingRoom = {
          loading: false,
          data: action.payload || false,
          error: false,
        };
        break;
      case CHATROOM_WAITING_LIST_FAILURE:
        draft.chatWaitingRoom = {
          loading: false,
          data: false,
          error: action.payload,
        };
        break;
      case RESET_APPOINTMENT_DATA:
        draft.appointment = {
          loading: false,
          data: false,
          error: false,
        };
        break;
      case RESET_RESEND_VERIFICATION_CODE:
        draft.verification = {
          loading: false,
          data: false,
          error: false,
        };
        break;
      case RESET_UPDATED_PATIENT:
        draft.updatedPatient = {
          loading: false,
          data: false,
          error: false,
        };
        break;
      case CHANGE_PASSWORD:
        draft.changePassword = {
          loading: true,
          data: false,
          error: false,
        };
        break;
      case CHANGE_PASSWORD_SUCCESS:
        draft.changePassword = {
          loading: false,
          data: action.payload,
          error: false,
        };
        break;
      case CHANGE_PASSWORD_FAILURE:
        draft.changePassword = {
          loading: false,
          data: false,
          error: action.payload,
        };
        break;
      case RESET_CHANGE_PASSWORD:
        draft.changePassword = {
          loading: false,
          data: false,
          error: false,
        };
        break;
      case VERIFICATION_TO_EMAIL:
        draft.sentToEmail = {
          loading: false,
          data: action.payload,
          error: false,
        };
        break;
      case GET_PATIENT_DEPENDENTS:
        draft.patientDependents = {
          ...draft.patientDependents,
          loading: true,
          error: false,
        };
        break;
      case GET_PATIENT_DEPENDENTS_SUCCESS:
        draft.patientDependents = {
          loading: false,
          data: action.payload,
          error: false,
        };
        break;
      case GET_PATIENT_DEPENDENTS_FAILURE:
        draft.patientDependents = {
          loading: false,
          data: false,
          error: action.payload,
        };
        break;

      case SHOW_DEPENDENTS_MODAL:
        draft.showDependentsModal.show = action.payload;
        break;

      case POST_PATIENT_IMPERSONATION:
        draft.patientImpersonation = {
          loading: true,
          data: false,
          error: false,
        };
        break;

      case POST_PATIENT_IMPERSONATION_SUCCESS:
        draft.patientImpersonation = {
          loading: false,
          data: action.payload,
          error: false,
        };
        draft.showDependentsModal.shownAtleastOnce = true;
        break;

      case POST_PATIENT_IMPERSONATION_FAILURE:
        draft.patientImpersonation = {
          loading: false,
          data: false,
          error: action.payload,
        };
        break;

      case RESET_PATIENT_IMPERSONATION:
        draft.patientImpersonation = {
          ...defaultProps,
        };
        draft.showDependentsModal.shownAtleastOnce = true;
        break;
      case GET_APP_VERSION:
        draft.appVersion = {
          loading: true,
          data: undefined,
          error: false,
        };
        break;
      case GET_APP_VERSION_SUCCESS:
        draft.appVersion = {
          loading: false,
          data: action.payload,
          error: false,
        };
        break;
      case GET_APP_VERSION_FAILURE:
        draft.appVersion = {
          loading: false,
          data: action.payload,
          error: true,
        };
        break;
      case POST_FAMILY_INVITE:
        draft.familyInvite = {
          loading: true,
          data: undefined,
          error: false,
        };
        break;
      case POST_FAMILY_INVITE_SUCCESS:
        draft.familyInvite = {
          loading: false,
          data: action.payload,
          error: false,
        };
        break;
      case POST_FAMILY_INVITE_FAILURE:
        draft.familyInvite = {
          loading: false,
          data: action.payload,
          error: true,
        };
        break;
      case RESET_FAMILY_INVITE_DATA:
        draft.familyInvite = {
          loading: false,
          data: false,
          error: false,
        };
        break;

      case POST_ADD_DEPENDENT:
        draft.addDependent = {
          loading: true,
          data: undefined,
          error: false,
        };
        break;
      case POST_ADD_DEPENDENT_SUCCESS:
        draft.addDependent = {
          loading: false,
          data: action.payload,
          error: false,
        };
        break;
      case POST_ADD_DEPENDENT_FAILURE:
        draft.addDependent = {
          loading: false,
          data: action.payload,
          error: true,
        };
        break;
      case CHATBOT_AUTHENTICATION:
        draft.chatbotAuth = {
          loading: true,
          data: undefined,
          error: false,
        };
        break;
      case CHATBOT_AUTHENTICATION_SUCCESS:
        draft.chatbotAuth = {
          loading: false,
          data: action.payload,
          error: false,
        };
        break;
      case CHATBOT_AUTHENTICATION_FAILURE:
        draft.chatbotAuth = {
          loading: false,
          data: action.payload,
          error: true,
        };
        break;
      case NURSE_SIGN_IN:
        draft.nurseSignin = {
          loading: true,
          data: undefined,
          error: false,
        };
        break;
      case NURSE_SIGN_IN_SUCCESS:
        draft.nurseSignin = {
          loading: false,
          data: action.payload,
          error: false,
        };
        break;
      case NURSE_SIGN_IN_FAILURE:
        draft.nurseSignin = {
          loading: false,
          data: action.payload,
          error: true,
        };
        break;
      case RESET_ADD_DEPENDENT:
        draft.addDependent = {
          loading: false,
          data: false,
          error: false,
        };
        break;
      case SEND_CHAT_MESSAGE_TO_MEMBER:
        draft.messageToMember = {
          loading: true,
          data: undefined,
          error: false,
        };
        break;
      case SEND_CHAT_MESSAGE_TO_MEMBER_SUCCESS:
        draft.messageToMember = {
          data: action.payload,
          error: false,
        };
        break;
      case RESET_SIGNIN:
        draft.signin = {
          ...defaultProps,
        };
        draft.nurseSignin = {
          ...defaultProps,
        };
        break;
      case GET_FAMILY_MEMBERS:
        draft.familyMembers = {
          loading: true,
          data: false,
          error: false,
        };
        break;
      case GET_FAMILY_MEMBERS_SUCCESS:
        draft.familyMembers = {
          loading: false,
          data: action.payload,
          error: false,
        };
        break;
      case SEND_CHAT_MESSAGE_TO_MEMBER_FAILURE:
        draft.messageToMember = {
          loading: false,
          data: action.payload,
          error: true,
        };
        break;
      case SEND_CHAT_MESSAGE_TO_NURSE:
        draft.messageToNurse = {
          loading: true,
          data: undefined,
          error: false,
        };
        break;
      case SEND_CHAT_MESSAGE_TO_NURSE_SUCCESS:
        draft.messageToNurse = {
          loading: false,
          data: action.payload,
          error: false,
        };
        break;
      case GET_FAMILY_MEMBERS_FAILURE:
        draft.familyMembers = {
          loading: false,
          data: false,
          error: action.payload,
        };
        break;
      case PUT_FAMILY_MEMBER_APPROVAL:
        draft.approveFamilyMember = {
          loading: true,
          data: false,
          error: false,
        };
        break;
      case PUT_FAMILY_MEMBER_APPROVAL_SUCCESS:
        draft.approveFamilyMember = {
          loading: false,
          data: action.payload,
          error: false,
        };
        break;
      case SEND_CHAT_MESSAGE_TO_NURSE_FAILURE:
        draft.messageToNurse = {
          loading: false,
          data: action.payload,
          error: true,
        };
        break;
      case MEMBER_JOIN_ROOM:
        draft.memberJoinRoom = {
          loading: true,
          data: undefined,
          error: false,
        };
        break;
      case MEMBER_JOIN_ROOM_SUCCESS:
        draft.memberJoinRoom = {
          loading: false,
          data: action.payload,
          error: false,
        };
        break;
      case MEMBER_JOIN_ROOM_FAILURE:
        draft.memberJoinRoom = {
          loading: false,
          data: action.payload,
          error: true,
        };
        break;
      case LEAVE_CHAT_ROOM:
        draft.leaveRoom = {
          loading: true,
          data: undefined,
          error: false,
        };
        break;
      case LEAVE_CHAT_ROOM_SUCCESS:
        draft.leaveRoom = {
          loading: false,
          data: action.payload,
          error: false,
        };
        break;
      case LEAVE_CHAT_ROOM_FAILURE:
        draft.leaveRoom = {
          loading: false,
          data: action.payload,
          error: true,
        };
        break;
      case MEMBER_HAS_JOINED_ROOM:
        draft.memberJoinRoomStatus = action.payload;
        break;
      case SELECTED_USER_TO_CHAT:
        draft.selectedUserToChat = action.payload;
        break;
      case NURSE_CONVERSATIONS:
        draft.nurseConversations = action.payload;
        break;
      case PUT_FAMILY_MEMBER_APPROVAL_FAILURE:
        draft.approveFamilyMember = {
          loading: false,
          data: false,
          error: action.payload,
        };
        break;
      case RESET_FAMILY_MEMBER_APPROVAL:
        draft.approveFamilyMember = {
          loading: false,
          data: false,
          error: false,
        };
        break;

      case GET_CHAT_MESSAGES:
        draft.chatMessages = {
          loading: true,
          data: false,
          error: false,
        };
        break;
      case GET_CHAT_MESSAGES_SUCCESS:
        draft.chatMessages = {
          loading: false,
          data: action.payload,
          error: false,
        };
        break;
      case GET_CHAT_MESSAGES_FAILURE:
        draft.chatMessages = {
          loading: false,
          data: false,
          error: action.payload,
        };
        break;
      case RESET_CHAT_MESSAGES:
        draft.chatMessages = {
          loading: false,
          data: false,
          error: false,
        };
        break;

      case POST_END_CONVERSATION:
        draft.endConversation = {
          loading: true,
          data: false,
          error: false,
        };
        break;
      case POST_END_CONVERSATION_SUCCESS:
        draft.endConversation = {
          loading: false,
          data: action.payload,
          error: false,
        };
        break;
      case POST_END_CONVERSATION_FAILURE:
        draft.endConversation = {
          loading: false,
          data: false,
          error: action.payload,
        };
        break;

      case NOTIFY_NURSE:
        draft.notifyNurse = {
          loading: true,
          data: false,
          error: false,
        };
        break;
      case NOTIFY_NURSE_SUCCESS:
        draft.notifyNurse = {
          loading: false,
          data: action.payload,
          error: false,
        };
        break;
      case NOTIFY_NURSE_FAILURE:
        draft.notifyNurse = {
          loading: false,
          data: false,
          error: action.payload,
        };
        break;

      case GET_MEMBER_INSURANCE_INFO_LIST:
        draft.memberInsuranceInfoList = {
          loading: false,
          data: false,
          error: action.payload,
        };
        break;
      case VERIFY_SSN:
        draft.verifySsn = {
          loading: true,
          data: false,
          error: false,
        };
        break;
      case GET_MEMBER_INSURANCE_INFO_LIST_SUCCESS:
        draft.memberInsuranceInfoList = {
          loading: false,
          data: false,
          error: action.payload,
        };
        break;
      case VERIFY_SSN_SUCCESS:
        draft.verifySsn = {
          loading: false,
          data: action.payload,
          error: false,
        };
        break;
      case GET_MEMBER_INSURANCE_INFO_LIST_FAILURE:
        draft.memberInsuranceInfoList = {
          loading: false,
          data: false,
          error: action.payload,
        };
        break;
      case VERIFY_SSN_FAILURE:
        draft.verifySsn = {
          loading: false,
          data: false,
          error: action.payload,
        };
        break;

      case GET_MEMBER_INSURANCE_INFO:
        draft.memberInsuranceInfo = {
          loading: false,
          data: false,
          error: action.payload,
        };
        break;
      case UPDATE_PATIENT_COMMS:
        draft.updatePatientComms = {
          loading: true,
          data: false,
          error: false,
        };
        break;
      case GET_MEMBER_INSURANCE_INFO_SUCCESS:
        draft.memberInsuranceInfo = {
          loading: false,
          data: false,
          error: action.payload,
        };
        break;
      case UPDATE_PATIENT_COMMS_SUCCESS:
        draft.updatePatientComms = {
          loading: false,
          data: action.payload,
          error: false,
        };
        break;
      case GET_MEMBER_INSURANCE_INFO_FAILURE:
        draft.memberInsuranceInfo = {
          loading: false,
          data: false,
          error: action.payload,
        };
        break;
      case UPDATE_PATIENT_COMMS_FAILURE:
        draft.updatePatientComms = {
          loading: false,
          data: false,
          error: action.payload,
        };
        break;

      case POST_MEMBER_INSURANCE_INFO:
        draft.addMemberInsuranceInfo = {
          loading: true,
          data: false,
          error: false,
        };
        break;
      case POST_MEMBER_INSURANCE_INFO_SUCCESS:
        draft.addMemberInsuranceInfo = {
          loading: false,
          data: action.payload,
          error: false,
        };
        break;
      case POST_MEMBER_INSURANCE_INFO_FAILURE:
        draft.addMemberInsuranceInfo = {
          loading: false,
          data: false,
          error: action.payload,
        };
        break;
      case RESET_POST_MEMBER_INSURANCE_INFO:
        draft.addMemberInsuranceInfo = {
          ...defaultProps,
        };
        break;

      case PUT_MEMBER_INSURANCE_INFO:
        draft.updateMemberInsuranceInfo = {
          loading: true,
          data: false,
          error: false,
        };
        break;
      case PUT_MEMBER_INSURANCE_INFO_SUCCESS:
        draft.updateMemberInsuranceInfo = {
          loading: false,
          data: action.payload,
          error: false,
        };
        break;
      case PUT_MEMBER_INSURANCE_INFO_FAILURE:
        draft.updateMemberInsuranceInfo = {
          loading: false,
          data: false,
          error: action.payload,
        };
        break;
      case RESET_PUT_MEMBER_INSURANCE_INFO:
        draft.updateMemberInsuranceInfo = {
          ...defaultProps,
        };
        break;
      case SEND_GI_MESSAGE: {
        draft.giMessage = {
          loading: true,
          data: false,
          error: false,
        };
        break;
      }
      case SEND_GI_MESSAGE_SUCCESS:
        draft.giMessage = {
          loading: false,
          data: action.payload || false,
          error: false,
        };

        break;
      case SEND_GI_MESSAGE_FAILURE:
        draft.giMessage = {
          loading: false,
          data: false,
          error: action.payload,
        };
        break;
      case RESET_GI_MESSAGE:
        draft.giMessage = defaultProps;
        break;
      case FIND_PATIENT: {
        draft.findPatient = {
          loading: true,
          data: false,
          error: false,
        };
        break;
      }
      case FIND_PATIENT_SUCCESS:
        draft.findPatient = {
          loading: false,
          data: action.payload || false,
          error: false,
        };

        break;
      case FIND_PATIENT_FAILURE:
        draft.findPatient = {
          loading: false,
          data: false,
          error: action.payload,
        };
        break;
    }
  });

export default registerReducer;
