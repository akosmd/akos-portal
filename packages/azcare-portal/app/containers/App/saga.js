import { takeLatest, call, put, select } from 'redux-saga/effects';
import { integrationApi, giApi } from 'utils/api';
import { INXITE_URL } from 'utils/config';

import { apiResponseEvaluator } from 'utils/sagaHelper';

import {
  makeSelectSignin,
  makeSelectVerifyAuth,
  makeSelectNurseSignin,
  makeSelectForgotPassword,
} from 'containers/App/selectors';

import {
  SIGN_IN,
  FORGOT_PASSWORD,
  FORGOT_PASSWORD_SEND_CODE,
  FORGOT_PASSWORD_VERIFY_CODE,
  FORGOT_PASSWORD_NEW_PASSWORD,
  REGISTER,
  SEND_VERIFICATION_CODE,
  RESEND_VERIFICATION_CODE,
  NIH_SEARCH,
  CHECK_BENEFITS,
  ACCOUNT_VERIFICATION,
  VERIFY_AUTH,
  GET_PATIENT_DETAIL,
  UPDATE_PATIENT_DETAIL,
  SEND_APPOINTMENT,
  CHANGE_PASSWORD,
  INIT_INXITE_SSO,
  GET_PATIENT_DEPENDENTS,
  POST_PATIENT_IMPERSONATION,
  GET_APP_VERSION,
  POST_FAMILY_INVITE,
  POST_ADD_DEPENDENT,
  CHATBOT_AUTHENTICATION,
  CHATROOM_WAITING_LIST,
  NURSE_SIGN_IN,
  SEND_CHAT_MESSAGE_TO_MEMBER,
  SEND_CHAT_MESSAGE_TO_NURSE,
  MEMBER_JOIN_ROOM,
  GET_FAMILY_MEMBERS,
  PUT_FAMILY_MEMBER_APPROVAL,
  LEAVE_CHAT_ROOM,
  GET_CHAT_MESSAGES,
  POST_END_CONVERSATION,
  NOTIFY_NURSE,
  GET_MEMBER_INSURANCE_INFO,
  PUT_MEMBER_INSURANCE_INFO,
  POST_MEMBER_INSURANCE_INFO,
  GET_MEMBER_INSURANCE_INFO_LIST,
  VERIFY_SSN,
  UPDATE_PATIENT_COMMS,
  SEND_GI_MESSAGE,
  FIND_PATIENT,
} from './constants';
import {
  logAppAppError,
  signInSuccess,
  signInFailure,
  registerSuccess,
  registerFailure,
  sendVerificationCodeSuccess,
  sendVerificationCodeFailure,
  nihSearchSuccess,
  nihSearchFailure,
  checkBenefitsSuccess,
  checkBenefitsFailure,
  accountVerificationSuccess,
  accountVerificationFailure,
  verifyAuthenticationSuccess,
  verifyAuthenticationFailure,
  getPatientDetailSuccess,
  getPatientDetailFailure,
  updatePatientSuccess,
  updatePatientFailure,
  sendAppointmentSuccess,
  sendAppointmentFailure,
  reSendVerificationCodeSuccess,
  reSendVerificationCodeFailure,
  changePasswordSuccess,
  changePasswordFailure,
  initializeInxiteSsoSuccess,
  initializeInxiteSsoFailure,
  getPatientDependentsSuccess,
  getPatientDependentsFailure,
  postPatientImpersonationSuccess,
  postPatientImpersonationFailure,
  getAppVersionSuccess,
  getAppVersionFailure,
  postFamilyInviteSuccess,
  postFamilyInviteFailure,
  postAddDependentSuccess,
  postAddDependentFailure,
  authenticateChatbotSuccess,
  authenticateChatbotFailure,
  getUsersWaitingInRoomSuccess,
  getUsersWaitingInRoomFailure,
  loginNurseSuccess,
  loginNurseFailure,
  sendMessageToMemberSuccess,
  sendMessageToMemberFailure,
  memberJoinRoomSuccess,
  memberJoinRoomFailure,
  sendMessageToNurseSuccess,
  sendMessageToNurseFailure,
  getFamilyMembersSuccess,
  getFamilyMembersFailure,
  putFamilyMemberApprovalSuccess,
  putFamilyMemberApprovalFailure,
  getChatMessagesSuccess,
  getChatMessagesFailure,
  postEndConversationSuccess,
  postEndConversationFailure,
  notifyNurseSuccess,
  notifyNurseFailure,
  getMemberInsuranceInfoSuccess,
  getMemberInsuranceInfoFailure,
  getMemberInsuranceInfoListSuccess,
  getMemberInsuranceInfoListFailure,
  postMemberInsuranceInfoSuccess,
  postMemberInsuranceInfoFailure,
  putMemberInsuranceInfoSuccess,
  putMemberInsuranceInfoFailure,
  verifySsnSuccess,
  verifySsnFailure,
  updatePatientCommsSuccess,
  updatePatientCommsFailure,
  sendGiMessageSuccess,
  sendGiMessageFailure,
  findPatientSuccess,
  findPatientFailure,
  forgotPasswordSuccess,
  forgotPasswordFailure,
} from './actions';

import { loginData, vdpcAccount } from './mockUp';

function* getToken() {
  const userInfo = yield select(makeSelectSignin());
  return userInfo.data.token;
}

function* getPatientDetails() {
  const patientInfo = yield select(makeSelectSignin());
  return patientInfo;
}

function* getVerifyToken() {
  const auth = yield select(makeSelectVerifyAuth());

  return auth.data.token;
}

function* getNurseToken() {
  const auth = yield select(makeSelectNurseSignin());

  return auth.data.token;
}

function* getForgotPasswordDetails() {
  const details = yield select(makeSelectForgotPassword());
  return details;
}
const doPatientSignin = payload =>
  integrationApi.post(`/patient/login`, payload, {
    headers: {
      'Content-Type': 'application/json',
    },
  });

export function* signInPatientSaga({ payload }) {
  try {
    // for mock up
    if (
      payload.employeeValidator === 'tempuser@akosmd.com' &&
      payload.password === 'akos1234'
    ) {
      yield put(signInSuccess(loginData.data));
    } else {
      const response = yield call(doPatientSignin, payload);

      yield apiResponseEvaluator(response, signInSuccess, signInFailure);
    }
  } catch (ex) {
    yield put(logAppAppError(ex));
  }
}

const doVerifyEmail = payload =>
  integrationApi.get(`/accounts/${payload}/member`, {
    headers: {
      'Content-Type': 'application/json',
    },
  });

export function* forgotPasswordSaga({ payload }) {
  try {
    const { type, data } = payload;
    const response = yield call(doVerifyEmail, data);
    yield apiResponseEvaluator(
      {
        ...response,
        data: {
          ...response.data,
          type,
        },
      },
      forgotPasswordSuccess,
      forgotPasswordFailure,
    );
  } catch (ex) {
    // yield put(forgotPasswordFailure(ex));
  }
}

const doForgotPasswordSendCode = payload =>
  integrationApi.post('/accounts/send-verification-code', payload, {
    headers: {
      'Content-Type': 'application/json',
    },
  });

export function* forgotPasswordSendCodeSaga({ payload }) {
  try {
    const { type, data } = payload;
    const userDetails = yield getForgotPasswordDetails();

    const response = yield call(doForgotPasswordSendCode, data);
    if (response.data.code === 200) {
      yield put(
        forgotPasswordSuccess({
          ...userDetails.data,
          result: {
            ...userDetails.data.result,
            receivedMode: data.receivedMode,
          },
          type,
        }),
      );
    } else {
      yield put(
        forgotPasswordFailure({
          data: {
            ...userDetails.data,
            type,
          },
          error: response.data,
        }),
      );
    }
  } catch (ex) {
    // yield put(forgotPasswordFailure(ex));
  }
}

const doVerifyCode = payload =>
  integrationApi.post('/accounts/verifycode', payload, {
    headers: {
      'Content-Type': 'application/json',
    },
  });

export function* forgotPasswordVerifyCodeSaga({ payload }) {
  try {
    const { type, data } = payload;
    const userDetails = yield getForgotPasswordDetails();

    const response = yield call(doVerifyCode, data);
    if (response.data.code === 200) {
      yield put(
        forgotPasswordSuccess({
          ...userDetails.data,
          verificationCode: data.verificationCode,
          type,
        }),
      );
    } else {
      yield put(
        forgotPasswordFailure({
          data: {
            ...userDetails.data,
            type,
          },
          error: response.data,
        }),
      );
    }
  } catch (ex) {
    yield put(
      forgotPasswordFailure(ex.data || { msg: 'Something went wrong' }),
    );
  }
}

const doUpdatePassword = payload =>
  integrationApi.post('/accounts/update-password', payload, {
    headers: {
      'Content-Type': 'application/json',
    },
  });

export function* forgotPasswordNewPasswordSaga({ payload }) {
  try {
    const { type, data } = payload;
    const userDetails = yield getForgotPasswordDetails();

    const response = yield call(doUpdatePassword, data);
    if (response.data.code === 200) {
      yield put(
        forgotPasswordSuccess({
          ...userDetails.data,
          type,
        }),
      );
    } else {
      yield put(
        forgotPasswordFailure({
          data: {
            ...userDetails.data,
            type,
          },
          error: response.data,
        }),
      );
    }
  } catch (ex) {
    yield put(
      forgotPasswordFailure(ex.data || { msg: 'Something went wrong' }),
    );
  }
}

const doNurseSignin = payload =>
  integrationApi.post(`/login`, payload, {
    headers: {
      'Content-Type': 'application/json',
    },
  });

export function* signInNurseSaga({ payload }) {
  try {
    const response = yield call(doNurseSignin, payload);
    yield apiResponseEvaluator(response, loginNurseSuccess, loginNurseFailure);
  } catch (ex) {
    yield put(logAppAppError(ex));
  }
}

const doSendVerificationCode = ({ token, payload }) =>
  integrationApi.post(`/pcp/verification_code`, payload, {
    headers: {
      'Content-Type': 'application/json',
      'X-AUTH-TOKEN': token,
    },
  });

export function* sendVerificationCodeSaga({ payload }) {
  try {
    const token = yield getToken();
    const response = yield call(doSendVerificationCode, { token, payload });
    yield apiResponseEvaluator(
      response,
      sendVerificationCodeSuccess,
      sendVerificationCodeFailure,
    );
  } catch (ex) {
    yield put(logAppAppError(ex));
  }
}

const doReSendVerificationCode = payload =>
  integrationApi.post(`patient/verification/resend`, payload, {
    headers: {
      'Content-Type': 'application/json',
    },
  });

export function* reSendVerificationCodeSaga({ payload }) {
  try {
    // const token = yield getToken();
    const response = yield call(doReSendVerificationCode, payload);
    yield apiResponseEvaluator(
      response,
      reSendVerificationCodeSuccess,
      reSendVerificationCodeFailure,
    );
  } catch (ex) {
    yield put(logAppAppError(ex));
    throw ex;
  }
}

const doPatientRegister = payload =>
  integrationApi.post('patient/register', payload, {
    headers: {
      'Content-Type': 'application/json',
    },
  });

export function* registerPatientSaga({ payload }) {
  try {
    const response = yield call(doPatientRegister, payload);
    yield apiResponseEvaluator(response, registerSuccess, registerFailure);
  } catch (ex) {
    yield put(logAppAppError(ex));
  }
}

const doNihSearch = payload =>
  integrationApi.post(
    `https://clinicaltables.nlm.nih.gov/api/rxterms/v3/search?terms=${payload}&ef=STRENGTHS_AND_FORMS`,
    {
      headers: {
        'Content-Type': 'application/json',
      },
    },
  );

export function* nihSearchSaga({ payload }) {
  try {
    const response = yield call(doNihSearch, payload);
    yield apiResponseEvaluator(response, nihSearchSuccess, nihSearchFailure);
  } catch (ex) {
    yield put(logAppAppError(ex));
  }
}

const doCheckBenefits = payload =>
  integrationApi.get(
    `https://staging-integration.akosmd.com/api/availity/coverage/${payload}`,
    {
      headers: {
        'Content-Type': 'application/json',
      },
    },
  );

export function* checkBenefitsSaga({ payload }) {
  try {
    const response = yield call(doCheckBenefits, payload);
    yield apiResponseEvaluator(
      response,
      checkBenefitsSuccess,
      checkBenefitsFailure,
    );
  } catch (ex) {
    yield put(logAppAppError(ex));
  }
}
const doInitInxiteSso = payload =>
  integrationApi.post(`${INXITE_URL}${payload}/sso`, {
    headers: {
      'Content-Type': 'application/json',
    },
  });

export function* initInxiteSsoSaga({ payload }) {
  try {
    const response = yield call(doInitInxiteSso, payload);
    yield apiResponseEvaluator(
      response,
      initializeInxiteSsoSuccess,
      initializeInxiteSsoFailure,
    );
  } catch (ex) {
    yield put(initializeInxiteSsoFailure(ex));
  }
}

const doAccountVerification = payload =>
  integrationApi.post(`patient/verification`, payload, {
    headers: {
      'Content-Type': 'application/json',
    },
  });

export function* accountVerificationSaga({ payload }) {
  try {
    const response = yield call(doAccountVerification, payload);
    yield apiResponseEvaluator(
      response,
      accountVerificationSuccess,
      accountVerificationFailure,
    );
  } catch (ex) {
    yield put(logAppAppError(ex));
  }
}

const doVerifyAuth = ({ token, payload }) =>
  integrationApi.post(`patient/verification/verify`, payload, {
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
  });

export function* verifyAuthSaga({ payload }) {
  try {
    if (
      payload.patientId === 'test-account' &&
      payload.verificationCode === '111222'
    ) {
      yield put(verifyAuthenticationSuccess(loginData.data));
    } else {
      const token = yield getVerifyToken();
      const patient = yield getPatientDetails();
      // for mockup login, remove condition when mock up is done
      if (
        patient.data.patient.email === 'demo@azcareplan.com' &&
        payload.verificationCode === '999999'
      ) {
        const respo = { ...patient.data, patient: vdpcAccount };
        yield put(verifyAuthenticationSuccess(respo));
      } else {
        const response = yield call(doVerifyAuth, { token, payload });
        yield apiResponseEvaluator(
          response,
          verifyAuthenticationSuccess,
          verifyAuthenticationFailure,
        );
      }
    }
  } catch (ex) {
    yield put(logAppAppError(ex));
  }
}

const doGetPatient = ({ token, payload }) =>
  integrationApi.get(`patient/${payload}`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

export function* getPatientDetailsSaga({ payload }) {
  try {
    const token = yield getVerifyToken();
    const response = yield call(doGetPatient, { token, payload });
    yield apiResponseEvaluator(
      response,
      getPatientDetailSuccess,
      getPatientDetailFailure,
    );
  } catch (ex) {
    yield put(logAppAppError(ex));
  }
}

const doUpdatePatientDetail = ({ token, payload }) => {
  const { id, ...rest } = payload;
  return integrationApi.put(`patient/${id}`, rest, {
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
  });
};

export function* updatePatientDetailsSaga({ payload }) {
  try {
    const token = yield getVerifyToken();
    const response = yield call(doUpdatePatientDetail, { token, payload });
    yield apiResponseEvaluator(
      response,
      updatePatientSuccess,
      updatePatientFailure,
    );
  } catch (ex) {
    yield put(logAppAppError(ex));
  }
}

const doUpdatePatientComms = ({ token, payload }) => {
  const { id, ...rest } = payload;
  return integrationApi.put(`members/vdpc_lite/${id}`, rest, {
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
  });
};

export function* updatePatientCommsSaga({ payload }) {
  try {
    const { token, ...rest } = payload;
    const response = yield call(doUpdatePatientComms, { token, payload: rest });
    yield apiResponseEvaluator(
      response,
      updatePatientCommsSuccess,
      updatePatientCommsFailure,
    );
  } catch (ex) {
    yield put(logAppAppError(ex));
  }
}

const doSendAppointment = ({ token, payload }) => {
  const { patientID, ...rest } = payload;
  return integrationApi.post(`/patient/${patientID}/appointment`, rest, {
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
  });
};

export function* sendAppointmentSaga({ payload }) {
  try {
    const token = yield getVerifyToken();
    if (token !== 'fake-token') {
      const response = yield call(doSendAppointment, { token, payload });
      yield apiResponseEvaluator(
        response,
        sendAppointmentSuccess,
        sendAppointmentFailure,
      );
    } else {
      yield put(sendAppointmentSuccess('demo account'));
    }
  } catch (ex) {
    yield put(logAppAppError);
  }
}

const doChangePassword = ({ token, payload }) =>
  integrationApi.post(`patient/${payload.userId}/change_password`, payload, {
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
  });

export function* changePasswordSaga({ payload }) {
  try {
    const token = yield getVerifyToken();
    const response = yield call(doChangePassword, { token, payload });
    yield apiResponseEvaluator(
      response,
      changePasswordSuccess,
      changePasswordFailure,
    );
  } catch (ex) {
    yield put(logAppAppError(ex));
  }
}

const doGetPatientDependents = ({ token, payload }) =>
  integrationApi.get(`patient/${payload}/dependents`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

export function* getPatientDependentsSaga({ payload }) {
  try {
    let token;
    let response;
    if (!payload.token) {
      token = yield getVerifyToken();
      response = yield call(doGetPatientDependents, { payload, token });
    } else {
      // eslint-disable-next-line prefer-destructuring
      token = payload.token;
      response = yield call(doGetPatientDependents, {
        payload: payload.id,
        token,
      });
    }

    yield apiResponseEvaluator(
      response,
      getPatientDependentsSuccess,
      getPatientDependentsFailure,
    );
  } catch (ex) {
    yield put(logAppAppError(ex));
  }
}

const doPostPatientImpersonation = ({ token, payload }) =>
  integrationApi.post(
    `patient/impersonate`,
    { toBeImpersonatedPatientId: payload },
    {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
        isImpersonating: true,
      },
    },
  );

export function* postPatientImpersonationSaga({ payload }) {
  try {
    const token = yield getVerifyToken();
    const response = yield call(doPostPatientImpersonation, { payload, token });
    yield apiResponseEvaluator(
      response,
      postPatientImpersonationSuccess,
      postPatientImpersonationFailure,
    );
  } catch (ex) {
    yield put(logAppAppError(ex));
  }
}

const doGetVersion = token =>
  integrationApi.get(`ui-versions`, undefined, {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      Authorization: `Bearer ${token}`,
    },
  });

export function* appVersionSaga() {
  try {
    const token = yield getToken();
    const response = yield call(doGetVersion, token);
    yield apiResponseEvaluator(
      response,
      getAppVersionSuccess,
      getAppVersionFailure,
    );
  } catch (ex) {
    yield put(getAppVersionFailure(ex));
  }
}

const doFamilyInvite = (token, payload) => {
  const { patientId, params } = payload;
  return integrationApi.post(`members/invitation/${patientId}`, params, {
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
  });
};

export function* postFamilyInviteSaga({ payload }) {
  try {
    const token = yield getToken();
    const response = yield call(doFamilyInvite, token, payload);
    yield apiResponseEvaluator(
      response,
      postFamilyInviteSuccess,
      postFamilyInviteFailure,
    );
  } catch (ex) {
    yield put(postFamilyInviteFailure(ex));
  }
}

const doAddDependent = (token, payload) => {
  const { memberGroupCode, params } = payload;
  return integrationApi.post(`members/add/${memberGroupCode}`, params, {
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
  });
};

export function* postAddDependentSaga({ payload }) {
  try {
    const token = yield getToken();
    const response = yield call(doAddDependent, token, payload);
    yield apiResponseEvaluator(
      response,
      postAddDependentSuccess,
      postAddDependentFailure,
    );
  } catch (ex) {
    yield put(postAddDependentFailure(ex));
  }
}
const doChatbotAuthentication = payload =>
  integrationApi.get(
    `https://akoshealthbotauntenticator.azurewebsites.net/chatBot?userName=${payload}`,
  );

export function* chatBotAuthenticationSaga({ payload }) {
  try {
    const response = yield call(doChatbotAuthentication, payload);

    yield apiResponseEvaluator(
      response,
      authenticateChatbotSuccess,
      authenticateChatbotFailure,
    );
  } catch (ex) {
    yield put(authenticateChatbotFailure(ex));
  }
}

const doGetChatroomWaitingList = token =>
  integrationApi.get(`chat/channel/75001`, {
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
  });

const doPutFamilyMemberApproval = ({ token, payload: { approve, id } }) =>
  integrationApi.put(`members/${id}/${approve}`, undefined, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

export function* getChatroomWaitingListSaga() {
  try {
    const token = yield getNurseToken();
    const response = yield call(doGetChatroomWaitingList, token);

    yield apiResponseEvaluator(
      response,
      getUsersWaitingInRoomSuccess,
      getUsersWaitingInRoomFailure,
    );
  } catch (ex) {
    yield put(getUsersWaitingInRoomFailure(ex));
  }
}

const dosendMessageToMember = ({ token, payload, userId }) =>
  integrationApi.post(`chat/sendmessage/member/${userId}`, payload, {
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
  });

export function* sendMessageToMemberSaga({ payload }) {
  try {
    const token = yield getNurseToken();
    const { userId } = payload;
    const response = yield call(dosendMessageToMember, {
      token,
      payload,
      userId,
    });

    yield apiResponseEvaluator(
      response,
      sendMessageToMemberSuccess,
      sendMessageToMemberFailure,
    );
  } catch (ex) {
    yield put(sendMessageToMemberFailure(ex));
  }
}

const dosendMessageToNurse = ({ token, payload, userId }) =>
  integrationApi.post(`chat/member/sendmessage/nurse/${userId}`, payload, {
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
  });

export function* putFamilyMemberApprovalSaga({ payload }) {
  try {
    const token = yield getVerifyToken();
    const response = yield call(doPutFamilyMemberApproval, { token, payload });
    yield apiResponseEvaluator(
      response,
      putFamilyMemberApprovalSuccess,
      putFamilyMemberApprovalFailure,
    );
  } catch (ex) {
    yield put(logAppAppError(ex));
  }
}

const doGetFamilyMembers = ({ token, payload }) =>
  integrationApi.get(`members/${payload}/list`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

export function* sendMessageToNurseSaga({ payload }) {
  try {
    const token = yield getToken();
    const { userId } = payload;
    const response = yield call(dosendMessageToNurse, {
      token,
      payload,
      userId,
    });

    yield apiResponseEvaluator(
      response,
      sendMessageToNurseSuccess,
      sendMessageToNurseFailure,
    );
  } catch (ex) {
    yield put(sendMessageToNurseFailure(ex));
  }
}

const doMemberJoinRoom = token =>
  integrationApi.post(`chat/member/channel/75001/join`, undefined, {
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
  });

export function* memberJoinRoomSaga() {
  try {
    const token = yield getToken();
    const response = yield call(doMemberJoinRoom, token);

    yield apiResponseEvaluator(
      response,
      memberJoinRoomSuccess,
      memberJoinRoomFailure,
    );
  } catch (ex) {
    yield put(sendMessageToMemberFailure(ex));
  }
}

const doLeaveRoom = ({ token, payload }) =>
  integrationApi.post(`chat/channel/75001/leave/${payload}`, undefined, {
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
  });

export function* leveRoomSaga({ payload }) {
  try {
    const token = yield getNurseToken();
    const response = yield call(doLeaveRoom, { token, payload });

    yield apiResponseEvaluator(
      response,
      memberJoinRoomSuccess,
      memberJoinRoomFailure,
    );
  } catch (ex) {
    yield put(sendMessageToMemberFailure(ex));
  }
}

export function* getFamilyMembersSaga({ payload }) {
  try {
    const token = yield getVerifyToken();
    const response = yield call(doGetFamilyMembers, { token, payload });
    yield apiResponseEvaluator(
      response,
      getFamilyMembersSuccess,
      getFamilyMembersFailure,
    );
  } catch (ex) {
    yield put(logAppAppError(ex));
  }
}

const doGetChatMessages = ({ token, payload }) =>
  integrationApi.get(`chat/member/channel/${payload}/conversations`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

export function* getChatMessagesSaga({ payload }) {
  try {
    let token = yield getVerifyToken();

    if (!token) {
      token = yield getNurseToken();
    }

    const response = yield call(doGetChatMessages, { token, payload });

    yield apiResponseEvaluator(
      response,
      getChatMessagesSuccess,
      getChatMessagesFailure,
    );
  } catch (ex) {
    yield put(getChatMessagesFailure(ex));
  }
}

const doPostEndConversation = ({ token, payload }) =>
  integrationApi.post(
    `chat/member/channel/${payload}/endconversations`,
    undefined,
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    },
  );

export function* postEndConvesationSaga({ payload }) {
  try {
    let token = yield getVerifyToken();

    if (!token) {
      token = yield getNurseToken();
    }

    const response = yield call(doPostEndConversation, { token, payload });

    yield apiResponseEvaluator(
      response,
      postEndConversationSuccess,
      postEndConversationFailure,
    );
  } catch (ex) {
    yield put(postEndConversationFailure(ex));
  }
}

const doNotifyNurse = ({ payload }) =>
  integrationApi.post(
    `https://chat.googleapis.com/v1/spaces/AAAAwgtXzH0/messages?key=AIzaSyDdI0hCZtE6vySjMm-WEfRq3CPzqKqqsHI&token=BdiO02-s5YjeWb39PeSeJ3CrULrnxHdEDhDkkhkvZ-k%3D`,
    payload,
  );

export function* notifyNurseSaga({ payload }) {
  try {
    const response = yield call(doNotifyNurse, { payload });

    yield apiResponseEvaluator(
      response,
      notifyNurseSuccess,
      notifyNurseFailure,
    );
  } catch (ex) {
    yield put(notifyNurseSuccess(ex));
  }
}

const doVerifySsn = ({ payload }) =>
  integrationApi.post(`members/vdpc_lite/login`, payload);

export function* verifySsnSaga({ payload }) {
  try {
    const response = yield call(doVerifySsn, { payload });
    yield apiResponseEvaluator(response, verifySsnSuccess, verifySsnFailure);
  } catch (ex) {
    yield put(verifySsnFailure(ex));
  }
}

const doGetMemberInsuranceInfo = ({ token, id }) =>
  integrationApi.get(`members/insurance/${id}`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

export function* getMemberInsuranceInfoSaga({ payload }) {
  try {
    const token = yield getVerifyToken();
    const response = yield call(doGetMemberInsuranceInfo, {
      token,
      id: payload,
    });

    yield apiResponseEvaluator(
      response,
      getMemberInsuranceInfoSuccess,
      getMemberInsuranceInfoFailure,
    );
  } catch (ex) {
    yield put(getChatMessagesFailure(ex));
  }
}

const doGetMemberInsuranceInfoList = ({ token }) =>
  integrationApi.get('members/insurance', {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

export function* getMemberInsuranceInfoListSaga() {
  try {
    const token = yield getVerifyToken();
    const response = yield call(doGetMemberInsuranceInfoList, {
      token,
    });

    yield apiResponseEvaluator(
      response,
      getMemberInsuranceInfoListSuccess,
      getMemberInsuranceInfoListFailure,
    );
  } catch (ex) {
    yield put(getChatMessagesFailure(ex));
  }
}

const doPostMemberInsuranceInfo = ({ token, payload }) =>
  integrationApi.post('members/insurance', payload, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

export function* postMemberInsuranceInfoSaga({ payload }) {
  try {
    const token = yield getVerifyToken();
    const response = yield call(doPostMemberInsuranceInfo, {
      token,
      payload,
    });

    yield apiResponseEvaluator(
      response,
      postMemberInsuranceInfoSuccess,
      postMemberInsuranceInfoFailure,
    );
  } catch (ex) {
    yield put(getChatMessagesFailure(ex));
  }
}

const doPutMemberInsuranceInfo = ({ token, payload: { id, params } }) =>
  integrationApi.put(`members/insurance/${id}`, params, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

export function* putMemberInsuranceInfoSaga({ payload }) {
  try {
    const token = yield getVerifyToken();
    const response = yield call(doPutMemberInsuranceInfo, {
      token,
      payload,
    });

    yield apiResponseEvaluator(
      response,
      putMemberInsuranceInfoSuccess,
      putMemberInsuranceInfoFailure,
    );
  } catch (ex) {
    yield put(getChatMessagesFailure(ex));
  }
}

const doSendGiMessage = ({ payload }) => giApi.post(`ask`, payload);

export function* sendGiMessageSaga({ payload }) {
  try {
    const response = yield call(doSendGiMessage, { payload });
    yield apiResponseEvaluator(
      response,
      sendGiMessageSuccess,
      sendGiMessageFailure,
    );
  } catch (ex) {
    yield put(sendGiMessageFailure(ex));
  }
}

const doFindPatient = ({ token, payload }) =>
  integrationApi.get(`search-patient`, {
    params: { ...payload },

    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

export function* findPatientSaga({ payload }) {
  let token = yield getVerifyToken();

  if (!token) {
    token = yield getNurseToken();
  }
  try {
    const response = yield call(doFindPatient, {
      token,
      payload,
    });

    yield apiResponseEvaluator(
      response,
      findPatientSuccess,
      findPatientFailure,
    );
  } catch (ex) {
    yield put(findPatientFailure(ex));
  }
}

export default function* patientSaga() {
  yield takeLatest(SIGN_IN, signInPatientSaga);
  yield takeLatest(FORGOT_PASSWORD, forgotPasswordSaga);
  yield takeLatest(FORGOT_PASSWORD_SEND_CODE, forgotPasswordSendCodeSaga);
  yield takeLatest(FORGOT_PASSWORD_VERIFY_CODE, forgotPasswordVerifyCodeSaga);
  yield takeLatest(FORGOT_PASSWORD_NEW_PASSWORD, forgotPasswordNewPasswordSaga);
  yield takeLatest(SEND_VERIFICATION_CODE, sendVerificationCodeSaga);
  yield takeLatest(RESEND_VERIFICATION_CODE, reSendVerificationCodeSaga);
  yield takeLatest(REGISTER, registerPatientSaga);
  yield takeLatest(NIH_SEARCH, nihSearchSaga);
  yield takeLatest(CHECK_BENEFITS, checkBenefitsSaga);
  yield takeLatest(ACCOUNT_VERIFICATION, accountVerificationSaga);
  yield takeLatest(VERIFY_AUTH, verifyAuthSaga);
  yield takeLatest(GET_PATIENT_DETAIL, getPatientDetailsSaga);
  yield takeLatest(UPDATE_PATIENT_DETAIL, updatePatientDetailsSaga);
  yield takeLatest(UPDATE_PATIENT_COMMS, updatePatientCommsSaga);
  yield takeLatest(SEND_APPOINTMENT, sendAppointmentSaga);
  yield takeLatest(CHANGE_PASSWORD, changePasswordSaga);
  yield takeLatest(INIT_INXITE_SSO, initInxiteSsoSaga);
  yield takeLatest(GET_PATIENT_DEPENDENTS, getPatientDependentsSaga);
  yield takeLatest(POST_PATIENT_IMPERSONATION, postPatientImpersonationSaga);
  yield takeLatest(GET_APP_VERSION, appVersionSaga);
  yield takeLatest(POST_FAMILY_INVITE, postFamilyInviteSaga);
  yield takeLatest(POST_ADD_DEPENDENT, postAddDependentSaga);
  yield takeLatest(CHATBOT_AUTHENTICATION, chatBotAuthenticationSaga);
  yield takeLatest(CHATROOM_WAITING_LIST, getChatroomWaitingListSaga);
  yield takeLatest(NURSE_SIGN_IN, signInNurseSaga);
  yield takeLatest(SEND_CHAT_MESSAGE_TO_MEMBER, sendMessageToMemberSaga);
  yield takeLatest(SEND_CHAT_MESSAGE_TO_NURSE, sendMessageToNurseSaga);
  yield takeLatest(MEMBER_JOIN_ROOM, memberJoinRoomSaga);
  yield takeLatest(LEAVE_CHAT_ROOM, leveRoomSaga);
  yield takeLatest(GET_FAMILY_MEMBERS, getFamilyMembersSaga);
  yield takeLatest(PUT_FAMILY_MEMBER_APPROVAL, putFamilyMemberApprovalSaga);
  yield takeLatest(GET_CHAT_MESSAGES, getChatMessagesSaga);
  yield takeLatest(POST_END_CONVERSATION, postEndConvesationSaga);
  yield takeLatest(NOTIFY_NURSE, notifyNurseSaga);
  yield takeLatest(
    GET_MEMBER_INSURANCE_INFO_LIST,
    getMemberInsuranceInfoListSaga,
  );
  yield takeLatest(GET_MEMBER_INSURANCE_INFO, getMemberInsuranceInfoSaga);
  yield takeLatest(PUT_MEMBER_INSURANCE_INFO, putMemberInsuranceInfoSaga);
  yield takeLatest(POST_MEMBER_INSURANCE_INFO, postMemberInsuranceInfoSaga);
  yield takeLatest(VERIFY_SSN, verifySsnSaga);
  yield takeLatest(SEND_GI_MESSAGE, sendGiMessageSaga);
  yield takeLatest(FIND_PATIENT, findPatientSaga);
}
