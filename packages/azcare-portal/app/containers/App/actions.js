/*
 *
 * Register actions
 *
 */

import {
  SIGN_IN,
  SIGN_IN_SUCCESS,
  SIGN_IN_FAILURE,
  FORGOT_PASSWORD,
  FORGOT_PASSWORD_SEND_CODE,
  FORGOT_PASSWORD_VERIFY_CODE,
  FORGOT_PASSWORD_NEW_PASSWORD,
  FORGOT_PASSWORD_SUCCESS,
  FORGOT_PASSWORD_FAILURE,
  CLEAR_FORGOT_PASSWORD,
  FORGOT_PASSWORD_RESEND_CODE,
  APP_ERROR,
  REGISTER,
  REGISTER_SUCCESS,
  REGISTER_FAILURE,
  SEND_VERIFICATION_CODE,
  SEND_VERIFICATION_CODE_SUCCESS,
  SEND_VERIFICATION_CODE_FAILURE,
  INXITE_URL,
  NIH_SEARCH,
  NIH_SEARCH_SUCCESS,
  NIH_SEARCH_FAILURE,
  CHECK_BENEFITS,
  CHECK_BENEFITS_SUCCESS,
  CHECK_BENEFITS_FAILURE,
  ACCOUNT_VERIFICATION,
  ACCOUNT_VERIFICATION_SUCCESS,
  ACCOUNT_VERIFICATION_FAILURE,
  VERIFY_AUTH,
  VERIFY_AUTH_SUCCESS,
  VERIFY_AUTH_FAILURE,
  GET_PATIENT_DETAIL,
  GET_PATIENT_DETAIL_SUCCESS,
  GET_PATIENT_DETAIL_FAILURE,
  UPDATE_PATIENT_DETAIL,
  UPDATE_PATIENT_DETAIL_SUCCESS,
  UPDATE_PATIENT_DETAIL_FAILURE,
  LOGOUT,
  LOGOUT_SUCCESS,
  RESET_REGISTER,
  SEND_APPOINTMENT,
  SEND_APPOINTMENT_SUCCESS,
  SEND_APPOINTMENT_FAILURE,
  RESET_APPOINTMENT_DATA,
  RESEND_VERIFICATION_CODE,
  RESEND_VERIFICATION_CODE_SUCCESS,
  RESEND_VERIFICATION_CODE_FAILURE,
  RESET_AUTH_FIELDS,
  RESET_RESEND_VERIFICATION_CODE,
  RESET_UPDATED_PATIENT,
  CHANGE_PASSWORD,
  CHANGE_PASSWORD_SUCCESS,
  CHANGE_PASSWORD_FAILURE,
  RESET_CHANGE_PASSWORD,
  INIT_INXITE_SSO,
  INIT_INXITE_SSO_SUCCESS,
  INIT_INXITE_SSO_FAILURE,
  VERIFICATION_TO_EMAIL,
  GET_PATIENT_DEPENDENTS,
  GET_PATIENT_DEPENDENTS_SUCCESS,
  GET_PATIENT_DEPENDENTS_FAILURE,
  SHOW_DEPENDENTS_MODAL,
  POST_PATIENT_IMPERSONATION,
  POST_PATIENT_IMPERSONATION_SUCCESS,
  POST_PATIENT_IMPERSONATION_FAILURE,
  RESET_PATIENT_IMPERSONATION,
  GET_APP_VERSION,
  GET_APP_VERSION_SUCCESS,
  GET_APP_VERSION_FAILURE,
  POST_FAMILY_INVITE,
  POST_FAMILY_INVITE_SUCCESS,
  POST_FAMILY_INVITE_FAILURE,
  RESET_FAMILY_INVITE_DATA,
  POST_ADD_DEPENDENT,
  POST_ADD_DEPENDENT_FAILURE,
  POST_ADD_DEPENDENT_SUCCESS,
  RESET_ADD_DEPENDENT,
  CHATBOT_AUTHENTICATION,
  CHATBOT_AUTHENTICATION_SUCCESS,
  CHATBOT_AUTHENTICATION_FAILURE,
  CHATROOM_WAITING_LIST,
  CHATROOM_WAITING_LIST_SUCCESS,
  CHATROOM_WAITING_LIST_FAILURE,
  NURSE_SIGN_IN,
  NURSE_SIGN_IN_SUCCESS,
  NURSE_SIGN_IN_FAILURE,
  SEND_CHAT_MESSAGE_TO_MEMBER,
  SEND_CHAT_MESSAGE_TO_MEMBER_SUCCESS,
  SEND_CHAT_MESSAGE_TO_MEMBER_FAILURE,
  MEMBER_JOIN_ROOM,
  MEMBER_JOIN_ROOM_SUCCESS,
  MEMBER_JOIN_ROOM_FAILURE,
  SEND_CHAT_MESSAGE_TO_NURSE,
  SEND_CHAT_MESSAGE_TO_NURSE_SUCCESS,
  SEND_CHAT_MESSAGE_TO_NURSE_FAILURE,
  MEMBER_HAS_JOINED_ROOM,
  SELECTED_USER_TO_CHAT,
  NURSE_CONVERSATIONS,
  RESET_SIGNIN,
  GET_FAMILY_MEMBERS,
  GET_FAMILY_MEMBERS_SUCCESS,
  GET_FAMILY_MEMBERS_FAILURE,
  PUT_FAMILY_MEMBER_APPROVAL,
  PUT_FAMILY_MEMBER_APPROVAL_SUCCESS,
  PUT_FAMILY_MEMBER_APPROVAL_FAILURE,
  RESET_FAMILY_MEMBER_APPROVAL,
  LEAVE_CHAT_ROOM,
  LEAVE_CHAT_ROOM_SUCCESS,
  LEAVE_CHAT_ROOM_FAILURE,
  GET_CHAT_MESSAGES,
  GET_CHAT_MESSAGES_SUCCESS,
  GET_CHAT_MESSAGES_FAILURE,
  RESET_CHAT_MESSAGES,
  POST_END_CONVERSATION,
  POST_END_CONVERSATION_SUCCESS,
  POST_END_CONVERSATION_FAILURE,
  NOTIFY_NURSE,
  NOTIFY_NURSE_SUCCESS,
  NOTIFY_NURSE_FAILURE,
  GET_MEMBER_INSURANCE_INFO_LIST,
  GET_MEMBER_INSURANCE_INFO_LIST_SUCCESS,
  GET_MEMBER_INSURANCE_INFO_LIST_FAILURE,
  GET_MEMBER_INSURANCE_INFO,
  GET_MEMBER_INSURANCE_INFO_SUCCESS,
  GET_MEMBER_INSURANCE_INFO_FAILURE,
  POST_MEMBER_INSURANCE_INFO,
  POST_MEMBER_INSURANCE_INFO_SUCCESS,
  POST_MEMBER_INSURANCE_INFO_FAILURE,
  PUT_MEMBER_INSURANCE_INFO,
  PUT_MEMBER_INSURANCE_INFO_SUCCESS,
  PUT_MEMBER_INSURANCE_INFO_FAILURE,
  RESET_POST_MEMBER_INSURANCE_INFO,
  RESET_PUT_MEMBER_INSURANCE_INFO,
  VERIFY_SSN,
  VERIFY_SSN_SUCCESS,
  VERIFY_SSN_FAILURE,
  UPDATE_PATIENT_COMMS,
  UPDATE_PATIENT_COMMS_SUCCESS,
  UPDATE_PATIENT_COMMS_FAILURE,
  SEND_GI_MESSAGE,
  SEND_GI_MESSAGE_SUCCESS,
  SEND_GI_MESSAGE_FAILURE,
  RESET_GI_MESSAGE,
  FIND_PATIENT,
  FIND_PATIENT_SUCCESS,
  FIND_PATIENT_FAILURE,
} from './constants';

export function signIn(payload) {
  return {
    type: SIGN_IN,
    payload,
  };
}

export function forgotPassword(payload) {
  return {
    type: FORGOT_PASSWORD,
    payload,
  };
}

export function forgotPasswordSendCode(payload) {
  return {
    type: FORGOT_PASSWORD_SEND_CODE,
    payload,
  };
}

export function forgotPasswordVerifyCode(payload) {
  return {
    type: FORGOT_PASSWORD_VERIFY_CODE,
    payload,
  };
}

export function forgotPasswordResendCode(payload) {
  return {
    type: FORGOT_PASSWORD_RESEND_CODE,
    payload,
  };
}

export function forgotPasswordNewPassword(payload) {
  return {
    type: FORGOT_PASSWORD_NEW_PASSWORD,
    payload,
  };
}

export function forgotPasswordSuccess(payload) {
  return {
    type: FORGOT_PASSWORD_SUCCESS,
    payload,
  };
}

export function forgotPasswordFailure(payload) {
  return {
    type: FORGOT_PASSWORD_FAILURE,
    payload,
  };
}

export function clearForgotPassword(payload) {
  return {
    type: CLEAR_FORGOT_PASSWORD,
    payload,
  };
}

export function signInSuccess(payload) {
  return {
    type: SIGN_IN_SUCCESS,
    payload,
  };
}

export function signInFailure(payload) {
  return {
    type: SIGN_IN_FAILURE,
    payload,
  };
}

export function logoutUser(payload) {
  return { type: LOGOUT, payload };
}

export function setLogoutSuccess(payload) {
  return { type: LOGOUT_SUCCESS, payload };
}

export function resetRegisterData(payload) {
  return { type: RESET_REGISTER, payload };
}

export function logAppAppError(payload) {
  return {
    type: APP_ERROR,
    payload,
  };
}

export function register(payload) {
  return {
    type: REGISTER,
    payload,
  };
}

export function registerSuccess(payload) {
  return {
    type: REGISTER_SUCCESS,
    payload,
  };
}

export function registerFailure(payload) {
  return {
    type: REGISTER_FAILURE,
    payload,
  };
}

export function sendVerificationCode(payload) {
  return {
    type: SEND_VERIFICATION_CODE,
    payload,
  };
}

export function sendVerificationCodeSuccess(payload) {
  return {
    type: SEND_VERIFICATION_CODE_SUCCESS,
    payload,
  };
}

export function sendVerificationCodeFailure(payload) {
  return {
    type: SEND_VERIFICATION_CODE_FAILURE,
    payload,
  };
}

export function reSendVerificationCode(payload) {
  return {
    type: RESEND_VERIFICATION_CODE,
    payload,
  };
}

export function reSendVerificationCodeSuccess(payload) {
  return {
    type: RESEND_VERIFICATION_CODE_SUCCESS,
    payload,
  };
}

export function reSendVerificationCodeFailure(payload) {
  return {
    type: RESEND_VERIFICATION_CODE_FAILURE,
    payload,
  };
}

export function nihSearch(payload) {
  return {
    type: NIH_SEARCH,
    payload,
  };
}

export function nihSearchSuccess(payload) {
  return {
    type: NIH_SEARCH_SUCCESS,
    payload,
  };
}

export function nihSearchFailure(payload) {
  return {
    type: NIH_SEARCH_FAILURE,
    payload,
  };
}

export function accountVerification(payload) {
  return {
    type: ACCOUNT_VERIFICATION,
    payload,
  };
}

export function accountVerificationSuccess(payload) {
  return {
    type: ACCOUNT_VERIFICATION_SUCCESS,
    payload,
  };
}

export function accountVerificationFailure(payload) {
  return {
    type: ACCOUNT_VERIFICATION_FAILURE,
    payload,
  };
}

export function updatePatient(payload) {
  return {
    type: UPDATE_PATIENT_DETAIL,
    payload,
  };
}

export function updatePatientSuccess(payload) {
  return {
    type: UPDATE_PATIENT_DETAIL_SUCCESS,
    payload,
  };
}

export function updatePatientFailure(payload) {
  return {
    type: UPDATE_PATIENT_DETAIL_FAILURE,
    payload,
  };
}

export function resetUpdatedPatient() {
  return {
    type: RESET_UPDATED_PATIENT,
  };
}

export function verifyAuthentication(payload) {
  return {
    type: VERIFY_AUTH,
    payload,
  };
}

export function verifyAuthenticationSuccess(payload) {
  return {
    type: VERIFY_AUTH_SUCCESS,
    payload,
  };
}

export function verifyAuthenticationFailure(payload) {
  return {
    type: VERIFY_AUTH_FAILURE,
    payload,
  };
}

export function getPatientDetail(payload) {
  return {
    type: GET_PATIENT_DETAIL,
    payload,
  };
}

export function getPatientDetailSuccess(payload) {
  return {
    type: GET_PATIENT_DETAIL_SUCCESS,
    payload,
  };
}

export function getPatientDetailFailure(payload) {
  return {
    type: GET_PATIENT_DETAIL_FAILURE,
    payload,
  };
}

export function checkBenefits(payload) {
  return {
    type: CHECK_BENEFITS,
    payload,
  };
}

export function checkBenefitsSuccess(payload) {
  return {
    type: CHECK_BENEFITS_SUCCESS,
    payload,
  };
}

export function checkBenefitsFailure(payload) {
  return {
    type: CHECK_BENEFITS_FAILURE,
    payload,
  };
}

export function resetLoginError() {
  return {
    type: SIGN_IN_FAILURE,
  };
}

export function setInxiteUrl(payload) {
  return {
    type: INXITE_URL,
    payload,
  };
}

export function sendAppointment(payload) {
  return {
    type: SEND_APPOINTMENT,
    payload,
  };
}

export function sendAppointmentSuccess(payload) {
  return {
    type: SEND_APPOINTMENT_SUCCESS,
    payload,
  };
}

export function sendAppointmentFailure(payload) {
  return {
    type: SEND_APPOINTMENT_FAILURE,
    payload,
  };
}

export function resetAuthFields() {
  return {
    type: RESET_AUTH_FIELDS,
  };
}

export function resetVerificationCode() {
  return {
    type: RESET_RESEND_VERIFICATION_CODE,
  };
}

export function resetAppointmentData(payload) {
  return {
    type: RESET_APPOINTMENT_DATA,
    payload,
  };
}
export function changePassword(payload) {
  return {
    type: CHANGE_PASSWORD,
    payload,
  };
}
export function changePasswordSuccess(payload) {
  return {
    type: CHANGE_PASSWORD_SUCCESS,
    payload,
  };
}
export function changePasswordFailure(payload) {
  return {
    type: CHANGE_PASSWORD_FAILURE,
    payload,
  };
}
export function resetChangePassword(payload) {
  return {
    type: RESET_CHANGE_PASSWORD,
    payload,
  };
}

export function initializeInxiteSso(payload) {
  return {
    type: INIT_INXITE_SSO,
    payload,
  };
}

export function initializeInxiteSsoSuccess(payload) {
  return {
    type: INIT_INXITE_SSO_SUCCESS,
    payload,
  };
}

export function initializeInxiteSsoFailure(payload) {
  return {
    type: INIT_INXITE_SSO_FAILURE,
    payload,
  };
}

export function setNotificationType(payload) {
  return {
    type: VERIFICATION_TO_EMAIL,
    payload,
  };
}

export function getPatientDependents(payload) {
  return {
    type: GET_PATIENT_DEPENDENTS,
    payload,
  };
}
export function getPatientDependentsSuccess(payload) {
  return {
    type: GET_PATIENT_DEPENDENTS_SUCCESS,
    payload,
  };
}
export function getPatientDependentsFailure(payload) {
  return {
    type: GET_PATIENT_DEPENDENTS_FAILURE,
    payload,
  };
}

export function showDependentsModal(payload) {
  return {
    type: SHOW_DEPENDENTS_MODAL,
    payload,
  };
}

export function postPatientImpersonation(payload) {
  return {
    type: POST_PATIENT_IMPERSONATION,
    payload,
  };
}
export function postPatientImpersonationSuccess(payload) {
  return {
    type: POST_PATIENT_IMPERSONATION_SUCCESS,
    payload,
  };
}
export function postPatientImpersonationFailure(payload) {
  return {
    type: POST_PATIENT_IMPERSONATION_FAILURE,
    payload,
  };
}
export function resetPatientImpersonation(payload) {
  return {
    type: RESET_PATIENT_IMPERSONATION,
    payload,
  };
}

export function getAppVersion() {
  return {
    type: GET_APP_VERSION,
  };
}
export function getAppVersionSuccess(payload) {
  return {
    type: GET_APP_VERSION_SUCCESS,
    payload,
  };
}
export function getAppVersionFailure(payload) {
  return {
    type: GET_APP_VERSION_FAILURE,
    payload,
  };
}

export function postFamilyInvite(payload) {
  return {
    type: POST_FAMILY_INVITE,
    payload,
  };
}
export function postFamilyInviteSuccess(payload) {
  return {
    type: POST_FAMILY_INVITE_SUCCESS,
    payload,
  };
}
export function postFamilyInviteFailure(payload) {
  return {
    type: POST_FAMILY_INVITE_FAILURE,
    payload,
  };
}
export function resetFamilyInviteData() {
  return {
    type: RESET_FAMILY_INVITE_DATA,
  };
}

export function postAddDependent(payload) {
  return {
    type: POST_ADD_DEPENDENT,
    payload,
  };
}
export function postAddDependentSuccess(payload) {
  return {
    type: POST_ADD_DEPENDENT_SUCCESS,
    payload,
  };
}
export function postAddDependentFailure(payload) {
  return {
    type: POST_ADD_DEPENDENT_FAILURE,
    payload,
  };
}

export function authenticateChatbot(payload) {
  return {
    type: CHATBOT_AUTHENTICATION,
    payload,
  };
}
export function authenticateChatbotSuccess(payload) {
  return {
    type: CHATBOT_AUTHENTICATION_SUCCESS,
    payload,
  };
}
export function authenticateChatbotFailure(payload) {
  return {
    type: CHATBOT_AUTHENTICATION_FAILURE,
    payload,
  };
}

export function resetAddDependent() {
  return {
    type: RESET_ADD_DEPENDENT,
  };
}

export function getUsersWaitingInRoom(payload) {
  return {
    type: CHATROOM_WAITING_LIST,
    payload,
  };
}

export function getUsersWaitingInRoomSuccess(payload) {
  return {
    type: CHATROOM_WAITING_LIST_SUCCESS,
    payload,
  };
}

export function getUsersWaitingInRoomFailure(payload) {
  return {
    type: CHATROOM_WAITING_LIST_FAILURE,
    payload,
  };
}

export function loginNurse(payload) {
  return {
    type: NURSE_SIGN_IN,
    payload,
  };
}

export function loginNurseSuccess(payload) {
  return {
    type: NURSE_SIGN_IN_SUCCESS,
    payload,
  };
}

export function loginNurseFailure(payload) {
  return {
    type: NURSE_SIGN_IN_FAILURE,
    payload,
  };
}

export function sendMessageToMember(payload) {
  return {
    type: SEND_CHAT_MESSAGE_TO_MEMBER,
    payload,
  };
}

export function sendMessageToMemberSuccess(payload) {
  return {
    type: SEND_CHAT_MESSAGE_TO_MEMBER_SUCCESS,
    payload,
  };
}

export function sendMessageToMemberFailure(payload) {
  return {
    type: SEND_CHAT_MESSAGE_TO_MEMBER_FAILURE,
    payload,
  };
}

export function sendMessageToNurse(payload) {
  return {
    type: SEND_CHAT_MESSAGE_TO_NURSE,
    payload,
  };
}

export function sendMessageToNurseSuccess(payload) {
  return {
    type: SEND_CHAT_MESSAGE_TO_NURSE_SUCCESS,
    payload,
  };
}

export function sendMessageToNurseFailure(payload) {
  return {
    type: SEND_CHAT_MESSAGE_TO_NURSE_FAILURE,
    payload,
  };
}

export function memberJoinRoom(payload) {
  return {
    type: MEMBER_JOIN_ROOM,
    payload,
  };
}

export function getFamilyMembers(payload) {
  return {
    type: GET_FAMILY_MEMBERS,
    payload,
  };
}
export function getFamilyMembersSuccess(payload) {
  return {
    type: GET_FAMILY_MEMBERS_SUCCESS,
    payload,
  };
}
export function getFamilyMembersFailure(payload) {
  return {
    type: GET_FAMILY_MEMBERS_FAILURE,
    payload,
  };
}

export function memberJoinRoomSuccess(payload) {
  return {
    type: MEMBER_JOIN_ROOM_SUCCESS,
    payload,
  };
}

export function memberJoinRoomFailure(payload) {
  return {
    type: MEMBER_JOIN_ROOM_FAILURE,
    payload,
  };
}

export function setMemberJoiningStatus(payload) {
  return {
    type: MEMBER_HAS_JOINED_ROOM,
    payload,
  };
}

export function setSelectedUsertoChat(payload) {
  return {
    type: SELECTED_USER_TO_CHAT,
    payload,
  };
}

export function setNurseConversation(payload) {
  return {
    type: NURSE_CONVERSATIONS,
    payload,
  };
}

export function resetSignin() {
  return {
    type: RESET_SIGNIN,
  };
}

export function putFamilyMemberApproval(payload) {
  return {
    type: PUT_FAMILY_MEMBER_APPROVAL,
    payload,
  };
}
export function putFamilyMemberApprovalSuccess(payload) {
  return {
    type: PUT_FAMILY_MEMBER_APPROVAL_SUCCESS,
    payload,
  };
}
export function putFamilyMemberApprovalFailure(payload) {
  return {
    type: PUT_FAMILY_MEMBER_APPROVAL_FAILURE,
    payload,
  };
}

export function leaveChatRoom(payload) {
  return {
    type: LEAVE_CHAT_ROOM,
    payload,
  };
}
export function leaveChatRoomSuccess(payload) {
  return {
    type: LEAVE_CHAT_ROOM_SUCCESS,
    payload,
  };
}
export function leaveChatRoomFailure(payload) {
  return {
    type: LEAVE_CHAT_ROOM_FAILURE,
    payload,
  };
}
export function resetFamilyMemberApproval(payload) {
  return {
    type: RESET_FAMILY_MEMBER_APPROVAL,
    payload,
  };
}

export function getChatMessages(payload) {
  return {
    type: GET_CHAT_MESSAGES,
    payload,
  };
}
export function getChatMessagesSuccess(payload) {
  return {
    type: GET_CHAT_MESSAGES_SUCCESS,
    payload,
  };
}
export function getChatMessagesFailure(payload) {
  return {
    type: GET_CHAT_MESSAGES_FAILURE,
    payload,
  };
}
export function resetChatMessages(payload) {
  return {
    type: RESET_CHAT_MESSAGES,
    payload,
  };
}

export function postEndConversation(payload) {
  return {
    type: POST_END_CONVERSATION,
    payload,
  };
}
export function postEndConversationSuccess(payload) {
  return {
    type: POST_END_CONVERSATION_SUCCESS,
    payload,
  };
}
export function postEndConversationFailure(payload) {
  return {
    type: POST_END_CONVERSATION_FAILURE,
    payload,
  };
}

export function notifyNurse(payload) {
  return {
    type: NOTIFY_NURSE,
    payload,
  };
}
export function notifyNurseSuccess(payload) {
  return {
    type: NOTIFY_NURSE_SUCCESS,
    payload,
  };
}
export function notifyNurseFailure(payload) {
  return {
    type: NOTIFY_NURSE_FAILURE,
    payload,
  };
}

export function getMemberInsuranceInfoList(payload) {
  return {
    type: GET_MEMBER_INSURANCE_INFO_LIST,
    payload,
  };
}
export function getMemberInsuranceInfoListSuccess(payload) {
  return {
    type: GET_MEMBER_INSURANCE_INFO_LIST_SUCCESS,
    payload,
  };
}
export function getMemberInsuranceInfoListFailure(payload) {
  return {
    type: GET_MEMBER_INSURANCE_INFO_LIST_FAILURE,
    payload,
  };
}
export function verifySsn(payload) {
  return {
    type: VERIFY_SSN,
    payload,
  };
}
export function verifySsnSuccess(payload) {
  return {
    type: VERIFY_SSN_SUCCESS,
    payload,
  };
}
export function verifySsnFailure(payload) {
  return {
    type: VERIFY_SSN_FAILURE,
    payload,
  };
}

export function getMemberInsuranceInfo(payload) {
  return {
    type: GET_MEMBER_INSURANCE_INFO,
    payload,
  };
}
export function getMemberInsuranceInfoSuccess(payload) {
  return {
    type: GET_MEMBER_INSURANCE_INFO_SUCCESS,
    payload,
  };
}
export function getMemberInsuranceInfoFailure(payload) {
  return {
    type: GET_MEMBER_INSURANCE_INFO_FAILURE,
    payload,
  };
}

export function postMemberInsuranceInfo(payload) {
  return {
    type: POST_MEMBER_INSURANCE_INFO,
    payload,
  };
}
export function postMemberInsuranceInfoSuccess(payload) {
  return {
    type: POST_MEMBER_INSURANCE_INFO_SUCCESS,
    payload,
  };
}
export function postMemberInsuranceInfoFailure(payload) {
  return {
    type: POST_MEMBER_INSURANCE_INFO_FAILURE,
    payload,
  };
}
export function resetPostMemberInsuranceInfo(payload) {
  return {
    type: RESET_POST_MEMBER_INSURANCE_INFO,
    payload,
  };
}

export function putMemberInsuranceInfo(payload) {
  return {
    type: PUT_MEMBER_INSURANCE_INFO,
    payload,
  };
}
export function putMemberInsuranceInfoSuccess(payload) {
  return {
    type: PUT_MEMBER_INSURANCE_INFO_SUCCESS,
    payload,
  };
}
export function putMemberInsuranceInfoFailure(payload) {
  return {
    type: PUT_MEMBER_INSURANCE_INFO_FAILURE,
    payload,
  };
}
export function resetPutMemberInsuranceInfo(payload) {
  return {
    type: RESET_PUT_MEMBER_INSURANCE_INFO,
    payload,
  };
}
export function updatePatientComms(payload) {
  return {
    type: UPDATE_PATIENT_COMMS,
    payload,
  };
}
export function updatePatientCommsSuccess(payload) {
  return {
    type: UPDATE_PATIENT_COMMS_SUCCESS,
    payload,
  };
}
export function updatePatientCommsFailure(payload) {
  return {
    type: UPDATE_PATIENT_COMMS_FAILURE,
    payload,
  };
}

export function sendGiMessage(payload) {
  return {
    type: SEND_GI_MESSAGE,
    payload,
  };
}

export function sendGiMessageSuccess(payload) {
  return {
    type: SEND_GI_MESSAGE_SUCCESS,
    payload,
  };
}

export function sendGiMessageFailure(payload) {
  return {
    type: SEND_GI_MESSAGE_FAILURE,
    payload,
  };
}

export function resetGiMessage(payload) {
  return {
    type: RESET_GI_MESSAGE,
    payload,
  };
}

export function findPatient(payload) {
  return {
    type: FIND_PATIENT,
    payload,
  };
}

export function findPatientSuccess(payload) {
  return {
    type: FIND_PATIENT_SUCCESS,
    payload,
  };
}

export function findPatientFailure(payload) {
  return {
    type: FIND_PATIENT_FAILURE,
    payload,
  };
}
