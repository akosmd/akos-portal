import { takeLatest, call, put, select } from 'redux-saga/effects';
import { restApi, api, newApi, connectApi } from 'utils/api';
import { restapiBasicAuth } from 'utils/config';

import { apiResponseEvaluator } from 'utils/sagaHelper';

import { makeSelectSignin } from 'containers/App/selectors';

import { logAppAppError } from './actions';

import {
  GET_INSURANCE_ELIGIBILITY,
  GET_PATIENT_INSURANCE,
  FIND_INSURANCE,
  SAVE_PATIENT_INSURANCE,
  COUPON_CODE,
  BRAINTREE_TOKEN,
  PAYMENT_CHECKOUT,
  GENERATE_PATIENT_CALL_ID,
  UPDATE_PATIENT_CALL_ID,
  ADD_USER_TO_WAITING_ROOM,
  NOTIFY_PROVIDERS,
  CHECK_WAITING_ROOM_STATUS,
  GET_OPENTOK_ROOM_KEYS,
  GET_DOC_ALIAS,
  CALL_END_BY_PATIENT,
  INVALIDATE_TOKEN,
  PROVIDER_SETTINGS,
  DISCONNECT_REASON,
} from './legacyConstants';

import {
  getInsuranceEligibilitySuccess,
  getInsuranceEligibilityFailure,
  getPatientInsuranceSuccess,
  getPatientInsuranceFailure,
  findInsuranceSuccess,
  findInsuranceFailure,
  savePatientInsuranceSuccess,
  savePatientInsuranceFailure,
  checkCouponCodeSuccess,
  checkCouponCodeFailure,
  getBrainTreeTokenSuccess,
  getBrainTreeTokenFailure,
  checkoutBraintreeSuccess,
  checkoutBraintreeFailure,
  generatePatientCallIdSuccess,
  generatePatientCallIdFailure,
  addUserToWaitingRoomSuccess,
  addUserToWaitingRoomFailure,
  notifyProvidersSuccess,
  notifyProvidersFailure,
  checkWaitingRoomStatusSuccess,
  checkWaitingRoomStatusFailure,
  getOpenTokRoomKeysSuccess,
  getOpenTokRoomKeysFailure,
  getDocAliasSuccess,
  getDocAliasFailure,
  updatePatientCallIdSuccess,
  updatePatientCallIdFailure,
  callEndByPatientSuccess,
  callEndByPatientFailure,
  invalidateTokenSuccess,
  invalidateTokenFailure,
  providerSettingsSuccess,
  providerSettingsFailure,
  disconnectReasonSuccess,
  disconnectReasonFailure,
} from './legacyActions';

function* getVerifyToken() {
  const auth = yield select(makeSelectSignin());

  return auth.data.newapi_token;
}

const doGetInsuranceEligibility = ({ payload, token }) =>
  restApi.post(`/v1/eligibility`, payload, {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'X-AUTH-TOKEN': token,
      Authorization: `Basic ${btoa(
        `${restapiBasicAuth.username}:${restapiBasicAuth.password}`,
      )}`,
    },
  });

export function* getInsuranceEligibilitySaga({ payload }) {
  try {
    const token = yield getVerifyToken();
    const response = yield call(doGetInsuranceEligibility, { payload, token });
    yield apiResponseEvaluator(
      response,
      getInsuranceEligibilitySuccess,
      getInsuranceEligibilityFailure,
    );
  } catch (ex) {
    yield put(logAppAppError(ex));
  }
}

const doGetPatientInsurance = ({ payload, token }) =>
  api.get(`/patient-insurance/get-patient-insurance/${payload}`, {
    headers: {
      'Content-Type': 'application/json',
      'X-AUTH-TOKEN': token,
    },
  });

export function* getPatientInsuranceSaga({ payload }) {
  try {
    const token = yield getVerifyToken();
    const response = yield call(doGetPatientInsurance, { payload, token });
    yield apiResponseEvaluator(
      response,
      getPatientInsuranceSuccess,
      getPatientInsuranceFailure,
    );
  } catch (ex) {
    yield put(logAppAppError(ex));
  }
}
const doFindInsurance = ({ payload, token }) =>
  api.get(`/patient-insurance/get-copay-amount/${payload}`, {
    headers: {
      'Content-Type': 'application/json',
      'X-AUTH-TOKEN': token,
    },
  });

export function* findInsuranceSaga({ payload }) {
  try {
    const token = yield getVerifyToken();
    const response = yield call(doFindInsurance, { payload, token });
    yield apiResponseEvaluator(
      response,
      findInsuranceSuccess,
      findInsuranceFailure,
    );
  } catch (ex) {
    yield put(logAppAppError(ex));
  }
}
const doSavePatientInsurance = ({ payload, token }) =>
  api.post(`/InsuranceController/savePatientInsurance`, payload, {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'X-AUTH-TOKEN': token,
    },
  });

export function* savePatientInsuranceSaga({ payload }) {
  try {
    const token = yield getVerifyToken();
    const response = yield call(doSavePatientInsurance, { payload, token });
    yield apiResponseEvaluator(
      response,
      savePatientInsuranceSuccess,
      savePatientInsuranceFailure,
    );
  } catch (ex) {
    yield put(logAppAppError(ex));
  }
}
const doCheckCouponCode = ({ payload, token }) =>
  api.post(`/user/isvalidpromocode`, payload, {
    headers: {
      'Content-Type': 'multipart/form-data',
      'X-AUTH-TOKEN': token,
    },
  });

export function* checkCouponCodeSaga({ payload }) {
  try {
    const token = yield getVerifyToken();
    const response = yield call(doCheckCouponCode, { payload, token });
    yield apiResponseEvaluator(
      response,
      checkCouponCodeSuccess,
      checkCouponCodeFailure,
    );
  } catch (ex) {
    yield put(logAppAppError(ex));
  }
}

const doGetBrainTreeToken = ({ token }) =>
  connectApi.post(`/braintree/client_token`, undefined, {
    headers: {
      'Content-Type': 'multipart/form-data',
      'X-AUTH-TOKEN': token,
    },
  });

export function* getBrainTreeTokenSaga() {
  try {
    const token = yield getVerifyToken();
    const response = yield call(doGetBrainTreeToken, { token });
    yield apiResponseEvaluator(
      response,
      getBrainTreeTokenSuccess,
      getBrainTreeTokenFailure,
    );
  } catch (ex) {
    yield put(logAppAppError(ex));
  }
}
const doBraintreeCheckout = ({ payload, token }) =>
  connectApi.post(`/braintree/checkout`, payload, {
    headers: {
      'Content-Type': 'application/json',
      'X-AUTH-TOKEN': token,
    },
  });

export function* braintreeCheckoutSaga({ payload }) {
  try {
    const token = yield getVerifyToken();
    const response = yield call(doBraintreeCheckout, { payload, token });
    yield apiResponseEvaluator(
      response,
      checkoutBraintreeSuccess,
      checkoutBraintreeFailure,
    );
  } catch (ex) {
    yield put(logAppAppError(ex));
  }
}
const doGeneratePatientCallId = ({ payload, token }) =>
  connectApi.post(`/api/pcp/generateCallIdByPatientId`, payload, {
    headers: {
      'Content-Type': 'application/json',
      'X-AUTH-TOKEN': token,
    },
  });

export function* generatePatientCallIdSaga({ payload }) {
  try {
    const token = yield getVerifyToken();
    const response = yield call(doGeneratePatientCallId, { payload, token });
    yield apiResponseEvaluator(
      response,
      generatePatientCallIdSuccess,
      generatePatientCallIdFailure,
    );
  } catch (ex) {
    yield put(logAppAppError(ex));
  }
}
const doUpdatePatientCallId = ({ payload, token }) =>
  connectApi.post(`/api/pcp/updatedataBycallId`, payload, {
    headers: {
      'Content-Type': 'application/json',
      'X-AUTH-TOKEN': token,
    },
  });

export function* updatePatientCallIdSaga({ payload }) {
  try {
    const token = yield getVerifyToken();
    const response = yield call(doUpdatePatientCallId, { payload, token });
    yield apiResponseEvaluator(
      response,
      updatePatientCallIdSuccess,
      updatePatientCallIdFailure,
    );
  } catch (ex) {
    yield put(logAppAppError(ex));
  }
}
const doAddUserToWaitingRoom = ({ payload, token }) =>
  connectApi.post(`/api/pcp/addUserToWaiting`, payload, {
    headers: {
      'Content-Type': 'application/json',
      'X-AUTH-TOKEN': token,
    },
  });

export function* addUserToWaitingRoomSaga({ payload }) {
  try {
    const token = yield getVerifyToken();
    const response = yield call(doAddUserToWaitingRoom, { payload, token });
    yield apiResponseEvaluator(
      response,
      addUserToWaitingRoomSuccess,
      addUserToWaitingRoomFailure,
    );
  } catch (ex) {
    yield put(logAppAppError(ex));
  }
}
const doCheckWaitingRoomStatus = ({ payload, token }) =>
  connectApi.post(`/api/pcp/checkstatusinwatingroom`, payload, {
    headers: {
      'Content-Type': 'application/json',
      'X-AUTH-TOKEN': token,
    },
  });

export function* checkWaitingRoomStatusSaga({ payload }) {
  try {
    const token = yield getVerifyToken();
    const response = yield call(doCheckWaitingRoomStatus, { payload, token });
    yield apiResponseEvaluator(
      response,
      checkWaitingRoomStatusSuccess,
      checkWaitingRoomStatusFailure,
    );
  } catch (ex) {
    yield put(checkWaitingRoomStatusFailure(ex));
  }
}

const doGetOpenTokRoomKeys = ({ payload, token }) =>
  connectApi.post(`/api/pcp/getOpentokRoomKeys`, payload, {
    headers: {
      'Content-Type': 'application/json',
      'X-AUTH-TOKEN': token,
    },
  });
// const doGetOpenTokRoomKeys = ({ token }) =>
//   connectApi.get(`/connect/sessiontokenapikey`, {
//     headers: {
//       'Content-Type': 'application/json',
//       'X-AUTH-TOKEN': token,
//     },
//   });

export function* getOpenTokRoomKeysSaga({ payload }) {
  try {
    const token = yield getVerifyToken();
    const response = yield call(doGetOpenTokRoomKeys, { payload, token });
    yield apiResponseEvaluator(
      response,
      getOpenTokRoomKeysSuccess,
      getOpenTokRoomKeysFailure,
    );
  } catch (ex) {
    yield put(logAppAppError(ex));
  }
}

const doGetDoctorAlias = ({ payload, token }) =>
  connectApi.post(`/api/pcp/getDocFromAlias`, payload, {
    headers: {
      'Content-Type': 'application/json',
      'X-AUTH-TOKEN': token,
    },
  });

export function* getDocAliasSaga({ payload }) {
  try {
    const token = yield getVerifyToken();
    const response = yield call(doGetDoctorAlias, { payload, token });
    yield apiResponseEvaluator(
      response,
      getDocAliasSuccess,
      getDocAliasFailure,
    );
  } catch (ex) {
    yield put(logAppAppError(ex));
  }
}

const doCallEndByPatient = ({ payload, token }) =>
  connectApi.post(`/api/pcp/callendbypatient`, payload, {
    headers: {
      'Content-Type': 'application/json',
      'X-AUTH-TOKEN': token,
    },
  });

export function* callEndByPatientSaga({ payload }) {
  try {
    const token = yield getVerifyToken();
    const response = yield call(doCallEndByPatient, { payload, token });
    yield apiResponseEvaluator(
      response,
      callEndByPatientSuccess,
      callEndByPatientFailure,
    );
  } catch (ex) {
    yield put(logAppAppError(ex));
  }
}

const doProviderSettings = ({ payload, token }) =>
  connectApi.get(`/connect/providersettings/${payload}`, {
    headers: {
      'Content-Type': 'application/json',
      'X-AUTH-TOKEN': token,
    },
  });

export function* providerSettingsSaga({ payload }) {
  try {
    const token = yield getVerifyToken();
    const response = yield call(doProviderSettings, { payload, token });
    yield apiResponseEvaluator(
      response,
      providerSettingsSuccess,
      providerSettingsFailure,
    );
  } catch (ex) {
    yield put(logAppAppError(ex));
  }
}

const doDisconnectReason = ({ payload, token }) =>
  connectApi.post(`/api/pcp/updatetcalldisconnectreason`, payload, {
    headers: {
      'Content-Type': 'application/json',
      'X-AUTH-TOKEN': token,
    },
  });

export function* disconnectReasonSaga({ payload }) {
  try {
    const token = yield getVerifyToken();
    const response = yield call(doDisconnectReason, { payload, token });
    yield apiResponseEvaluator(
      response,
      disconnectReasonSuccess,
      disconnectReasonFailure,
    );
  } catch (ex) {
    yield put(logAppAppError(ex));
  }
}

const doInvalidateToken = ({ token }) =>
  newApi.post(
    `/v1/tokens/invalidate`,
    { access_token: token },
    {
      headers: {
        'Content-Type': 'application/json',
        'X-AUTH-TOKEN': token,
      },
    },
  );

export function* invalidateTokenSaga() {
  try {
    const token = yield getVerifyToken();
    const response = yield call(doInvalidateToken, { token });
    yield apiResponseEvaluator(
      response,
      invalidateTokenSuccess,
      invalidateTokenFailure,
    );
  } catch (ex) {
    yield put(logAppAppError(ex));
  }
}

const doNotifiyProviders = ({ payload, token }) =>
  newApi.post(`/connect/notify`, payload, {
    headers: {
      'Content-Type': 'application/json',
      'X-AUTH-TOKEN': token,
    },
  });

export function* notifyProvidersSaga({ payload }) {
  try {
    const token = yield getVerifyToken();
    const response = yield call(doNotifiyProviders, { payload, token });
    yield apiResponseEvaluator(
      response,
      notifyProvidersSuccess,
      notifyProvidersFailure,
    );
  } catch (ex) {
    yield put(logAppAppError(ex));
  }
}

export default function* legacySaga() {
  yield takeLatest(GET_INSURANCE_ELIGIBILITY, getInsuranceEligibilitySaga);
  yield takeLatest(GET_PATIENT_INSURANCE, getPatientInsuranceSaga);
  yield takeLatest(FIND_INSURANCE, findInsuranceSaga);
  yield takeLatest(SAVE_PATIENT_INSURANCE, savePatientInsuranceSaga);
  yield takeLatest(COUPON_CODE, checkCouponCodeSaga);
  yield takeLatest(BRAINTREE_TOKEN, getBrainTreeTokenSaga);
  yield takeLatest(PAYMENT_CHECKOUT, braintreeCheckoutSaga);
  yield takeLatest(GENERATE_PATIENT_CALL_ID, generatePatientCallIdSaga);
  yield takeLatest(ADD_USER_TO_WAITING_ROOM, addUserToWaitingRoomSaga);
  yield takeLatest(NOTIFY_PROVIDERS, notifyProvidersSaga);
  yield takeLatest(CHECK_WAITING_ROOM_STATUS, checkWaitingRoomStatusSaga);
  yield takeLatest(GET_OPENTOK_ROOM_KEYS, getOpenTokRoomKeysSaga);
  yield takeLatest(GET_DOC_ALIAS, getDocAliasSaga);
  yield takeLatest(UPDATE_PATIENT_CALL_ID, updatePatientCallIdSaga);
  yield takeLatest(CALL_END_BY_PATIENT, callEndByPatientSaga);
  yield takeLatest(INVALIDATE_TOKEN, invalidateTokenSaga);
  yield takeLatest(PROVIDER_SETTINGS, providerSettingsSaga);
  yield takeLatest(DISCONNECT_REASON, disconnectReasonSaga);
}
