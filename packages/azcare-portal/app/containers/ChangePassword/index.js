import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { push } from 'connected-react-router';

import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { compose } from 'redux';

import Typography from '@material-ui/core/Typography';
import { Grid, Card } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';

import {
  changePassword as changePasswordAction,
  resetChangePassword,
  logoutUser,
} from 'containers/App/actions';
import {
  makeSelectSignin,
  makeSelectChangePassword,
} from 'containers/App/selectors';

import TextField from 'components/TextField';
import GradientButton from 'components/GradientButton';

import {
  initialState,
  initialField,
  handleChange,
  handleEqualityChange,
  highlightFormErrors,
} from 'utils/formHelper';

const defaultState = {
  currentPassword: {
    ...initialField,
    id: 'currentPassword',
    caption: 'Current Password',
  },
  newPassword: { ...initialField, id: 'newPassword', caption: 'New Password' },
  confirmPassword: {
    ...initialField,
    id: 'confirmPassword',
    caption: 'Confirm Password',
  },
  samePassword: false,
  ...initialState,
};

const useStyles = makeStyles(theme => ({
  root: {
    padding: 20,
    borderRadius: 10,
    boxShadow: theme.palette.boxShadow.main,
  },
  wrapper: {
    height: 'calc(100vh - 115px)',
    display: 'flex',
    flex: 1,
    justifyContent: 'center',
  },
  buttonLabel: {
    fontWeight: 'bold',
    color: theme.typography.color.dark,
  },
}));

function ChangePassword(props) {
  const {
    changePasswordDetails,
    enqueueSnackbar,
    dispatch,
    doPasswordChange,
    userInfo,
    doResetChangePassword,
    doLogOutUser,
  } = props;

  const [changePassword, setChangePassword] = useState({ ...defaultState });

  useEffect(() => {
    if (
      changePasswordDetails.error &&
      changePasswordDetails.error.status === 403
    ) {
      enqueueSnackbar(changePasswordDetails.error.data, {
        variant: 'error',
      });
      dispatch(doResetChangePassword());
    } else if (changePasswordDetails.data) {
      enqueueSnackbar('Your password has been successfully updated', {
        variant: 'success',
      });

      dispatch(doResetChangePassword());
      dispatch(doLogOutUser());
    }
  }, [changePasswordDetails]);

  const onInputChange = field => event => {
    handleChange({
      field: field.id,
      state: changePassword,
      event,
      saveStepFunc: setChangePassword,
    });
  };

  const onPasswordChange = () => event => {
    handleEqualityChange({
      field1: changePassword.newPassword,
      field2: changePassword.confirmPassword,
      state: changePassword,
      event,
      saveStepFunc: setChangePassword,
    });
  };
  const classes = useStyles();
  const handleCancel = () => {
    dispatch(push('/'));
  };
  const handleChangePassword = () => {
    const {
      currentPassword,
      newPassword,
      confirmPassword,
      completed,
    } = changePassword;

    if (!completed) {
      enqueueSnackbar(
        'Please fill out all required fields marked with asterisk *',
        {
          variant: 'error',
        },
      );
      highlightFormErrors(changePassword, setChangePassword);
    } else if (
      currentPassword.value === confirmPassword.value &&
      currentPassword.value === newPassword.value
    ) {
      enqueueSnackbar(
        'Current password and New Password should not be the same',
        {
          variant: 'error',
        },
      );
    } else {
      dispatch(
        doPasswordChange({
          userId: userInfo.data.patient.id,
          oldPassword: currentPassword.value,
          newPassword: newPassword.value,
          newPasswordConfirmation: confirmPassword.value,
        }),
      );
    }
  };

  return (
    <>
      <Grid
        container
        direction="column"
        spacing={2}
        className={classes.wrapper}
        alignContent="center"
      >
        <Grid item xs={12} md={5}>
          <Card className={classes.root}>
            <Typography variant="h5">
              Change Password
              <Typography variant="body1" color="textPrimary">
                Input your current Password and new Password below
              </Typography>
            </Typography>
            <Grid container spacing={1}>
              <Grid item xs={12}>
                <TextField
                  variant="outlined"
                  fullWidth
                  field={changePassword.currentPassword}
                  onChange={onInputChange(changePassword.currentPassword)}
                  type="password"
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  variant="outlined"
                  field={changePassword.newPassword}
                  onChange={onPasswordChange(changePassword.newPassword)}
                  type="password"
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  variant="outlined"
                  field={changePassword.confirmPassword}
                  onChange={onPasswordChange(changePassword.confirmPassword)}
                  type="password"
                />
              </Grid>
              <Grid item xs={12}>
                <GradientButton
                  variant="contained"
                  size="large"
                  fullWidth
                  onClick={handleChangePassword}
                  loading={changePasswordDetails.loading}
                >
                  {'CHANGE MY PASSWORD'}
                </GradientButton>
              </Grid>
              <Grid item xs={12}>
                <GradientButton
                  variant="outlined"
                  size="large"
                  fullWidth
                  onClick={handleCancel}
                  loading={changePasswordDetails.loading}
                >
                  <Typography className={classes.buttonLabel}>
                    {'CANCEL'}
                  </Typography>
                </GradientButton>
              </Grid>
            </Grid>
          </Card>
        </Grid>
      </Grid>
    </>
  );
}

const { func, object } = PropTypes;

ChangePassword.propTypes = {
  dispatch: func.isRequired,
  enqueueSnackbar: func.isRequired,
  doPasswordChange: func.isRequired,
  changePasswordDetails: object.isRequired,
  userInfo: object.isRequired,
  doResetChangePassword: func.isRequired,
  doLogOutUser: func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  changePasswordDetails: makeSelectChangePassword(),
  userInfo: makeSelectSignin(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    doPasswordChange: changePasswordAction,
    doResetChangePassword: resetChangePassword,
    doLogOutUser: logoutUser,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(ChangePassword);
