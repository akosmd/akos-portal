/**
 *
 * Login
 *
 */

import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { push } from "connected-react-router";

import { useCookies } from "react-cookie";

import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";
import { compose } from "redux";

import { Grid, Typography, Card, CardContent } from "@material-ui/core";
import Link from "@material-ui/core/Link";
import MyLink from "common-components/MyLink";
import TextField from "common-components/TextField";
import GradientButton from "common-components/GradientButton";

import { makeStyles } from "@material-ui/styles";

import {
	signIn,
	resetAuthFields,
	setNotificationType,
	resetSignin
} from "common-core/actions";
import { makeSelectSignin } from "common-core/selectors";

import {
	initialField,
	initialState,
	handleChange,
	highlightFormErrors,
	extractFormValues,
	formatPhoneNumber
} from "utils/formHelper";

import { INXITE_SSO_URL } from "utils/config";

const useStyles = makeStyles(theme => ({
	root: {
		padding: 20,
		borderRadius: 10,
		boxShadow: theme.palette.boxShadow.main
	},
	wrapper: {
		height: "calc(100vh - 115px)",
		display: "flex",
		flex: 1,
		justifyContent: "center"
	},
	headerContainer: {
		paddingBottom: 10
	},
	footerContainer: {
		padding: "10px 0",
		borderTop: `1px solid ${theme.palette.background.default}`
	},
	container: {
		padding: "10px 0"
	},
	text: {
		padding: 10,
		textAlign: "center"
	},
	buttonLabel: {
		fontWeight: "bold",
		color: theme.typography.color.dark
	}
}));

const employeeValidator = {
	...initialField,
	id: "employeeValidator",
	caption: "User ID",
	placeholder: "Phone Number or Email",
	errorMessage: "Phone Number or Email is required"
};
const password = {
	...initialField,
	id: "password",
	caption: "Password",
	placeholder: "Enter Password",
	errorMessage: "Password is required"
};
export function Login({
	dispatch,
	signin,
	doPatientSignin,
	doResetAuthFields,
	enqueueSnackbar,
	doSetNotificationType,
	doResetSignin
}) {
	// eslint-disable-next-line no-unused-vars
	const classes = useStyles();
	const [cookies, setCookie] = useCookies(["name"]);
	const [login, setLogin] = useState({
		employeeValidator,
		password,
		...initialState
	});

	useEffect(() => {
		// eslint-disable-next-line no-useless-escape
		const isSafari = !!navigator.userAgent.match(/Version\/[\d\.]+.*Safari/);

		if (
			isSafari &&
			!document.cookie.match(/^(.*;)?\s*fixed\s*=\s*[^;]+(.*)?$/)
		) {
			setCookie("fixed", "fixed", {
				path: "/",
				expires: new Date("Tue, 19 Jan 2038 03:14:07 UTC")
			});
			window.location.replace(`${INXITE_SSO_URL}safari/iframe`);
		}
	}, []);

	useEffect(() => {
		if (signin.error && signin.error.status === 401) {
			enqueueSnackbar("Invalid Phone Number or Email or Password", {
				variant: "error"
			});
			dispatch(doResetSignin());
		} else if (signin.data) {
			dispatch(push("/2fa"));
			dispatch(doResetAuthFields());
		}
	}, [signin]);

	const onInputChange = field => event => {
		console.warn("login", field);
		handleChange({
			field: field.id,
			state: login,
			event,
			saveStepFunc: setLogin
		});
	};

	const handleSubmit = () => {
		if (!login.completed) {
			enqueueSnackbar("Please input your Email and Password", {
				variant: "error"
			});
			highlightFormErrors(login, setLogin);
		}
		if (login.completed) {
			const params = extractFormValues(login);
			const isEmail = params.employeeValidator.includes("@");
			dispatch(doSetNotificationType(isEmail));
			dispatch(
				doPatientSignin({
					...params,
					employeeValidator: parseInt(params.employeeValidator, 10)
						? formatPhoneNumber(params.employeeValidator)
						: params.employeeValidator
				})
			);
		}
	};

	return (
		<>
			<Grid
				container
				spacing={2}
				direction="column"
				className={classes.wrapper}
				alignContent="center"
			>
				<Grid item xs={12} md={5}>
					<Card className={classes.root}>
						<CardContent>
							<Grid item xs={12} className={classes.headerContainer}>
								<Typography variant="h5">
									SIGN IN
									<Typography variant="body1" color="textPrimary">
										Sign in with your registered phone number or email address
									</Typography>
								</Typography>
							</Grid>

							<Grid item xs={12} sm={12} className={classes.container}>
								<TextField
									keyEvents={{
										handleKeys: ["enter"],
										onKeyEvent: handleSubmit
									}}
									field={login.employeeValidator}
									variant="outlined"
									caption="User ID"
									onChange={onInputChange(login.employeeValidator)}
									required
								/>
								<TextField
									keyEvents={{
										handleKeys: ["enter"],
										onKeyEvent: handleSubmit
									}}
									caption="Password"
									field={login.password}
									type="password"
									variant="outlined"
									onChange={onInputChange(login.password)}
									// required
								/>
							</Grid>
							<Grid item xs={12} className={classes.container}>
								<GradientButton
									variant="contained"
									fullWidth
									onClick={handleSubmit}
									color="primary"
									size="large"
									loading={signin.loading}
								>
									{"Proceed"}
								</GradientButton>
							</Grid>
							<Grid item xs={12} className={classes.text}>
								<Typography variant="body1">
									Need to verify your account?{" "}
									<Link
										to="/account-verification"
										component={MyLink}
										variant="body1"
										color="primary"
									>
										<strong>Verify here</strong>
									</Link>
								</Typography>
							</Grid>
							<Grid item xs={12} className={classes.text}>
								<Typography variant="body1">Forgot Password? </Typography>
								<Typography
									variant="body1"
									onClick={() => dispatch(push("/forgot-password"))}
									color="primary"
								>
									<strong> Click here</strong>
								</Typography>
							</Grid>
							<Grid item xs={12} className={classes.footerContainer}>
								<Typography variant="body1" className={classes.text}>
									First time user?{" "}
								</Typography>
								<GradientButton
									onClick={() => dispatch(push("/register"))}
									variant="outlined"
									color="primary"
									fullWidth
									className={classes.text}
								>
									<Typography className={classes.buttonLabel}>
										REGISTER HERE
									</Typography>
								</GradientButton>
							</Grid>
						</CardContent>
					</Card>
				</Grid>
			</Grid>
			<iframe
				id="inxiteFrame"
				title="Inxite"
				src={`${INXITE_SSO_URL}logout`}
				style={{ display: "none" }}
			/>
		</>
	);
}

const { func, object } = PropTypes;

Login.propTypes = {
	dispatch: func.isRequired,
	doPatientSignin: func.isRequired,
	doResetAuthFields: func.isRequired,
	signin: object,
	enqueueSnackbar: func.isRequired,
	doSetNotificationType: func.isRequired,
	doResetSignin: func.isRequired
};
const mapStateToProps = createStructuredSelector({
	signin: makeSelectSignin()
});
function mapDispatchToProps(dispatch) {
	return {
		dispatch,
		doPatientSignin: signIn,
		doResetAuthFields: resetAuthFields,
		doSetNotificationType: setNotificationType,
		doResetSignin: resetSignin
	};
}

const withConnect = connect(
	mapStateToProps,
	mapDispatchToProps
);

export default compose(withConnect)(Login);
