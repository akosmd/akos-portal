import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';

import FlexView from 'components/FlexView';
import { Grid, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import Button from 'components/GradientButton';
import TextField from 'components/TextField';

import { highlightFormErrors, handleEqualityChange } from 'utils/formHelper';
import { forgotPasswordNewPassword } from 'containers/App/actions';
import { makeSelectForgotPassword } from 'containers/App/selectors';

const useStyles = makeStyles(theme => ({
  buttonLabel: {
    fontWeight: 'bold',
    color: theme.typography.color.dark,
  },
  buttonContainer: {
    padding: '20px 0',
  },
}));

const initialValue = {
  newPassword: {
    id: 'newPassword',
    caption: 'New Password',
    required: true,
    fullWidth: true,
    pristine: true,
    error: true,
    placeholder: 'Enter your password here',
  },
  confirmPassword: {
    id: 'confirmPassword',
    caption: 'Confirm Password',
    required: true,
    fullWidth: true,
    pristine: true,
    error: true,
    placeholder: 'Enter your password here',
  },
};

const Form = ({ forgotPassword, dispatch, doUpdatePassword }) => {
  const classes = useStyles();
  const { data: userDetails } = forgotPassword;

  const [input, setInput] = useState(initialValue);

  const onInputChange = () => event => {
    handleEqualityChange({
      field1: input.newPassword,
      field2: input.confirmPassword,
      state: input,
      event,
      saveStepFunc: setInput,
    });
  };
  const handleSubmit = () => {
    if (!input.completed) {
      highlightFormErrors(input, setInput);
    } else {
      dispatch(
        doUpdatePassword({
          type: 4,
          data: {
            password: input.newPassword.value,
            verificationCode: userDetails.verificationCode,
            email: userDetails.result.email,
            userType: 'member',
            receivedMode: userDetails.result.receivedMode,
            type: 'member',
          },
        }),
      );
    }
  };
  return (
    <FlexView direction="column" justify="center">
      <Grid container spacing={2} justify="center">
        <Grid item xs={12} sm={8}>
          <Typography variant="h5">Reset Password</Typography>
        </Grid>
        <Grid item xs={12} sm={8}>
          <Typography variant="h5">
            {'Your new password must meet the following criteria:'}
          </Typography>
        </Grid>
        <Grid item xs={12} sm={8}>
          <ul>
            <li>
              <Typography>
                {'Must be at least 8 characters in length'}
              </Typography>
            </li>
            <li>
              <Typography>
                {'Must contain at least 1 UPPERCASE character'}
              </Typography>
            </li>
            <li>
              <Typography>
                {'Must contain at least 1 lowercase character'}
              </Typography>
            </li>
            <li>
              <Typography>Must contain at least 1 number</Typography>
            </li>
            <li>
              <Typography>
                {'Must contain at least 1 special character'}
              </Typography>
            </li>
          </ul>
        </Grid>
        <Grid item xs={12} sm={8}>
          <TextField
            keyEvents={{
              handleKeys: ['enter'],
              onKeyEvent: handleSubmit,
            }}
            field={input.newPassword}
            onChange={onInputChange(input.newPassword)}
            variant="outlined"
            required
            type="password"
          />
          <TextField
            keyEvents={{
              handleKeys: ['enter'],
              onKeyEvent: handleSubmit,
            }}
            field={input.confirmPassword}
            onChange={onInputChange(input.confirmPassword)}
            variant="outlined"
            required
            type="password"
          />
        </Grid>
      </Grid>
      <Grid
        container
        spacing={2}
        justify="center"
        className={classes.buttonContainer}
      >
        <Grid item xs={12} sm={4}>
          <Button
            variant="contained"
            fullWidth
            onClick={handleSubmit}
            color="primary"
            size="large"
            loading={false}
            type="submit"
          >
            {'RESET PASSWORD'}
          </Button>
        </Grid>
      </Grid>
    </FlexView>
  );
};

const { func, array } = PropTypes;

Form.propTypes = {
  dispatch: func.isRequired,
  doUpdatePassword: func.isRequired,
  forgotPassword: array.isRequired,
};

const mapDispatchToProps = dispatch => ({
  dispatch,
  doUpdatePassword: forgotPasswordNewPassword,
});

const mapStateToProps = createStructuredSelector({
  forgotPassword: makeSelectForgotPassword(),
});
export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(Form);
