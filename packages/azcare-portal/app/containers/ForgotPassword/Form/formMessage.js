import React from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import { compose } from 'redux';
import { push } from 'connected-react-router';

import FlexView from 'components/FlexView';
import { Grid, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import Button from 'components/GradientButton';
import { clearForgotPassword } from 'containers/App/actions';

const useStyles = makeStyles(theme => ({
  buttonLabel: {
    fontWeight: 'bold',
    color: theme.typography.color.dark,
  },
  text: {
    textAlign: 'center',
  },
  buttonContainer: {
    padding: '20px 0',
  },
}));

const Form = ({ dispatch, onClear }) => {
  const classes = useStyles();

  const handleButton = () => {
    dispatch(onClear());
    dispatch(push('/login'));
  };
  return (
    <FlexView direction="column" justify="center">
      <Grid container spacing={2} justify="center">
        <Grid item xs={12} sm={8}>
          <Typography variant="h4" className={classes.text}>
            {'Congratulations'}
          </Typography>
        </Grid>
        <Grid item xs={12} sm={8}>
          <Typography variant="body1" className={classes.text}>
            {
              "You have successfully changed your Password. Please wait while we are redirecting you to back to the Login screen. If this doesn't work, please click the button below."
            }
          </Typography>
        </Grid>
      </Grid>
      <Grid
        container
        spacing={2}
        justify="center"
        className={classes.buttonContainer}
      >
        <Grid item xs={12} sm={4}>
          <Button
            variant="outlined"
            fullWidth
            onClick={handleButton}
            color="primary"
            size="large"
            loading={false}
          >
            <Typography className={classes.buttonLabel}>
              GO BACK TO LOGIN
            </Typography>
          </Button>
        </Grid>
      </Grid>
    </FlexView>
  );
};

const { func } = PropTypes;

Form.propTypes = {
  dispatch: func.isRequired,
  onClear: func.isRequired,
};

const mapDispatchToProps = dispatch => ({
  dispatch,
  onClear: clearForgotPassword,
});

export default compose(
  connect(
    null,
    mapDispatchToProps,
  ),
)(Form);
