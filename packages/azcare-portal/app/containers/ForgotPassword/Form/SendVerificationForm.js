import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Radio from '@material-ui/core/Radio';

import FlexView from 'components/FlexView';
import { Grid, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import Button from 'components/GradientButton';
import { makeSelectForgotPassword } from 'containers/App/selectors';
import { forgotPasswordSendCode } from 'containers/App/actions';
import { formatPhoneNumber } from 'utils/formHelper';

const useStyles = makeStyles(theme => ({
  buttonLabel: {
    fontWeight: 'bold',
    color: theme.typography.color.dark,
  },
  container: {
    padding: '20px 0',
  },
}));

const Form = ({ dispatch, doForgotPasswordSendCode, forgotPassword }) => {
  const classes = useStyles();
  const { data: userDetails, loading } = forgotPassword;
  const [value, setValue] = useState('phone');

  const handleSubmit = () => {
    dispatch(
      doForgotPasswordSendCode({
        type: 2,
        data: {
          email: userDetails.result && userDetails.result.email,
          receivedMode: value,
          name:
            userDetails.result &&
            `${userDetails.result.first_name} ${userDetails.result.last_name}`,
          mobileNumber: userDetails.result && userDetails.result.phone,
          origin: 'azcare_portal',
          type: 'member',
        },
      }),
    );
  };
  return (
    <FlexView direction="column" justify="center">
      <Grid container spacing={2} justify="center">
        <Grid item xs={12} sm={8}>
          <Typography variant="h5">Enter Verification Code</Typography>
        </Grid>
        <Grid item xs={12} sm={8}>
          <Typography variant="body1">
            {
              'Your Verification Code is on its way. Once received, enter it in the box provided below:'
            }
          </Typography>
        </Grid>
        <Grid item xs={12} sm={8}>
          <Typography variant="h5">
            {'How would you like to receive the verification code?'}
          </Typography>
        </Grid>
        <Grid item xs={12} sm={8}>
          <RadioGroup
            aria-label="received-method"
            name="customized-radios"
            value={value}
            onChange={e => setValue(e.target.value)}
          >
            <FormControlLabel
              value="phone"
              control={<Radio color="primary" />}
              label={
                userDetails.result &&
                `Text: ${formatPhoneNumber(userDetails.result.phoneNo).value}`
              }
            />
            <FormControlLabel
              value="email"
              control={<Radio color="primary" />}
              label={userDetails.result && `Email: ${userDetails.result.email}`}
            />
          </RadioGroup>
        </Grid>
      </Grid>
      <Grid
        container
        spacing={2}
        justify="center"
        className={classes.container}
      >
        <Grid item xs={12} sm={4}>
          <Button
            variant="contained"
            fullWidth
            onClick={handleSubmit}
            color="primary"
            size="large"
            loading={loading}
          >
            {'SEND VERIFICATION CODE'}
          </Button>
        </Grid>
      </Grid>
    </FlexView>
  );
};

const mapStateToProps = createStructuredSelector({
  forgotPassword: makeSelectForgotPassword(),
});

const mapDispatchToProps = dispatch => ({
  dispatch,
  doForgotPasswordSendCode: forgotPasswordSendCode,
});

const { func, array } = PropTypes;

Form.propTypes = {
  dispatch: func.isRequired,
  doForgotPasswordSendCode: func.isRequired,
  forgotPassword: array.isRequired,
};

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(Form);
