import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';

import FlexView from 'components/FlexView';
import { Grid, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import Button from 'components/GradientButton';
import TextField from 'components/TextField';

import { handleChange, highlightFormErrors } from 'utils/formHelper';
import {
  forgotPasswordVerifyCode,
  forgotPasswordResendCode,
} from 'containers/App/actions';
import { makeSelectForgotPassword } from 'containers/App/selectors';

const useStyles = makeStyles(theme => ({
  buttonLabel: {
    fontWeight: 'bold',
    color: theme.typography.color.dark,
  },
  buttonContainer: {
    padding: '20px 0',
  },
  text: {
    padding: '0 10px',
  },
}));

const Form = ({ dispatch, doVerifyCode, forgotPassword, doResendCode }) => {
  const classes = useStyles();
  const { data: userDetails, loading } = forgotPassword;
  const [input, setInput] = useState({
    verificationCode: {
      id: 'verificationCode',
      caption: 'Verification Code',
      required: true,
      fullWidth: true,
      pristine: true,
      error: true,
      placeholder: 'Enter verification Code here',
      errorMessage: 'Invalid Code',
    },
  });

  const onInputChange = field => event => {
    handleChange({
      field: field.id,
      state: input,
      event,
      saveStepFunc: setInput,
    });
  };
  const handleSubmit = () => {
    if (!input.completed) {
      highlightFormErrors(input, setInput);
    } else {
      dispatch(
        doVerifyCode({
          type: 3,
          data: {
            verificationCode: input.verificationCode.value,
            mobileNumber: userDetails.result && userDetails.result.phoneNo,
            receivedMode: userDetails.result && userDetails.result.receivedMode,
            email: userDetails.result && userDetails.result.email,
            type: 'member',
          },
        }),
      );
    }
  };

  const handleResend = () => {
    dispatch(doResendCode({ ...userDetails }));
  };

  return (
    <FlexView direction="column" justify="center">
      <Grid container spacing={2} justify="center">
        <Grid item xs={12} sm={8} md={8} lg={8} xl={8}>
          <Typography variant="body1">
            {
              'Your Verification Code is on its way. Once received, enter it in the box provided below:'
            }
          </Typography>
        </Grid>
        <Grid item xs={12} sm={8} md={8} lg={8} xl={8}>
          <TextField
            keyEvents={{
              handleKeys: ['enter'],
              onKeyEvent: handleSubmit,
            }}
            field={input.verificationCode}
            onChange={onInputChange(input.verificationCode)}
            variant="outlined"
            required
          />
        </Grid>
        <Grid item xs={12} sm={8} md={8} lg={8} xl={8}>
          <FlexView>
            <Typography variant="body1">
              {"Didn't receive the code or the code didn't work?"}
            </Typography>
            <Typography
              variant="body1"
              color="primary"
              onClick={handleResend}
              className={classes.text}
            >
              <strong>Verify here</strong>
            </Typography>
          </FlexView>
        </Grid>
      </Grid>
      <Grid
        container
        spacing={2}
        justify="center"
        className={classes.buttonContainer}
      >
        <Grid item xs={12} sm={4}>
          <Button
            variant="contained"
            fullWidth
            onClick={handleSubmit}
            color="primary"
            size="large"
            loading={loading}
            type="submit"
          >
            {'VERIFY MY CODE'}
          </Button>
        </Grid>
      </Grid>
    </FlexView>
  );
};

const { func, array } = PropTypes;

Form.propTypes = {
  dispatch: func.isRequired,
  forgotPassword: array.isRequired,
  doVerifyCode: func.isRequired,
  doResendCode: func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  forgotPassword: makeSelectForgotPassword(),
});

const mapDispatchToProps = dispatch => ({
  dispatch,
  doVerifyCode: forgotPasswordVerifyCode,
  doResendCode: forgotPasswordResendCode,
});

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(Form);
