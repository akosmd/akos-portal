import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { push } from 'connected-react-router';
import { createStructuredSelector } from 'reselect';

import FlexView from 'components/FlexView';
import { Grid, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import Button from 'components/GradientButton';
import TextField from 'components/TextField';

import { handleEmailChange, highlightFormErrors } from 'utils/formHelper';
import { forgotPassword as forgotPasswordAction } from 'containers/App/actions';
import { makeSelectForgotPassword } from 'containers/App/selectors';

const useStyles = makeStyles(theme => ({
  buttonLabel: {
    fontWeight: 'bold',
    color: theme.typography.color.dark,
  },
  buttonContainer: {
    padding: '20px 0',
  },
}));

const Form = ({
  enqueueSnackbar,
  dispatch,
  doForgotPassword,
  forgotPassword,
}) => {
  const classes = useStyles();
  const { loading } = forgotPassword;
  const [email, setEmail] = useState({
    emailAddress: {
      id: 'emailAddress',
      caption: 'Email Address',
      required: true,
      fullWidth: true,
      pristine: true,
      error: true,
      placeholder: 'Enter your Registered Email',
      errorMessage: 'Invalid Email Address',
    },
  });

  const onInputChange = field => event => {
    handleEmailChange({
      field: field.id,
      state: email,
      event,
      saveStepFunc: setEmail,
    });
  };
  const handleSubmit = () => {
    if (!email.completed) {
      enqueueSnackbar('Invalid Email Address', {
        variant: 'error',
      });
      highlightFormErrors(email, setEmail);
    } else {
      dispatch(
        doForgotPassword({
          type: 1,
          data: email.emailAddress.value,
        }),
      );
    }
  };

  const handleCancel = () => {
    dispatch(push('/login'));
  };

  return (
    <FlexView direction="column" justify="center">
      <Grid container spacing={2} justify="center">
        <Grid item xs={12} sm={8} md={8}>
          <Typography variant="body1">
            {
              'Please enter your login email address and we will help you reset your password.'
            }
          </Typography>
        </Grid>
        <Grid item xs={12} sm={8} md={8}>
          {' '}
          <TextField
            keyEvents={{
              handleKeys: ['enter'],
              onKeyEvent: handleSubmit,
            }}
            field={email.emailAddress}
            onChange={onInputChange(email.emailAddress)}
            variant="outlined"
            required
          />
        </Grid>
      </Grid>
      <Grid
        container
        spacing={2}
        justify="center"
        className={classes.buttonContainer}
      >
        <Grid item xs={12} sm={4}>
          <Button
            variant="contained"
            fullWidth
            onClick={handleSubmit}
            color="primary"
            size="large"
            loading={loading}
            type="submit"
          >
            {'CONTINUE'}
          </Button>
        </Grid>
        <Grid item xs={12} sm={4}>
          <Button
            variant="outlined"
            fullWidth
            onClick={handleCancel}
            color="primary"
            size="large"
            loading={loading}
          >
            <Typography className={classes.buttonLabel}>
              BACK TO LOGIN
            </Typography>
          </Button>
        </Grid>
      </Grid>
    </FlexView>
  );
};

const { func, array } = PropTypes;

Form.propTypes = {
  dispatch: func.isRequired,
  enqueueSnackbar: func.isRequired,
  doForgotPassword: func.isRequired,
  forgotPassword: array.isRequired,
};

const mapDispatchToProps = dispatch => ({
  dispatch,
  doForgotPassword: forgotPasswordAction,
});

const mapStateToProps = createStructuredSelector({
  forgotPassword: makeSelectForgotPassword(),
});
export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(Form);
