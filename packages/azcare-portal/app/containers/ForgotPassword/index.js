import React from 'react';

import { makeStyles } from '@material-ui/styles';
import { Grid, Typography, Card, CardContent } from '@material-ui/core';

import Panel from './Panel';

const useStyles = makeStyles(theme => ({
  root: {
    padding: 20,
    borderRadius: 10,
    boxShadow: theme.palette.boxShadow.main,
    display: 'flex',
    justifyContent: 'center',
    width: '100%',
  },
  container: {
    width: '100%',
    justifyContent: 'center',
    display: 'flex',
    flexDirection: 'column',
  },
  wrapper: {
    width: '100%',
    height: 'calc(100vh - 115px)',
    display: 'flex',
    flex: 1,
    justifyContent: 'center',
  },
}));

const ForgotPassword = () => {
  const classes = useStyles();
  return (
    <Grid
      container
      direction="column"
      className={classes.wrapper}
      alignContent="center"
    >
      <Grid item xs={12} sm={12} md={9} className={classes.container}>
        <Card className={classes.root}>
          <CardContent className={classes.container}>
            <Typography variant="h5">
              {'Forgot Your Member Portal Password?'}
            </Typography>
            <Typography variant="body1">
              {'Follow the steps below to complete the Process'}
            </Typography>
            <Panel />
          </CardContent>
        </Card>
      </Grid>
    </Grid>
  );
};

export default ForgotPassword;
