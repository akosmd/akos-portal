import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Grid, Typography } from '@material-ui/core';
import { makeStyles, styled } from '@material-ui/styles';
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';
import { useSnackbar } from 'notistack';

import { makeSelectForgotPassword } from 'containers/App/selectors';

import EnterEmailForm from '../Form/EnterEmailForm';
import SendVerificationForm from '../Form/SendVerificationForm';
import VerifyCodeForm from '../Form/VerifyCodeForm';
import ResetPasswordForm from '../Form/ResetPasswordForm';
import FormMessage from '../Form/formMessage';

const useStyles = makeStyles(theme => ({
  buttonLabel: {
    fontWeight: 'bold',
    color: theme.typography.color.dark,
  },
  text: {
    padding: '0 10px',
  },
  textComplete: {
    padding: '0 10px',
    fontWeight: 'bold',
    color: theme.palette.primary.main,
  },
  container: {
    padding: '20px 0',
  },
}));

const PanelDiv = styled('div')(({ visible }) => ({
  display: visible ? 'flex' : 'none',
}));

const StepDiv = styled('div')(({ theme, active }) => ({
  background: active
    ? theme.palette.background.light
    : theme.palette.background.dark,
  color: active ? theme.palette.primary.main : theme.typography.color.light,
  borderRadius: '50%',
  height: 25,
  width: 25,
  textAlign: 'center',
}));

const PannelButton = styled('div')(({ theme, active }) => ({
  display: 'flex',
  flexDirection: 'row',
  padding: 10,
  background: active
    ? theme.palette.primary.main
    : theme.palette.background.main,
  color: active ? theme.typography.color.light : theme.typography.color.gray,
}));

const Steps = ({ step, isActive, label }) => {
  const classes = useStyles();
  return isActive > step ? (
    <PannelButton active={isActive === step}>
      <CheckCircleOutlineIcon color="primary" />
      <Typography className={classes.textComplete}>{label}</Typography>
    </PannelButton>
  ) : (
    <PannelButton active={isActive === step}>
      <StepDiv active={isActive === step}>
        <Typography>{step + 1}</Typography>
      </StepDiv>
      <Typography className={classes.text}>{label}</Typography>
    </PannelButton>
  );
};

export const PanelHeader = ({ isActive }) => {
  const classes = useStyles();
  return (
    <Grid container justify="center" className={classes.container}>
      <Grid item xs={12} md={3}>
        <Steps step={0} isActive={isActive} label="ENTER EMAIL" />
      </Grid>
      <Grid item xs={12} md={3}>
        <Steps step={1} isActive={isActive} label="VERIFY IDENTITY" />
      </Grid>
      <Grid item xs={12} md={3}>
        <Steps step={2} isActive={isActive} label="VERIFY CODE" />
      </Grid>
      <Grid item xs={12} md={3}>
        <Steps step={3} isActive={isActive} label="RESET PASSWORD" />
      </Grid>
    </Grid>
  );
};

export const PanelBody = ({ isActive }) => {
  const { enqueueSnackbar } = useSnackbar();

  return (
    <>
      <PanelDiv visible={isActive === 0}>
        <EnterEmailForm enqueueSnackbar={enqueueSnackbar} />
      </PanelDiv>
      <PanelDiv visible={isActive === 1}>
        <SendVerificationForm enqueueSnackbar={enqueueSnackbar} />
      </PanelDiv>
      <PanelDiv visible={isActive === 2}>
        <VerifyCodeForm />
      </PanelDiv>
      <PanelDiv visible={isActive === 3}>
        <ResetPasswordForm />
      </PanelDiv>
      <PanelDiv visible={isActive === 4}>
        <FormMessage />
      </PanelDiv>
    </>
  );
};

const Panel = ({ forgotPassword }) => {
  const { data: userDetails } = forgotPassword;
  return (
    <>
      <PanelHeader isActive={userDetails && userDetails.type} />
      <PanelBody isActive={userDetails && userDetails.type} />
    </>
  );
};

const mapStateToProps = createStructuredSelector({
  forgotPassword: makeSelectForgotPassword(),
});

const { string, number, object } = PropTypes;
Steps.propTypes = {
  step: number.isRequired,
  isActive: number.isRequired,
  label: string,
};

Panel.propTypes = {
  forgotPassword: object.isRequired,
};

PanelBody.propTypes = {
  isActive: number,
};

PanelHeader.propTypes = {
  isActive: number,
};

export default compose(
  connect(
    mapStateToProps,
    null,
  ),
)(Panel);
