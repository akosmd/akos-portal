/**
 *
 * Asynchronously loads the component for Benefits
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
