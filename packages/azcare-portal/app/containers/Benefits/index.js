/**
 *
 * Benefits
 *
 */

import React, { useState } from 'react';

import { makeStyles, withStyles } from '@material-ui/core/styles';
import { Grid } from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MuiExpansionPanel from '@material-ui/core/ExpansionPanel';
import MuiExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';

import Typography from '@material-ui/core/Typography';

import sbcQuestions from './sbc.json';
import medicalEvents from './common-medical-event.json';
import turstmarkLogo from './logo-with-tagline.png';

// import InsuranceList from './insurance-list';

const ExpansionPanel = withStyles({
  root: {
    border: '1px solid rgba(0, 0, 0, .125)',
    // boxShadow: 'none',
    '&:not(:last-child)': {
      borderBottom: 0,
    },
    '&:first-child': {
      borderTopLeftRadius: '.5rem',
      borderTopRightRadius: '.5rem',
    },
    '&:last-child': {
      borderBottomLeftRadius: '.5rem',
      borderBottomRightRadius: '.5rem',
    },
    '&:before': {
      display: 'none',
    },
    '&$expanded': {
      margin: 'auto',
    },
  },
  expanded: {},
})(MuiExpansionPanel);

const ExpansionPanelSummary = withStyles({
  root: {
    marginBottom: -1,
    minHeight: 56,
    '&$expanded': {
      minHeight: 56,
    },
    '&$entered': {
      backgroundColor: 'red',
    },
  },
  content: {
    '&$expanded': {
      margin: '12px 0',
    },
  },
  expanded: {},
})(MuiExpansionPanelSummary);

const useStyles = makeStyles(theme => ({
  root: {
    border: '1px solid rgba(0, 0, 0, .125)',
    boxShadow: 'none',
    '&:not(:last-child)': {
      borderBottom: 0,
    },
    '&:before': {
      display: 'none',
    },
    '&$expanded': {
      margin: 'auto',
    },
  },
  subRoot: {
    width: '100%',
    justifyContent: 'center',
  },
  details: { backgroundColor: '#e8e8e8', padding: '8px' },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    flexBasis: '33.33%',
    [theme.breakpoints.down('md')]: {
      flexBasis: '50%',
    },
    flexShrink: 0,
    height: '2.2rem',
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    width: '100%',
    [theme.breakpoints.down('md')]: {
      textAlign: 'right',
      fontSize: theme.typography.pxToRem(12),
    },
  },
  tertiaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    flexBasis: '90%',
    width: '100%',
    textAlign: 'right',
    [theme.breakpoints.down('md')]: {
      display: 'none',
    },
  },

  headerText: {
    height: '2.2rem',
  },
  inline: {
    display: 'inline',
  },
  content: {
    '&$expanded': {
      margin: '12px 0',
    },
  },
  expanded: {
    margin: '.5rem',
  },
  answerText: {
    paddingLeft: '1rem',
  },
}));

function Benefits() {
  const [expanded, setExpanded] = useState(null);
  const classes = useStyles();

  const handleExpandedRoot = panel => (event, newExpanded) => {
    setExpanded(newExpanded ? panel : false);
  };

  const renderQuestions = () => {
    const questions = sbcQuestions.questions.map((q, idx) => (
      <ExpansionPanel key={`question-${idx + 1}`} elevation={0}>
        <ExpansionPanelSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1bh-content"
          id="panel1bh-header"
        >
          <Typography color="primary">{q.question}</Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <Typography className={classes.answerText}>{q.answer}</Typography>
        </ExpansionPanelDetails>
      </ExpansionPanel>
    ));
    return <div className={classes.subRoot}>{questions}</div>;
  };

  const renderMedicalEvents = () => {
    const events = medicalEvents.events.map((q, idx) => (
      <ExpansionPanel key={`question-${idx + 1}`} elevation={0}>
        <ExpansionPanelSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1bh-content"
          id="panel1bh-header"
        >
          <Typography color="primary">{q.event}</Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <Grid container>
            <Grid item xs={12} md={6}>
              <Typography>
                <strong>Services You May Need</strong>
              </Typography>
              <ul>
                {q.services.map((service, i) => (
                  <li key={`service-${i + 1}`}>{service}</li>
                ))}
              </ul>
            </Grid>
            <Grid item xs={12} md={6}>
              <Typography>
                <strong>What You Will Pay</strong>
              </Typography>
              <ul>
                {q.youPay.map((youPay, i) => (
                  <li key={`youPay-${i + 1}`}>{youPay}</li>
                ))}
              </ul>
            </Grid>
          </Grid>
        </ExpansionPanelDetails>
      </ExpansionPanel>
    ));
    return <div className={classes.subRoot}>{events}</div>;
  };

  const renderMainPanels = () => (
    <div>
      {/* <ExpansionPanel
        square
        className={expanded === 'questions' ? classes.expanded : undefined}
        expanded={expanded === 'questions'}
        elevation={0}
        onChange={handleExpandedRoot('questions')}
      >
        <ExpansionPanelSummary
          // expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1d-content"
          id="panel1d-header"
        >
          <Typography className={classes.heading}>
            <img
              src={turstmarkLogo}
              alt="Trustmark"
              style={{ height: '100%' }}
            />
          </Typography>
          <Typography
            className={classes.secondaryHeading}
            variant="h6"
            color="primary"
            align="left"
          >
            INSURANCE INFORMATION
          </Typography>
          <Typography
            className={classes.tertiaryHeading}
            variant="h6"
            color="secondary"
            align="right"
          >
            MEDICAL
          </Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails className={classes.details}>
          <InsuranceList enqueueSnackbar={enqueueSnackbar} />
        </ExpansionPanelDetails>
      </ExpansionPanel> */}

      <ExpansionPanel
        square
        className={expanded === 'questions' ? classes.expanded : undefined}
        expanded={expanded === 'questions'}
        elevation={0}
        onChange={handleExpandedRoot('questions')}
      >
        <ExpansionPanelSummary
          // expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1d-content"
          id="panel1d-header"
        >
          <Typography className={classes.heading}>
            <img
              src={turstmarkLogo}
              alt="Trustmark"
              style={{ height: '100%' }}
            />
          </Typography>
          <Typography
            className={classes.secondaryHeading}
            variant="h6"
            color="primary"
            align="left"
          >
            IMPORTANT QUESTIONS
          </Typography>
          <Typography
            className={classes.tertiaryHeading}
            variant="h6"
            color="secondary"
            align="right"
          >
            MEDICAL
          </Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails className={classes.details}>
          {renderQuestions()}
        </ExpansionPanelDetails>
      </ExpansionPanel>
      <ExpansionPanel
        square
        elevation={0}
        className={expanded === 'services' ? classes.expanded : undefined}
        expanded={expanded === 'services'}
        onChange={handleExpandedRoot('services')}
      >
        <ExpansionPanelSummary
          // expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1d-content"
          id="panel1d-header"
        >
          <Typography className={classes.heading}>
            <img
              src={turstmarkLogo}
              alt="Trustmark"
              style={{ height: '100%' }}
            />
          </Typography>
          <Typography
            className={classes.secondaryHeading}
            variant="h6"
            color="primary"
            align="left"
          >
            COMMON MEDICAL EVENTS
          </Typography>
          <Typography
            variant="h6"
            color="secondary"
            className={classes.tertiaryHeading}
          >
            MEDICAL
          </Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails className={classes.details}>
          {renderMedicalEvents()}
        </ExpansionPanelDetails>
      </ExpansionPanel>
    </div>
  );

  return renderMainPanels();
}

export default Benefits;
