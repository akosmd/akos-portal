import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';
import moment from 'moment';

import { makeStyles } from '@material-ui/core/styles';
import {
  Grid,
  TableBody,
  TableRow,
  TableCell,
  Button,
} from '@material-ui/core';

import AkosTable from 'components/Table';

import {
  makeSelectSignin,
  makeSelectMemberInsuranceInfoList,
  makeSelectAddMemberInsuranceInfo,
  makeSelectUpdateMemberInsuranceInfo,
} from 'containers/App/selectors';
import {
  getMemberInsuranceInfoList,
  postMemberInsuranceInfo,
  putMemberInsuranceInfo,
  resetPostMemberInsuranceInfo,
  resetPutMemberInsuranceInfo,
} from 'containers/App/actions';

import { extractFormValues } from 'utils/formHelper';

import InsuranceFormModal from './insurance-form-modal';

const useStyles = makeStyles(theme => ({
  actionButton: {
    display: 'flex',
    justifyContent: 'flex-end',
    marginTop: '1rem',
    [theme.breakpoints.down('sm')]: {
      justifyContent: 'flex-start',
      marginBottom: '1rem',
    },
  },
  pointer: {
    cursor: 'pointer',
  },
}));

const tableHeader = [
  {
    id: 'carrierName',
    numeric: false,
    disablePadding: false,
    label: 'Carrier Name',
  },
  {
    id: 'policyNumber',
    numeric: false,
    disablePadding: false,
    label: 'Policy Number',
  },
  {
    id: 'renewalDate',
    numeric: false,
    disablePadding: false,
    label: 'Renewal Date',
  },
  {
    id: 'expiryDate',
    numeric: false,
    disablePadding: false,
    label: 'Expiry Date',
  },
];

function InsuranceList({
  dispatch,
  enqueueSnackbar,
  insuranceInfoList,
  doGetMemberInsuranceInfoList,
  doPostMemberInsuranceInfo,
  doResetPostMemberInsuranceInfo,
  doPutMemberInsuranceInfo,
  doResetPutMemberInsuranceInfo,
  addInsuranceInfo,
  updateInsuranceInfo,
  patientData,
}) {
  const classes = useStyles();

  const [insuranceForm, setInsuranceForm] = useState({ open: false });

  useEffect(() => {
    if (patientData.data) {
      dispatch(doGetMemberInsuranceInfoList());
    }
  }, []);

  useEffect(() => {
    if (addInsuranceInfo.data) {
      enqueueSnackbar('Successfully added new insurance', {
        variant: 'success',
      });
      dispatch(doResetPostMemberInsuranceInfo());
      dispatch(doGetMemberInsuranceInfoList());
      handleInsuranceFormClose();
    }
  }, [addInsuranceInfo]);

  useEffect(() => {
    if (updateInsuranceInfo.data) {
      enqueueSnackbar('Successfully updated the insurance', {
        variant: 'success',
      });
      dispatch(doResetPutMemberInsuranceInfo());
      dispatch(doGetMemberInsuranceInfoList());
      handleInsuranceFormClose();
    }
  }, [updateInsuranceInfo]);

  const handleInsuranceFormAddOpen = () => {
    setInsuranceForm({
      open: true,
      method: 'add',
    });
  };

  const handleUpdateInsuranceInfo = data => {
    setInsuranceForm({
      open: true,
      method: 'update',
      data,
    });
  };

  const handleInsuranceFormClose = () => {
    setInsuranceForm({
      ...insuranceForm,
      open: false,
    });
  };

  const handleInsuranceSubmit = payload => {
    const values = extractFormValues(payload);
    const params = {
      ...values,
      renewalDate: moment(values.renewalDate).format('YYYY/MM/DD'),
      expiryDate: moment(values.expiryDate).format('YYYY/MM/DD'),
    };

    dispatch(
      insuranceForm.method === 'add'
        ? doPostMemberInsuranceInfo(params)
        : doPutMemberInsuranceInfo({ id: insuranceForm.data.id, params }),
    );
  };

  const renderBody = () => {
    const { data: rows } = insuranceInfoList;
    if (!Array.isArray(rows)) return null;
    return (
      <TableBody>
        {rows.map((row, i) => (
          <TableRow
            className={classes.pointer}
            key={`row-${i + 1}`}
            hover
            tabIndex={-1}
            onClick={() => handleUpdateInsuranceInfo(row)}
          >
            <TableCell padding="default" variant="body">
              {row.carrierName || ''}
            </TableCell>
            <TableCell padding="default" variant="body">
              {row.policyNumber || ''}
            </TableCell>
            <TableCell variant="body">{row.renewalDate || ''}</TableCell>
            <TableCell variant="body">{row.expiryDate || ''}</TableCell>
          </TableRow>
        ))}
      </TableBody>
    );
  };

  return (
    <>
      <Grid container spacing={3}>
        <Grid item xs={12} className={classes.actionButton}>
          <Button
            variant="contained"
            color="primary"
            onClick={handleInsuranceFormAddOpen}
          >
            Add Insurance
          </Button>
        </Grid>
        <Grid item xs={12}>
          <AkosTable
            title="Member Insurance Information List"
            loading={insuranceInfoList.loading}
            // avatar={() => (
            //   <Avatar aria-label="Client List" className={classes.avatar}>
            //     <CompaniesIcon />
            //   </Avatar>
            // )}
            data={insuranceInfoList.data || []}
            header={tableHeader}
            body={renderBody}
            // options={renderFilter}
          />
        </Grid>
      </Grid>
      <InsuranceFormModal
        open={insuranceForm.open}
        data={insuranceForm.data}
        loading={
          insuranceForm.method === 'add'
            ? addInsuranceInfo.loading
            : updateInsuranceInfo.loading
        }
        onClose={handleInsuranceFormClose}
        onSubmit={handleInsuranceSubmit}
        enqueueSnackbar={enqueueSnackbar}
      />
    </>
  );
}

const { func, object } = PropTypes;
InsuranceList.propTypes = {
  enqueueSnackbar: func.isRequired,
  dispatch: func.isRequired,
  doGetMemberInsuranceInfoList: func.isRequired,
  doPostMemberInsuranceInfo: func.isRequired,
  doResetPostMemberInsuranceInfo: func.isRequired,
  doPutMemberInsuranceInfo: func.isRequired,
  doResetPutMemberInsuranceInfo: func.isRequired,
  patientData: object.isRequired,
  insuranceInfoList: object.isRequired,
  addInsuranceInfo: object.isRequired,
  updateInsuranceInfo: object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  patientData: makeSelectSignin(),
  insuranceInfoList: makeSelectMemberInsuranceInfoList(),
  addInsuranceInfo: makeSelectAddMemberInsuranceInfo(),
  updateInsuranceInfo: makeSelectUpdateMemberInsuranceInfo(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    doGetMemberInsuranceInfoList: getMemberInsuranceInfoList,
    doPostMemberInsuranceInfo: postMemberInsuranceInfo,
    doPutMemberInsuranceInfo: putMemberInsuranceInfo,
    doResetPostMemberInsuranceInfo: resetPostMemberInsuranceInfo,
    doResetPutMemberInsuranceInfo: resetPutMemberInsuranceInfo,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(InsuranceList);
