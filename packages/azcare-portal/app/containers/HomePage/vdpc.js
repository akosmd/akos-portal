import React, { useState, useEffect } from 'react';

import PropTypes from 'prop-types';

import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { push } from 'connected-react-router';
import { compose } from 'redux';
import moment from 'moment';

import AkosCard from 'components/AkosCard';

import MedicationsIcon from 'images/svgs/pharmacy.svg';
import AssessmentIcon from 'images/svgs/health-risk-assesment.svg';
import RecordsIcon from 'images/svgs/health-record.svg';
import BenefitsIcon from 'images/svgs/benefits-wallette.svg';
import MessageIcon from 'images/svgs/messages.svg';
import ProviderIcon from 'images/svgs/talk-to-provider.svg';
import PharmacyIcon from 'images/svgs/medication-lookup.svg';
import AppointmentIcon from 'images/svgs/my-appointment.svg';
import FAQICon from 'images/svgs/FAQ.svg';

import { Grid } from '@material-ui/core';

import RxSpark from 'containers/RxSpark';

import {
  setInxiteUrl,
  initializeInxiteSso,
  getPatientDependents,
  showDependentsModal,
} from 'containers/App/actions';
import { generatePatientCallId } from 'containers/App/legacyActions';

import {
  makeSelectVerifyAuth,
  makeSelectInxiteSso,
  makeSelectInxiteUrl,
  makePatientDependents,
  makePatientImpersonation,
  makeShowDependentsModal,
  makeSelectActualPatient,
} from 'containers/App/selectors';

import { makeSelectPatientCallId } from 'containers/App/legacySelectors';

import { INXITE_SSO_URL } from 'utils/config';

function VdpcButtons({
  dispatch,
  doSetUrl,
  callId,
  doInitInxiteSso,
  doGeneratePatientCallId,
  account,
  patient,
  inxiteSso,
  doGetPatientDependents,
}) {
  const [showPharmacy, setShowPharmacy] = useState(false);
  const [inxiteLoaded, setInxiteLoaded] = useState(false);

  useEffect(() => {
    if (patient.error) {
      dispatch(push('/login'));
    } else if (patient.data) {
      const { patient: detail } = patient.data;
      const { uuid } = detail;
      setInxiteLoaded(false);
      initiateInxiteSSO(uuid);

      const params = {
        patientId: detail.patientLegacyId,
        patientEmail: detail.email,
        patientDob: detail.birthDateAt,
        uuid,
        staffid: 500,
        call_started: moment(Date.now()).format('YYYY/MM/DD'),
        room_name: 'medical',
        call_type: 'connect',
        device_type: window.navigator.userAgent,
      };
      if (!callId.data && !callId.loading)
        dispatch(doGeneratePatientCallId(params));

      initiateInxiteSSO(uuid);
      dispatch(doGetPatientDependents(detail.id));
    }
  }, [patient, callId]);

  const initiateInxiteSSO = uuid => {
    dispatch(doInitInxiteSso(uuid));
  };

  const onPharmacyClick = () => {
    setShowPharmacy(!showPharmacy);
  };

  const onInxiteClick = url => {
    dispatch(doSetUrl(url));
    dispatch(push('/360'));
  };

  const onInxiteLoaded = () => {
    if (!inxiteLoaded) {
      setInxiteLoaded(true);
    }
  };

  return (
    <Grid container spacing={10}>
      <Grid item xs={12} sm={6} md={4}>
        <AkosCard
          loaderStyle={{ height: 150 }}
          caption="Health Risk Assessment"
          icon={() => <AssessmentIcon width={150} height={150} />}
          loading={!inxiteLoaded}
          onClick={() =>
            onInxiteClick(
              `${INXITE_SSO_URL}assessments/${
                inxiteSso.data.inxite_patient_id
              }`,
            )
          }
        />
      </Grid>
      <Grid item xs={12} sm={6} md={4}>
        <AkosCard
          loaderStyle={{ height: 150 }}
          caption="Talk to a Provider"
          icon={() => <ProviderIcon width={150} height={150} />}
          onClick={() => dispatch(push('/chat?talkToProvider=true'))}
        />
      </Grid>
      <Grid item xs={12} sm={6} md={4}>
        {!showPharmacy && (
          <AkosCard
            loaderStyle={{ height: 150 }}
            caption="Medication Lookup"
            icon={() => <PharmacyIcon width={150} height={150} />}
            onClick={onPharmacyClick}
          />
        )}
        {showPharmacy && (
          <RxSpark
            onClick={onPharmacyClick}
            zipCode={account.data.patient.zipCode}
          />
        )}
      </Grid>
      <Grid item xs={12} sm={6} md={4}>
        <AkosCard
          loaderStyle={{ height: 150 }}
          caption="Medical Records"
          icon={() => <RecordsIcon width={150} height={150} />}
          loading={!inxiteLoaded}
          onClick={() =>
            onInxiteClick(
              `${INXITE_SSO_URL}unifiedrecord/${
                inxiteSso.data.inxite_patient_id
              }`,
            )
          }
        />
      </Grid>
      <Grid item xs={12} sm={6} md={4}>
        <AkosCard
          loaderStyle={{ height: 150 }}
          caption="My Appointments"
          icon={() => <AppointmentIcon width={150} height={150} />}
          onClick={() => dispatch(push('/create-appointment'))}
        />
      </Grid>
      <Grid item xs={12} sm={6} md={4}>
        <AkosCard
          loaderStyle={{ height: 150 }}
          caption="My Medications"
          icon={() => <MedicationsIcon width={150} height={150} />}
          loading={!inxiteLoaded}
          onClick={() =>
            onInxiteClick(
              `${INXITE_SSO_URL}unifiedrecord/${
                inxiteSso.data.inxite_patient_id
              }#medications`,
            )
          }
        />
      </Grid>
      <Grid item xs={12} sm={6} md={4}>
        <AkosCard
          loaderStyle={{ height: 150 }}
          caption="FAQs"
          icon={() => <FAQICon width={150} height={150} />}
          onClick={() => dispatch(push('/faq'))}
        />
      </Grid>

      <Grid item xs={12} sm={6} md={4}>
        <AkosCard
          loaderStyle={{ height: 150 }}
          caption="Messages"
          icon={() => <MessageIcon width={150} height={150} />}
          loading={!inxiteLoaded}
          onClick={() => onInxiteClick(`${INXITE_SSO_URL}messages/view`)}
        />
      </Grid>
      <Grid item xs={12} sm={6} md={4}>
        <AkosCard
          loaderStyle={{ height: 150 }}
          caption="Benefits Wallet"
          icon={() => <BenefitsIcon width={150} height={150} />}
          onClick={() => dispatch(push('/benefits'))}
        />
      </Grid>

      <Grid item xs={12} sm={6} md={4}>
        {inxiteSso.data && (
          <iframe
            id="inxiteFrame"
            title="Inxite"
            src={inxiteSso.data.sso_url}
            style={{ display: 'none' }}
            onLoad={onInxiteLoaded}
          />
        )}
      </Grid>
    </Grid>
  );
}

const { func, object } = PropTypes;
VdpcButtons.propTypes = {
  dispatch: func.isRequired,
  doSetUrl: func.isRequired,
  doInitInxiteSso: func.isRequired,
  doGetPatientDependents: func.isRequired,
  doGeneratePatientCallId: func.isRequired,
  inxiteSso: object.isRequired,
  account: object.isRequired,
  patient: object.isRequired,
  callId: object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  patient: makeSelectActualPatient(),
  account: makeSelectVerifyAuth(),
  inxiteSso: makeSelectInxiteSso(),
  inxiteUrl: makeSelectInxiteUrl(),
  dependents: makePatientDependents(),
  impersonatedPatient: makePatientImpersonation(),
  impersonationShowModal: makeShowDependentsModal(),
  callId: makeSelectPatientCallId(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    doSetUrl: setInxiteUrl,
    doInitInxiteSso: initializeInxiteSso,
    doGetPatientDependents: getPatientDependents,
    doShowDependentsModal: showDependentsModal,
    doGeneratePatientCallId: generatePatientCallId,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(VdpcButtons);
