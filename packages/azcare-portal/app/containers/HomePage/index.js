/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 */

import React from 'react';
import PropTypes from 'prop-types';

import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { Grid, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { makeSelectVerifyAuth } from 'containers/App/selectors';

import VdpcButtons from './vdpc';
import IndividualButtons from './individual';

const useStyles = makeStyles({
  root: {
    borderRadius: 10,
    padding: '50px 20px',
    maxWidth: 1200,
    margin: 'auto',
  },
  title: {
    paddingBottom: 50,
  },
});
function HomePage({ account }) {
  const buttons = {
    70001: <IndividualButtons />,
    70002: <VdpcButtons />,
    70003: <VdpcButtons />,
    70004: <VdpcButtons />,
  };
  const classes = useStyles();
  if (!account.data) return null;
  return (
    <div className={classes.root}>
      <Grid container spacing={1} justify="center">
        <Grid item xs={12} xl={10}>
          <Typography className={classes.title} variant="h6">
            Dashboard
          </Typography>
          {buttons[account.data.patient.membershipTypeId]}
        </Grid>
      </Grid>
    </div>
  );
}

const { object } = PropTypes;
HomePage.propTypes = {
  account: object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  account: makeSelectVerifyAuth(),
});

const withConnect = connect(
  mapStateToProps,
  null,
);

export default compose(withConnect)(HomePage);
