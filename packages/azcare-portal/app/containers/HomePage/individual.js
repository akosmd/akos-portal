import React, { useState, useEffect } from 'react';

import PropTypes from 'prop-types';

import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { push } from 'connected-react-router';
import { compose } from 'redux';

import AkosCard from 'components/AkosCard';
import RecordsIcon from 'images/svgs/health-record.svg';
import BenefitsIcon from 'images/svgs/benefits-wallette.svg';
import MessageIcon from 'images/svgs/messages.svg';
import ProviderIcon from 'images/svgs/talk-to-provider.svg';
import PharmacyIcon from 'images/svgs/medication-lookup.svg';
import AppointmentIcon from 'images/svgs/my-appointment.svg';
import FAQICon from 'images/svgs/FAQ.svg';
import PricingIcon from 'images/svgs/pricing-and-plans.svg';
import { Grid } from '@material-ui/core';

import RxSpark from 'containers/RxSpark';

import {
  setInxiteUrl,
  getPatientDependents,
  showDependentsModal,
} from 'containers/App/actions';
import {
  makeSelectVerifyAuth,
  makeSelectInxiteSso,
  makeSelectInxiteUrl,
  makePatientDependents,
  makePatientImpersonation,
  makeShowDependentsModal,
} from 'containers/App/selectors';

function IndividualButtons({ dispatch, account, doGetPatientDependents }) {
  const [showPharmacy, setShowPharmacy] = useState(false);

  useEffect(() => {
    if (account.error) {
      dispatch(push('/login'));
    } else if (account.data) {
      dispatch(doGetPatientDependents(account.data.patient.id));
    }
  }, [account]);

  const onPharmacyClick = () => {
    setShowPharmacy(!showPharmacy);
  };

  const onPricingClick = () => {
    window.open('https://akosmd.com/pricing/', '_blank');
  };

  return (
    <Grid container spacing={10}>
      <Grid item xs={12} sm={6} md={4}>
        <AkosCard
          loaderStyle={{ height: 150 }}
          caption="Talk to a Provider"
          icon={() => <ProviderIcon width={150} height={150} />}
          onClick={() => dispatch(push('/chat?talkToProvider=true'))}
        />
      </Grid>
      <Grid item xs={12} sm={6} md={4}>
        {!showPharmacy && (
          <AkosCard
            loaderStyle={{ height: 150 }}
            caption="Medication Lookup"
            icon={() => <PharmacyIcon width={150} height={150} />}
            onClick={onPharmacyClick}
          />
        )}
        {showPharmacy && (
          <RxSpark
            onClick={onPharmacyClick}
            zipCode={account.data.patient.zipCode}
          />
        )}
      </Grid>
      <Grid item xs={12} sm={6} md={4}>
        <AkosCard
          loaderStyle={{ height: 150 }}
          caption="My Appointments"
          icon={() => <AppointmentIcon width={150} height={150} />}
          onClick={() => dispatch(push('/create-appointment'))}
        />
      </Grid>

      <Grid item xs={12} sm={6} md={4}>
        <AkosCard
          loaderStyle={{ height: 150 }}
          caption="FAQs"
          icon={() => <FAQICon width={150} height={150} />}
          onClick={() => dispatch(push('/faq'))}
        />
      </Grid>
      <Grid item xs={12} sm={6} md={4}>
        <AkosCard
          loaderStyle={{ height: 150 }}
          caption="Pricing & Plans"
          icon={() => <PricingIcon width={150} height={150} />}
          onClick={onPricingClick}
        />
      </Grid>
      <Grid item xs={12} sm={6} md={4}>
        <AkosCard
          loaderStyle={{ height: 150 }}
          caption="Messages (coming soon)"
          icon={() => <MessageIcon width={150} height={150} />}
          disabled
          onClick={() => {}}
        />
      </Grid>
      <Grid item xs={12} sm={6} md={4}>
        <AkosCard
          loaderStyle={{ height: 150 }}
          caption="Health Records (coming soon)"
          icon={() => <RecordsIcon width={150} height={150} />}
          disabled
          onClick={() => {}}
        />
      </Grid>
      <Grid item xs={12} sm={6} md={4}>
        <AkosCard
          loaderStyle={{ height: 150 }}
          caption="Benefits Wallet (coming soon)"
          icon={() => <BenefitsIcon width={150} height={150} />}
          disabled
          onClick={() => {}}
        />
      </Grid>
    </Grid>
  );
}

const { func, object } = PropTypes;
IndividualButtons.propTypes = {
  dispatch: func.isRequired,
  doGetPatientDependents: func.isRequired,
  account: object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  account: makeSelectVerifyAuth(),
  inxiteSso: makeSelectInxiteSso(),
  inxiteUrl: makeSelectInxiteUrl(),
  dependents: makePatientDependents(),
  impersonatedPatient: makePatientImpersonation(),
  impersonationShowModal: makeShowDependentsModal(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    doSetUrl: setInxiteUrl,
    doGetPatientDependents: getPatientDependents,
    doShowDependentsModal: showDependentsModal,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(IndividualButtons);
