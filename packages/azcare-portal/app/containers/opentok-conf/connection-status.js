import React from 'react';
import PropTypes from 'prop-types';
function ConnectionStatus({ connected }) {
  const status = connected ? 'Connected' : 'Disconnected';
  return (
    <div className="connectionStatus">
      <strong>Status:</strong> {status}
    </div>
  );
}

ConnectionStatus.propTypes = {
  connected: PropTypes.bool,
};
export default ConnectionStatus;
