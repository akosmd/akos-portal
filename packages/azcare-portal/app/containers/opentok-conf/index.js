import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { push } from 'connected-react-router';

import { makeStyles } from '@material-ui/core/styles';

import { OTSession, OTStreams, preloadScript } from 'opentok-react';

import {
  Grid,
  Typography,
  IconButton,
  Tooltip,
  Button,
} from '@material-ui/core';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import CallEndIcon from '@material-ui/icons/CallEnd';
import VideoCamOn from '@material-ui/icons/Videocam';
import VideoCamOff from '@material-ui/icons/VideocamOff';
import MicOn from '@material-ui/icons/Mic';
import MicOff from '@material-ui/icons/MicOff';
import WarningIcon from '@material-ui/icons/Warning';

import { makeSelectActualPatient } from 'containers/App/selectors';
import {
  makeSelectAddUserToWaitingRoom,
  makeSelectNotifyProviders,
  makeSelectWaitingRoomStatus,
  makeSelectOpenTokRoomKeys,
  makeSelectDocAlias,
  makeSelectPatientCallId,
} from 'containers/App/legacySelectors';

import {
  addUserToWaitingRoom,
  notifyProviders,
  checkWaitingRoomStatus,
  getOpenTokRoomKeys,
  generatePatientCallId,
  getDocAlias,
  updatePatientCallId,
  callEndByPatient,
  providerSettings,
  disconnectReason,
  resetWaitingRoom,
} from 'containers/App/legacyActions';

import { OPEN_TOK_API } from 'utils/config';

import Publisher from './publisher';
import Subscriber from './subscriber';

import './style.css';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
  text: {
    [theme.breakpoints.down('md')]: {
      display: 'none',
    },
  },
  controls: {
    marginTop: '-2.5rem',
    paddingBottom: '2rem',

    zIndex: theme.zIndex.drawer + 1,
    position: 'fixed',
    padding: '1rem',
    display: 'flex',
    left: 0,
    bottom: '0',
    width: '100%',
    color: 'white',

    justifyContent: 'center',
  },
  videoRoot: {
    width: '100%',
    height: '100%',
    '& > div': {
      height: '100%',
    },
    '& > div > div': {
      height: '100%',
    },
    '& > div > div > div': {
      height: '100%',
    },
    padding: '1px',
  },
  videoRootFull: {
    width: '100%',
    height: '100%',
    '& > div': {
      height: '100%',
    },
    '& > div > div': {
      height: '100%',
    },
    '& > div > div > div': {
      height: '100%',
    },
  },
  parentContainer: {
    height: '79vh',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  parentContainerFull: {
    height: '100vh',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'flex-start',
    marginTop: '50px',
  },
}));

function OpenTokConf({
  roomName,
  openTokKeys,
  patient,
  docAlias,
  callId,
  waitingRoomStatus,
  addUserToRoom,
  dispatch,
  doAddUserToRoom,
  doNotifyProviders,
  doCheckWaitingRoomStatus,
  doGetOpenTokRoomKeys,
  doGetDocAlias,
  doUpdatePatientCallId,
  doCallEndByPatient,
  doProviderSettings,
  doDisconnectReason,
  doResetWaitingRoom,
}) {
  const [error, setError] = useState();
  // eslint-disable-next-line no-unused-vars
  const [connected, setConnected] = useState(false);
  const [addingToRoom, setAddingToRoom] = useState(false);
  const [subscribed, setSubscribe] = useState(false);
  const sessionRef = React.useRef(null);

  const [audio, setAudio] = useState(true);
  const [video, setVideo] = useState(true);
  const [showAlert, setShowAlert] = useState(false);
  const [supported, setSupported] = useState(true);
  const [browserError, setBrowserError] = useState(false);

  const classes = useStyles();

  useEffect(() => {
    // eslint-disable-next-line no-useless-escape
    const isSafari = !!navigator.userAgent.match(/Version\/[\d\.]+.*Safari/);
    const iOS =
      /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
    if (iOS && !isSafari) {
      setSupported(false);
      setBrowserError(true);
    }
  }, []);

  useEffect(() => {
    if (patient.data && !addingToRoom) {
      dispatch(
        doCheckWaitingRoomStatus({
          patient_id: patient.data.patient.patientLegacyId,
        }),
      );
      dispatch(doGetDocAlias({ alias: roomName }));
    }
  }, [callId]);

  useEffect(() => {
    if (patient.data && !addingToRoom) {
      dispatch(
        doCheckWaitingRoomStatus({
          patient_id: patient.data.patient.patientLegacyId,
        }),
      );
      dispatch(doGetDocAlias({ alias: roomName }));
    }
  }, []);
  useEffect(() => {
    if (
      !waitingRoomStatus.loading &&
      !addUserToRoom.data &&
      callId.data &&
      docAlias.data &&
      docAlias.data.result.length > 0 &&
      !addingToRoom
    ) {
      const firstDoc = docAlias.data.result[0];
      const params = {
        docId: firstDoc.id,
        groupId: firstDoc.group_id,
        userId: patient.data.patient.patientLegacyId,
        symptom: '[]',
        allergy: '["No Allergies"]',
        medication: '["No Medications"]',
        medical_condition: '["No Pre-existing medical conditions"]',
        call_id: `${callId.data.result.call_id}`,
        date_of_injury: '',
        state_of_injury: '',
        employer_name: '',
        alias: roomName,
      };
      dispatch(doAddUserToRoom(params));
      dispatch(doUpdatePatientCallId(params));

      setAddingToRoom(true);
    }
  }, [callId, docAlias, addUserToRoom]);

  useEffect(() => {
    if (
      (!waitingRoomStatus.loading && waitingRoomStatus.data) ||
      addUserToRoom.data
    ) {
      dispatch(doNotifyProviders({ roomName }));
      dispatch(
        doGetOpenTokRoomKeys({ id: patient.data.patient.patientLegacyId }),
      );
    }
  }, [waitingRoomStatus, addUserToRoom]);

  const handleSetAudio = () => {
    setAudio(!audio);
  };

  const handleSetVideo = () => {
    setVideo(!video);
  };

  const sessionEvents = {
    sessionConnected: () => {
      setConnected(true);
    },
    sessionDisconnected: () => {
      setConnected(false);
    },
  };

  const onError = err => {
    setError(`Failed to connect: ${err.message}`);
  };

  const handleEndCall = () => {
    setShowAlert(true);
  };

  const handleClose = () => {
    setBrowserError(false);
    dispatch(push('/'));
  };

  const handleQuit = () => {
    dispatch(doCallEndByPatient({ callid: `${callId.data.result.call_id}` }));
    dispatch(doProviderSettings(roomName));
    dispatch(
      doDisconnectReason({
        sessionId: openTokKeys.data.result[0].session,
        reason: 'Call ended by user',
      }),
    );
    dispatch(doResetWaitingRoom());
    setTimeout(() => {
      window.location.href = '/';
    }, 2000);
  };

  const renderAlert = () => (
    <Dialog
      open={browserError}
      onClose={handleClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">Unsupported Browser</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          <WarningIcon color="error" /> <strong>Warning! </strong>Unfortunately
          the browser you are using is currently not supported. We only support
          latest Safari browser at this time, please open the link using Safari.
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose} color="primary">
          Close this window
        </Button>
      </DialogActions>
    </Dialog>
  );

  if (!openTokKeys.data) return null;
  if (!supported) return renderAlert();

  return (
    <OTSession
      apiKey={OPEN_TOK_API}
      sessionId={openTokKeys.data.result[0].session}
      token={openTokKeys.data.result[0].token}
      eventHandlers={sessionEvents}
      ref={sessionRef}
      onError={onError}
    >
      <Grid
        container
        alignItems="center"
        spacing={1}
        justify="center"
        className={
          !subscribed ? classes.parentContainerFull : classes.parentContainer
        }
      >
        <Grid item xs={12} lg={8} className={classes.text}>
          <Typography variant="h6" align="center">
            Hello{' '}
            {`${patient.data.patient.firstName} ${
              patient.data.patient.lastName
            } `}
            ! Thanks for joining our consultation room. Our providers are aware
            of your arrival in this waiting room and they will be with you
            shortly.
          </Typography>
          <Typography align="center">
            <em>
              Please make sure you allow the portal to access your camera and
              microphone when prompted.
            </em>
          </Typography>
        </Grid>
        <Grid
          item
          xs={12}
          lg={6}
          className={!subscribed ? classes.videoRootFull : classes.videoRoot}
          style={{ padding: '1px' }}
        >
          {error ? <div id="error">{error}</div> : null}

          <Publisher
            session={
              sessionRef.current !== null ? sessionRef.current : undefined
            }
            handleSetAudio={handleSetAudio}
            handleSetVideo={handleSetVideo}
            audio={audio}
            video={video}
            showAlert={showAlert}
            setShowAlert={setShowAlert}
            onQuit={handleQuit}
          />
        </Grid>

        {!subscribed && (
          <Grid item xs={12} lg={6}>
            <Typography variant="h6" align="center" className={classes.text}>
              Please wait, we will be in touch shortly
            </Typography>
            <OTStreams>
              <Subscriber onSubscribed={setSubscribe} />
            </OTStreams>
          </Grid>
        )}
        {subscribed && (
          <Grid
            item
            xs={12}
            lg={6}
            className={classes.videoRoot}
            style={{ padding: '1px' }}
          >
            <OTStreams>
              <Subscriber onSubscribed={setSubscribe} />
            </OTStreams>
          </Grid>
        )}

        <Grid item xs={12} className={classes.controls}>
          <Tooltip title="End Call">
            <IconButton size="medium" onClick={handleEndCall}>
              <CallEndIcon color="error" fontSize="large" />
            </IconButton>
          </Tooltip>

          <Tooltip title="Toggle Video">
            <IconButton size="medium" onClick={handleSetVideo}>
              {!video ? (
                <VideoCamOff color="secondary" fontSize="large" />
              ) : (
                <VideoCamOn color="secondary" fontSize="large" />
              )}
            </IconButton>
          </Tooltip>
          <Tooltip title="Mute/Unmute Audio">
            <IconButton size="medium" onClick={handleSetAudio}>
              {!audio ? (
                <MicOff color="secondary" fontSize="large" />
              ) : (
                <MicOn color="secondary" fontSize="large" />
              )}
            </IconButton>
          </Tooltip>
        </Grid>
      </Grid>
    </OTSession>
  );
}

const { object, func, string } = PropTypes;
OpenTokConf.propTypes = {
  dispatch: func.isRequired,
  roomName: string,
  openTokKeys: object.isRequired,
  addUserToRoom: object.isRequired,
  patient: object.isRequired,
  callId: object.isRequired,
  waitingRoomStatus: object.isRequired,
  docAlias: object.isRequired,
  doAddUserToRoom: func.isRequired,
  doNotifyProviders: func.isRequired,
  doCheckWaitingRoomStatus: func.isRequired,
  doGetOpenTokRoomKeys: func.isRequired,
  doGetDocAlias: func.isRequired,
  doUpdatePatientCallId: func.isRequired,
  doCallEndByPatient: func.isRequired,
  doProviderSettings: func.isRequired,
  doDisconnectReason: func.isRequired,
  doResetWaitingRoom: func.isRequired,
};
OpenTokConf.defaultProps = {
  roomName: 'demo',
};
const mapStateToProps = createStructuredSelector({
  patient: makeSelectActualPatient(),
  waitingRoomStatus: makeSelectWaitingRoomStatus(),
  openTokKeys: makeSelectOpenTokRoomKeys(),
  notifyProviders: makeSelectNotifyProviders(),
  addUserToRoom: makeSelectAddUserToWaitingRoom(),
  docAlias: makeSelectDocAlias(),
  callId: makeSelectPatientCallId(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    doAddUserToRoom: addUserToWaitingRoom,
    doNotifyProviders: notifyProviders,
    doCheckWaitingRoomStatus: checkWaitingRoomStatus,
    doGetOpenTokRoomKeys: getOpenTokRoomKeys,
    doGetDocAlias: getDocAlias,
    doUpdatePatientCallId: updatePatientCallId,
    doCallEndByPatient: callEndByPatient,
    doProviderSettings: providerSettings,
    doDisconnectReason: disconnectReason,
    doResetWaitingRoom: resetWaitingRoom,
    doGenerateCallId: generatePatientCallId,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(preloadScript(OpenTokConf));
