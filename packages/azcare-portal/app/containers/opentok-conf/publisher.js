import React, { Fragment, useState } from 'react';
import { OTPublisher } from 'opentok-react';
import PropTypes from 'prop-types';

import { Button, Typography } from '@material-ui/core';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

function Publisher({
  onQuit,
  audio,
  video,
  handleSetAudio,
  handleSetVideo,
  showAlert,
  setShowAlert,
}) {
  const [error, setError] = useState(false);

  const handleError = e => {
    if (e.code === 1500) {
      setError(
        "It looks like you've block permission to access your camera and microphone. Check your browser settings.",
      );
    }
  };

  const handleClose = () => {
    setShowAlert(!showAlert);
  };

  const handleConfirmQuit = () => {
    handleSetVideo();
    handleSetAudio();
    handleClose();
    onQuit();
  };

  const renderConfirmEndCall = () => (
    <Dialog
      open={showAlert}
      onClose={handleClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">Confirm End Call</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          Do you really want to end this call?
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleConfirmQuit} color="primary">
          Yes
        </Button>
        <Button onClick={handleClose} color="primary" autoFocus>
          No
        </Button>
      </DialogActions>
    </Dialog>
  );

  return (
    <Fragment>
      {error && (
        <div id="error">
          <Typography variant="h6" align="center">
            {error}
          </Typography>
        </div>
      )}
      <OTPublisher
        properties={{
          publishAudio: audio,
          publishVideo: video,
          videoSource: undefined,
        }}
        onError={handleError}
      />
      {renderConfirmEndCall()}
    </Fragment>
  );
}

const { bool, func } = PropTypes;
Publisher.propTypes = {
  onQuit: func.isRequired,
  audio: bool,
  video: bool,
  showAlert: bool,
  handleSetAudio: func,
  handleSetVideo: func,
  setShowAlert: func,
};
export default Publisher;
