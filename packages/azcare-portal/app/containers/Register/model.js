import moment from 'moment';

import { initialState, initialField } from 'utils/formHelper';

export const medicalHistoryModel = {
  medications: {
    ...initialField,
    id: 'medications',
    caption: 'Medications',
    required: false,
  },
  preExistingConditions: {
    ...initialField,
    id: 'preExistingConditions',
    caption: 'Pre-existing medical conditions',
    required: false,
  },
  allergies: {
    ...initialField,
    id: 'allergies',
    caption: 'Allergies',
    required: false,
  },
};

export const dependentModel = {
  firstName: {
    ...initialField,
    id: 'firstName',
    caption: 'First Name',
    required: true,
  },
  middleName: {
    ...initialField,
    id: 'middleName',
    caption: 'Middle Name',
    error: false,
  },
  lastName: {
    ...initialField,
    id: 'lastName',
    caption: 'Last Name',
    required: true,
  },
  dateOfBirth: {
    ...initialField,
    id: 'dateOfBirth',
    caption: 'Date of Birth',
    value: null,
  },
  gender: {
    ...initialField,
    id: 'gender',
    caption: 'Gender',
    variant: 'outlined',
  },
  address1: { ...initialField, id: 'address1', caption: 'Address 1' },
  address2: {
    ...initialField,
    id: 'address2',
    caption: 'Address 2',
    required: false,
    error: false,
  },
  city: { ...initialField, id: 'city', caption: 'City' },
  state: { ...initialField, id: 'state', caption: 'State' },
  country: {
    ...initialField,
    id: 'country',
    caption: 'Country',
    value: 'US',
    prisine: false,
    error: false,
  },
  zipCode: { ...initialField, id: 'zipCode', caption: 'Zip Code' },
  ...initialState,
};

export default {
  firstName: {
    ...initialField,
    id: 'firstName',
    caption: 'First Name',
    required: false,
    error: false,
  },
  middleName: {
    ...initialField,
    id: 'middleName',
    caption: 'Middle Name',
    required: false,
    error: false,
  },
  lastName: {
    ...initialField,
    id: 'lastName',
    caption: 'Last Name',
  },
  dateOfBirth: {
    ...initialField,
    id: 'dateOfBirth',
    caption: 'Date of Birth',
    errorMessage: 'You should be atleast 19 years old to register',
    value: null,
  },
  email: {
    ...initialField,
    id: 'email',
    caption: 'Email Address',
  },
  password: {
    ...initialField,
    id: 'password',
    caption: 'Password',
    type: 'password',
  },
  confirmPassword: {
    ...initialField,
    id: 'confirmPassword',
    caption: 'Confirm Password',
    type: 'password',
  },
  phone: {
    ...initialField,
    id: 'phone',
    caption: 'Phone Number',
  },
  gender: {
    ...initialField,
    id: 'gender',
    caption: 'Gender',
    variant: 'outlined',
  },
  address1: { ...initialField, id: 'address1', caption: 'Address 1' },
  address2: {
    ...initialField,
    id: 'address2',
    caption: 'Address 2',
    required: false,
    error: false,
  },
  city: { ...initialField, id: 'city', caption: 'City' },
  country: {
    ...initialField,
    id: 'country',
    caption: 'Country',
    value: 'US',
    prisine: false,
    error: false,
  },
  state: { ...initialField, id: 'state', caption: 'State' },
  zipCode: {
    ...initialField,
    id: 'zipCode',
    caption: 'Zip Code',
  },
  ethnicity: {
    ...initialField,
    id: 'ethnicity',
    caption: 'Ethnicity',
  },
  race: {
    ...initialField,
    id: 'race',
    caption: 'Race',
  },
  consent: {
    ...initialField,
    id: 'consent',
    caption: 'I Agree',
    required: false,
    value: 0,
  },
  signature: {
    ...initialField,
    base64: null,
    filename: null,
    filetype: null,
    dateStamp: moment(Date.now()).format('YYYY-MM-DD HH:mm:ss'),
    isFile: true,
    id: 'signature',
    required: false,
    error: false,
    caption: 'Signature',
  },

  fullName: {
    ...initialField,
    id: 'fullName',
    caption: 'Or, Type your Full name if you cannot sign above',
    required: false,
    error: false,
  },
  ...initialState,
};
