import React from 'react';
import PropTypes from 'prop-types';
import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Typography,
} from '@material-ui/core';
import Button from 'components/GradientButton';

export default function ExistsDialog({ open }) {
  return (
    <div>
      <Dialog
        open={open}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          <Typography color="secondary">Member Exists</Typography>
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            It seems your account is registered already. Please login.
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button href="/login">Yes, take me to Login Screen</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

ExistsDialog.propTypes = {
  open: PropTypes.bool,
};
