import React from 'react';
import PropTypes from 'prop-types';
import { Typography } from '@material-ui/core';
import Button from 'components/GradientButton';

function NotificationMessage({ recipient }) {
  return (
    <Typography component="div" align="center">
      <Typography variant="h6" align="center" color="secondary">
        Verification Link Sent
      </Typography>
      <Typography>
        Verification link sent to <strong>{recipient}</strong>, please check.
        Once verified you can login to your account.
      </Typography>
      <br />
      <Button href="/">Login Now</Button>
    </Typography>
  );
}

NotificationMessage.propTypes = {
  recipient: PropTypes.string,
};

export default NotificationMessage;
