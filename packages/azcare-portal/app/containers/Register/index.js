import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { push } from 'connected-react-router';

import _ from 'lodash';

import SignatureCanvas from 'react-signature-canvas';

import moment from 'moment';
import { createStructuredSelector } from 'reselect';
import { Grid, Typography, Card } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import TextField from 'components/TextField';
import Button from 'components/GradientButton';
import DatePickerField from 'components/DatePickerField';
import SelectField from 'components/Select';
import ConsentForm from 'components/Consent';
import DateFnsUtils from '@date-io/date-fns';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';

import InputLabel from '@material-ui/core/InputLabel';

// eslint-disable-next-line no-unused-vars
import SignaturePad from 'react-signature-pad-wrapper';

import useMediaQuery from '@material-ui/core/useMediaQuery';
import statesList from 'utils/stateList.json';

import {
  handleChange,
  highlightFormErrors,
  handleEmailChange,
  handleDateChange,
  extractFormValues,
  handlePhoneChange,
  handleEqualityChange,
  formatPhoneNumber,
} from 'utils/formHelper';
import countries from 'utils/countries.json';
import ethnicities from 'utils/ethnicities.json';
import races from 'utils/races.json';

import {
  register as registerRequest,
  resetRegisterData,
  logoutUser,
  findPatient,
} from 'containers/App/actions';

import {
  makeSelectRegister,
  makeSelectFindPatient,
} from 'containers/App/selectors';
import model from './model';

import NoticePage from './confirmRegistration';
import NotificationMessage from './notificationMessage';

import { patientExists } from './patientEvaluator';
import ExistsDialog from './existsDialog';

const useStyles = makeStyles(theme => ({
  root: {
    padding: '40px 0',
  },
  formContainer: {
    width: '100%',
    alignSelf: 'center',
  },
  cardContainer: {
    padding: 20,
  },
  paddedContainer: {
    paddingTop: 15,
  },
  loginLink: {
    marginLeft: '1rem',
  },
  signingPad: {
    border: `1px solid ${theme.palette.input.active}`,
    padding: '1rem',
    borderRadius: '4px',
    width: '100%',
    height: '200px',
  },
  signingLabel: {
    padding: '1rem 0',
    color: theme.typography.color.gray,
  },
  sigControls: {
    display: 'flex',
    justifyContent: 'space-between',
    padding: '10px 0',
  },
  padding: {
    padding: '15px 0',
  },
  buttonLabel: {
    fontWeight: 'bold',
    color: theme.typography.color.dark,
  },
}));

export function Register({
  doRegister,
  dispatch,
  enqueueSnackbar,
  registrationData,
  foundPatients,
  doResetRegister,
  doLogoutUser,
  history,
  doFindPatient,
}) {
  const [register, setRegister] = useState({ ...model });
  const [readConsent, setReadConsent] = useState(false);
  const [sigPad, setSigPad] = useState(null);
  const [showNotice, setshowNotice] = useState(false);
  const [showConfirm, setshowConfirm] = useState(false);
  const [sendTo, setSendTo] = useState('');
  const [showExists, setShowExists] = useState(false);

  const [showMemberGroupCodeField, setShowMemberGroupCodeField] = useState(
    false,
  );
  const isMobile = useMediaQuery(theme => theme.breakpoints.down('sm'));

  // when user tries to register, make sure to cleanup existing store
  useEffect(() => {
    dispatch(doLogoutUser());
  }, []);

  useEffect(() => {
    if (registrationData.data) {
      setRegister({ ...model });
      setshowNotice(false);
      setshowConfirm(true);
      dispatch(doResetRegister());
    }
  }, [registrationData]);

  useEffect(() => {
    const {
      location: { search },
    } = history;
    if (search !== '') {
      evaluateQs(search);
    }
  }, [history]);

  useEffect(() => {
    if (foundPatients.data) {
      const formValues = extractFormValues(register);
      const exists = patientExists({
        patients: foundPatients.data,
        patientToFind: formValues,
      });
      if (exists) setShowExists(true);
    }
  }, [foundPatients]);

  const evaluateQs = search => {
    const params = search.split('&');
    const code = params[0].replace('?memberGroupCode=', '');
    setShowMemberGroupCodeField(true);
    setRegister({
      ...register,
      memberGroupCode: {
        ...register.memberGroupCode,
        value: code,
        error: false,
        pristine: false,
        readOnly: true,
      },
    });
  };

  const classes = useStyles();

  const onInputChange = field => event => {
    handleChange({
      field: field.id,
      state: register,
      event,
      saveStepFunc: setRegister,
    });
  };

  const onEmailChange = field => event => {
    handleEmailChange({
      field: field.id,
      state: register,
      event,
      saveStepFunc: setRegister,
    });
    const params = {
      limit: 100,
      find: event.target.value,
      look_up_columns: ['email'],
    };
    const debounced = _.debounce(() => dispatch(doFindPatient(params)), 2000);
    if (event.target.value) debounced();
  };

  const onDateChange = field => value => {
    const years = moment(Date.now()).diff(moment(value), 'years');
    if (years > 18) {
      handleDateChange({
        field: field.id,
        state: register,
        value,
        saveStepFunc: setRegister,
      });
      const params = {
        limit: 100,
        find: moment(value).format('YYYY-MM-DD'),
        look_up_columns: ['birth_date_at'],
      };
      const debounced = _.debounce(() => dispatch(doFindPatient(params)), 2000);
      if (value) debounced();
    } else {
      handleDateChange({
        field: field.id,
        state: register,
        value: null,
        error: true,

        saveStepFunc: setRegister,
      });
    }
  };

  const onPasswordChange = () => event => {
    handleEqualityChange({
      field1: register.password,
      field2: register.confirmPassword,
      state: register,
      event,
      saveStepFunc: setRegister,
    });
  };

  const onCountryChange = event => {
    const { value: country } = event.target;
    const isUs = country === 'US';
    register.state = {
      ...register.state,
      value: '',
      error: isUs,
      pristine: true,
      required: isUs,
    };
    register.zipCode = {
      ...register.zipCode,
      value: '',
      error: isUs,
      pristine: true,
      required: isUs,
    };

    handleChange({
      field: register.country.id,
      state: register,
      event,
      saveStepFunc: setRegister,
    });
  };

  const handleSubmit = (notifyVia, to) => {
    const formValues = extractFormValues(register);
    const dateOfBirth = moment(formValues.dateOfBirth).format('YYYY-MM-DD');
    const params = {
      ...formValues,
      dateOfBirth,
      sendTo: notifyVia,
    };
    setSendTo(to);
    setshowConfirm(true);
    dispatch(doRegister(params));
  };

  const handleShowNotif = () => {
    const hasSigned =
      register.signature.base64 || register.fullName.value !== '';
    if (!register.completed || !hasSigned) {
      highlightFormErrors(register, setRegister);
      enqueueSnackbar('Please fill out all fields marked with asterisk *', {
        variant: 'error',
      });
    } else {
      setshowNotice(true);
    }
  };

  const onSigned = () => {
    const signature = {
      ...model.signature,
      dateStamp: moment(Date.now()).format('YYYY-MM-DD HH:mm:ss'),
      base64: sigPad.getTrimmedCanvas().toDataURL('image/png'),
      filename: `signature-${Date.now()}.png`,
      filetype: 'image/png',
    };
    setRegister({
      ...register,
      signature,
      fullName: { ...register.fullName, error: false },
    });
  };
  const clear = () => {
    const hasFullName = register.fullName.value !== '';
    setRegister({
      ...register,
      signature: model.signature,
      fullName: { ...register.fullName, error: !hasFullName },
    });
    sigPad.clear();
  };

  const handleBack = () => {
    setshowNotice(!showNotice);
  };

  const handleSearch = field => event => {
    handleChange({
      field: field.id,
      state: register,
      event,
      saveStepFunc: setRegister,
    });
    const params = {
      limit: 100,
      find: event.target.value,
      look_up_columns: ['email', 'last_name', 'first_name', 'phone'],
    };
    const debounced = _.debounce(() => dispatch(doFindPatient(params)), 2000);
    if (event.target.value) debounced();
  };

  const handlePhoneSearch = field => event => {
    handlePhoneChange({
      field: field.id,
      state: register,
      event,
      saveStepFunc: setRegister,
    });
    const params = {
      limit: 100,
      find: formatPhoneNumber(event.target.value).value,
      look_up_columns: ['phone'],
    };
    const debounced = _.debounce(() => dispatch(doFindPatient(params)), 2000);
    if (event.target.value) debounced();
  };

  if (showConfirm) return <NotificationMessage recipient={sendTo} />;

  if (showNotice) {
    return (
      <NoticePage
        onSendEmail={handleSubmit}
        onSendPhone={handleSubmit}
        onBack={handleBack}
        details={register}
      />
    );
  }

  return (
    <>
      <MuiPickersUtilsProvider utils={DateFnsUtils}>
        <form autoComplete="off" className={classes.root}>
          <Grid spacing={2} container direction="column">
            <Grid item xs={12} md={8} className={classes.formContainer}>
              <Card className={classes.cardContainer}>
                <Typography variant="h4">Registration</Typography>
                <Typography variant="body1" color="textPrimary">
                  Please provide the information below to create an account
                  profile
                </Typography>
                <Grid item xs={12} className={classes.paddedContainer}>
                  <Grid
                    container
                    spacing={3}
                    justify="center"
                    alignItems="center"
                  >
                    <Grid item xs={12} md={4}>
                      <TextField
                        keyEvents={{
                          handleKeys: ['enter'],
                          onKeyEvent: handleShowNotif,
                        }}
                        field={register.firstName}
                        variant="outlined"
                        onChange={onInputChange(register.firstName)}
                      />
                    </Grid>
                    <Grid item xs={12} md={4}>
                      <TextField
                        keyEvents={{
                          handleKeys: ['enter'],
                          onKeyEvent: handleShowNotif,
                        }}
                        field={register.middleName}
                        variant="outlined"
                        onChange={onInputChange(register.middleName)}
                      />
                    </Grid>

                    <Grid item xs={12} md={4}>
                      <TextField
                        keyEvents={{
                          handleKeys: ['enter'],
                          onKeyEvent: handleShowNotif,
                        }}
                        field={register.lastName}
                        variant="outlined"
                        onChange={handleSearch(register.lastName)}
                      />
                    </Grid>
                    <Grid item xs={12} md={6}>
                      <DatePickerField
                        keyEvents={{
                          handleKeys: ['enter'],
                          onKeyEvent: handleShowNotif,
                        }}
                        format="MM/dd/yyyy"
                        openTo="year"
                        maxDate={moment().add(-19, 'year')}
                        clearable
                        fullWidth
                        field={register.dateOfBirth}
                        onChange={onDateChange(register.dateOfBirth)}
                        disableFuture
                      />
                    </Grid>
                    <Grid item xs={12} md={6}>
                      <TextField
                        keyEvents={{
                          handleKeys: ['enter'],
                          onKeyEvent: handleShowNotif,
                        }}
                        field={register.email}
                        variant="outlined"
                        type="email"
                        onChange={onEmailChange(register.email)}
                      />
                    </Grid>
                    <Grid item xs={12} md={6}>
                      {showMemberGroupCodeField && (
                        <TextField
                          keyEvents={{
                            handleKeys: ['enter'],
                            onKeyEvent: handleShowNotif,
                          }}
                          field={register.firstName}
                          variant="outlined"
                          onChange={onInputChange(register.firstName)}
                        />
                      )}
                    </Grid>
                    {!isMobile && <Grid item md={6} />}
                    <Grid item xs={12} md={6}>
                      <TextField
                        field={register.password}
                        type="password"
                        variant="outlined"
                        onChange={onPasswordChange(register.password)}
                        keyEvents={{
                          handleKeys: ['enter'],
                          onKeyEvent: handleShowNotif,
                        }}
                      />
                    </Grid>
                    <Grid item xs={12} md={6}>
                      <TextField
                        field={register.confirmPassword}
                        variant="outlined"
                        type="password"
                        onChange={onPasswordChange(register.confirmPassword)}
                        keyEvents={{
                          handleKeys: ['enter'],
                          onKeyEvent: handleShowNotif,
                        }}
                      />
                    </Grid>
                    <Grid item xs={12} md={6}>
                      <TextField
                        field={register.phone}
                        variant="outlined"
                        onChange={handlePhoneSearch(register.phone)}
                        keyEvents={{
                          handleKeys: ['enter'],
                          onKeyEvent: handleShowNotif,
                        }}
                      />
                    </Grid>
                    <Grid item xs={12} md={6}>
                      <SelectField
                        keyEvents={{
                          handleKeys: ['enter'],
                          onKeyEvent: handleShowNotif,
                        }}
                        field={register.gender}
                        options={[
                          { name: '', value: '' },
                          { name: 'Male', value: '12000' },
                          { name: 'Female', value: '12001' },
                        ]}
                        variant="outlined"
                        onChange={onInputChange(register.gender)}
                      />
                    </Grid>
                    <Grid item xs={12} md={6}>
                      <TextField
                        field={register.address1}
                        variant="outlined"
                        onChange={onInputChange(register.address1)}
                        keyEvents={{
                          handleKeys: ['enter'],
                          onKeyEvent: handleShowNotif,
                        }}
                      />
                    </Grid>
                    <Grid item xs={12} md={6}>
                      <TextField
                        field={register.address2}
                        variant="outlined"
                        onChange={onInputChange(register.address2)}
                        keyEvents={{
                          handleKeys: ['enter'],
                          onKeyEvent: handleShowNotif,
                        }}
                      />
                    </Grid>
                    <Grid item xs={12} md={6}>
                      <TextField
                        field={register.city}
                        variant="outlined"
                        onChange={onInputChange(register.city)}
                        keyEvents={{
                          handleKeys: ['enter'],
                          onKeyEvent: handleShowNotif,
                        }}
                      />
                    </Grid>
                    <Grid item xs={12} md={6}>
                      <SelectField
                        keyEvents={{
                          handleKeys: ['enter'],
                          onKeyEvent: handleShowNotif,
                        }}
                        field={register.country}
                        options={countries}
                        variant="outlined"
                        onChange={onCountryChange}
                      />
                    </Grid>
                    <Grid item xs={12} md={6}>
                      {register.country.value === 'US' ? (
                        <SelectField
                          keyEvents={{
                            handleKeys: ['enter'],
                            onKeyEvent: handleShowNotif,
                          }}
                          field={register.state}
                          options={statesList}
                          variant="outlined"
                          onChange={onInputChange(register.state)}
                        />
                      ) : (
                        <TextField
                          keyEvents={{
                            handleKeys: ['enter'],
                            onKeyEvent: handleShowNotif,
                          }}
                          field={register.state}
                          variant="outlined"
                          onChange={onInputChange(register.state)}
                        />
                      )}
                    </Grid>
                    <Grid item xs={12} md={6}>
                      <TextField
                        field={register.zipCode}
                        variant="outlined"
                        onChange={onInputChange(register.zipCode)}
                        keyEvents={{
                          handleKeys: ['enter'],
                          onKeyEvent: handleShowNotif,
                        }}
                      />
                    </Grid>
                    <Grid item xs={12} md={6}>
                      <SelectField
                        keyEvents={{
                          handleKeys: ['enter'],
                          onKeyEvent: handleShowNotif,
                        }}
                        field={register.ethnicity}
                        options={ethnicities}
                        variant="outlined"
                        onChange={onInputChange(register.ethnicity)}
                      />
                    </Grid>
                    <Grid item xs={12} md={6}>
                      <SelectField
                        keyEvents={{
                          handleKeys: ['enter'],
                          onKeyEvent: handleShowNotif,
                        }}
                        field={register.race}
                        options={races}
                        variant="outlined"
                        onChange={onInputChange(register.race)}
                      />
                    </Grid>
                  </Grid>
                  <Grid item xs={12}>
                    <ConsentForm
                      profile={register}
                      readConsent={readConsent}
                      handleChange={handleChange}
                      setReadConsent={setReadConsent}
                      setProfile={setRegister}
                    />
                  </Grid>
                  <Grid item xs={12} md={12}>
                    {/* <FormControl margin="normal" required fullWidth> */}
                    <InputLabel
                      htmlFor="signature"
                      className={classes.signingLabel}
                    >
                      Signature
                    </InputLabel>
                    <SignatureCanvas
                      penColor="black"
                      id="signature"
                      onEnd={onSigned}
                      ref={ref => {
                        setSigPad(ref);
                      }}
                      canvasProps={{
                        className: classes.signingPad,
                        redrawOnResize: true,
                      }}
                      clearOnResize={false}
                    />
                    {/* </FormControl> */}
                  </Grid>
                  <Grid item xs={12} md={12} className={classes.sigControls}>
                    <Button
                      variant="outlined"
                      onClick={clear}
                      disabled={!register.signature.base64}
                    >
                      Clear Signature
                    </Button>
                  </Grid>
                  <Grid item xs={12} className={classes.paddedContainer}>
                    <TextField
                      field={register.fullName}
                      variant="outlined"
                      onChange={onInputChange(register.fullName)}
                    />
                  </Grid>
                  <Grid container spacing={1}>
                    <Grid item xs={12} sm={4} className={classes.padding}>
                      <Button
                        variant="contained"
                        size="large"
                        onClick={handleShowNotif}
                        fullWidth
                        disabled={
                          !readConsent ||
                          !register.consent.value ||
                          registrationData.loading
                        }
                      >
                        Save & continue
                      </Button>
                    </Grid>
                    <Grid item xs={12} sm={4} className={classes.padding}>
                      <Button
                        size="large"
                        variant="outlined"
                        fullWidth
                        onClick={() => dispatch(push('/login'))}
                      >
                        <Typography className={classes.buttonLabel}>
                          {'BACK TO SIGN IN'}
                        </Typography>
                      </Button>
                    </Grid>
                  </Grid>
                </Grid>
              </Card>
            </Grid>
          </Grid>
        </form>
      </MuiPickersUtilsProvider>
      <ExistsDialog open={showExists} />
    </>
  );
}

const { func, object } = PropTypes;
Register.propTypes = {
  enqueueSnackbar: func.isRequired,
  dispatch: func.isRequired,
  doResetRegister: func.isRequired,
  doRegister: func.isRequired,
  doLogoutUser: func.isRequired,
  registrationData: object.isRequired,
  history: object.isRequired,
  foundPatients: object.isRequired,
  doFindPatient: func.isRequired,
};

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    doResetRegister: resetRegisterData,
    doRegister: registerRequest,
    doLogoutUser: logoutUser,
    doFindPatient: findPatient,
  };
}

const mapStateToProps = createStructuredSelector({
  registrationData: makeSelectRegister(),
  foundPatients: makeSelectFindPatient(),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(Register);
