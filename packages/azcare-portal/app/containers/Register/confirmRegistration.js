import React from 'react';
import PropTypes from 'prop-types';
import { Grid, Typography } from '@material-ui/core';
import EmailIcon from '@material-ui/icons/Email';
import PhoneAndroidIcon from '@material-ui/icons/PhoneAndroid';
import EditIcon from '@material-ui/icons/Edit';

import { makeStyles } from '@material-ui/core/styles';
import Button from 'components/GradientButton';

const useStyles = makeStyles({
  header: {
    marginTop: '2rem',
  },
  buttonContainer: {
    marginTop: '1rem',
  },
  centered: {
    marginTop: '2rem',
    display: 'flex',
    justifyContent: 'center',
  },
});
function ConfirmPage({ details, onSendEmail, onSendPhone, onBack }) {
  const classes = useStyles();
  return (
    <Grid
      container
      alignItems="center"
      justify="center"
      alignContent="center"
      spacing={1}
    >
      <Grid item xs={12} className={classes.header}>
        <Typography variant="h5" align="center" color="secondary">
          Verification Notice
        </Typography>
        <Typography align="center" component="div" className={classes.header}>
          Hello <strong>{details.firstName.value},</strong>
          <br /> We will be sending you a confirmation link to confirm your
          registration with us. Please select how you want to be notified.
        </Typography>
      </Grid>

      <Grid
        container
        alignItems="center"
        justify="center"
        alignContent="center"
        spacing={1}
      >
        <Grid item xs={12} md={4} direction="row">
          <Typography variant="h6" align="center">
            Option 1
          </Typography>
          <Grid item xs={12} md={12} align="center">
            <Button
              variant="contained"
              startIcon={<EmailIcon />}
              color="primary"
              onClick={() => onSendEmail('email', details.email.value)}
            >
              Send to {details.email.value}
            </Button>
          </Grid>
        </Grid>

        <Grid item xs={12} md={4} direction="row">
          <Typography variant="h6" align="center">
            Option 2
          </Typography>
          <Grid item xs={12} md={12} align="center">
            <Button
              variant="contained"
              startIcon={<PhoneAndroidIcon />}
              color="primary"
              onClick={() => onSendPhone('mobile', details.phone.value)}
            >
              Send to {details.phone.value}
            </Button>
          </Grid>
        </Grid>
      </Grid>
      <Grid item xs={12} className={classes.centered}>
        <Button startIcon={<EditIcon />} onClick={onBack}>
          Edit my Information
        </Button>
      </Grid>
    </Grid>
  );
}

const { object, func } = PropTypes;

ConfirmPage.propTypes = {
  details: object.isRequired,
  onSendEmail: func.isRequired,
  onSendPhone: func.isRequired,
  onBack: func.isRequired,
};

export default ConfirmPage;
