import moment from 'moment';

export const cleanedPhone = phone => {
  const cleaned = phone
    .replace('(', '')
    .replace(')', '')
    .replace('-', '')
    .replace(' ', '');

  return cleaned;
};

export const patientExists = ({ patients, patientToFind }) => {
  if (!patientExists || (patients && patients.length === 0)) return false;

  let exists = patients.find(
    p =>
      p.firstName.toLowerCase() === patientToFind.firstName.toLowerCase() &&
      p.lastName.toLowerCase() === patientToFind.lastName.toLowerCase() &&
      moment(p.birthDateAt).format('YYYY-MM-DD') ===
        moment(patientToFind.dateOfBirth).format('YYYY-MM-DD'),
  );

  if (!exists) {
    // find by Email
    exists = patients.find(
      p => p.email.toLowerCase() === patientToFind.email.toLowerCase(),
    );
  }

  if (!exists) {
    // find by Phone Number
    exists = patients.find(
      p => cleanedPhone(p.phoneNumber) === cleanedPhone(patientToFind.phone),
    );
  }
  return exists;
};
