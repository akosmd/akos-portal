/* eslint-disable jsx-a11y/anchor-is-valid */
/**
 *
 * Login
 *
 */

import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'connected-react-router';

import { createStructuredSelector } from 'reselect';

import { compose } from 'redux';
import { Grid, Typography, Link, Card } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import TextField from 'components/TextField';
import GradientButton from 'components/GradientButton';
import MyLink from 'components/MyLink';
import {
  verifyAuthentication,
  reSendVerificationCode,
  resetVerificationCode,
  initializeInxiteSso,
  resetSignin,
} from 'containers/App/actions';

import {
  makeSelectSignin,
  makeSelectVerifyAuth,
  makeIsSentToEmail,
  makeSelectVerificationCode,
  makeSelectInxiteSso,
} from 'containers/App/selectors';

const useStyles = makeStyles(theme => ({
  root: {
    padding: 20,
    borderRadius: 10,
    boxShadow: theme.palette.boxShadow.main,
  },
  wrapper: {
    height: 'calc(100vh - 115px)',
    display: 'flex',
    flex: 1,
    justifyContent: 'center',
  },
  headerContainer: {
    paddingBottom: 10,
  },
  footerContainer: {
    padding: '10px 0',
    borderTop: `1px solid ${theme.palette.background.default}`,
  },
  container: {
    padding: '10px 0',
  },
  text: {
    padding: 10,
    textAlign: 'center',
  },
  buttonLabel: {
    fontWeight: 'bold',
    color: theme.typography.color.dark,
  },
}));

export function TwoFactorVerification({
  dispatch,
  doVerifyAuth,
  doResendVerificationCode,
  doResetVerificationCode,
  doResetSignin,
  signin,
  auth,
  verificationCode,
  isSentToEmail,
  inxiteSso,
  doInitInxiteSso,
  enqueueSnackbar,
}) {
  const [code, setCode] = useState('');
  const classes = useStyles();
  useEffect(() => {
    if (!signin.data) {
      dispatch(push('/login'));
    }
  }, [signin]);

  useEffect(() => {
    if (verificationCode.data) {
      let message = `New Authentication code sent to your email ${
        signin.data.patient.email
      }`;
      if (!isSentToEmail.data)
        message = `New Authentication code sent to your Phone ${
          signin.data.patient.phoneNumber
        }`;

      enqueueSnackbar(message, {
        variant: 'success',
      });
      dispatch(doResetVerificationCode());
    }
  }, [verificationCode]);

  useEffect(() => {
    if (signin.data) {
      initiateInxiteSSO(signin.data.patient.uuid);
    }
  }, [signin]);

  useEffect(() => {
    if (auth.error && auth.error.status === 403) {
      enqueueSnackbar('Invalid Authentication Code', {
        variant: 'error',
      });
    } else if (auth.data) {
      dispatch(push('/'));
    }
  }, [auth]);

  const initiateInxiteSSO = uuid => {
    dispatch(doInitInxiteSso(uuid));
  };

  const handleVerify = () => {
    if (!code || code === '') {
      enqueueSnackbar('Please provide 6 digit Authentication code', {
        variant: 'error',
      });
    } else
      dispatch(
        doVerifyAuth({
          patientId: signin.data.patient.id,
          verificationCode: code,
        }),
      );
  };
  const onCodeChange = e => {
    const { value } = e.target;
    setCode(value);
  };

  const handleResendCode = () => {
    dispatch(
      doResendVerificationCode({
        employeeValidator: signin.data.patient.email,
        employerCode: signin.data.patient.employerCode,
      }),
    );
  };

  const handleReLogin = () => {
    dispatch(doResetSignin());
  };

  const sentTo = isSentToEmail.data
    ? signin.data.patient.email
    : signin.data.patient.phoneNumber;

  return (
    <>
      <Grid
        container
        spacing={2}
        direction="column"
        className={classes.wrapper}
        alignContent="center"
      >
        <Grid item xs={12} md={4}>
          <Card className={classes.root}>
            <Grid item xs={12} className={classes.headerContainer}>
              <Typography variant="h5">
                Two Factor Authentication
                <Typography variant="body1" color="textPrimary">
                  For your security, please enter the <strong>6-digit</strong>{' '}
                  verfication code sent to{' '}
                  <strong>{signin.data ? sentTo : ''}</strong>
                </Typography>
              </Typography>
            </Grid>
            <Grid item xs={12} className={classes.container}>
              <TextField
                keyEvents={{
                  handleKeys: ['enter'],
                  onKeyEvent: handleVerify,
                }}
                field={{
                  id: 'verificationCode',
                  caption: 'Enter Verification Code',
                  required: true,
                  pristine: true,
                  error: true,
                  fullWidth: true,
                }}
                variant="outlined"
                onChange={onCodeChange}
              />
            </Grid>
            <Grid item xs={12} className={classes.text}>
              <Typography variant="body1">
                Did not receive the code?{' '}
                <Link
                  onClick={handleResendCode}
                  component={MyLink}
                  variant="body1"
                  color="primary"
                  to="#"
                >
                  <strong>Resend Code</strong>
                </Link>
              </Typography>
            </Grid>
            <Grid item xs={12} className={classes.container}>
              <GradientButton
                variant="contained"
                size="large"
                onClick={handleVerify}
                fullWidth
                loading={auth.loading}
              >
                {'Proceed'}
              </GradientButton>
            </Grid>
            <Grid item xs={12} className={classes.footerContainer}>
              <GradientButton
                onClick={handleReLogin}
                variant="outlined"
                color="primary"
                fullWidth
                size="large"
                className={classes.text}
              >
                <Typography className={classes.buttonLabel}>
                  BACK TO LOGIN
                </Typography>
              </GradientButton>
            </Grid>
          </Card>
        </Grid>
      </Grid>
      {inxiteSso.data && (
        <iframe
          id="inxiteFrame"
          title="Inxite"
          src={inxiteSso.data.sso_url}
          style={{ display: 'none' }}
        />
      )}
    </>
  );
}

const { func, object } = PropTypes;
TwoFactorVerification.propTypes = {
  auth: object.isRequired,
  verificationCode: object.isRequired,
  signin: object.isRequired,
  dispatch: func.isRequired,
  enqueueSnackbar: func.isRequired,
  doVerifyAuth: func.isRequired,
  doResendVerificationCode: func.isRequired,
  isSentToEmail: object.isRequired,
  doResetVerificationCode: func.isRequired,
  doInitInxiteSso: func.isRequired,
  doResetSignin: func.isRequired,
  inxiteSso: object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  signin: makeSelectSignin(),
  auth: makeSelectVerifyAuth(),
  verificationCode: makeSelectVerificationCode(),
  isSentToEmail: makeIsSentToEmail(),
  inxiteSso: makeSelectInxiteSso(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    doVerifyAuth: verifyAuthentication,
    doResendVerificationCode: reSendVerificationCode,
    doResetVerificationCode: resetVerificationCode,
    doInitInxiteSso: initializeInxiteSso,
    doResetSignin: resetSignin,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(TwoFactorVerification);
