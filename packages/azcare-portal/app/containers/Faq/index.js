import React from 'react';

import { Grid, Typography, Card } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  container: {
    paddingBottom: 50,
  },
  mainContainer: {
    padding: 20,
  },
  headerContainer: {
    padding: 10,
  },
  root: {
    padding: 20,
    borderRadius: 10,
    boxShadow: theme.palette.boxShadow.main,
  },
  title: {
    fontWeight: 'bold',
    paddingBottom: 10,
  },
  paddedText: {
    paddingBottom: 25,
  },
}));

function Faq() {
  const classes = useStyles();
  return (
    <Grid container justify="center" className={classes.mainContainer}>
      <Grid item xs={12} md={11}>
        <Grid container className={classes.headerContainer}>
          <Grid item xs={12}>
            <Typography variant="h6" color="textPrimary">
              AZ CARE FAQs
            </Typography>
          </Grid>
        </Grid>
        <Grid className={classes.wrapper} container spacing={2}>
          <Card className={classes.root}>
            <Grid item xs={12} className={classes.paddedText}>
              <Typography variant="h6" color="secondary">
                GENERAL
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography variant="h6" className={classes.title}>
                1. What is AZ Care?
              </Typography>
            </Grid>
            <Grid item xs={12} className={classes.paddedText}>
              <Typography variant="h6" color="textPrimary">
                AZ Care is a revolutionary telemedicine company providing
                patients virtual access to board-certified physicians 24/7/365.
                AZ Care puts medical care in the palm of your hand. Simply
                download the AZ Care app to your smartphone or tablet and you
                can have a virtual consultation with a physician in minutes
                anytime, anywhere.
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography variant="h6" className={classes.title}>
                2. When is AZ Care available for physician consultations?
              </Typography>
            </Grid>
            <Grid item xs={12} className={classes.paddedText}>
              <Typography variant="h6" color="textPrimary">
                AZ Care is available 24/7/365. If you have access to a
                smartphone or tablet, you can have a virtual consult with a
                board certified physician anytime, anywhere.
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography variant="h6" className={classes.title}>
                3. How do I contact AZ Care?
              </Typography>
            </Grid>
            <Grid item xs={12} className={classes.paddedText}>
              <Typography variant="h6" color="textPrimary">
                Simply download the AZ Care app to your smartphone or tablet.
                Our AZ Care app is available for download on the App Store and
                the Google Play. With just a few clicks to set up your account,
                you can have a virtual consult with a doctor in minutes.
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography variant="h6" className={classes.title}>
                4. Is AZ Care safe and private?
              </Typography>
            </Grid>
            <Grid item xs={12} className={classes.paddedText}>
              <Typography variant="h6" color="textPrimary">
                Confidentiality is a top priority for AZ Care. Our app has been
                designed on a HIPAA compliant platform so you can rest assured
                your information is securely and privately stored.
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography variant="h6" className={classes.title}>
                5. How are the physicians in your network selected?
              </Typography>
            </Grid>
            <Grid item xs={12} className={classes.paddedText}>
              <Typography variant="h6" color="textPrimary">
                Each of our physicians undergo a rigorous credentialing process
                based upon guidelines set by the National Committee for Quality
                Assurance (NCQA). All physicians in the AZ Care Preferred
                Provider Network are board-certified, licensed and credentialed.
                All AZ Care physicians have completed our AZ Care comprehensive
                training program.
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography variant="h6" className={classes.title}>
                6. What medical conditions do you treat?
              </Typography>
            </Grid>
            <Grid item xs={12} className={classes.paddedText}>
              <Typography variant="h6" color="textPrimary">
                Our physicians can diagnose and treat a wide range of
                non-emergency medical conditions. Click here for a list of
                common conditions treated.
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography variant="h6" className={classes.title}>
                7. Can I choose the physician I want to speak with?
              </Typography>
            </Grid>
            <Grid item xs={12} className={classes.paddedText}>
              <Typography variant="h6" color="textPrimary">
                You will first speak with a care coordinator who will confirm
                your medical history and assess your symptoms. The care
                coordinator will determine if your condition can be treated by a
                virtual consultation. The care coordinator will select the best
                physician to treat your medical condition.jVjjjj
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography variant="h6" className={classes.title}>
                8. Can your physicians prescribe medication?
              </Typography>
            </Grid>
            <Grid item xs={12} className={classes.paddedText}>
              <Typography variant="h6" color="textPrimary">
                When medically necessary, AZ Care physicians are able to
                prescribe a wide range of medications to treat your condition.
                AZ Care physicians do not prescribe or renew a prescription for
                controlled substances regulated by the U.S. Drug Enforcement
                Agency that have been designated as U.S. controlled substances.
                Click here to view a list of controlled substances. In addition,
                AZ Care physicians will not prescribe or renew large quantities
                of medications for any condition.
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography variant="h6" className={classes.title}>
                9. Can I choose the pharmacy if I need a prescription?
              </Typography>
            </Grid>
            <Grid item xs={12} className={classes.paddedText}>
              <Typography variant="h6" color="textPrimary">
                If our physician determines a medication is medically necessary,
                they can write a prescription for non-narcotic medications which
                will be sent electronically to the pharmacy of your choice.
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography variant="h6" className={classes.title}>
                10. Can medical forms, such as work/school excuses be provided?
              </Typography>
            </Grid>
            <Grid item xs={12} className={classes.paddedText}>
              <Typography variant="h6" color="textPrimary">
                Our physicians are able to provide simple forms such as
                work/school excuses or return to work/school documents as
                clinically appropriate.
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography variant="h6" className={classes.title}>
                11. What if my condition cannot be treated by AZ Care?
              </Typography>
            </Grid>
            <Grid item xs={12} className={classes.paddedText}>
              <Typography variant="h6" color="textPrimary">
                If for any reason your condition falls outside the scope of what
                AZ Care covers, a care coordinator will direct you to a
                preferred healthcare center in your area, so you can get the
                immediate care you need.
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography variant="h6" className={classes.title}>
                12. How do I update my account information or reset my password
                or pin?
              </Typography>
            </Grid>
            <Grid item xs={12} className={classes.paddedText}>
              <Typography variant="h6" color="textPrimary">
                You can update your account information, password or PIN under
                “My Account” from the main menu in the app.
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography variant="h6" className={classes.title}>
                13. Can I change my email address that I use to log in?
              </Typography>
            </Grid>
            <Grid item xs={12} className={classes.paddedText}>
              <Typography variant="h6" color="textPrimary">
                Your patient profile is connected to the email address that was
                used to create the account. For security reasons, you cannot
                change the email address assigned to the profile. You will need
                to create a new account in order to change your email address.
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography variant="h6" className={classes.title}>
                14. How do I update my medical history?
              </Typography>
            </Grid>
            <Grid item xs={12} className={classes.paddedText}>
              <Typography variant="h6" color="textPrimary">
                You can update your medical history under “My Health” from the
                main menu in the app.
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography variant="h6" className={classes.title}>
                15. How much does AZ Care cost?
              </Typography>
            </Grid>
            <Grid item xs={12} className={classes.paddedText}>
              <Typography variant="h6" color="textPrimary">
                AZ Care saves you more than time. We save you money. Our flat
                fee of $79 for a consult with our board-certified physician is
                lower than the average urgent care fee. Memberships are also
                available for individuals and families at $9.99 per month (with
                a reduced consult fee of $40). But that’s not all. We also offer
                a new unlimited plan, allowing members and their families to
                access board-certified physicians as often as needed for a flat
                rate of $100 per month. Members on the unlimited plan can save
                an additional 10% for paying annually. That’s a $120 in savings!
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography variant="h6" className={classes.title}>
                16. Will I be charged if the care coordinator determines that my
                condition cannot be treated by a virtual consult?
              </Typography>
            </Grid>
            <Grid item xs={12} className={classes.paddedText}>
              <Typography variant="h6" color="textPrimary">
                If your care coordinator determines that your medical condition
                falls outside the scope of a virtual consult and refers you to
                an ER, urgent care or a doctor’s office, you will not be charged
                for the assessment.
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography variant="h6" className={classes.title}>
                17. What payment methods are accepted?
              </Typography>
            </Grid>
            <Grid item xs={12} className={classes.paddedText}>
              <Typography variant="h6" color="textPrimary">
                AZ Care accepts payment from FSA, HSA, and HRA accounts, as well
                as all major debit and credit cards.
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography variant="h6" className={classes.title}>
                18. Does AZ Care accept medical insurance?
              </Typography>
            </Grid>
            <Grid item xs={12} className={classes.paddedText}>
              <Typography variant="h6" color="textPrimary">
                AZ Care does not currently accept medical insurance. However,
                you can pay for the fee with your HSA account.
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography variant="h6" className={classes.title}>
                19. Do I need wifi to use AZ Care?
              </Typography>
            </Grid>
            <Grid item xs={12} className={classes.paddedText}>
              <Typography variant="h6" color="textPrimary">
                Although we do recommend you use WiFi for the best possible
                experience, it is not required to contact AZ Care.
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography variant="h6" className={classes.title}>
                20. What if my connection is poor?
              </Typography>
            </Grid>
            <Grid item xs={12} className={classes.paddedText}>
              <Typography variant="h6" color="textPrimary">
                If you do experience connection issues, you can select to have a
                voice only consult which should improve your connection.
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography variant="h6" className={classes.title}>
                21. Can I get assistance setting up my account?
              </Typography>
            </Grid>
            <Grid item xs={12} className={classes.paddedText}>
              <Typography variant="h6" color="textPrimary">
                Our Member Service Team is available 24/7/365 to assist
                patients. We are happy to assist in setting up your account.
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography variant="h6" className={classes.title}>
                22. How do I find your app in the app store and google play?
              </Typography>
            </Grid>
            <Grid item xs={12} className={classes.paddedText}>
              <Typography variant="h6" color="textPrimary">
                Search using the term AZ Care on the Apple App Store or the
                Google Play Store or you can click the links below.
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography variant="h6" className={classes.title}>
                23. Is AZ Care satisfaction guaranteed?
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography variant="h6" color="textPrimary">
                Patient satisfaction is a top priority for AZ Care. We also
                strive to continually improve the patient experience. We welcome
                your feedback, both positive or otherwise. Following each
                consult, you will have the opportunity to rate AZ Care and your
                physician. These ratings are published on our app and website.
                Our patients are also sent a satisfaction survey to evaluate
                their experience. These results are reviewed for quality
                assurance and used as part of our continuous improvement
                process. We also encourage patients to contact our Member
                Service Team at any time with questions, comments or feedback.
              </Typography>
            </Grid>
          </Card>
        </Grid>
      </Grid>
    </Grid>
  );
}

export default Faq;
