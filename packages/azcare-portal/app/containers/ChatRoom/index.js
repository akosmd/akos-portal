/**
 *
 * Chat
 *
 */

import React, { useState, useEffect } from 'react';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';
import { push } from 'connected-react-router';

import { connect } from 'react-redux';
import { compose } from 'redux';

import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';

import { Grid, CardHeader, Button } from '@material-ui/core';

import 'typeface-lato';
import logo from 'images/azcare-plan-logo.png';

import Echo from 'laravel-echo';
import 'pusher-js';

import {
  makeSelectVerifyAuth,
  makePatientImpersonation,
  makeSelectMemberJoinRoom,
  makeSelectMemberJoinRoomStatus,
  makeSelectChatMessages,
} from 'containers/App/selectors';

import {
  memberJoinRoom,
  sendMessageToNurse,
  setMemberJoiningStatus,
  getChatMessages,
  notifyNurse,
} from 'containers/App/actions';

import { GOODBYE_MESSAGE } from 'utils/config';

import MessageControl from './messageControl';

import getOptions, { USER_TYPES } from './config';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },

  grid: {
    width: '99%',
    margin: '0 auto',
    [theme.breakpoints.down('md')]: {
      height: '95vh',
    },
    [theme.breakpoints.up('md')]: {
      height: '98vh',
    },
  },
  card: {
    minWidth: 375,
    height: 500,
    minHeight: 400,
    position: 'fixed',
    bottom: theme.spacing(10),
    right: theme.spacing(2),
    [theme.breakpoints.down('md')]: {
      minWidth: '96%',
      height: '90%',
      width: '96%',
      right: theme.spacing(1),
    },
    display: 'flex',
    flexDirection: 'column',
  },

  cardFullScreen: {
    display: 'flex',
    height: '97%',
    flexDirection: 'column',
    alignContent: 'stretch',
    justifyContent: 'stretch',
  },
  cardContent: {
    minHeight: 300,
    width: '100%',
    height: '100%',
  },

  cardHeaderTitle: {
    color: theme.palette.secondary.main,
  },

  cardRoot: {
    background: 'linear-gradient(to right, #55bcbe 0%,#85c3a6 100%) !important',
  },
  cardSubHeader: {
    color: theme.palette.primary.main,
  },
  fullContent: {
    display: 'flex',
    height: '90vh',
    overflowY: 'scroll',
    padding: 0,
    paddingBottom: '60px !important',
    [theme.breakpoints.up('md')]: {
      paddingBottom: '0 !important',
    },
  },

  chatAvatar: {
    background: '#12bcc5',
  },
}));

function IndividualChat({
  dispatch,
  memberRoom,
  doJoinRoom,
  patient,
  impersonatedPatient,
  doSendMessageToNurse,
  doGetChatMessages,
  doNotifyNurse,
  chatMessages,
}) {
  const classes = useStyles();
  const [actualPatient, setActualPatient] = useState();
  // const [echo, setEcho] = useState();
  const [conversations, setConversations] = useState([]);
  const [sendToUserId, setSendToUserId] = useState();
  const [message, setMessage] = useState();
  const [hasJoined, setHasJoned] = useState(false);
  const [typing] = useState(false);

  useEffect(() => {
    if (chatMessages.data && chatMessages.data.length) {
      const messages = chatMessages.data.map(chat => ({
        id: chat.from.id,
        from: `${chat.from.first_name} ${chat.from.last_name}`,
        message: chat.message,
      }));
      setConversations(messages);

      if (actualPatient && actualPatient.patient && actualPatient.patient.id) {
        const nurseMessage = messages.filter(
          msg => msg.id !== actualPatient.patient.id,
        );
        if (nurseMessage.length) {
          setSendToUserId(nurseMessage[nurseMessage.length - 1].id);
        }
      }
    }
  }, [chatMessages.data]);

  useEffect(() => {
    if (patient.data) {
      setActualPatient(patient.data);
      setConversations([
        ...conversations,
        {
          message: `Hi ${
            patient.data.patient.firstName
          }! Please wait, our team will be in touch with you shortly`,
        },
      ]);
    }
    if (impersonatedPatient.data) {
      setActualPatient(impersonatedPatient.data);
      setConversations([
        ...conversations,
        {
          message: `Hi ${
            impersonatedPatient.data.patient.firstName
          }! Please wait, our team will be in touch with you shortly`,
        },
      ]);
    }
  }, [patient, impersonatedPatient]);

  useEffect(() => {
    if (!memberRoom.data && !hasJoined) {
      dispatch(doJoinRoom());

      setHasJoned(true);
    }
    if (memberRoom.data && actualPatient) {
      const { origin } = window.location;
      dispatch(doGetChatMessages(memberRoom.data.conference_no));

      dispatch(
        doNotifyNurse({
          text: `A new member is waiting on the chatroom
      Name: ${actualPatient.patient.firstName} ${actualPatient.patient.lastName}
      Visit this link to start chatting ${origin}/chatroom/nurse`,
        }),
      );
      joinRoom();
    }
  }, [memberRoom, actualPatient]);

  const handleQuit = () => {
    setTimeout(() => dispatch(push('/')), 5000);
  };

  const joinRoom = () => {
    const echo = new Echo(getOptions(actualPatient, USER_TYPES.member));
    const roomEcho = new Echo(getOptions(actualPatient, USER_TYPES.member));
    roomEcho.join('MemberWaitingRoom');

    echo.join(actualPatient.patient.userChannelName);
    echo.private(actualPatient.patient.userChannelName).whisper('typing', {
      name: actualPatient.patient.email,
    });

    echo
      .private(actualPatient.patient.userChannelName)
      .listen('UserChatEvent', e => {
        setConversations(prevState => [
          ...prevState,
          { from: e.fullName, message: e.message },
        ]);
        if (e.message === GOODBYE_MESSAGE) handleQuit();
        if (!sendToUserId) setSendToUserId(e.senderId);
      });
  };

  const sendMessage = () => {
    const from = `${actualPatient.patient.firstName} ${
      actualPatient.patient.lastName
    }`;
    setConversations(prevState => [
      ...prevState,
      { from, id: actualPatient.patient.id, message },
    ]);

    const params = {
      message,
      conferenceNo: memberRoom.data.conference_no,
      userId: sendToUserId,
    };

    dispatch(doSendMessageToNurse(params));
    setMessage('');
  };

  const onChange = e => {
    const { value } = e.target;

    setMessage(value);
  };

  const disabled = !sendToUserId;

  const renderFullScreen = () => (
    <div className={classes.root}>
      <Grid
        container
        direction="row"
        justify="space-around"
        alignItems="stretch"
        className={classes.grid}
      >
        <Grid item xs={12}>
          <Card elevation={1} className={classes.cardFullScreen}>
            <CardHeader
              className={classes.cardRoot}
              subheader={
                <div style={{ display: 'flex' }}>
                  <figure
                    style={{
                      display: 'flex',
                      width: 210,
                      height: 35,
                      margin: '0 10px',
                    }}
                  >
                    <img src={logo} alt="Akos" style={{ width: '100%' }} />
                  </figure>
                </div>
              }
              action={
                <Button onClick={() => dispatch(push('/'))}>
                  Back to Home
                </Button>
              }
              classes={{
                title: classes.cardHeaderTitle,
                subheader: classes.cardSubHeader,
              }}
            />
            <CardContent className={classes.fullContent}>
              <MessageControl
                onSendMessage={sendMessage}
                message={message}
                onChange={onChange}
                disabled={disabled}
                messages={conversations}
                typing={typing}
                user={actualPatient ? actualPatient.patient : undefined}
              />
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </div>
  );

  return renderFullScreen();
}

const { object, func } = PropTypes;
IndividualChat.propTypes = {
  patient: object.isRequired,
  impersonatedPatient: object.isRequired,
  memberRoom: object.isRequired,
  history: object.isRequired,
  dispatch: func.isRequired,
  doSendMessageToNurse: func.isRequired,
  doSetMemberJoiningStatus: func.isRequired,
  doNotifyNurse: func.isRequired,
  doGetChatMessages: func.isRequired,
  chatMessages: object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  patient: makeSelectVerifyAuth(),
  impersonatedPatient: makePatientImpersonation(),
  memberRoom: makeSelectMemberJoinRoom(),
  memberHasJoinedRoom: makeSelectMemberJoinRoomStatus(),
  chatMessages: makeSelectChatMessages(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    doJoinRoom: memberJoinRoom,
    doSendMessageToNurse: sendMessageToNurse,
    doSetMemberJoiningStatus: setMemberJoiningStatus,
    doGetChatMessages: getChatMessages,
    doNotifyNurse: notifyNurse,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(IndividualChat);
