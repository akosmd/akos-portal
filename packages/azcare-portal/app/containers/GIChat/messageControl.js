import React, { useRef, useEffect } from 'react';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import { compose } from 'redux';

import { makeStyles } from '@material-ui/core/styles';
import KeyboardEventHandler from 'react-keyboard-event-handler';
import { Button } from '@material-ui/core';
import 'typeface-lato';

import MultipleOptions from './multipleOptions';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    width: '100%',
  },

  card: {
    minWidth: 375,
    height: 500,
    minHeight: 400,
    position: 'fixed',
    bottom: theme.spacing(10),
    right: theme.spacing(2),
    [theme.breakpoints.down('md')]: {
      minWidth: '96%',
      height: '90%',
      width: '96%',
      right: theme.spacing(1),
    },
    display: 'flex',
    flexDirection: 'column',
  },

  inputBox: {
    backgroundColor: 'white',
    color: 'black',
    fontFamily: 'inherit',
    fontSize: 'inherit',
    height: '100%',
    width: '100%',
    borderWidth: '0px',
    borderStyle: 'initial',
    borderColor: 'initial',
    borderImage: 'initial',
    outline: '0px',
    padding: '0px',
    flex: '1 1 0%',
    overflow: 'visible',
  },
  responseButton: {
    margin: '.25rem',
    width: '97%',
  },
  sendButton: {
    backgroundColor: 'transparent',
    height: '100%',
    width: '40px',
    borderWidth: '0px',
    borderStyle: 'initial',
    borderColor: 'initial',
    borderImage: 'initial',
    outline: '0px',
    padding: '0px',
    '& svg': {
      fill: 'rgb(118, 118, 118);',
    },
    '&:hover svg': {
      fill: 'rgb(51, 51, 51);',
    },
  },
  userMessage: {
    backgroundColor: '#0078d7',
    border: '#0078d7',
    color: '#fff',
    borderRadius: '5px',
    padding: '.5rem',
    marginRight: '1rem',
  },
  senderMessage: {
    backgroundColor: '#ECEFF6',
    border: '#ECEFF6',
    borderRadius: '5px',
    padding: '.5rem',
    marginLeft: '1rem',
  },
  message: {
    display: 'flex',
  },
  messageHolder: {
    display: 'flex',
    flexDirection: 'row-reverse',
  },
  sender: {
    textAlign: 'right',
    marginRight: '1rem',
    fontStyle: 'italic',
  },
  receiver: {
    textAlign: 'left',
    marginLeft: '1rem',
    fontStyle: 'italic',
  },
  senderOl: {
    padding: '1rem',
    paddingInlineStart: '1rem',
  },
  typing: {
    fontStyle: 'italic',
    paddingLeft: '1rem',
    paddingBottom: '1rem',
  },
}));

function MessageControl({
  messages,
  user,
  message,
  onChange,
  onSendMessage,
  onResponse,
  typing,
  disabled,
}) {
  const classes = useStyles();

  const messagesEndRef = useRef(null);

  const scrollToBottom = () => {
    messagesEndRef.current.scrollIntoView({ behavior: 'smooth' });
  };

  useEffect(scrollToBottom, [messages]);

  const renderMessages = () => {
    if (!messages) return null;

    return (
      <ul className={classes.senderOl} id="message_list">
        {messages.map((msg, idx) => {
          const msgIsFromUser = user === msg.from;
          const { options, message: receivedMessage, controlId } = msg;
          const hasNone =
            options && options.find(o => o.toLowerCase() === 'none');
          const filteredOptions = options
            ? options.filter(o => o.toLowerCase() !== 'none')
            : [];
          return (
            <li
              key={`msg-${idx + 1}`}
              className={
                msgIsFromUser ? classes.messageHolder : classes.message
              }
            >
              <div>
                <div
                  className={
                    msgIsFromUser ? classes.userMessage : classes.senderMessage
                  }
                >
                  {receivedMessage}
                  {filteredOptions &&
                    controlId === 's' &&
                    filteredOptions.map((option, x) => (
                      <Button
                        key={`opt-${x + 1}`}
                        className={classes.responseButton}
                        color="primary"
                        onClick={() => onResponse(option)}
                        fullWidth
                        variant="contained"
                      >
                        {option}
                      </Button>
                    ))}
                  {filteredOptions && controlId === 'm' && (
                    <MultipleOptions
                      filteredOptions={filteredOptions}
                      onResponse={onResponse}
                      hasNone={hasNone}
                    />
                  )}
                </div>
                {/* <div
                  className={msgIsFromUser ? classes.sender : classes.receiver}
                >
                  {msgIsFromUser ? 'You' : msg.from}
                </div> */}
              </div>
            </li>
          );
        })}
      </ul>
    );
  };

  return (
    <div className={classes.root}>
      <div style={{ flex: '1 1 0%' }}>
        <div
          style={{
            display: 'flex',
            flexDirection: 'column',
            height: '95%',
            overflowY: 'auto',
            width: '100%',
          }}
        >
          <div style={{ flex: '1 1 0%' }} />
          <ul style={{ margin: 0, padding: 0 }} className={classes.senderOl}>
            <li>
              <div style={{ marginTop: 10, marginBottom: 10 }}>
                {renderMessages()}
              </div>
            </li>
            {typing && (
              <li className={classes.message}>
                <div className={classes.typing}>
                  <em>Healtchare Assistant is typing a message</em>
                </div>
              </li>
            )}
          </ul>
          <div id="scrollRef" ref={messagesEndRef} />
        </div>

        <div style={{ bottom: 0 }}>
          <div
            style={{
              backgroundColor: 'white',
              minHeight: 40,
              alignItems: 'stretch',
              borderTop: '1px solid rgb(230, 230, 230)',
              display: 'flex',
            }}
          >
            <div
              style={{
                alignItems: 'center',
                padding: 10,
                flex: '10000 1 0%',
                display: 'flex',
                bottom: 0,
              }}
            >
              <KeyboardEventHandler
                handleKeys={['enter']}
                onKeyEvent={onSendMessage}
                style={{ width: '100%' }}
              >
                <input
                  aria-label="Sendbox"
                  data-id="webchat-sendbox-input"
                  placeholder="Type your message"
                  type="text"
                  readOnly={disabled}
                  className={classes.inputBox}
                  value={message}
                  onChange={onChange}
                />
              </KeyboardEventHandler>
            </div>
            <div>
              <button
                className={classes.sendButton}
                title="Send"
                type="button"
                disabled={disabled}
                onClick={onSendMessage}
              >
                <svg height="28" viewBox="0 0 45.7 33.8" width="28">
                  <path
                    clipRule="evenodd"
                    d="M8.55 25.25l21.67-7.25H11zm2.41-9.47h19.26l-21.67-7.23zm-6 13l4-11.9L5 5l35.7 11.9z"
                  />
                </svg>
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

const { array, func, string, bool } = PropTypes;
MessageControl.propTypes = {
  messages: array.isRequired,
  user: string,
  message: string,
  onChange: func.isRequired,
  onResponse: func.isRequired,
  disabled: bool,
  typing: bool,
  onSendMessage: func.isRequired,
};

const mapStateToProps = createStructuredSelector({});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(MessageControl);
