/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 */

import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import PropTypes from 'prop-types';
import { createStructuredSelector } from 'reselect';

import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';

import {
  Grid,
  CardHeader,
  Tooltip,
  IconButton,
  Menu,
  MenuItem,
} from '@material-ui/core';

import HelpIcon from '@material-ui/icons/Help';

import { sendGiMessage, resetGiMessage } from 'containers/App/actions';
import { makeSelectGiMessage } from 'containers/App/selectors';
import 'typeface-lato';

import logo from 'images/azcare-plan-logo.png';

import MessageControl from './messageControl';
const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },

  grid: {
    width: '99%',
    margin: '0 auto',
    [theme.breakpoints.down('md')]: {
      height: '95vh',
    },
    [theme.breakpoints.up('md')]: {
      height: '98vh',
    },
  },
  card: {
    minWidth: 375,
    height: 500,
    minHeight: 400,
    position: 'fixed',
    bottom: theme.spacing(10),
    right: theme.spacing(2),
    [theme.breakpoints.down('md')]: {
      minWidth: '96%',
      height: '90%',
      width: '96%',
      right: theme.spacing(1),
    },
    display: 'flex',
    flexDirection: 'column',
  },

  cardFullScreen: {
    display: 'flex',
    height: '97%',
    flexDirection: 'column',
    alignContent: 'stretch',
    justifyContent: 'stretch',
  },
  cardContent: {
    minHeight: 300,
    width: '100%',
    height: '100%',
  },

  cardHeaderTitle: {
    color: theme.palette.secondary.main,
  },

  cardRoot: {
    background: 'linear-gradient(to right, #55bcbe 0%,#85c3a6 100%) !important',
  },
  cardSubHeader: {
    color: theme.palette.primary.main,
  },
  fullContent: {
    display: 'flex',
    height: '90vh',
    overflowY: 'scroll',
    padding: 0,
    paddingBottom: '60px !important',
    [theme.breakpoints.up('md')]: {
      paddingBottom: '0 !important',
    },
  },

  chatAvatar: {
    background: '#12bcc5',
  },
}));

function HomePage({
  dispatch,
  doSendMessage,
  doResetMessage,
  receivedMessage,
}) {
  const [message, setMessage] = useState();
  const [conversation, setConversation] = useState([]);
  const [helpAnchor, setHelpAnchor] = useState(null);
  const [sessionId, setSessionId] = useState();
  const [typing, setTyping] = useState(false);
  const classes = useStyles();
  const isMenuOpen = Boolean(helpAnchor);

  const startChat = () => {
    setConversation([
      {
        from: 'bot',
        message:
          "Hi, How may I help you? You can type any symptoms you may have and you can type 'Cancel', 'Exit' any time if you wish to end the conversation.",
        options: [],
        controlId: undefined,
      },
    ]);
    setSessionId(Math.floor(Math.random() * 1000));
  };
  useEffect(() => {
    dispatch(doResetMessage());
    startChat();
  }, []);

  useEffect(() => {
    if (receivedMessage && receivedMessage.data && conversation.length > 0) {
      const {
        ai_response: {
          chat,
          options,
          meta_data: { control_id: controlId },
        },
      } = receivedMessage.data;
      if (chat)
        setConversation(prevState => [
          ...prevState,
          { from: 'bot', message: chat, options, controlId },
        ]);
      setTyping(false);
    }
  }, [receivedMessage]);
  const onChange = e => {
    const { value } = e.target;

    setMessage(value);
  };

  const onSubmit = () => {
    if (message && message !== '') {
      setConversation(prevState => [...prevState, { from: 'user', message }]);
      const params = {
        query: message,
        userid: '123',
        bot_name: 'test',
        sessionid: `${sessionId}`,
      };
      setTyping(true);
      dispatch(doSendMessage(params));
      dispatch(doResetMessage());
      setMessage('');
    }
  };
  const onResponseSelected = response => {
    if (response && response !== '') {
      setConversation(prevState => [
        ...prevState,
        { from: 'user', message: response },
      ]);
      const params = {
        query: response,
        userid: '123',
        bot_name: 'test',
        sessionid: `${sessionId}`,
      };
      setTyping(true);
      dispatch(doSendMessage(params));
      dispatch(doResetMessage());
      setMessage('');
    }
  };
  const handleHelpOpen = event => {
    setHelpAnchor(event.target);
  };

  const handleQuit = () => {
    const params = {
      query: 'exit',
      userid: '123',
      bot_name: 'test',
      sessionid: `${sessionId}`,
    };

    dispatch(doSendMessage(params));
  };

  const hanleHelpClose = () => {
    setHelpAnchor(null);
  };

  const handleStartOver = () => {
    const first = document.getElementById('message_list');
    handleQuit();
    startChat();
    if (first) first.innerHTML = '';
  };

  const renderHelp = (
    <Menu
      anchorEl={helpAnchor}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={isMenuOpen}
      onClose={hanleHelpClose}
    >
      <MenuItem onClick={handleStartOver}>Start over</MenuItem>
      <MenuItem onClick={handleQuit}>Quit</MenuItem>
    </Menu>
  );

  return (
    <div className={classes.root}>
      <Grid
        container
        direction="row"
        justify="space-around"
        alignItems="stretch"
        className={classes.grid}
      >
        <Grid item xs={12}>
          <Card elevation={1} className={classes.cardFullScreen}>
            <CardHeader
              className={classes.cardRoot}
              subheader={
                <div style={{ display: 'flex' }}>
                  <figure
                    style={{
                      display: 'flex',
                      width: 210,
                      height: 35,
                      margin: '0 10px',
                    }}
                  >
                    <img src={logo} alt="Akos" style={{ width: '100%' }} />
                  </figure>
                </div>
              }
              classes={{
                title: classes.cardHeaderTitle,
                subheader: classes.cardSubHeader,
              }}
              action={
                <div>
                  <Tooltip title="Help">
                    <IconButton
                      aria-label="Help"
                      color="secondary"
                      onClick={handleHelpOpen}
                    >
                      <HelpIcon />
                    </IconButton>
                  </Tooltip>

                  {renderHelp}
                </div>
              }
            />
            <CardContent className={classes.fullContent}>
              <MessageControl
                onSendMessage={onSubmit}
                message={message}
                onChange={onChange}
                onResponse={onResponseSelected}
                messages={conversation}
                typing={typing}
                user="user"
              />
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </div>
  );
}

const { object, func } = PropTypes;
HomePage.propTypes = {
  dispatch: func.isRequired,
  receivedMessage: object.isRequired,
  doSendMessage: func.isRequired,
  doResetMessage: func.isRequired,
};
const mapStateToProps = createStructuredSelector({
  receivedMessage: makeSelectGiMessage(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    doSendMessage: sendGiMessage,
    doResetMessage: resetGiMessage,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(HomePage);
