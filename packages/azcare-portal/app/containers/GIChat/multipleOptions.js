import React, { useState } from 'react';
import PropTypes from 'prop-types';
import {
  Button,
  FormGroup,
  Checkbox,
  FormControlLabel,
} from '@material-ui/core';

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  responseButton: {
    margin: '.25rem',
    width: '97%',
  },
  sendButton: {
    backgroundColor: 'transparent',
    height: '100%',
    width: '40px',
    borderWidth: '0px',
    borderStyle: 'initial',
    borderColor: 'initial',
    borderImage: 'initial',
    outline: '0px',
    padding: '0px',
    '& svg': {
      fill: 'rgb(118, 118, 118);',
    },
    '&:hover svg': {
      fill: 'rgb(51, 51, 51);',
    },
  },
});

function MultipleOptions({ filteredOptions, onResponse, hasNone }) {
  const [selectedOptions, setSelectedOptions] = useState('');
  const classes = useStyles();
  const addSelectedOption = option => e => {
    const { checked } = e.target;
    if (checked) {
      if (selectedOptions === '') setSelectedOptions(`${option};`);
      else setSelectedOptions(`${selectedOptions}${option};`);
    } else {
      const cleaned = selectedOptions.replace(`${option};`, '');
      setSelectedOptions(cleaned);
    }
  };

  const sendMultipReponse = () => {
    const messageToSend = selectedOptions.slice(0, -1);
    onResponse(messageToSend);
    setSelectedOptions('');
  };
  return (
    <div>
      {filteredOptions.map((option, x) => (
        <FormGroup row key={`opt-${x + 1}`}>
          <FormControlLabel
            control={
              <Checkbox
                // checked={state.checkedA}
                onChange={addSelectedOption(option)}
                value={option}
              />
            }
            label={option.replace('nan', 'None of the above')}
          />
        </FormGroup>
      ))}
      <Button
        className={classes.responseButton}
        color="primary"
        onClick={() => sendMultipReponse()}
        fullWidth
        disabled={selectedOptions === ''}
        variant="contained"
      >
        Submit
      </Button>
      {hasNone && (
        <Button
          className={classes.responseButton}
          color="primary"
          onClick={() => onResponse('none')}
          fullWidth
          disabled={selectedOptions !== ''}
          variant="contained"
        >
          None
        </Button>
      )}
    </div>
  );
}

const { array, func, bool } = PropTypes;
MultipleOptions.propTypes = {
  filteredOptions: array.isRequired,
  onResponse: func.isRequired,
  hasNone: bool,
};
export default MultipleOptions;
