import React from 'react';
import PropTypes from 'prop-types';

import { createStructuredSelector } from 'reselect';

import { connect } from 'react-redux';
import { compose } from 'redux';

import { Typography, Grid } from '@material-ui/core';

import { VDPC_CONFERENCE_URL } from 'utils/config';

import GradientButton from 'components/GradientButton';
import { makeSelectPaymentAmount } from 'containers/App/legacySelectors';

import Coupon from './coupon-code';
import Braintree from './braintree';
function Copay({
  patient,
  amount,
  grandTotal,
  onConferenceClick,
  enqueueSnackbar,
}) {
  const confAddress = `${VDPC_CONFERENCE_URL}${
    patient.uuid
  }&location=AkosChat360&auto=1`;

  const renderPayment = () => {
    if (grandTotal > 0)
      return (
        <Braintree
          patient={patient}
          amount={grandTotal}
          enqueueSnackbar={enqueueSnackbar}
          confAddress={confAddress}
        />
      );
    return (
      <GradientButton size="large" onClick={onConferenceClick}>
        Video Conference Now
      </GradientButton>
    );
  };
  return (
    <Grid container spacing={2}>
      <Grid item xs={12}>
        <Coupon
          patient={patient}
          enqueueSnackbar={enqueueSnackbar}
          amount={amount}
        />
      </Grid>
      <Grid item xs={12}>
        <Typography variant="h4" color="primary">
          Copay Amount: ${' '}
          {typeof grandTotal !== 'object' ? grandTotal.toFixed(2) : 0.0}
        </Typography>
      </Grid>
      <Grid item xs={12}>
        {renderPayment()}
      </Grid>
    </Grid>
  );
}

const { object, number, func } = PropTypes;

Copay.propTypes = {
  patient: object.isRequired,
  amount: number,
  grandTotal: number,
  enqueueSnackbar: func.isRequired,
  onConferenceClick: func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  grandTotal: makeSelectPaymentAmount(),
});

const withConnect = connect(
  mapStateToProps,
  null,
);

export default compose(withConnect)(Copay);
